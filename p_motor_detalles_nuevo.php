<?php

	// CONSTANTES PARA DEFINIR LA POSICIÓN DE LOS TEXTOS ENCONTRADOS
	
	/*
	define ("MUY_ARRIBA", 1);
	define ("ARRIBA", 2);
	define ("MITAD_ARRIBA", 3);
	define ("MITAD_ABAJO", 4);
	define ("ABAJO", 5);
	define ("MUY_ABAJO", 6);
	*/
	define ("ARRIBA", 1);
	define ("MITAD", 2);
	define ("ABAJO", 3);

	// FUNCIÓN QUE DEVUELVE LA POSICIÓN RELATIVA DE UNA CADENA. PARA ELLO SE RECIBE LA
	// POSICIÓN ENCONTRADA PARA LA CADENA, Y LA LONGITUD DEL TEXTO EN DONDE SE ENCONTRÓ
	// LA CADENA (máximo).
	// Los valores están entre muy arriba y muy abajo. Con estos datos se podrá decidir sobre
	// si la cadena buscada cumple o no con el criterio o patrón de búsqueda. Esto es clave,
	// porque a veces se va a encontrar el patrón de texto, pero al estar en una zona inadecuada
	// de la página, no cumple con el patrón buscado
	function posicion_relativa($maximo, $pos) {

		$unidad = intdiv($maximo, 3);
		if ($pos < $unidad) return ARRIBA;
		if ($pos < $unidad * 2) return MITAD;
		return ABAJO;

	}

	function motor_detalles_ajustes() {

		global $pdo;

	    try {
		    $sql2 = "SELECT * FROM tblpaginas WHERE procesado_nueva_trd = 0";
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
		    $totalPaginas = $query2->rowCount();
		    $elExpediente = $resPaginas[0]['expediente'];
			}
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	} // TRY de tabla páginas   

	  	$p = 0;
	  	while ($p < count($resPaginas)) {

	  		// -----------------------------------------------------------
	  		// CASO 1

	  		$paso = 0;

		  	$x1 = stripos("=" . $resPaginas[$p]['contenido'], "constancia de reparto");
		  	$x2 = stripos("=" . $resPaginas[$p]['contenido'], "atentamente");
		  	if ($x1 && $x2) {
	  			// SE CAMBIA EL CRITERIO (o subcriterio de la página)

	  			$instruccion = "UPDATE tblpaginas SET criterio = 585, subcriterio = 0 WHERE pagina = " . $resPaginas[$p]['pagina'];
	  			try {
	  				$sql2 = $instruccion;
	  				$query2 = $pdo->prepare($sql2);
	  				$query2->execute();
	  			}
			  	catch (PDOException $ex) {
			    	print_r($ex);
			  	} // TRY de actualización de páginas

			  	$paso = 1;

		  	}

	  		// -----------------------------------------------------------
	  		// CASO 2

	  		if ($paso == 0) {

			  	$x1 = stripos("=" . $resPaginas[$p]['contenido'], "fíjese");
			  	$x2 = stripos("=" . $resPaginas[$p]['contenido'], "fecha y hora");
			  	$x3 = stripos("=" . $resPaginas[$p]['contenido'], "se fijara nueva fecha");
			  	$x4 = stripos("=" . $resPaginas[$p]['contenido'], "se fijará nueva fecha");
			  	$x5 = stripos("=" . $resPaginas[$p]['contenido'], "fijar");
			  	$x6 = stripos("=" . $resPaginas[$p]['contenido'], "se llevará a cabo");
			  	$x6 = stripos("=" . $resPaginas[$p]['contenido'], "se llevara a cabo");

			  	if (($x1 && $x2) || $x3 || $x4 || ($x5 && $x2) || $x6) {
		  			// SE CAMBIA EL CRITERIO (o subcriterio de la página)

		  			$instruccion = "UPDATE tblpaginas SET criterio = 586, subcriterio = 0 WHERE pagina = " . $resPaginas[$p]['pagina'];
		  			try {
		  				$sql2 = $instruccion;
		  				$query2 = $pdo->prepare($sql2);
		  				$query2->execute();
		  			}
				  	catch (PDOException $ex) {
				    	print_r($ex);
				  	} // TRY de actualización de páginas
				  	$paso = 1;
			  	}
			  }

	  		// -----------------------------------------------------------
	  		// CASO 3

	  		if ($paso == 0) {

			  	$x1 = stripos("=" . $resPaginas[$p]['contenido'], "indiciado");
			  	$x2 = stripos("=" . $resPaginas[$p]['contenido'], "dejo constancia");

			  	if ($x1 && $x2) {
		  			$instruccion = "UPDATE tblpaginas SET criterio = 612, subcriterio = 0 WHERE pagina = " . $resPaginas[$p]['pagina'];
		  			try {
		  				$sql2 = $instruccion;
		  				$query2 = $pdo->prepare($sql2);
		  				$query2->execute();
		  			}
				  	catch (PDOException $ex) {
				    	print_r($ex);
				  	} // TRY de actualización de páginas
				  	$paso = 1;			  		
			  	}

	  		// -----------------------------------------------------------
	  		// CASO 4

	  		if ($paso == 0) {

			  	$x1 = stripos("=" . $resPaginas[$p]['contenido'], "asignacion defensor");
			  	$x2 = stripos("=" . $resPaginas[$p]['contenido'], "asignación defensor");

				  	if ($x1 || $x2) {
			  			$instruccion = "UPDATE tblpaginas SET criterio = 0, subcriterio = 602 WHERE pagina = " . $resPaginas[$p]['pagina'];
			  			try {
			  				$sql2 = $instruccion;
			  				$query2 = $pdo->prepare($sql2);
			  				$query2->execute();
			  			}
					  	catch (PDOException $ex) {
					    	print_r($ex);
					  	} // TRY de actualización de páginas
					  	$paso = 1;	

				  		$x10 = stripos("=" . $resPaginas[$p + 1]['contenido'], "cordialmente");	

				  		if ($x10 > 0) {
				  			$instruccion = "UPDATE tblpaginas SET criterio = 0, subcriterio = 602 WHERE pagina = " . $resPaginas[$p + 1]['pagina'];
				  			try {
				  				$sql2 = $instruccion;
				  				$query2 = $pdo->prepare($sql2);
				  				$query2->execute();
				  			}
						  	catch (PDOException $ex) {
						    	print_r($ex);
						  	} // TRY de actualización de páginas			  			
				  		}			  		
				  	}

				}

			}

	  		// -----------------------------------------------------------
	  		// CASO 5

	  		if ($paso == 0) {

			  	$x1 = stripos("=" . $resPaginas[$p]['contenido'], "notificación audiencia");
			  	$x2 = stripos("=" . $resPaginas[$p]['contenido'], "notificacion audiencia");
			  	$x3 = stripos("=" . $resPaginas[$p]['contenido'], "se llevará a cabo");
			  	$x4 = stripos("=" . $resPaginas[$p]['contenido'], "se llevara a cabo");

			  	if ($x1 || $x2 || $x3 || $x4) {
		  			$instruccion = "UPDATE tblpaginas SET criterio = 586, subcriterio = 0 WHERE pagina = " . $resPaginas[$p]['pagina'];
		  			try {
		  				$sql2 = $instruccion;
		  				$query2 = $pdo->prepare($sql2);
		  				$query2->execute();
		  			}
				  	catch (PDOException $ex) {
				    	print_r($ex);
				  	} // TRY de actualización de páginas
				  	$paso = 1;			  		
			  	}
			}

		  	$p = $p + 1;
		}
	}

	function motor_detalles($codigo, $tipo = 1, $depuracion = 0) {

		global $pdo;

		$especial = 0;

		if ($especial) {
			echo "==> Llamado a Motor Detalles (" . $codigo . ", " . $tipo . ") <br>";
		}

		// ALGORITMO BÁSICO (I)

		/* 	1. Se colocan a disposición las páginas. Esto se requiere, porque la idea del
		       algoritmo es buscar el criterio a lo largo de todas ellas. Al encontrar la
		       página (o páginas) se crea un modelo de análisis para indicar el comienzo (o
		       el final) del criterio en el paquete de paginas evaluado

		    2. Se extrae la información TRD EXTRA del criterio a evaluar. El uso del criterio 
		       extra hace referencia a un modelo avanzado que se sobrepone a cualquier
		       análisis previo. Se basa en ponderaciones y ubicación dentro de la página */

	    try {
		    $sql2 = "SELECT * FROM tblpaginas WHERE procesado_nueva_trd = 0";
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
		    $totalPaginas = $query2->rowCount();
		    $elExpediente = $resPaginas[0]['expediente'];
			}
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	} // TRY de tabla páginas   

	    // SE EXTRAE LA TRD EXTRA DEL CRITERIO
	    // El criteiro Extra hace referencia a un modelo más avanzado de metarregla cuya idea
	    // es buscar patrones más específicos que se sobreponen a cualquier análisis previo
	    // ---------------------------------------------------------------------------------
	    // ES IMPORTANTE RECORDAR QUE MÁS ADELANTE LA SERIE Y LA SUBSERIE SE DEBEN EXTRAR DEL
	    // ANÁLISIS DE LOS DOCUMENTOS Y DE LAS BASES DE DATOS RELACIONADAS

	    try {
		    $sql2 = "SELECT * FROM tbltrdextra WHERE serie = 270 AND subserie = 245 AND codigo = " . $codigo;
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $resTrdExtra = $query2->fetchAll(PDO::FETCH_ASSOC);
			}
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	} // TRY de tabla TRD EXTRA

	  	// ---------------------------------------------------------------------------------------

	  	// SE DIVIDE EL PATRÓN DE página inicial EN SUS SUB-PATRONES SEPARADOS POR PUNTO Y COMA
	  	// Cada sub-patrón inicial puede generar un comienzo de paquete documental, siempre y
	  	// cuando se encuntre presente en alguna de las páginas del expediente. Esta es la razón
	  	// por la cual más adelante se crea un ciclo para las páginas (dentro del ciclo de los
	  	// paquetes)

	  	if ($depuracion) {
	  		echo "<br><br>aaa - DEPURANDO EL NUEVO MOTOR ===>>>" . "<br>";
	  		echo "NÚMERO DE REGISTROS DE LA NUEVA TRD = " . count($resTrdExtra) . "<br>";
	  		echo "CONJUNTO DE REGLAS EXTRAS<br>";
	  		print_r($resTrdExtra); echo "<br>";
	  	}

	  	// NO SE DEBE OLVIDAR QUE CADA PATRÓN ESTÁ COMPUESTO POR FRASES, CADA UNA DE LAS CUALES
	  	// POSEE UNA UBICACIÓN ESPECÍFICA. ESTE ES EL MOTIVO POR EL CUAL, CADA VEZ QUE SE DIVIDE
	  	// UN PAQUETE O FRASE, DE MANERA SINCRÓNICA SE DEBE DIVIDIR LA ESTRUCTURA DE 'ubicacion_inicio'
	  	// PARA QUE SE PUEDA VALIDAR DE QUE MANERA cada frase SE SINCRONIZA CON cada ubicación

	  	$paquete = explode(";", $resTrdExtra[0]['patron_inicio']);
	  	$ubicacion_paquete = explode(";", $resTrdExtra[0]['ubicacion_inicio']);

	  	// SE PREPARAN DOS ARREGLOS: a) to, b) td. El vector 'to' (tabla origen) se utiliza para 
	  	// almacenar las páginas que dan inicio a los agrupamientos de criterios

	  	// El vector 'td' (tabla destino) guarda todos los cierres de las páginas inicio de la tabla
	  	// origen. Más adelante se verá como sincronizar estos dos arreglos

	  	// El vector 'tz' se utiliza para la generación de una tabla intermedia que recoge las 
	  	// páginas finales. Posteriormente se combinará con la tabla 'to' para descubrir la
	  	// estructura correcta de la tabla 'td'

	  	$to = Array();
	  	$td = Array();
	 	$tz = Array(); 

	 	if ($elExpediente == '54874610609020188003900' && $codigo == 583) {
	 		$to[] = 2;
	 	}

	 	// ANÁLISIS DE TODOS LOS PAQUETES
	 	// ------------------------------

	 	if ($depuracion) {
	 		echo "<br>EL PAQUETE ANTES DE INICIAR EL ANÁLISIS DE PATRONES<br><br>";
	 		print_r($paquete);
	 		echo "<br>";
	 	}

	  	$f = 0;
	  	while ($f < count($paquete)) {

	  		// AQUÍ SE PROCESA CADA PAQUETE. SE DIVIDE EN FRASES SEPARADAS POR COMAS (,)

	  		// CADA PAQUETE ESTÁ SEPARADO DE LOS DEMÁS PAQUETES POR UN PUNTO Y COMA
	  		// Dentro de cada paquete pueden existir frases que se deben cumplir SIMULTÁNEAMENTE
	  		// Es por ello, que en este punto se divide el paquete en frases y seguidamente
	  		// se recorren todas las páginas. Solamente aquellas páqinas que cumplan con TODAS
	  		// las frases, serán consideradas inicio del paquete documetal (en el futuro se utilizará
	  		// ponderación, para que no necesariamente deban estar todas las frases. Pero esto se
	  		// hará en una siguiente fase)

	  		// NOTA: frase y ubicación frase están sincronizados estructuralmente desde el contenido
	  		// de la tabla en la base de datos. Esto debe ser verificado para evitar errores. Lo que 
	  		// esto significa es que las estructuras deben ser isomorfas. 
	  		// Ejemplo: 'prueba_1,prueba2;prueba3', debe tener una estructura similar a la siguiente
	  		// en el campo de ubicacion_inicio: '1,2;1'. Como se ve, los puntos y comas y las comas
	  		// son similares en ambos campos

	  		$frase = explode("-", $paquete[$f]);
	  		$ubicacion_frase = explode("-", $ubicacion_paquete[$f]);

	  		if ($depuracion) {
	  			echo "LAS FRASES ANTES DE INGRESAR AL ANÁLISIS DE LAS PÁGINAS: " . "<br>";
	  			print_r($frase);
	  			echo "<br>";
	  		}

	  		// AQUÍ SE RECORREN TODAS LAS PÁGINAS, BUSCANDO EL CONJUNTO DE FRASES DEL PAQUETE

	  		if ($depuracion) {
	  			echo "NÚMERO DE PÁGINAS = " . count($resPaginas) . "<br><br>";
	  		}

	  		// SE ANALIZAN TODAS LAS PÁGINAS, EN LA BÚSQUEDA DE TODAS LAS FRASES
	  		// -----------------------------------------------------------------
	  		$p = 0;
	  		while ($p < count($resPaginas)) {

	  			// PARA CADA PÁGINA DENTRO DEL CICLO, DEBE CUMPLIRSE QUE TODAS LAS FRASES
	  			// ESTÁN PRESENTES. Esto implica un ciclo de frases, según se ve a continuación

	  			$valida = 0; // Esta variable vale cero inicialmente, indicando con ello que
	  						 // la página no será válida hasta tanto no cumpla con verificar que
	  						 // todas las frases se encuentran dentro de la página

	  			// ANÁLISIS DE TODAS LAS FRASES
	  			// ----------------------------
	  			$cf = 0;
	  			while ($cf < count($frase)) {

	  				// SE PROCESAN LAS FRASES

	  				// Se busca si la frase está en la página
	  				// Se agrega el símbolo '=' con la idea de que la frase sea encontrada siempre
	  				// a partir de la posición 1 (no en la posición 0)
	  				$x = stripos("=" . $resPaginas[$p]['contenido'], $frase[$cf]);

	  				if ($depuracion) { 
	  					/* echo "<br>===================<br><br>";
	  					echo "PÁGINA ANALIZADA = " . ($p + 1) . "<br>";
	  					echo "FRASE BUSCADA = " . $frase[$cf] . "<br>";
	  					echo "POSICIÓN EN DONDE SE ENCONTRÓ = " . $x . "<br><br>"; */
	  				}

	  				// Se almacena la posición en la que se encontró la frase en 
	  				// el arreglo 'pos'. Si la posición vale cero, entonces no se habría encontrado
	  				// la frase

	  				if ($x == 0) {
	  					// NO SE ENCONTRÓ LA FRASE. SE OMITE LA PÁGINA (en esta versión)
	  					// Se deja 'valida' igual a cero. Al salir del ciclo, se evalúa 'valida'
	  					// y como es cero se omite la página (no se graba en 'pos'

	  					/*if ($depuracion) {
	  						echo "SE OMITE LA PÁGINA: " . ($p + 1) . ", PUES NO ENCONTRÓ LA FRASE: " . $frase[$cf] . "<br>";
	  					}*/

	  					break;
	  				}
	  				else {
	  					// LA PÁGINA PARECE VÁLIDA. SOLO QUE AHORA DEBE VERIFICARSE QUE LA FRASE
	  					// SE ENCUENTRA EN EL SITIO CORRECTO

	  					if ($depuracion) {
	  						echo "<hr>PÁGINA: " . $p . " <br>SE ENCONTRÓ EL PATRÓN: " . $frase[$cf] . "<br>";
	  						echo "LA FRASE ESTÁ DENTRO DE LA PÁGINA. SE DEBE VERIFICAR LA UBICACIÓN<br>";
	  					}

	  					$laUbicacionSolicitada = $ubicacion_frase[$cf];

	  					if ($depuracion) {
	  						echo "<br>UBICACIÓN SOLICITADA: " . $laUbicacionSolicitada . "<br>";
	  					}

	  					$laUbicacionCalculada = posicion_relativa(strlen($resPaginas[$p]['contenido']), $x);

						if ($laUbicacionCalculada == $laUbicacionSolicitada) {
							// LA FRASE ES VÁLIDA. SE DEBE SEGUIR DENTRO DEL CICLO
							// PARA EVALUAR LAS RESTANTES FRASES
							if ($depuracion) {
								echo "<hr>LA PÁGINA: " . ($p) . " CON LONGITUD: " . strlen($resPaginas[$p]['contenido']) . " CONTIENE LA FRASE: " . $frase[$cf] . " EN LA UBICACIÓN: " . $laUbicacionCalculada . " Y DEBE ESTAR UBICADA EN: " . $laUbicacionSolicitada . "<br>";
								echo "CUMPLE !!!! LA PÁGINA " . ($p) . " SE DEBE INCLUIR<br><br>";

								// SE PONE valida A UNO. MIENTRAS TODAS LAS FRASES RATIFIQUEN ESTE VALOR
								// LA PÁGINA SERÁ INCLUIDA. SI UNA, Y SOLO UNA, DE LAS FRASES NO CUMPLA,
								// SE SALDRÁ DEL CICLO Y LA PÁGINA NO CUMPLIRÁ, PUES valida será igual
								// A CERO
							}

							// Se pone válida a 1 y se continúa dentro del ciclo
							$valida = 1;
						}
						else
						{
							// LA FRASE NO ESTÁ EN LA UBICACIÓN CORRECTA. SE DEBE ABANDONAR EL CICLO,
							// PUES LA PÁGINA NO CUMPLE CON QUE UNA DE LAS FRASES ESTÉ EN EL SITIO
							// CORRECTO
							if ($depuracion) {
								echo "<br>LA PÁGINA: " . ($p) . " CON LONGITUD: " . strlen($resPaginas[$p]['contenido']) . " CONTIENE LA FRASE: " . $frase[$cf] . " EN LA UBICACIÓN CALCULADA : " . $laUbicacionCalculada . " Y DEBE ESTAR UBICADA EN: " . $laUbicacionSolicitada . "<br>";
								echo "NO CUMPLE! sale del ciclo hacia una nueva página<br><br>";
							}
							$valida = 0;
							break;
						}

	  				}

	  				// Se repite para las siguientes frases
	  				$cf = $cf + 1;
	  			} // Cierre del ciclo de frases

	  			// SE VERIFICA SI TODAS LAS FRASES SE ENCUENTRAN DENTRO DE LA PÁGINA. EN CASO AFIRMATIVO
	  			// LA PÁGINA SE AGREGA COMO PÁGINA INICIAL EN LA TABLA 'to' (Tabla Origen)

	  			if ($valida > 0) {

	  				// Se agrega la página a la lista de páginas iniciales
	  				$to[] = $resPaginas[$p]['pagina'];

	  			} // Cierre comparación que verifica si la página es válida

	  			else {

	  			}

	  			$p = $p + 1;

	  		} // Cierre del ciclo de páginas en el análisis de páginas iniciales

	  		$f = $f + 1;
	  	} // Cierre del ciclo de paquetes del patrón inicial

	  	// ---------------------------------------------------------------------------------------

	  	if ($depuracion) {

	  		$s  = 'TABLAS to Y td (origen, destino => inicial, cierre)<br>';
	  		$s .= 'Código = ' . $codigo . ' Tipo = ' . $tipo . "<br><br>";
	  		$s .= 'PROCESO = APERTURA<br><br>';

	  		// SE CREA UNA TABLA HTML
	  		$s .= '<table border="1">';
	  		$s .= '<tr><th>Nro</th><th>Origen</th></tr>';
	  		$s .= '<tr>';
	  		$if = 0;
	  		// SE RECORRE LA TABLA Y SE AGREGAN LAS FILAS A LA TABLA HTML
	  		while ($if < count($to)) {
	  			$s .= '<tr><td>' . $if . '</td><td>' . $to[$if] . '</td></tr>';
	  			$if = $if + 1;
	  		} // Cierre del ciclo de recorrido de las filas de to y td

	  		$s .= '</table>';

	  		// SE MUESTRA LA TABLA HTML (y de este modo, las tablas to y td)
	  		echo $s; 

	  	} // Cierre ciclo de depuración

	  	// ---------------------------------------------------------------------------------------

	  	// AQUÍ SE CONTINÚA CON EL CICLO DE BÚSQUEDA DE PÁGINAS DE CIERRE

	  	// NOTA ************* Inicialmente solo se tratará de casos de inicio, sin cierre
	  	// Esto simplificará (por ahora) la ejecución intuitiva del programa

	  	// DE MANERA TEMPORAL, SE IMPRIME EL CONTENIDO DE to Y DE td, para validar el
	  	// trabajo realizado

	  	// ---------------------------------------------------------------------------------------

	  	// PROCESO DE CIERRE DE LAS PÁGINAS
	  	// Se aplica el mismo proceso 'inicial' para el cierre de las páginas correspondientes
	  	// En caso de que no exista el cierre, entonces se tendrán páginas individuales

	  	// SE ELIMINAN LOS COMENTARIOS, PUES LA ESTRUCTURA DE FINAL ES IGUAL A LA DE INICIAL

	  	// EL PATRÓN INICIAL SIEMPRE DEBE EXISTIR. PERO EL PATRÓN FINAL PODRÍA NO EXISTIR.
	  	// EN CASO DE QUE NO EXISTA, EL ANÁLISIS QUE SE HACE A CONTINUACIÓN, DEBE OMITIRSE
	  	// Y CONTINUAR CON EL PROCESO DE COMPACTACIÓN

	  	if ($resTrdExtra[0]['patron_final'] == '') {
	  		// SE OMITE EL ANÁLISIS QUE VA A CONTINUACIÓN
	  	}
	  	else {

	  		if ($depuracion) {
	  			echo "<br><br>ANÁLISIS DE CIERRE DE LOS PAQUETES DE CLASIFICACIÓN: <br><br>";
	  		}

		  	$paquete = explode(";", $resTrdExtra[0]['patron_final']);
		  	$ubicacion_paquete = explode(";", $resTrdExtra[0]['ubicacion_final']);

		  	// -----------------------------------------------------------------------------------

		 	// ANÁLISIS DE TODOS LOS PAQUETES
		 	// ------------------------------
		  	$f = 0;
		  	while ($f < count($paquete)) {

		  		// AQUÍ SE PROCESA CADA PAQUETE. SE DIVIDE EN FRASES SEPARADAS POR COMAS (,)

		  		// CADA PAQUETE ESTÁ SEPARADO DE LOS DEMÁS PAQUETES POR UN PUNTO Y COMA
		  		// Dentro de cada paquete pueden existir frases que se deben cumplir SIMULTÁNEAMENTE
		  		// Es por ello, que en este punto se divide el paquete en frases y seguidamente
		  		// se recorren todas las páginas. Solamente aquellas páqinas que cumplan con TODAS
		  		// las frases, serán consideradas inicio del paquete documetal (en el futuro se utilizará
		  		// ponderación, para que no necesariamente deban estar todas las frases. Pero esto se
		  		// hará en una siguiente fase)

		  		// NOTA: frase y ubicación frase están sincronizados estructuralmente desde el contenido
		  		// de la tabla en la base de datos. Esto debe ser verificado para evitar errores. Lo que 
		  		// esto significa es que las estructuras deben ser isomorfas. 
		  		// Ejemplo: 'prueba_1,prueba2;prueba3', debe tener una estructura similar a la siguiente
		  		// en el campo de ubicacion_inicio: '1,2;1'. Como se ve, los puntos y comas y las comas
		  		// son similares en ambos campos

		  		$frase = explode("-", $paquete[$f]);
		  		$ubicacion_frase = explode("-", $ubicacion_paquete[$f]);

		  		if ($depuracion) {
		  			echo "LAS FRASES ANTES DE INGRESAR AL ANÁLISIS DE LAS PÁGINAS: " . "<br>";
		  			print_r($frase);
		  		}

		  		// AQUÍ SE RECORREN TODAS LAS PÁGINAS, BUSCANDO EL CONJUNTO DE FRASES DEL PAQUETE

		  		if ($depuracion) {
		  			echo "NÚMERO DE PÁGINAS = " . count($resPaginas) . "<br>";
		  		}

		  		// SE ANALIZAN TODAS LAS PÁGINAS, EN LA BÚSQUEDA DE TODAS LAS FRASES
		  		// -----------------------------------------------------------------
		  		$p = 0;
		  		while ($p < count($resPaginas)) {

		  			// PARA CADA PÁGINA DENTRO DEL CICLO, DEBE CUMPLIRSE QUE TODAS LAS FRASES
		  			// ESTÁN PRESENTES. Esto implica un ciclo de frases, según se ve a continuación

		  			$valida = 0; // Esta variable vale cero inicialmente, indicando con ello que
		  						 // la página no será válida hasta tanto no cumpla con verificar que
		  						 // todas las frases se encuentran dentro de la página

		  			// ANÁLISIS DE TODAS LAS FRASES
		  			// ----------------------------
		  			$cf = 0;
		  			while ($cf < count($frase)) {

		  				// SE PROCESAN LAS FRASES

		  				// Se busca si la frase está en la página
		  				// Se agrega el símbolo '=' con la idea de que la frase sea encontrada siempre
		  				// a partir de la posición 1 (no en la posición 0)
		  				$x = stripos("=" . $resPaginas[$p]['contenido'], $frase[$cf]);

		  				if ($depuracion) { 
		  					echo "<br>===================<br><br>";
		  					echo "PÁGINA ANALIZADA = " . ($p + 1) . "<br>";
		  					echo "FRASE BUSCADA = " . $frase[$cf] . "<br>";
		  					echo "POSICIÓN EN DONDE SE ENCONTRÓ = " . $x . "<br><br>";
		  				}

		  				// Se almacena la posición en la que se encontró la frase en 
		  				// el arreglo 'pos'. Si la posición vale cero, entonces no se habría encontrado
		  				// la frase

		  				if ($x == 0) {
		  					// NO SE ENCONTRÓ LA FRASE. SE OMITE LA PÁGINA (en esta versión)
		  					// Se deja 'valida' igual a cero. Al salir del ciclo, se evalúa 'valida'
		  					// y como es cero se omite la página (no se graba en 'pos'

		  					if ($depuracion) {
		  						echo "SE OMITE LA PÁGINA: " . ($p + 1) . ", PUES NO ENCONTRÓ LA FRASE: " . $frase[$cf] . "<br>";
		  					}
		  					break;
		  				}
		  				else {
		  					// LA PÁGINA PARECE VÁLIDA. SOLO QUE AHORA DEBE VERIFICARSE QUE LA FRASE
		  					// SE ENCUENTRA EN EL SITIO CORRECTO

		  					if ($depuracion) {
		  						echo "LA FRASE ESTÁ DENTRO DE LA PÁGINA. SE DEBE VERIFICAR LA UBICACIÓN<br>";
		  					}

		  					$laUbicacion = $ubicacion_frase[$cf];
							if (posicion_relativa(strlen($resPaginas[$p]['contenido']), $x) == $laUbicacion) {
								// LA FRASE ES VÁLIDA. SE DEBE SEGUIR DENTRO DEL CICLO
								// PARA EVALUAR LAS RESTANTES FRASES
								if ($depuracion) {
									echo "LA PÁGINA: " . ($p + 1) . " CON LONGITUD: " . strlen($resPaginas[$p]['contenido']) . "CONTIENE LA FRASE: " . $frase[$cf] . " EN LA UBICACIÓN: " . $x . " Y QUE DEBE ESTAR UBICADA EN: " . $laUbicacion . "<br>";
									echo "CUMPLE !!!! LA PÁGINA " . ($p + 1) . " SE DEBE INCLUIR<br><br>";

									// SE PONE valida A UNO. MIENTRAS TODAS LAS FRASES RATIFIQUEN ESTE VALOR
									// LA PÁGINA SERÁ INCLUIDA. SI UNA, Y SOLO UNA, DE LAS FRASES NO CUMPLA,
									// SE SALDRÁ DEL CICLO Y LA PÁGINA NO CUMPLIRÁ, PUES valida será igual
									// A CERO
								}

								// Se pone válida a 1 y se continúa dentro del ciclo
								$valida = 1;
							}
							else
							{
								// LA FRASE NO ESTÁ EN LA UBICACIÓN CORRECTA. SE DEBE ABANDONAR EL CICLO,
								// PUES LA PÁGINA NO CUMPLE CON QUE UNA DE LAS FRASES ESTÉ EN EL SITIO
								// CORRECTO
								if ($depuracion) {
									echo "LA PÁGINA: " . ($p + 1) . " CON LONGITUD: " . strlen($resPaginas[$p]['contenido']) . "CONTIENE LA FRASE: " . $frase[$cf] . " EN LA UBICACIÓN: " . $x . " Y DEBE ESTAR UBICADA EN: " . $laUbicacion . "<br>";
									echo "NO CUMPLE! sale del ciclo hacia una nueva página<br><br>";
								}
								$valida = 0;
								break;
							}

		  				}

		  				// Se repite para las siguientes frases
		  				$cf = $cf + 1;
		  			} // Cierre del ciclo de frases

		  			// SE VERIFICA SI TODAS LAS FRASES SE ENCUENTRAN DENTRO DE LA PÁGINA. EN CASO AFIRMATIVO
		  			// LA PÁGINA SE AGREGA COMO PÁGINA INICIAL EN LA TABLA 'to' (Tabla Origen)

		  			if ($valida > 0) {

		  				// Se agrega la página a la lista de páginas iniciales
		  				$td[] = $resPaginas[$p]['pagina'];

		  			} // Cierre comparación que verifica si la página es válida

		  			$p = $p + 1;

		  		} // Cierre del ciclo de páginas en el análisis de páginas iniciales

		  		$f = $f + 1;
		  	} // Cierre del ciclo de paquetes del patrón inicial

		} // Cierre del 'if' que verifica la existencia de un patrón de cierre

	  	// ---------------------------------------------------------------------------------------

	  	// PROCESO DE CONSOLIDACIÓN DE LOS DOCUMENTOS GENERADOS
	  	// Una vez cerrados los documentos, se revisará (en caso necesario) el estado de
	  	// las tablas to y td

	  	if (count($to) == 0) {
	  		// NO SE ENCONTRARON PÁGINAS DE INICIO. Se omite el proceso de compactación
	  		// para el código recibido, pues ninguna página coincidió con el criterio de búsqueda
	  	}
	  	else {

	  		// PUESTO QUE PARA CADA PÁGINA DE APERTURA DEBE HABER UNA Y SOLO UNA PÁGINA
	  		// DE CIERRE, EL PROCESO CONSISTE EN CREAR UNA td POR CADA to. LUEGO SE INDICARÁN
	  		// LOS VALORES DE td A PARTIR DE LOS VALORES DE tz COMBINADOS CON to

	  		// SE RECORREN TODOS LOS to, COMO ELEMENTO DE CONTROL PARA CREAR LOS td
	  		$tt = 0;
	  		while ($tt < count($to)) {
	  			// POR CADA to SE CREA UN td
	  			$td[] = 0;
	  			$tt = $tt + 1;
	  		} // Cierre del ciclo que permite generar el array td

	  		// SE RECORRE LA TABLA to CON EL PROPÓSITO DE CORREGIR td
	  		// SE UTILIZA tz COMO CLAVE PARA DETERMINAR EL td CORRESPONDIENTE A CADA to
	  		// ES POR ESTO QUE SE DEBE MANEJAR UN ÍNDICE QUE CONTROLE EL AVANCE SOBRE tz

	  		// NOTA: Si tz NO tiene registros, entonces esto significa que el patrón de cierre
	  		//       no existe, y el siguiente conjunto de instrucciones no debe ejecutarse

	  		if (count($tz) == 0) {

	  			// No existe patrón de cierre. En este caso, todos los td serán iguales a todos los to

	  			$xm = 0;
	  			while ($xm < count($to)) {
	  				$td[$xm] = $to[$xm];
	  				$xm = $xm + 1;
	  			} // Cierre del ciclo que asigna a todos los td, los valores contenidos en el
	  			  // correspondiente to

	  		}
	  		else {

		  		$indz = 0;

		  		$tf = 0;
		  		while ($tf < count($to)) {

		  			// SI ESTAMOS EN EL ÚLTIMO REGISTRO DE to HAY DOS POSIBILIDADES:
		  			// a) Estamos al final de indz, es decir, ya se agotó tz. En este caso
		  			//    no hay tz para realimentar td. Entonces, en td (frente a to) colocamos
		  			//    el mismo valor que en to (es decir, página única)
		  			// b) No estamos al final de indz. Se tiene un valor potencial de tz. 
		  			//    Aquí aparecen DOS nuevas posibilidades: 
		  			// b-1) NINGÚN valor de tz es mayor o igual a to. En este caso, frente a to
		  			//      se coloca el mismo valor para td (página única)
		  			// b-2) EXISTE AL MENOS un valor de tz que es MAYOR que to. Frente a to
		  			//      se coloca dicho valor, pues esté número es el cierre de la to

		  			if ($tf == count($to) - 1) {
		  				if ($indz >= count($tz)) {
		  					$td[$tf] = $to[$tf];
		  				}
		  				else {
		  					$td[$tf] = $to[$tf];
		  					$nn = 0;
		  					while ($nn < count($tz)) {
		  						if ($tz[$nn] > $to[$tf]) {
		  							// td toma el valor en tz. Este es el cierre para to
		  							$td[$tf] = $tz[$nn];
		  							// Ya se hizo el cambio. Debe salir del ciclo
		  							break;
		  						}
		  						$nn = $nn + 1;
		  					} // Cierre ciclo de búsqueda en tz de valores mayores a to
		  				}
		  			}
		  			else {

			  			// SI NO ESTAMOS EN EL ÚLTIMO REGISTRO, ESTO SIGNIFICA QUE DESPUÉS DE LA to
			  			// EXISTE OTRA to. ESTO IMPLICA QUE tz DEBE ANALIZARSE DE OTRA MANERA, SEGÚN
			  			// SE INDICA A CONTINUACIÓN

			  			// SI PARA to EXISTE UNA tz QUE ES MAYOR, PERO MENOR QUE EL VALOR DE (to + 1),
			  			// ENTONCES DICHO VALOR SE COLOCA EN td FRENTE A LA to. ES EL CIERRE DE LA to

			  			// SI PARA to NO EXISTE UNA tz QUE SEA MAYOR QUE ELLA Y MENOR QUE to + 1, 
			  			// ENTONCES LA tz NO ES UN CIERRE PARA LA to, Y FRENTE A LA to SE COLOCA EN
			  			// td EL MISMO VALOR QUE EN LA to. ESTO SIGNIFICA QUE ESTAMOS ANTE UNA PÁGINA
			  			// ÚNICA

			  			// NOTA: Todos los análisis anteriores implican un cuidadoso control de tz 
			  			//       a través de indz. Esto debe validarse con ejemplos específicos

	  					$td[$tf] = $to[$tf];
	  					$nn = 0;
	  					while ($nn < count($tz)) {
	  						if ($tz[$nn] > $to[$tf] && $tz[$nn] < $to[$tf + 1]) {
	  							// td toma el valor en tz. Este es el cierre para to
	  							$td[$tf] = $tz[$nn];
	  							// Ya se hizo el cambio. Debe salir del ciclo
	  							break;
	  						}
	  						$nn = $nn + 1;
	  					} // Cierre ciclo de búsqueda en tz de valores mayores a to		  				

		  			} // Cierre del 'if' que verifica si estamos o no ante el último to (tf == count (to - 1))

		  			$tf = $tf + 1;
		  		} // Cierre del ciclo que recorre to para calcular td (tf < count to)

		  	} // Cierre de instrucción 'if' que verifica la existencia de tz (count to == 0)

	  	} // Cierre de la condición que detecta si existen tabla to (count to == 0)

	  	// ---------------------------------------------------------------------------------------

	  	// SE IMPRIME FINALMENTE EL RESULTADO OBTENIDO EN to Y td
	  	// Antes de proceder a grabar los resultados, se visualiza en pantalla si los datos
	  	// obtenidos se corresponden con los contenidos de las páginas

	  	if ($depuracion) {

	  		$s  = 'TABLAS to Y td (origen, destino => inicial, cierre)<br>';
	  		$s .= 'Código = ' . $codigo . ' Tipo = ' . $tipo . "<br><br>";
	  		$s .= 'PROCESO = APERTURA + CIERRE<br><br>';
	  		// SE CREA UNA TABLA HTML
	  		$s .= '<table border="1">';
	  		$s .= '<tr><th>Nro</th><th>Origen</th><th>Destino</th></tr>';
	  		$s .= '<tr>';

	  		if ($depuracion) {
	  			echo "TAMAÑO DE TO = " . count($to) . "<br>";
	  		}

	  		$if = 0;
	  		// SE RECORRE LA TABLA Y SE AGREGAN LAS FILAS A LA TABLA HTML
	  		while ($if < count($to)) {
	  			$s .= '<tr><td>' . $if . '</td><td>' . $to[$if] . '</td><td>' . $td[$if] . '</td></tr>';
	  			$if = $if + 1;
	  		} // Cierre del ciclo de recorrido de las filas de to y td

	  		$s .= '</tr>';
	  		$s .= '</table>';

	  		// SE MUESTRA LA TABLA HTML (y de este modo, las tablas to y td)
	  		echo $s; 

	  	} // Cierre ciclo de depuración

	  	// ---------------------------------------------------------------------------------------

	  	// ÚLTIMO PASO: GRABAR LOS REGISTROS CORRESPONDIENTES A LAS PÁGINAS AFECTADAS
	  	// Aquí se graban los registros de páginas a partir de to y td. Esto cierra y finaliza
	  	// todo el trabajo desarrollado

	  	// NOTA: Los valores almacenados en to y en td, son los 'id' de las páginas afectadas
	  	//       Estos valores (id) serán usados en las instruccions UPDATE más adelante

	  	$tg = 0;
	  	while ($tg < count($to)) {

	  		// SE CREA UN CICLO DE MODIFICACIÓN DE CRITERIO (o subcriterio) EN tblpaginas
	  		// PARA CADA FILA DE to. En este caso, se va desde to hasta td, colocando el mismo
	  		// valor de criterio para los registros de tblpaginas. Esto garantiza que al grabar
	  		// los registros de tblpaginas se dispondrá de información unificada (paquetes de
	  		// registros de páginas, en lugar de páginas aisladas que en realidad deberían formar
	  		// parte de un paquete de información)

	  		$ww = $to[$tg];
	  		while ($ww <= $td[$tg]) {

	  			// SE CAMBIA EL CRITERIO (o subcriterio de la página)

	  			$instruccion = "UPDATE tblpaginas SET ";

	  			// SI TIPO = 0, es criterio, SI TIPO = 1, es subcriterio

	  			if ($tipo == 0) {
	  				$instruccion .= "criterio = " . $codigo . ", subcriterio = 0 "; 
	  			}
	  			else {
	  				$instruccion .= "subcriterio = " . $codigo . ", criterio = 0 "; 
	  			} // Cierre del 'if' que analiza el tipo. Este análisis permite grabar
	  			  // el criterio o el subcriterio, según el tipo recibido

	  			// SE CAMBIA LA BANDERA, PARA QUE ESTOS REGISTROS NO PUEDAN VOLVER A SER AFECTADOS
	  			$instruccion .= ", procesado_nueva_trd = 1 ";

	  			// SE UTILIZA EL VALOR DE ww, PUES ESTA VARIABLE ESTÁ ITERANDO SOBRE EL RANGO
	  			// to HASTA td, LOS CUALES CONSTITUYEN EL RANGO A MODIFICAR
	  			$instruccion .= " WHERE pagina = " . $ww;

	  			// AQUÍ SE GRABA EL REGISTRO. ESTO SE HACE PARA TODOS LOS CASOS DETERMINADOS
	  			// POR to HASTA td

	  			if ($depuracion) {
	  				echo "INSTRUCCIÓN DE GRABACIÓN = " . $instruccion . "<br>";
	  			}

	  			if ($especial) {
	  				echo "GRABACIÓN EN PÁGINA: " . $ww . " INSTRUCCIÓN: " . $instruccion . "<br>";
	  			}

	  			try {
	  				$sql2 = $instruccion;
	  				$query2 = $pdo->prepare($sql2);
	  				$query2->execute();
	  			}
			  	catch (PDOException $ex) {
			    	print_r($ex);
			  	} // TRY de actualización de páginas

			  	// SE GUARDA EN LA VARIABLE resPaginas[ww], QUE EL REGISTRO YA FUE MODIFICADO
			  	// LA IDEA ES QUE EN LA PRÓXIMA ITERACIÓN NO SE MODIFIQUEN ESTOS VALORES

			  	$resPaginas[$ww]['procesado_nueva_trd'] = 1;

			  	if ($depuracion) {
			  		echo "<br>Página protegida: " . $ww . "<br>";
			  		echo "Valor de protección: " . $resPaginas[$ww]['procesado_nueva_trd'] . "<br>";
			  	}

			  	if ($depuracion) {
				  	if ($ww == 18) {
				  	  // SE MUESTRA COMO ESTÁN LAS PÁGINAS
					  if ($depuracion) {
					    echo "AQUÍ MIRAMOS EL ESTADO DE PÁGINAS ANTES DE SALIR DE MOTOR<br><br>";

					    try {
					      $sql22 = "SELECT * FROM tblpaginas";
					      $query22 = $pdo->prepare($sql22);
					      $query22->execute();
					      $resPaginas22 = $query22->fetchAll(PDO::FETCH_ASSOC);
					      $totalPaginas22 = $query22->rowCount();
					      $elExpediente22 = $resPaginas22[0]['expediente'];
					    }
					    catch (PDOException $ex) {
					      print_r($ex);
					    } // TRY de tabla páginas  

					    $xf = 0;
					    while ($xf < count($resPaginas22)) {
					      echo "Página = " . $xf . " Criterio = " . $resPaginas22[$xf]['criterio'] . " Subcriterio = " . $resPaginas22[$xf]['subcriterio'] . "<br>";
					      $xf = $xf + 1;
					    }
					  }
				  	}
				  }

	  			$ww = $ww + 1;
	  		} // Cierre del ciclo de modificación (actualización de criterios - ww <= td [tg])

	  		$tg = $tg + 1;
	  	} // Cierre del ciclo de grabación

	  	// MANEJO DE CASOS EXCEPCIONALES

	  	// 1) Si se encuentra la palabra "cúmplase" al final de la página, el código de la página
	  	//    potencialmente es el cierre de la página anterior (o alguna anterior). Una primera
	  	//    aproximación es asignarle el código de la página anterior, evaluando casos
	  	//    específicos. Por ahora, basta con realizar dicha asignación, y cuando se presenten
	  	//    casos anormales, evaluar dichos casos.

/*	  	if ($depuracion) {
	  		echo "<br><br>bbb - ANÁLISIS DE LOS CIERRES FORZOSOS<br><br>";
	  	}

	  	$pag = 0;
	  	while ($pag < count($resPaginas)) {
	  		if ($pag > 0) {
	  			// Se analiza después de la primera página
	  			$posicion = stripos("=" . $resPaginas[$pag]['contenido'], 'cúmplase');

	  			if ($depuracion) {
	  				echo "POSICIÓN DE cúmplase: " . $posicion . "<br>";
	  			}

	  			if ($posicion > 0) {
	  				// Esta parece ser una página de cierre
	  				// Para que lo sea, debe cumplirse que esté ABAJO
	  				if ( posicion_relativa( strlen($resPaginas[$pag]['contenido']), $posicion ) == ABAJO )
	  				try {
	  					$sql9 = "UPDATE tblpaginas SET criterio = " . $resPaginas[$pag - 1]['criterio'] . ", subcriterio = " . $resPaginas[$pag - 1]['subcriterio'] . " WHERE pagina = " . $resPaginas[$pag]['pagina'];
	  					$query9 = $pdo->prepare($sql9);
	  					$query9->execute();
	  					$resPaginas[$pag]['criterio'] = $resPaginas[$pag - 1]['criterio'];
	  					$resPaginas[$pag]['subcriterio'] = $resPaginas[$pag - 1]['subcriterio'];
	  				}
	  				catch (PDOException $ex) {
	  					print_r($ex);
	  				}
	  			}
	  		}
	  		$pag = $pag + 1;
	  	} */

	  	// ---------------------------------------------------------------------------------------

	  			// CASO DE FRONTERA

	  			/* 

			    try {
				    $sql2 = "SELECT * FROM tblpaginas WHERE procesado_nueva_trd = 0";
				    $query2 = $pdo->prepare($sql2);
				    $query2->execute();
				    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
				    $totalPaginas = $query2->rowCount();
				    $elExpediente = $resPaginas[0]['expediente'];
					}
			  	catch (PDOException $ex) {
			    	print_r($ex);
			  	} // TRY de tabla páginas   

	  			// SI LA PÁGINA NO ES VÁLIDA, ENTONCES SE EVALÚA UNA SITUACIÓN DE FRONTERA
  				// SI LA PÁGINA CONTIENE LA PALABRA resuelve, ENTONCES SE ASUME QUE LA
  				// PÁGINA ES EL CIERRE DE LA INMEDIATAMENTE ANTERIOR, Y SE PROCEDE SEGÚN
  				// EL PROTOCOLO
				$x = stripos("=" . $resPaginas[$p]['contenido'], 'resuelve');
				if ($x > 0) {
					// SE ENCONTRÓ LA PALABRA DENTRO DE LA PÁGINA !!!
					// SE REALIZA EL CIERRE, INCLUIDAS LAS BASES DE DATOS
					if ($depuracion) {
						echo "SE ENCONTRÓ UN CIERRE EN LA PÁGINA: " . $p . " CON LA ";
						echo "PALABRA resuelve" . " SE COPIA EL criterio Y EL subcriterio ";
						echo "PREVIO EN LA PÁGINA ACTUAL (siempre y cuando la página sea ";
						echo "MAYOR QUE CERO <br><br>";
					}
					$instruccion = "UPDATE tblpaginas SET criterio = " . $resPaginas[$p - 1]['criterio'] . ", subcriterio = " . $resPaginas[$p - 1]['subcriterio'] . ", procesado_nueva_trd = 1 WHERE pagina = " . $p;
					if ($depuracion) {
						echo "INSTRUCCIÓN PARA CIERRE DE PÁGINA: " . $instruccion . "<br>";
					}
		  			try {
		  				$sql24 = $instruccion;
		  				$query24 = $pdo->prepare($sql24);
		  				$query24->execute();
		  			}
				  	catch (PDOException $ex) {
				    	print_r($ex);
				  	}
				}

				*/

	} // FINAL: Cierre de la función => motor_detalles()

?>