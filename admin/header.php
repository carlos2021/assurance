<?php include "funciones.php"; ?>

<?php 
  $sql = "SELECT * FROM tblconfiguracion";
  $query = $pdo->prepare($sql);
  $query->execute();
  $resConfiguracion = $query->fetchAll(PDO::FETCH_ASSOC);

  $repositorio = $resConfiguracion[0]['repositorio'];
  $origen = $resConfiguracion[0]['origen'];
  $nro_abogados = $resConfiguracion[0]['nro_abogados'];
  $estado_sistema = $resConfiguracion[0]['estado_sistema'];

  $sql = "SELECT * FROM tblestado WHERE id = " . $estado_sistema;
  $query = $pdo->prepare($sql);
  $query->execute();
  $resEstado = $query->fetchAll(PDO::FETCH_ASSOC);

  $nombre_estado = $resEstado[0]['nombre'];
  
  $nroExpedientes = null;
  nro_expedientes($repositorio);

  $rutaD = "c:\\xampp\\htdocs\\assurance\\expedientes\\";
  $path = 'c:\\xampp\\htdocs\\assurance\\' . ENTIDAD;
  $tabla = Array();
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>HTML</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
	html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
	</style>
  <style>
    .separador-10 {
      margin-top: 10px;
    }
    .separador-20 {
      margin-top: 20px;
    }
    .separador-30 {
      margin-top: 30px;
    }
  </style>
</head>

<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  <span style="font-size:16px; font-weight:bold;">Menú</span></button>
  <span class="w3-bar-item w3-right"><span style="font-size:16px; font-weight:bold;">ASSURANCE</span></span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="w3images/avatar2.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span><span style="font-size:18px;">Bienvenido, <br><strong>Gestión Expedientes</strong></span></span><br>
      <!-- <a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-cog"></i></a> -->
    </div>
  </div>
  <hr>
  <div class="w3-container w3-black">
    <h5><span style="font-size:18px; font-weight:bold;">Tablero Digital</span></h5>
  </div>

  <div class="w3-bar-block">
    <a href="index.php" id="configuracion" class="w3-bar-item w3-button w3-padding w3-white" target="_self"><i class="fa fa-cogs"></i>  <span style="font-size:18px;">Configuración</span></a>
  </div>  

  <div class="w3-bar-block">
    <a href="p_repositorio.php?tipo=0" id="repositorio" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-folder-o"></i>  <span style="font-size:18px;">Repositorio</span></a>
  </div>  

  <div class="w3-bar-block">
    <a href="p_compactacion.php?procesado=inicio" id="compactacion" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-sign-in"></i>  <span style="font-size:18px;">Compactación</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_ajuste_carpetas.php?procesado=inicio" id="ajuste" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-folder"></i>  <span style="font-size:18px;">Ajustar Carpetas</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_repartir.php?procesado=no" id="repartir" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-caret-square-o-right"></i>  <span style="font-size:18px;">Repartir Expedientes</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_procesar.php" id="procesar" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-caret-square-o-right"></i>  <span style="font-size:18px;">Procesar Expedientes</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_agregar_extra.php" id="agregar_extra" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-bar-chart"></i>  <span style="font-size:18px;">Agregar TRD Extra</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_listar_extra.php" id="listar_extra" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-battery-three-quarters"></i>  <span style="font-size:18px;">Listar Extras</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="p_revisar_tipos_docs.php" id="revisar_tipos_docs" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-battery-three-quarters"></i>  <span style="font-size:18px;">Revisar TRDs Criterios</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="ver_resultados.php" id="ver_resultados" class="w3-bar-item w3-button w3-padding w3-white" target="_self"><i class="fa fa-cogs"></i>  <span style="font-size:18px;">Resultados</span></a>
  </div>  

</nav>

<script>
  function activar() {
    document.getElementById("abogado_01").click();
    document.getElementById("abogado_02").click();
    document.getElementById("abogado_09").click();
    document.getElementById("abogado_03").click();
    document.getElementById("abogado_04").click();
    document.getElementById("abogado_05").click();
    document.getElementById("abogado_06").click();
    document.getElementById("abogado_07").click();
    document.getElementById("abogado_08").click();
    document.getElementById("abogado_10").click();
    document.getElementById("abogado_11").click();
    document.getElementById("abogado_12").click();
  }
</script>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

