<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('subir_procesar');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>Gestión de Expedientes</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Subir y Procesar</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">
    <form method="post" action="s_modificar_configuracion.php">
      <button class="w3-button w3-dark-grey" type="file"><i class="fa fa-search w3-margin-right"></i> <span style="font-weight: bold;">Realizar Tarea</span></button>
    </form>
  </div>

  <!-- End page content -->
</div>
