<?php 
	include "funciones.php";

	$depuracion = 0;

	$sql = "SELECT * FROM tblabogados WHERE activo = 1 AND id_abogado = " . ID_ABOGADO;
	$query = $pdo->prepare($sql);
	$query->execute();
	$resAbogados = $query->fetchAll(PDO::FETCH_ASSOC);

	$expediente = $resAbogados[0]['expediente'];

	$origen = _REPOSITORIO;
	$destino = _EXPEDIENTES;

	$instruccion = "robocopy " . $origen . " " . $destino . " /MIR";

	if ($depuracion) {
		echo "EXPEDIENTE = " . $expediente . '<br>';
		echo "ORIGEN = " . $origen . '<br>';
		echo "DESTINO = " . $destino . '<br>';
		echo "INSTRUCCIÓN = " . $instruccion . '<br>';
	}

	shell_exec($instruccion);	

	header("Location: comando.php");
?>
