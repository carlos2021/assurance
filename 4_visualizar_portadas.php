<?php include "funciones.php"; ?>

<?php $configuracion = 0; ?>

<?php

  if ($configuracion) {
    echo "<br><br>ENTRANDO A VISUALIZACIÓN DE PORTADAS<br><br>";
  }

  ?>

  <?php if ($configuracion) { ?>
    <span style="font-weight:bold;">REGRESAR AL MENÚ</span>
    <a href="index.php"><button>ATRÁS</button></a>
  <?php } ?>

  <?php

    if ($configuracion) {
    	echo "<hr>VISUALIZAR PORTADAS<hr><br>";
    }

    $sql = "SELECT * FROM tblportada";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $s = '';

    if ($query->rowCount() > 0){
    	foreach($results as $result) {

        $result['radicacion'] = str_replace('-', ' ', $result['radicacion']);

    		$s .= $result['id'] . " EXPEDIENTE: " . '<span style="font-weight:bold;">' . $result['expediente'] . "</span><br><br>";
    		$s .= '<table border="1">';
    		$s .= '<tr><th>Campo</th><th>Valor</th></tr>';
        $_link = trim(str_replace('c:\xampp\htdocs', '', $result['link']));
    		$s .= '<tr><td>&nbsp;Archivo&nbsp;</td><td>&nbsp;' . '<a href="' . $_link . '" target="_blank">' . $result['archivo'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Ciudad&nbsp;</td><td>&nbsp;' . $result['ciudad'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Despacho&nbsp;</td><td>&nbsp;' . $result['despacho'] . '&nbsp;</td></tr>';
        if ($result['serie'] == 270)
          $cad1 = 'EXPEDIENTES DE PROCESOS JUDICIALES';
        if ($result['subserie'] == 245)
          $cad2 = 'EXPEDIENTES DE PROCESOS JUDICIALES PENALES LEY 906 DE 2004';
    		$s .= '<tr><td>&nbsp;Serie&nbsp;</td><td>&nbsp;' . $cad1 . '&nbsp;</td></tr>';
        $s .= '<tr><td>&nbsp;Subserie&nbsp;</td><td>&nbsp;' . $cad2 . '&nbsp;</td></tr>';
        $s .= '<tr><td>&nbsp;Fecha&nbsp;</td><td>&nbsp;' . $result['fecha'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Número de Proceso&nbsp;</td><td>&nbsp;' . $result['radicacion'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Parte Procesal A&nbsp;</td><td>&nbsp;' . $result['parte_procesal_a'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Parte Procesal B&nbsp;</td><td>&nbsp;' . $result['parte_procesal_b'] . '&nbsp;</td></tr>';
        $s .= '<tr><td>&nbsp;Proceso&nbsp;</td><td>&nbsp;' . $result['proceso'] . '&nbsp;</td></tr>';
     		$s .= '<tr><td>&nbsp;Terceros&nbsp;</td><td>&nbsp;' . $result['terceros'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Cuaderno&nbsp;</td><td>&nbsp;' . $result['cuaderno'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Tipo Expediente&nbsp;</td><td>&nbsp;' . $result['tipo_expediente'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Número de Carpetas&nbsp;</td><td>&nbsp;' . $result['nro_carpetas'] . '&nbsp;</td></tr>';
    		$s .= '<tr><td>&nbsp;Carpetas Digitales&nbsp;</td><td>&nbsp;' . $result['nro_carpetas_digital'] . '&nbsp;</td></tr>';
    		$s .= '</table><br><hr><br>';
    	}

      if ($configuracion) {
      	echo $s;
      }

    }

    $pdo = null;
?>

<?php if ($configuracion) { ?>
  <span style="font-weight:bold;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>
<?php } ?>

<?php 
  if ($configuracion) {
    echo "<br><br>SALIENDO DE VISUALIZACIÓN PORTADA<br><br>";
  }
?>
