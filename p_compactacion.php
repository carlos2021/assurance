<?php include "admin/header.php"; ?>

<?php
  $procesado = $_GET['procesado'];
?>

<script>
  var div = document.getElementById('compactacion');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">
  <!-- Header -->
  <div class="w3-panel w3-margin-top w3-bottombar" style="margin-top:22px; margin-left:16px; margin-right:16px;">
 
    <div class="w3-panel">
      <h4><b><i class="fa fa-folder-open-o"></i> &nbsp;Proceso de Compactación</b></h4>
      <div class="w3-section">
        <span class="w3-margin-right" style="font-size:18px;">Compactar:</span> 
        <div class="w3-dropdown-hover">
          <button class="w3-button w3-black" id="compactar" onclick="proceso_compactar()">Ejecutar</button>
        </div>
      </div>
    </div>

  </div>

  <div class="w3-container">

    <div class="w3-panel w3-pale-red" id="inicio" style="visibility:hidden;">
      <h3>Inicio Proceso de Compactación</h3>
      <p>El sistema une los archivos PDF de acuerdo con las reglas del Protocolo II</p>
      <p><i class="fa fa-spinner w3-spin" style="font-size:64px"></i></p>
    </div>

    <div class="w3-panel w3-green" id = "finalizacion" style="visibility:hidden;">
      <h3>Compactación Finalizada</h3>
      <p>El sistema ha unido los PDF. Para visualizar el resultado, haga clic en el botón: Repositorio</p>
    </div>

    <div class="w3-dropdown-hover" id="panel_boton" style="visibility:hidden;">
      <button class="w3-button w3-black" id="boton_finalizacion" onclick="mostrar_repositorio()">If a Ajuste de Carpetas</button>
    </div>

  </div>

  <!-- End page content -->
</div>

<script>

  var proc = "<?php echo $procesado; ?>";

  if (proc == 'finalizado') {
    document.getElementById("inicio").style.display = 'none';
    document.getElementById("finalizacion").style.visibility = 'visible';
    document.getElementById("panel_boton").style.visibility = 'visible';    
  }

  function proceso_compactar() {
    document.getElementById("inicio").style.visibility = 'visible';
    window.open("http://localhost/assurance/" + "<?php echo _ENTIDAD; ?>" + "/compactar_archivos.php", "_self");
  }

  function mostrar_repositorio() {
    window.open("http://localhost/assurance/" + "<?php echo _ENTIDAD; ?>" + "/p_ajuste_carpetas.php?procesado=inicio", "_self");    
  }
</script>
