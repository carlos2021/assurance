<?php include "db_config_exp.php"; ?>
<?php

function depurar_cadena($text) {
    $ori = array(" en ", " de ", " la ", " el ", " los ", " las ", " es ", " a ", " por ", " parte ", " del ", " y ", " o " , " cual ", " y/o ", " e ", " que ", " sobre ", " para ", " al ", " pone ", " se ", " no ", "<br>");
    $rem = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
    $text = str_replace($ori, $rem, $text);
    return $text;
}

function buscar_cadena($texto, $dato) {
    $dato = trim($dato);
    $dato = str_replace("  ", " ", $dato);
    $dato = str_replace("  ", " ", $dato);

    $dummy = explode(" ", $dato);

    // PENDIENTE DE CONSTRUCCIÓN
}

function t ($s) {
    return utf8_decode($s);
}

function sp ($n) {
    $lin = '';
    $i = 0;
    while ($i < $n) {
        $lin = ' ' . $lin;
        $i = $i + 1;
    }
    return $lin;
}

function slugify($text)
{
    // Strip html tags
    $text=strip_tags($text);
    // Quitar ?, ", '
    $text = str_replace('$', '', $text);
    $text = str_replace('?', '', $text);
    $text = str_replace('"', '', $text);
    $text = str_replace('\'', '', $text);
    // Replace non letter or digits by -
    // $text = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', ' ', $text)));
    // $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // Transliterate
    // setlocale(LC_ALL, 'es_ES.utf8');
    // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // Remove unwanted characters
    //$text = preg_replace('~[^-\w]+~', '', $text);
    // Trim
    $text = trim($text);
    // Remove duplicate -
    // $ext = preg_replace('~-+~', ' ', $text);
    // Lowercase
    $text = strtolower($text);
    // Check if it is empty
    return $text;
}

function espacios( $n ) {
    $i = 0;
    $aux = '';
    while ($i < $n * 8) {
        $aux = $aux . '&nbsp;';
        $i = $i + 1;
    }
    return $aux;
}

function es_letra($car) {
    if ( (ord($car) >= 65 && ord($car) <= 90) || (ord($car) >= 97 && ord($car) <= 122) )
        return true;
    else
        return false;
}

function buscar_mes($texto, $pos = 0) {
    $z = $pos;
    // DETECCIÓN DEL MES
    $m1 = stripos($texto, 'enero', $z);
    $m2 = stripos($texto, 'febrero', $z);
    $m3 = stripos($texto, 'marzo', $z);
    $m4 = stripos($texto, 'abril', $z);
    $m5 = stripos($texto, 'mayo', $z);
    $m6 = stripos($texto, 'junio', $z);
    $m7 = stripos($texto, 'julio', $z);
    $m8 = stripos($texto, 'agosto', $z);
    $m9 = stripos($texto, 'septiembre', $z);
    $m10 = stripos($texto, 'octubre', $z);
    $m11 = stripos($texto, 'noviembre', $z);
    $m12 = stripos($texto, 'diciembre', $z);
    $m13 = stripos($texto, 'Ñero', $z);

    // CONVERSIÓN DEL MES A VALOR NUMÉRICO
    $mes = '';
    if ($m1 > 0) { $mes = '01'; $posicion = $m1; $long = 6; }
    else if ($m2 > 0) { $mes = '02'; $posicion = $m2; $long = 8; }
    else if ($m3 > 0) { $mes = '03'; $posicion = $m3; $long = 6; }
    else if ($m4 > 0) { $mes = '04'; $posicion = $m4; $long = 6; }
    else if ($m5 > 0) { $mes = '05'; $posicion = $m5; $long = 5; }
    else if ($m6 > 0) { $mes = '06'; $posicion = $m6; $long = 6; }
    else if ($m7 > 0) { $mes = '07'; $posicion = $m7; $long = 6; }
    else if ($m8 > 0) { $mes = '08'; $posicion = $m8; $long = 7; }
    else if ($m9 > 0) { $mes = '09'; $posicion = $m9; $long = 11; }
    else if ($m10 > 0) { $mes = '10'; $posicion = $m10; $long = 8; }
    else if ($m11 > 0) { $mes = '11'; $posicion = $m11; $long = 10; }
    else if ($m12 > 0) { $mes = '12'; $posicion = $m12; $long = 10; }
    else if ($m13 > 0) { $mes = '01'; $posicion = $m1; $long = 6; }
    else { $mes = '00'; $posicion = 0; $long = 0;}

    $resp = Array();

    $resp[] = $mes;
    $resp[] = $posicion;
    $resp[] = $long;

    return $resp;
}

function valor_dia_en_letras($num_letras) {
    $dia = '';
    if (strcmp($num_letras, 'uno') == 0 || strcmp($num_letras, 'primero') == 0) $dia = '01';
    if (strcmp($num_letras, 'dos') == 0) $dia = '02';
    if (strcmp($num_letras, 'tres') == 0) $dia = '03';
    if (strcmp($num_letras, 'cuatro') == 0) $dia = '04';
    if (strcmp($num_letras, 'cinco') == 0) $dia = '05';
    if (strcmp($num_letras, 'seis') == 0) $dia = '06';
    if (strcmp($num_letras, 'siete') == 0) $dia = '07';
    if (strcmp($num_letras, 'ocho') == 0) $dia = '08';
    if (strcmp($num_letras, 'nueve') == 0) $dia = '09';
    if (strcmp($num_letras, 'diez') == 0) $dia = '10';
    if (strcmp($num_letras, 'once') == 0) $dia = '11';
    if (strcmp($num_letras, 'doce') == 0) $dia = '12';
    if (strcmp($num_letras, 'trece') == 0) $dia = '13';
    if (strcmp($num_letras, 'catorce') == 0) $dia = '14';
    if (strcmp($num_letras, 'quince') == 0) $dia = '15';
    if (strcmp($num_letras, 'dieciseis') == 0) $dia = '16';
    if (strcmp($num_letras, 'diecisiete') == 0) $dia = '17';
    if (strcmp($num_letras, 'dieciocho') == 0) $dia = '18';
    if (strcmp($num_letras, 'diecinueve') == 0) $dia = '19';
    if (strcmp($num_letras, 'veinte') == 0) $dia = '20';
    if (strcmp($num_letras, 'veintiuno') == 0) $dia = '21';
    if (strcmp($num_letras, 'veintidos') == 0) $dia = '22';
    if (strcmp($num_letras, 'veintitres') == 0) $dia = '23';
    if (strcmp($num_letras, 'veinticuatro') == 0) $dia = '24';
    if (strcmp($num_letras, 'veinticinco') == 0) $dia = '25';
    if (strcmp($num_letras, 'veintiseis') == 0) $dia = '26';
    if (strcmp($num_letras, 'veintisiete') == 0) $dia = '27';
    if (strcmp($num_letras, 'veintiocho') == 0) $dia = '28';
    if (strcmp($num_letras, 'veintinueve') == 0) $dia = '29';
    if (strcmp($num_letras, 'treinta') == 0) $dia = '30';
    if (strcmp($num_letras, 'treintaiuno') == 0) $dia = '31';
    if (strcmp($num_letras, 'treintayuno') == 0) $dia = '31';
    return $dia;
} 

function vacio($s) {
    if (strcmp($s, '') == 0)
        return true;
    else
        return false;
}

function encontrar_fecha($texto) {

    $mostrar = 0;
    $mostrar_2 = 0;
    // BÚSQUEDA ESTÁNDAR
    $resultado = 'sin-procesar';
    $ciclo = 0;
    $maximo = strlen($texto);
    $pos = stripos($texto, 'fecha', 0);

    if ($pos == 0)
        $resultado = 'fecha-no-encontrada';
    else {
        if ($mostrar) {
            echo "POS búsqueda fecha = " . $pos . "<br>";
        }
        while ($pos < $maximo) {
            $ciclo = $ciclo + 1;
            if ($ciclo > 20) {
                echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO !!! <hr><br>";
                exit(1);
            }
            if ($mostrar) 
                echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";
            if ($mostrar) {
                echo "TEXTO ENVIADO A: encontrar_fecha_dia_mes_anio: " . $texto . "<br><br>";
                echo "POS ENVIADO: " . $pos . "<br><br>";
            }
            $fecha = encontrar_fecha_dia_mes_anio($texto, $pos);
            if ($mostrar)
                echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
            if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                $resultado = $fecha;
                break;
            }
            else {
                if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                    $resultado = $fecha;
                    break;
                }
                if ($mostrar)
                    echo "POS de la fecha no valida = " . $pos . "<br>";
                $pos = stripos($texto, 'fecha', $pos + 1);
                if ($mostrar)
                    echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                if ($pos == 0) {
                    $resultado = $fecha;
                    break; 
                }
            }
        }
    }

    // BÚSQUEDA POR MES: mes - día - año
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

        // BÚSQUEDA ESTÁNDAR
        $ciclo = 0;
        $maximo = strlen($texto);
        $un_mes = buscar_mes($texto, 0);

        if ($mostrar) {
            echo "<br>MES ENCONTRADO (versión NO estándar): <br>"; 
            echo "MES = " . $un_mes[0] . " POSICIÓN = " . $un_mes[1] . " LONGITUD = " . $un_mes[2] . "<br>";
        }

        $mes = $un_mes[0];
        $pos = $un_mes[1];
        $long = $un_mes[2];

        if ($pos == 0)
            $resultado = 'fecha-no-encontrada';
        else {
            while ($pos < $maximo) {
                $ciclo = $ciclo + 1;
                if ($ciclo > 20) {
                    echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO !!! <hr><br>";
                    exit(1);
                }
                if ($mostrar) 
                    echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";
                $fecha = encontrar_fecha_por_mes($texto, $mes, $pos, $long);
                if ($mostrar)
                    echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
                if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                    $resultado = $fecha;
                    break;
                }
                else {
                    if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                        $resultado = $fecha;
                        break;
                    }
                    if ($mostrar)
                        echo "POS de la fecha no valida = " . $pos . "<br>";
                    $pos = stripos($texto, 'fecha', $pos + 1);
                    if ($mostrar)
                        echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                    if ($pos == 0) {
                        $resultado = $fecha;
                        break; 
                    }
                }
            }            
        }
    }

    // BÚSQUEDA POR MES: antecedido por día - luego mes - luego año
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

        // BÚSQUEDA ESTÁNDAR
        $ciclo = 0;
        $maximo = strlen($texto);
        $un_mes = buscar_mes($texto, 0);

        if ($mostrar) {
            echo "<br>MES ENCONTRADO (versión NO estándar) (antecedido por el día): <br>"; 
            echo "MES = " . $un_mes[0] . " POSICIÓN = " . $un_mes[1] . " LONGITUD = " . $un_mes[2] . "<br>";
        }

        $mes = $un_mes[0];
        $pos = $un_mes[1];
        $long = $un_mes[2];

        if ($pos == 0)
            $resultado = 'fecha-no-encontrada';
        else {
            while ($pos < $maximo) {
                $ciclo = $ciclo + 1;
                if ($ciclo > 20) {
                    echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO !!! <hr><br>";
                    exit(1);
                }
                if ($mostrar) 
                    echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";

                $fecha = encontrar_fecha_por_mes_antecedido_dia($texto, $mes, $pos, $long);

                if ($mostrar)
                    echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
                if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                    $resultado = $fecha;
                    break;
                }
                else {
                    if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                        $resultado = $fecha;
                        break;
                    }
                    if ($mostrar)
                        echo "POS de la fecha no valida = " . $pos . "<br>";
                    $pos = stripos($texto, 'fecha', $pos + 1);
                    if ($mostrar)
                        echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                    if ($pos == 0) {
                        $resultado = $fecha;
                        break; 
                    }
                }
            }            
        }
    }

    // BÚSQUEDA POR SLASH
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {
        $pos = stripos($texto, '/');
        if ($pos > 0) {
            while ($pos < strlen($texto)) {

                if ($mostrar_2) {
                    echo "POS = " . $pos . "<br>";
                }

                //////////////////////////////
                $n1 = '';
                $z = $pos - 1;
                if ($mostrar_2) {
                    echo "z = " . $z . "<br>";
                }
                while($z > 0) {
                    if (!is_numeric($texto[$z])) {
                        break;
                    }
                    else {
                        $n1 = $texto[$z] . $n1;
                        if ($mostrar_2) {
                            echo "NUEVO VALOR DE n1 = " . $n1 . "<br>";
                        } 
                    }
                    $z = $z - 1;
                }
                //////////////////////////////
                $n2 = '';
                if (!vacio($n1)) {
                    $z = $pos + 1;
                    while($z < strlen($texto)) {
                        if (!is_numeric($texto[$z]))
                            break;
                        else
                            $n2 = $n2 . $texto[$z]; 
                        $z = $z + 1;
                    }                   
                }
                //////////////////////////////
                $n3 = '';
                if (!vacio($n2)) {
                    if (strcmp($texto[$z], '/') == 0) {
                        $t = $z + 1;
                        while ($t < strlen($texto)) {
                            if (!is_numeric($texto[$t]))
                                break;
                            else
                                $n3 = $n3 . $texto[$t];
                            $t = $t + 1;
                        }
                    }
                }
                //////////////////////////////
                $vn1 = intval($n1);
                $vn2 = intval($n2);
                $vn3 = intval($n3);

                $anio = 0;
                $mes = 0;
                $dia = 0;

                if ($mostrar_2) {
                    echo "n1 = " . $n1 . "<br>";
                    echo "n2 = " . $n2 . "<br>";
                    echo "n3 = " . $n3 . "<br>";
                }

                if ($vn1 > 1900) {
                    $anio = $n1;
                    if ($vn2 > 12 && $vn2 < 32) {
                        $mes = $vn2;
                        $dia = $vn3;
                    }
                    if ($vn3 > 12 && $vn3 < 32) {
                        $mes = $vn3;
                        $dia = $vn2;
                    }
                    if ($mes == 0) {
                        $mes = $vn2;
                        $dia = $vn3;
                    }
                }
                else if ($vn2 > 1900) {
                    $anio = $n2;
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn3;
                    }
                    if ($vn3 > 12 && $vn3 < 32) {
                        $mes = $vn3;
                        $dia = $vn1;
                    }
                    if ($mes == 0) {
                        $mes = $vn3;
                        $dia = $vn1;
                    }
                }
                else if ($vn3 > 1900) {
                    $anio = $n3;
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn2;
                    }
                    if ($vn2 > 12 && $vn2 < 32) {
                        $mes = $vn2;
                        $dia = $vn1;
                    }
                    if ($mes == 0) {
                        $mes = $vn2;
                        $dia = $vn1;
                    }
                }

                if ($anio == 0) {
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn2;
                        $anio = $vn3;
                    }
                    else if ($vn2 > 12 && $vn2 < 32) {
                        $dia = $vn1;
                        $mes = $vn2;
                        $anio = $vn3;
                    }
                    else if ($vn3 > 12 && $vn3 < 32) {
                        $anio = $vn1;
                        $dia = $vn2;
                        $mes = $vn3;
                    }
                }

                if ($anio == 0) {
                    $dia = $vn1;
                    $mes = $vn2;
                    $anio = $vn3;
                }
                //////////////////////////////
                if (($dia > 0 && $dia < 32) && 
                    ($mes > 0 && $mes < 13) &&
                    ($anio > 1900 && $anio < 2025)) {
                    // SE CONSTRUYE LA FECHA
                    if ($anio < 25) {
                        $anio = $anio + 2000;
                    }

                    $_anio = strval($anio);
                    $_mes = strval($mes);
                    $_dia = strval($dia);

                    while (strlen($_anio) < 4) {
                        $_anio = '0' . $_anio;
                    }

                    while (strlen($_mes) < 2) {
                        $_mes = '0' . $_mes;
                    }

                    while (strlen($_dia) < 2) {
                        $_dia = '0' . $_dia;
                    }

                    $aux = $_anio . "/" . $_mes. "/" . $_dia;

                    // SE CONVIERTE A FORMATO UNIX
                    $d = strtotime($aux);

                    // SE CONVIERTE A FORMATO FECHA PHP
                    $fecha = date('Y-m-d', $d);

                    // SE RETORNA EL VALOR DE FECHA CALCULADO
                    return $fecha;
                    //////////////////////////////
                }
                else {
                    $posicion = $pos + 12;
                    if ($posicion > strlen($texto)){
                        break;
                    }
                    $pos = stripos($texto, '/', $posicion); 
                    if ($pos == 0) {
                        break;
                    }             
                } 
            }           
        }
    }

    // BÚSQUEDA POR GUIONES
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

    }

    return $resultado;
}

function encontrar_fecha_dia_mes_anio($texto, $pos = 0) {

    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;

        if ($maximo > strlen($texto)) {
            $maximo = strlen($maximo) - 1;
        }

        $maximo = $maximo - 1;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos + 5;
        if ($k > strlen($texto)) {
            $k = strlen($texto) - 1;
        }
        $k = $k - 1;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL MES
        $m1 = stripos($texto, 'enero', $z);
        $m2 = stripos($texto, 'febrero', $z);
        $m3 = stripos($texto, 'marzo', $z);
        $m4 = stripos($texto, 'abril', $z);
        $m5 = stripos($texto, 'mayo', $z);
        $m6 = stripos($texto, 'junio', $z);
        $m7 = stripos($texto, 'julio', $z);
        $m8 = stripos($texto, 'agosto', $z);
        $m9 = stripos($texto, 'septiembre', $z);
        $m10 = stripos($texto, 'octubre', $z);
        $m11 = stripos($texto, 'noviembre', $z);
        $m12 = stripos($texto, 'diciembre', $z);
        $m13 = stripos($texto, 'Ñero', $z);

        // CONVERSIÓN DEL MES A VALOR NUMÉRICO
        $mes = '01';
        if ($m1 > 0) $mes = '01';
        else if ($m2 > 0) $mes = '02';
        else if ($m3 > 0) $mes = '03';
        else if ($m4 > 0) $mes = '04';
        else if ($m5 > 0) $mes = '05';
        else if ($m6 > 0) $mes = '06';
        else if ($m7 > 0) $mes = '07';
        else if ($m8 > 0) $mes = '08';
        else if ($m9 > 0) $mes = '09';
        else if ($m10 > 0) $mes = '10';
        else if ($m11 > 0) $mes = '11';
        else if ($m12 > 0) $mes = '12';
        else if ($m13 > 0) $mes = '01';
        else {
            if ($mostrar) echo "<br>(5) - ESTA SALIENDO PUES mes es erróneo -- <br>";
            return $fecha_invalida;           
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_mes($texto, $mes, $pos, $long) {
    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;
        if ($maximo > strlen($texto)) {
            $maximo = strlen($texto) - 1;
        }

        $maximo = $maximo - 1;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos + $long;
        if ($k > strlen($texto)) {
            $k = $k - 1;
        }

        $k = $k - 1;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $dia = '';
        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_mes_antecedido_dia($texto, $mes, $pos, $long) {
    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos - 10;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $dia = '';
        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        if ($mostrar) {
            echo "<br>AÑO ENCONTRADO = " . $anio . " MES = " . $mes . " DÍA = " . $dia . "<br>";
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_slash($texto, $pos = 0) {

}

function encontrar_fecha_por_guion($texto, $pos = 0) {

}

/* function ajustar_nombres_ruta($ruta, $nivel) {


   echo "DENTRO DE LA FUNCIÓN <br><br>";
   // abre un directorio y lo lista recursivamente
   if (is_dir($ruta)) {

      if ($dh = opendir($ruta)) {
         while (($file = readdir($dh)) !== false) {
            // Encuentra el directorio o un archivo
            // Verifica inicialmente que el directorio no se corresponda con un punto de retorno al directorio padre
            // o con el directorio punto, el cual es el punto de entrada al directorio
            if ($file != "." && $file != "..") {

                if (strcasecmp(filetype($ruta.$file), 'file') == 0) {
                    // ES UN ARCHIVO
                    // c:\xampp\htdocs\assurance\expedientes\PEX01\13001600112920110293600\C01.pdf
                    $extension = pathinfo($ruta.$file, PATHINFO_EXTENSION); // pdf
                    $basename = pathinfo($ruta.$file, PATHINFO_BASENAME); // C01.pdf
                    $filename = pathinfo($ruta.$file, PATHINFO_FILENAME); // C01
                    $dirname = pathinfo($ruta.$file, PATHINFO_DIRNAME); // c:\xampp\htdocs\assurance\expedientes\PEX01\13001600112920110293600

                    $final = substr($dirname, strlen($dirname) - 11);

                    echo "<br>DATOS EXTRAÍDOS DEL DIRECTORIO <br>";
                    echo "RUTA = " . $ruta . "<br>";
                    echo "FILE = " . $file . "<br>";
                    $extension = pathinfo($ruta.$file, PATHINFO_EXTENSION);
                    $basename = pathinfo($ruta.$file, PATHINFO_BASENAME);
                    $filename = pathinfo($ruta.$file, PATHINFO_FILENAME);
                    $dirname = pathinfo($ruta.$file, PATHINFO_DIRNAME);
                    echo "EXTENSION = " . $extension . "<br>";
                    echo "BASENAME = " . $basename . "<br>";               
                    echo "FILENAME = " . $filename . "<br>";                
                    echo "DIRNAME = " . $dirname . "<br>";
                    echo "TIPO DEL ARCHIVO = " . filetype($ruta.$file) . "<br>";
                    echo "LINK = " . $ruta.$file . "<br>";

                    echo "FINAL = " . $final . "<br><br>";

                    $parte_nombre = substr($file, 0, 2);

                    if (strcasecmp($parte_nombre, "C0") == 0) {
                       rename($ruta.$file, $ruta."Proceso".$final.$file); 
                       $file = $final.$file;  
                       echo "NUEVO FILE = " . $file . "<br>";                     
                    }
                }

                // SE PROCESA EL ARCHIVO, ya sea un directorio o un documento
                // Se agregan sus características a la tabla PHP
                
                // Las caracteríticas que se agregan a la tabla son las siguientes:
                
                // (nombre del archivo o directorio), (tipo del archivo), (nivel de profundidad)
                // Se agregan dos campos con valor cero, que posteriormete serán modificados
                // Estos se corresponden con el total de expedientes y archivos (diferentes a directorios) que
                // dependen del nodo actual
            }
            if (is_dir($ruta . $file) && $file!="." && $file!=".."){
               // Estamos aqu¨ª si el archivo es un directorio, distinto de "." y ".."
               // EL PROCESO CONTIN0‰3A DE MANERA RECURSIVA

               listar_directorios_ruta($ruta . $file . "\\", $nivel + 1);
            }
         }

      closedir($dh);
      }
   } else {
        // La ruta no es valida

   }    
} */

function nro_expedientes($ruta) {
    global $nroExpedientes;
    if (is_dir($ruta)) {
        if ($dh = opendir($ruta)) {
            while (($file = readdir($dh)) !== false) {
                if ($file != "." && $file != "..") {
                    if (filetype($ruta . $file) == 'dir') {
                        $nroExpedientes = $nroExpedientes + 1;
                    } 
                }
                if (is_dir($ruta . $file) && $file != "." && $file != "..") {
                    nro_expedientes($ruta . $file . "\\");
                }                
            }
        }
    }
    else {
        $nroExpedientes = 0;
    }
}

function listar_directorios_ruta($ruta, $nivel){
    
   global $tabla;

   // abre un directorio y lo lista recursivamente
   if (is_dir($ruta)) {

      if ($dh = opendir($ruta)) {
         while (($file = readdir($dh)) !== false) {
            // Encuentra el directorio o un archivo
            // Verifica inicialmente que el directorio no se corresponda con un punto de retorno al directorio padre
            // o con el directorio punto, el cual es el punto de entrada al directorio
            if ($file != "." && $file != "..") {

                // SE PROCESA EL ARCHIVO, ya sea un directorio o un documento
                // Se agregan sus características a la tabla PHP
                
                // Las caracteríticas que se agregan a la tabla son las siguientes:
                
                // (nombre del archivo o directorio), (tipo del archivo), (nivel de profundidad)
                // Se agregan dos campos con valor cero, que posteriormete serán modificados
                // Estos se corresponden con el total de expedientes y archivos (diferentes a directorios) que
                // dependen del nodo actual
                $arr = Array();

                $arr[] = $file; // 0 = archivo (file)
                $arr[] = filetype($ruta.$file); // 1 = tipo del archivo
                $arr[] = $nivel; // 2 = nivel
                $arr[] = 0; // 3 = número de expedientes
                $arr[] = 0; // 4 = número de documentos
                $arr[] = $ruta.$file; // 5 = LINK

                /* 
                    indice 0 = nombre del archivo
                    índice 1 = tipo del archivo (dir, file)
                    índice 2 = nivel
                    índice 3 = número de expedientes
                    índice 4 = número de documentos
                    índice 5 = LINK
                */

                $tabla[] = $arr;
            }
            if (is_dir($ruta . $file) && $file!="." && $file!=".."){
               // Estamos aqu¨ª si el archivo es un directorio, distinto de "." y ".."
               // EL PROCESO CONTIN0‰3A DE MANERA RECURSIVA

               listar_directorios_ruta($ruta . $file . "\\", $nivel + 1);
            }
         }

      closedir($dh);
      }
   } else {
        // La ruta no es valida
        $tabla = null;         
   }
}

function compactar_archivos($ruta) {

    global $tabla;
    global $rutaD;
    global $path;

    $path = RAIZ;

    /* 
        indice 0 = nombre del archivo
        índice 1 = tipo del archivo (dir, file)
        índice 2 = nivel
        índice 3 = número de expedientes
        índice 4 = número de documentos
        índice 5 = LINK
    */

    global $tabla;
    global $archivos;

    // Se busca el nivel más alto
    $nivel_max = 1;

    $i = 1;
    while ($i < count($tabla)) {
        if ($tabla[$i][2] > $nivel_max) {
            $nivel_max = $tabla[$i][2];
        }
        $i = $i + 1;
    }

    // Se evalúa desde el nivel 1 hasta el nivel más alto
    // Para cada nivel se genera una lista de archivos por expediente (o por carpeta)
    // Se procesa cada lista de archivos. Se busca el documento origen
    // Se preserva el nombre del documento origen

    $lista = Array();

    $i = 1;
    while ($i <= $nivel_max) {
        $directorios = Array();

        echo "PASADA CUANDO i = " . $i . "<br>";
        echo "LONGITUD TABLA = " . count($tabla) . "<br>";

        $k = 1;
        while ($k < count($tabla)) {
            if ($tabla[$k][2] == $i && strcasecmp($tabla[$k][1], 'dir') == 0) {
                $directorios[] = $k;
            }
            $k = $k + 1;
        }

        $directorios[] = count($tabla);

        echo "SE MUESTRA EL 'directorio'" . "<br><br>";

        $tt = 0;
        while ($tt < count($directorios)) {
            echo $directorios[$tt] . " - ";
            $tt = $tt + 1;
        }

        echo "<br><br>";

        echo "LONGITUD DEL DIRECTORIO = " . count($directorios) . "<br>";

        $z = 0;
        while ($z < count($directorios) - 1) {
            echo "VALOR z = " . $z . " VALOR LIMITE (count directorios) = " . count($directorios) . "<br>";
            $nivel = $tabla[$directorios[$z]][2];
            $w1 = $directorios[$z];
            if ($z == count($directorios) - 1) {
                $w2 = $w1;
            }
            else {
                $w2 = $directorios[$z + 1];
            }

            $arreglo = Array();
            $nn = $w1;
            echo "<br><br>";
            while ($nn < $w2) {
                $archi = $tabla[$nn][0];
                $dummy = explode(".", $archi);
                $ext = $dummy[count($dummy) - 1];
                if (strcasecmp($ext, "pdf") == 0 && $tabla[$nn][2] == $nivel + 1) {
                    echo $nn . " -- ARCHIVO = " . $tabla[$nn][0] . "<br>";
                    $arreglo[] = $tabla[$nn][5];
                }
                $nn = $nn + 1;
            }

            $lista[] = $arreglo;

            $z = $z + 1;
        }     
        $i = $i + 1;
    }

    // Se imprime la lista para validar el trabajo

    echo "ARCHIVO DE COMPACTACIÓN: " . "<br><br>";
    $fila = 0;
    echo count($lista) . " Longitud de lista" . "<br>";
    while ($fila < count($lista)) {
        $columna = 0;
        while ($columna < count($lista[$fila])) {
            echo "LISTA [" . $fila . ", " . $columna ."] = " . $lista[$fila][$columna] . "<br>";
            $columna = $columna + 1;
        }
        echo "<br>";
        $fila = $fila + 1;
    }

    // Se elige el nombre clave del paquete de archivos. Se utiliza el que tenga el nombre más corto

    echo "<br>";

    $fila = 0;
    while ($fila < count($lista)) {
        $columna_elegida = -1;
        $min_len = 0;
        $columna = 0;
        while ($columna < count($lista[$fila])) {
            $pos1 = stripos($lista[$fila][$columna], "C01");
            $pos2 = stripos($lista[$fila][$columna], "C1");
            if ($columna_elegida == -1 && $pos1 > 0) {
                $columna_elegida = $columna;
            }
            if ($columna_elegida == -1 && $pos2 > 0) {
                $columna_elegida = $columna;
            }
            if (strlen($lista[$fila][$columna]) < strlen($lista[$fila][$min_len])) {
                $min_len = $columna;
            }
            $columna = $columna + 1;
        }
        if ($columna_elegida == -1) {
            $columna_elegida = $min_len;
        }

        echo "FILA ACTIVA = " . $fila . " COLUMNA SELECCIONADA = " . $columna_elegida . " LINK = " . $lista[$fila][$columna_elegida] . "<br>";

        $instruccion_1 = "pdftk ";
        $archivos = $lista[$fila][$columna_elegida] . " ";

        $cc = 0;
        while ($cc < count($lista[$fila])) {
            if ($cc != $columna_elegida) {
                $archivos = $archivos . $lista[$fila][$cc] . " ";
            }
            $cc = $cc + 1;
        }

        $instruccion_2 = " cat output ";
        $destino = $lista[$fila][$columna_elegida];

        $destino = "c:\\xampp\\htdocs\\assurance\\directorio\\PEX01\\1000\\temporalC04.pdf";

        $salida = "temporal.pdf";  

        $instruccion = $instruccion_1 . $archivos . $instruccion_2 . $destino;

        echo "INSTRUCCIÓN A EJECUTAR = " . $instruccion . "<br><br>"; 

        shell_exec($instruccion);   

        $fila = $fila + 1;
    }

    // Se compactan todos los archivos en uno solo
    // Se le cambia el nombre al archivo, por el del origen
}

function calcular_expedientes() {

    global $tabla;
 
    $i = 0;
    while ($i < count($tabla)) {
       
        if ($tabla[$i][2] == 0) {
    
            $nExp = 0;
            $nDoc = 0;
    
            $j = $i + 1;
            while ( ($j < count($tabla)) && ($tabla[$j][2] != 0) ) {
    
                if ( (strcmp($tabla[$j][1], 'dir') == 0) && ($tabla[$j][2] == 1) ) {
                    $nExp = $nExp + 1;
                }
                if (strcmp($tabla[$j][1], 'file') == 0) {
                    $archi = $tabla[$j][0];
                    // echo "ARCHIVO = " . $archi . "<br>";
                    $dummy = explode('.', $archi);
                    // print_r($dummy);
                    if (strcasecmp($dummy[count($dummy) - 1], 'pdf') == 0) {
                        $nDoc = $nDoc + 1;
                        // echo "VALOR DE nDoc = " . $nDoc . "<br>";
                    }
                }
    
                $j = $j + 1;
            }
    
            $tabla[$i][3] = $nExp;
            $tabla[$i][4] = $nDoc;
    
        }
    
        else if (strcmp($tabla[$i][1], 'dir') == 0) {
            
            $nExp = 0;
            $nDoc = 0;
    
            $auxNivel = $tabla[$i][2];
            
            $j = $i + 1;
            while ( ($j < count($tabla)) && ($tabla[$j][2] != $auxNivel) ) {
                if (strcmp($tabla[$j][1], 'file') == 0) {
                    $archi = $tabla[$j][0];
                    // echo "ARCHIVO = " . $archi . "<br>";
                    $dummy = explode('.', $archi);
                    // print_r($dummy);
                    if (strcasecmp($dummy[count($dummy) - 1], 'pdf') == 0) {
                        $nDoc = $nDoc + 1;
                        // echo "VALOR DE nDoc = " . $nDoc . "<br>";
                    }
                }
                $j = $j + 1;
            }
            
            $tabla[$i][4] = $nDoc;
        } else {
            // Para los restantes elementos no se ejecuta ninguna acciÓn        
        }
        
        $i = $i + 1;
    }
    // LA TABLA ESTÁ LISTA
}

function mostrar_tabla_expedientes() {
    
    global $tabla;
    global $rutaD;
    global $path;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
    				 // y tabla se debe preservar como es originalmente
    
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . $local[$f][0] . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else if ($c == 5) {
                $_link = trim(str_replace('c:\xampp\htdocs', '', $tabla[$f][$c]));
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . '<a href="' . $_link . '" target="_blank">' . $local[$f][$c] . '</a>';
            } else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<hr><span>DOCUMENTOS: ' . $path . $rutaD . '</span><hr><br>';
    $s = $s . '<table border="1">';
    $s .= '<tr><th>Archivo</th><th>Tipo</th><th>Nivel</th><th>Exp</th><th>Doc</th><th>LINK</th></tr>';
    foreach ( $local as $r ) {
            $s .= '<tr>';
            foreach ( $r as $v ) {
                    $s .= '<td>'.$v.'</td>';
            }
            $s .= '</tr>';
    }
    $s .= '</table><br><hr>';

    if ($depuracion) {
        echo $s;    
    }
}

function mostrar_tabla_expedientes_base() {
    
    global $tabla;
    global $rutaD;
    global $path;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
                     // y tabla se debe preservar como es originalmente
    
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . $local[$f][0] . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<hr><span>DOCUMENTOS: ' . $path . $rutaD . '</span><hr><br>';
    $s = $s . '<table border="1">';
    $s .= '<tr><th>Archivo</th><th>Tipo</th><th>Nivel</th><th>Exp</th><th>Doc</th><th>LINK</th></tr>';
    foreach ( $local as $r ) {
        $s .= '<tr>';
        foreach ( $r as $v ) {
            $s .= '<td>'.$v.'</td>';
        }
       $s .= '</tr>';
    }
    $s .= '</table><br><hr>';

    if ($depuracion) { 
        echo $s;
    }

}

function grabar_tabla_expedientes_base() {

    global $tabla;

    // AQUI SE DEBE GRABAR LA TABLA UTILIZANDO PDO
    // SE GRABA EN LA TABLA: tblExpedientes

    //////////////////////////////////////////////////////////////////
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
 
    $sql = "TRUNCATE TABLE tblexpedientesbase";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    try {
        for($i=0; $i<count($tabla); $i++){
            if ($tabla[$i][2] == 0) {
                $sql = "INSERT INTO tblexpedientesbase (archivo, tipo, nivel, expedientes, documentos, link) VALUES (:archivo, :tipo, :nivel, :expedientes, :documentos, :link)";
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':archivo', $tabla[$i][0], PDO::PARAM_STR);
                $stmt->bindParam(':tipo', $tabla[$i][1], PDO::PARAM_STR);
                $stmt->bindParam(':nivel', $tabla[$i][2], PDO::PARAM_INT);
                $stmt->bindParam(':expedientes', $tabla[$i][3], PDO::PARAM_INT);
                $stmt->bindParam(':documentos', $tabla[$i][4], PDO::PARAM_INT);
                $stmt->bindParam(':link', $tabla[$i][5], PDO::PARAM_STR);
                $stmt->execute();
            }
        }
        
    } catch (Exception $ex) {
      print_r($ex);
    }

    ///////////////////////////////////////////////////////////////////
}

function grabar_tabla_expedientes() {

    global $tabla;

    // AQUI SE DEBE GRABAR LA TABLA UTILIZANDO PDO
    // SE GRABA EN LA TABLA: tblExpedientes

    //////////////////////////////////////////////////////////////////
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
    
    try {
        for($i=0; $i<count($tabla); $i++){
            $sql = "INSERT INTO tblexpedientes (archivo, tipo, nivel, expedientes, documentos, link) VALUES (:archivo, :tipo, :nivel, :expedientes, :documentos, :link)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':archivo', $tabla[$i][0], PDO::PARAM_STR);
            $stmt->bindParam(':tipo', $tabla[$i][1], PDO::PARAM_STR);
            $stmt->bindParam(':nivel', $tabla[$i][2], PDO::PARAM_INT);
            $stmt->bindParam(':expedientes', $tabla[$i][3], PDO::PARAM_INT);
            $stmt->bindParam(':documentos', $tabla[$i][4], PDO::PARAM_INT);
            $stmt->bindParam(':link', $tabla[$i][5], PDO::PARAM_STR);
            $stmt->execute();
    }
        
    } catch (Exception $ex) {
      print_r($ex);
    }

    ///////////////////////////////////////////////////////////////////
}

function limpiar_tabla_expedientes() {
    
    //////////////////////////////////////////////////////////////////

    $depuracion = 0;
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
 
    $sql = "TRUNCATE TABLE tblexpedientes";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    if ($depuracion) {
        echo "LIMPIEZA DE TABLA EXPEDIENTES COMPLETADA<br>";
        echo "Proceso Base Completado<hr>";        
    }

}

function renderizar( ) {
    global $tabla, $respuesta;

    $paso = 0;

    $nivel = -1;

    $f = 0;
    while ($f < count($tabla)) {
          // El Nivel del elemento actual puede se de tres tipos distintos:
          // a) El Nivel es mayor al actual.
          // b) El Nivel es igual al actual. 
          // c) El Nivel es menor al actual. En este caso, debemos cerrar TODOS los niveles anteriores
          
          // Primer caso: a) Nuevo elemento, Nivel_nodo > Nivel_actual

          if ($tabla[$f][2] > $nivel) {
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '<li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';                    
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                    
            }
            $nivel = $nivel + 1;
          }
          
          // Segundo caso: b) Cierre de directorio, Nivel_nodo == Nivel_actual
          
          else if ($tabla[$f][2] == $nivel) {
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '</ul></li><li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';
                $nivel = $nivel + 1;
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                    
            }
          }
          
          // Tercer caso: c) Nivel_actual < Nivel_nodo
          // Cierre de TODOS los niveles hasta el Nivel del nuevo nodo.
          
          else {
             while ($nivel > $tabla[$f][2]) {
                 $respuesta = $respuesta . '</ul></li>';
                 $nivel = $nivel - 1;
            }
            $nivel = $tabla[$f][2];
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '<li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';                     
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                     
            }
          }
        $f = $f + 1;
    }
}

function buscar_numero($texto, $cad = '') {

    $mostrar_f = 0;

    $numero = '';

    $pos = 0;
    if (strcmp($cad, '') != 0) {
        $pos = stripos($texto, $cad);
    }

    $i = $pos;
    while (!is_numeric($texto[$i]) && $i < strlen($texto)) {
        $i = $i + 1;
    }

    if ($i == strlen($texto)) {
        return $numero; // No encontró un número
    }

    $k = $i;

    if ($mostrar_f) {
        echo "K = " . $k . "<br>I = " . $i . "<br>POS = " . $pos . "<br>";
        echo "LONGITUD DE TEXTO = " . strlen($texto) . "<br>";
    }

    while ($i < strlen($texto) && is_numeric($texto[$i]) ) {
        $i = $i + 1;   
    }
    $numero = mb_substr($texto, $k, $i - $k);
    return $numero;
}

?>