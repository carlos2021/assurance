<?php include "db_config_exp.php"; ?>
<?php include "configuracion.php"; ?>

<?php

define ("LOC_ARRIBA", 1);
define ("LOC_MITAD", 2);
define ("LOC_ABAJO", 3);

$anio = Array();
$anio[] = '2022'; $anio[] = '2021'; $anio[] = '2020'; $anio[] = '2019'; $anio[] = '2018';
$anio[] = '2017'; $anio[] = '2016'; $anio[] = '2015'; $anio[] = '2014'; $anio[] = '2013';
$anio[] = '2012'; $anio[] = '2011'; $anio[] = '2010'; $anio[] = '2009'; $anio[] = '2008';
$anio[] = '2007'; $anio[] = '2006'; $anio[] = '2005'; $anio[] = '2004'; $anio[] = '2003';
$anio[] = '2002'; $anio[] = '2001'; $anio[] = '2000';

$cuadernos = Array();
$cuadernos[] = "C1"; $cuadernos[] = "C2"; $cuadernos[] = "C3"; $cuadernos[] = "C4"; 
$cuadernos[] = "C5"; $cuadernos[] = "C6"; $cuadernos[] = "C7"; $cuadernos[] = "C8"; 
$cuadernos[] = "C9"; $cuadernos[] = "C10"; $cuadernos[] = "C11"; $cuadernos[] = "C12"; 
$cuadernos[] = "C13"; $cuadernos[] = "C14"; $cuadernos[] = "C15"; $cuadernos[] = "C16"; 
$cuadernos[] = "C17"; $cuadernos[] = "C18"; $cuadernos[] = "C19"; $cuadernos[] = "C20"; 
$cuadernos[] = "C21"; $cuadernos[] = "C22"; $cuadernos[] = "C23"; $cuadernos[] = "C24"; 
$cuadernos[] = "C25"; 
$cuadernos[] = "C01"; $cuadernos[] = "C02"; $cuadernos[] = "C03"; $cuadernos[] = "C04"; 
$cuadernos[] = "C05"; $cuadernos[] = "C06"; $cuadernos[] = "C07"; $cuadernos[] = "C08"; 
$cuadernos[] = "C09";

function comprimir_cadena_especial($text) {

    $text = " " . $text . " ";

    while (stripos($text, "  ")) {
        $text = str_replace("  ", " ", $text);
    }
    $texto = str_replace(" i ", " ", $text);
    $texto = str_replace(" I ", " ", $text);
    return $text;

    $text = trim($text);
}

function buscar_cuaderno($nombre, $direccion = 1) { // dirección = 0, comienzo - dirección = 1, final

    global $cuadernos;

    $nombre = "=" . $nombre;

    $i = 0;
    while ($i < count($cuadernos)) {
        if ($direccion == 0) {
            $pos = stripos($nombre, $cuadernos[$i]);
        }
        else {
            $pos = strripos($nombre, $cuadernos[$i]);
        }
        if ( $pos > 0) {
            return $cuadernos[$i]; 
        }
        $i = $i + 1;
    }

    return "";
}

function buscar_fecha_de_anexos($nombre, $posicion = 0) {

    global $anio;

    $arr = Array();

    $nombre = "=" . $nombre;

    $i = 0;
    while ($i < count($anio)) {
        $pos = stripos($nombre, $anio[$i], $posicion);
        if ($pos > 0) {
            $arr[] = $pos - 1;
            $arr[] = substr($nombre, $pos, 8);
            return $arr; 
        }
        $i = $i + 1;
    }

    $arr[] = -1;
    $arr[] = '';
    return $arr;
}

function buscar_folio($nombre, $posicion) {
    $pos = stripos($nombre, 'Folio');
    $elFolio = substr($nombre, $pos + 5, $posicion - ($pos + 5));
    return $elFolio;
}

function depurar_cadena($text) {
    $ori = array(" en ", " de ", " la ", " el ", " los ", " las ", " es ", " a ", " por ", " parte ", " del ", " y ", " o " , " cual ", " y/o ", " e ", " que ", " sobre ", " para ", " al ", " pone ", " se ", " no ", " sus " , " por ", " esta ", " este ", " caso ", "<br>");
    $rem = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
    $text = str_replace($ori, $rem, $text);
    return $text;
}

function buscar_cadena($texto, $dato) {
    $dato = trim($dato);
    $dato = str_replace("  ", " ", $dato);
    $dato = str_replace("  ", " ", $dato);

    $dummy = explode(" ", $dato);

    // PENDIENTE DE CONSTRUCCIÓN
}

function t ($s) {
    return utf8_decode($s);
}

function sp ($n) {
    $lin = '';
    $i = 0;
    while ($i < $n) {
        $lin = ' ' . $lin;
        $i = $i + 1;
    }
    return $lin;
}

function slugify($text)
{
    // Strip html tags
    $text=strip_tags($text);
    // Quitar ?, ", '
    $text = str_replace('$', '', $text);
    $text = str_replace('?', '', $text);
    $text = str_replace('"', '', $text);
    $text = str_replace('\'', '', $text);
    $text = str_replace('\r\n', '', $text);
    $text = str_replace('\r', '', $text);
    $text = str_replace('\n', '', $text);
    $text = str_replace('<br>', '', $text);
    // Replace non letter or digits by -
    // $text = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', ' ', $text)));
    // $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // Transliterate
    // setlocale(LC_ALL, 'es_ES.utf8');
    // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // Remove unwanted characters
    //$text = preg_replace('~[^-\w]+~', '', $text);
    // Trim
    $text = trim($text);
    // Remove duplicate -
    // $ext = preg_replace('~-+~', ' ', $text);
    // Lowercase
    $text = strtolower($text);
    // Check if it is empty
    return $text;
}

function espacios( $n ) {
    $i = 0;
    $aux = '';
    while ($i < $n * 8) {
        $aux = $aux . '&nbsp;';
        $i = $i + 1;
    }
    return $aux;
}

function es_letra($car) {
    if ( (ord($car) >= 65 && ord($car) <= 90) || (ord($car) >= 97 && ord($car) <= 122) )
        return true;
    else
        return false;
}

function buscar_mes($texto, $pos = 0) {
    $z = $pos;
    // DETECCIÓN DEL MES
    $m1 = stripos($texto, 'enero', $z);
    $m2 = stripos($texto, 'febrero', $z);
    $m3 = stripos($texto, 'marzo', $z);
    $m4 = stripos($texto, 'abril', $z);
    $m5 = stripos($texto, 'mayo', $z);
    $m6 = stripos($texto, 'junio', $z);
    $m7 = stripos($texto, 'julio', $z);
    $m8 = stripos($texto, 'agosto', $z);
    $m9 = stripos($texto, 'septiembre', $z);
    $m10 = stripos($texto, 'octubre', $z);
    $m11 = stripos($texto, 'noviembre', $z);
    $m12 = stripos($texto, 'diciembre', $z);
    $m13 = stripos($texto, 'Ñero', $z);

    // CONVERSIÓN DEL MES A VALOR NUMÉRICO
    $mes = '';
    if ($m1 > 0) { $mes = '01'; $posicion = $m1; $long = 6; }
    else if ($m2 > 0) { $mes = '02'; $posicion = $m2; $long = 8; }
    else if ($m3 > 0) { $mes = '03'; $posicion = $m3; $long = 6; }
    else if ($m4 > 0) { $mes = '04'; $posicion = $m4; $long = 6; }
    else if ($m5 > 0) { $mes = '05'; $posicion = $m5; $long = 5; }
    else if ($m6 > 0) { $mes = '06'; $posicion = $m6; $long = 6; }
    else if ($m7 > 0) { $mes = '07'; $posicion = $m7; $long = 6; }
    else if ($m8 > 0) { $mes = '08'; $posicion = $m8; $long = 7; }
    else if ($m9 > 0) { $mes = '09'; $posicion = $m9; $long = 11; }
    else if ($m10 > 0) { $mes = '10'; $posicion = $m10; $long = 8; }
    else if ($m11 > 0) { $mes = '11'; $posicion = $m11; $long = 10; }
    else if ($m12 > 0) { $mes = '12'; $posicion = $m12; $long = 10; }
    else if ($m13 > 0) { $mes = '01'; $posicion = $m1; $long = 6; }
    else { $mes = '00'; $posicion = 0; $long = 0;}

    $resp = Array();

    $resp[] = $mes;
    $resp[] = $posicion;
    $resp[] = $long;

    return $resp;
}

function buscar_el_numero($texto, $patron) {

    $pos = stripos($texto, $patron);
    $i = $pos + strlen($patron);

    while ($i < strlen($texto)) {
        if (is_numeric($texto[$i])) {
            break;
        }
        $i = $i + 1;
    }

    if ($i == strlen($texto) - 1) {
        return $texto[strlen($texto) - 1];
    }

    if ($i >= strlen($texto)) {
        return ""; // NO LO ENCONTRÓ
    }

    else {
        $resp = "";
        while ($i < strlen($texto)) {
            if (!is_numeric($texto[$i])) {
                break;
            }
            $resp = $resp . $texto[$i];
            $i = $i + 1;
        }

        return $resp;
    }
}

function valor_dia_en_letras($num_letras) {
    $dia = '';
    if (strcmp($num_letras, 'uno') == 0 || strcmp($num_letras, 'primero') == 0) $dia = '01';
    if (strcmp($num_letras, 'dos') == 0) $dia = '02';
    if (strcmp($num_letras, 'tres') == 0) $dia = '03';
    if (strcmp($num_letras, 'cuatro') == 0) $dia = '04';
    if (strcmp($num_letras, 'cinco') == 0) $dia = '05';
    if (strcmp($num_letras, 'seis') == 0) $dia = '06';
    if (strcmp($num_letras, 'siete') == 0) $dia = '07';
    if (strcmp($num_letras, 'ocho') == 0) $dia = '08';
    if (strcmp($num_letras, 'nueve') == 0) $dia = '09';
    if (strcmp($num_letras, 'diez') == 0) $dia = '10';
    if (strcmp($num_letras, 'once') == 0) $dia = '11';
    if (strcmp($num_letras, 'doce') == 0) $dia = '12';
    if (strcmp($num_letras, 'trece') == 0) $dia = '13';
    if (strcmp($num_letras, 'catorce') == 0) $dia = '14';
    if (strcmp($num_letras, 'quince') == 0) $dia = '15';
    if (strcmp($num_letras, 'dieciseis') == 0) $dia = '16';
    if (strcmp($num_letras, 'diecisiete') == 0) $dia = '17';
    if (strcmp($num_letras, 'dieciocho') == 0) $dia = '18';
    if (strcmp($num_letras, 'diecinueve') == 0) $dia = '19';
    if (strcmp($num_letras, 'veinte') == 0) $dia = '20';
    if (strcmp($num_letras, 'veintiuno') == 0) $dia = '21';
    if (strcmp($num_letras, 'veintidos') == 0) $dia = '22';
    if (strcmp($num_letras, 'veintitres') == 0) $dia = '23';
    if (strcmp($num_letras, 'veinticuatro') == 0) $dia = '24';
    if (strcmp($num_letras, 'veinticinco') == 0) $dia = '25';
    if (strcmp($num_letras, 'veintiseis') == 0) $dia = '26';
    if (strcmp($num_letras, 'veintisiete') == 0) $dia = '27';
    if (strcmp($num_letras, 'veintiocho') == 0) $dia = '28';
    if (strcmp($num_letras, 'veintinueve') == 0) $dia = '29';
    if (strcmp($num_letras, 'treinta') == 0) $dia = '30';
    if (strcmp($num_letras, 'treintaiuno') == 0) $dia = '31';
    if (strcmp($num_letras, 'treintayuno') == 0) $dia = '31';
    return $dia;
} 

function vacio($s) {
    if (strcmp($s, '') == 0)
        return true;
    else
        return false;
}

function encontrar_fecha($texto) {

    $mostrar = 0;
    $mostrar_2 = 0;

    $texto = "=" . $texto . "=";

    /////////////////////////////////////////////////////////////////////////
    // BÚSQUEDA ESTÁNDAR
    /////////////////////////////////////////////////////////////////////////

    $resultado = 'sin-procesar';
    $ciclo = 0;
    $maximo = strlen($texto);

    $pos = stripos($texto, 'fecha', 0);

    if ($pos == 0)
        $resultado = 'fecha-no-encontrada';
    else {
        if ($mostrar) {
            echo "POS búsqueda fecha = " . $pos . "<br>";
        }
        while ($pos < $maximo) {
            $ciclo = $ciclo + 1;
            if ($ciclo > 20) {
                echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO (1101) !!! <hr><br>";
                exit(1);
            }
            if ($mostrar) 
                echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";
            if ($mostrar) {
                echo "TEXTO ENVIADO A: encontrar_fecha_dia_mes_anio: " . $texto . "<br><br>";
                echo "POS ENVIADO: " . $pos . "<br><br>";
            }
            $fecha = encontrar_fecha_dia_mes_anio($texto, $pos);
            if ($mostrar)
                echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
            if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                $resultado = $fecha;
                break;
            }
            else {
                if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                    $resultado = $fecha;
                    break;
                }
                if ($mostrar)
                    echo "POS de la fecha no valida = " . $pos . "<br>";
                $pos = stripos($texto, 'fecha', $pos + 1);
                if ($mostrar)
                    echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                if ($pos == 0) {
                    $resultado = $fecha;
                    break; 
                }
            }
        }
    }

    // BÚSQUEDA POR MES: mes - día - año
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

        // BÚSQUEDA ESTÁNDAR
        $ciclo = 0;
        $maximo = strlen($texto);
        $un_mes = buscar_mes($texto, 0);

        if ($mostrar) {
            echo "<br>MES ENCONTRADO (versión NO estándar): <br>"; 
            echo "MES = " . $un_mes[0] . " POSICIÓN = " . $un_mes[1] . " LONGITUD = " . $un_mes[2] . "<br>";
        }

        $mes = $un_mes[0];
        $pos = $un_mes[1];
        $long = $un_mes[2];

        if ($pos == 0)
            $resultado = 'fecha-no-encontrada';
        else {
            while ($pos < $maximo) {
                $ciclo = $ciclo + 1;
                if ($ciclo > 20) {
                    echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO !!! <hr><br>";
                    exit(1);
                }
                if ($mostrar) 
                    echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";
                $fecha = encontrar_fecha_por_mes($texto, $mes, $pos, $long);
                if ($mostrar)
                    echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
                if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                    $resultado = $fecha;
                    break;
                }
                else {
                    if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                        $resultado = $fecha;
                        break;
                    }
                    if ($mostrar)
                        echo "POS de la fecha no valida = " . $pos . "<br>";
                    $pos = stripos($texto, 'fecha', $pos + 1);
                    if ($mostrar)
                        echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                    if ($pos == 0) {
                        $resultado = $fecha;
                        break; 
                    }
                }
            }            
        }
    }

    // BÚSQUEDA POR MES: antecedido por día - luego mes - luego año
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

        // BÚSQUEDA ESTÁNDAR
        $ciclo = 0;
        $maximo = strlen($texto);
        $un_mes = buscar_mes($texto, 0);

        if ($mostrar) {
            echo "<br>MES ENCONTRADO (versión NO estándar) (antecedido por el día): <br>"; 
            echo "MES = " . $un_mes[0] . " POSICIÓN = " . $un_mes[1] . " LONGITUD = " . $un_mes[2] . "<br>";
        }

        $mes = $un_mes[0];
        $pos = $un_mes[1];
        $long = $un_mes[2];

        if ($pos == 0)
            $resultado = 'fecha-no-encontrada';
        else {
            while ($pos < $maximo) {
                $ciclo = $ciclo + 1;
                if ($ciclo > 20) {
                    echo "<br><hr>ENCONTRAMOS UN CICLO INFINITO !!! <hr><br>";
                    exit(1);
                }
                if ($mostrar) 
                    echo "ESTAMOS EN EL CICLO. VALOR DE POS = " . $pos . "<br>";

                $fecha = encontrar_fecha_por_mes_antecedido_dia($texto, $mes, $pos, $long);

                if ($mostrar)
                    echo "FECHA DEVUELTA POR la función que hace la tarea = " . $fecha . "<br>";
                if (strcmp($fecha, 'fecha-no-valida') != 0 && strcmp($fecha, 'fecha-no-encontrada') != 0) {
                    $resultado = $fecha;
                    break;
                }
                else {
                    if (strcmp($fecha, 'fecha-no-encontrada') == 0) {
                        $resultado = $fecha;
                        break;
                    }
                    if ($mostrar)
                        echo "POS de la fecha no valida = " . $pos . "<br>";
                    $pos = stripos($texto, 'fecha', $pos + 1);
                    if ($mostrar)
                        echo "POS encontrado para siguiente fecha = " . $pos . "<br>";
                    if ($pos == 0) {
                        $resultado = $fecha;
                        break; 
                    }
                }
            }            
        }
    }

    // BÚSQUEDA POR SLASH
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {
        $pos = stripos($texto, '/');
        if ($pos > 0) {
            while ($pos < strlen($texto)) {

                if ($mostrar_2) {
                    echo "POS = " . $pos . "<br>";
                }

                //////////////////////////////
                $n1 = '';
                $z = $pos - 1;
                if ($mostrar_2) {
                    echo "z = " . $z . "<br>";
                }
                while($z > 0) {
                    if (!is_numeric($texto[$z])) {
                        break;
                    }
                    else {
                        $n1 = $texto[$z] . $n1;
                        if ($mostrar_2) {
                            echo "NUEVO VALOR DE n1 = " . $n1 . "<br>";
                        } 
                    }
                    $z = $z - 1;
                }
                //////////////////////////////
                $n2 = '';
                if (!vacio($n1)) {
                    $z = $pos + 1;
                    while($z < strlen($texto)) {
                        if (!is_numeric($texto[$z]))
                            break;
                        else
                            $n2 = $n2 . $texto[$z]; 
                        $z = $z + 1;
                    }                   
                }
                //////////////////////////////
                $n3 = '';
                if (!vacio($n2)) {
                    if (strcmp($texto[$z], '/') == 0) {
                        $t = $z + 1;
                        while ($t < strlen($texto)) {
                            if (!is_numeric($texto[$t]))
                                break;
                            else
                                $n3 = $n3 . $texto[$t];
                            $t = $t + 1;
                        }
                    }
                }
                //////////////////////////////
                $vn1 = intval($n1);
                $vn2 = intval($n2);
                $vn3 = intval($n3);

                $anio = 0;
                $mes = 0;
                $dia = 0;

                if ($mostrar_2) {
                    echo "n1 = " . $n1 . "<br>";
                    echo "n2 = " . $n2 . "<br>";
                    echo "n3 = " . $n3 . "<br>";
                }

                if ($vn1 > 1900) {
                    $anio = $n1;
                    if ($vn2 > 12 && $vn2 < 32) {
                        $mes = $vn2;
                        $dia = $vn3;
                    }
                    if ($vn3 > 12 && $vn3 < 32) {
                        $mes = $vn3;
                        $dia = $vn2;
                    }
                    if ($mes == 0) {
                        $mes = $vn2;
                        $dia = $vn3;
                    }
                }
                else if ($vn2 > 1900) {
                    $anio = $n2;
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn3;
                    }
                    if ($vn3 > 12 && $vn3 < 32) {
                        $mes = $vn3;
                        $dia = $vn1;
                    }
                    if ($mes == 0) {
                        $mes = $vn3;
                        $dia = $vn1;
                    }
                }
                else if ($vn3 > 1900) {
                    $anio = $n3;
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn2;
                    }
                    if ($vn2 > 12 && $vn2 < 32) {
                        $mes = $vn2;
                        $dia = $vn1;
                    }
                    if ($mes == 0) {
                        $mes = $vn2;
                        $dia = $vn1;
                    }
                }

                if ($anio == 0) {
                    if ($vn1 > 12 && $vn1 < 32) {
                        $mes = $vn1;
                        $dia = $vn2;
                        $anio = $vn3;
                    }
                    else if ($vn2 > 12 && $vn2 < 32) {
                        $dia = $vn1;
                        $mes = $vn2;
                        $anio = $vn3;
                    }
                    else if ($vn3 > 12 && $vn3 < 32) {
                        $anio = $vn1;
                        $dia = $vn2;
                        $mes = $vn3;
                    }
                }

                if ($anio == 0) {
                    $dia = $vn1;
                    $mes = $vn2;
                    $anio = $vn3;
                }
                //////////////////////////////
                if (($dia > 0 && $dia < 32) && 
                    ($mes > 0 && $mes < 13) &&
                    ($anio > 1900 && $anio < 2025)) {
                    // SE CONSTRUYE LA FECHA
                    if ($anio < 25) {
                        $anio = $anio + 2000;
                    }

                    $_anio = strval($anio);
                    $_mes = strval($mes);
                    $_dia = strval($dia);

                    while (strlen($_anio) < 4) {
                        $_anio = '0' . $_anio;
                    }

                    while (strlen($_mes) < 2) {
                        $_mes = '0' . $_mes;
                    }

                    while (strlen($_dia) < 2) {
                        $_dia = '0' . $_dia;
                    }

                    $aux = $_anio . "/" . $_mes. "/" . $_dia;

                    // SE CONVIERTE A FORMATO UNIX
                    $d = strtotime($aux);

                    // SE CONVIERTE A FORMATO FECHA PHP
                    $fecha = date('Y-m-d', $d);

                    // SE RETORNA EL VALOR DE FECHA CALCULADO
                    return $fecha;
                    //////////////////////////////
                }
                else {
                    $posicion = $pos + 12;
                    if ($posicion > strlen($texto)){
                        break;
                    }
                    $pos = stripos($texto, '/', $posicion); 
                    if ($pos == 0) {
                        break;
                    }             
                } 
            }           
        }
    }

    // BÚSQUEDA POR GUIONES
    if (strcmp($resultado, 'fecha-no-valida') == 0 || strcmp($resultado, 'fecha-no-encontrada') == 0) {

    }

    return $resultado;
}

function encontrar_fecha_dia_mes_anio($texto, $pos = 0) {

    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;

        if ($maximo > strlen($texto)) {
            $maximo = strlen($maximo) - 1;
        }

        $maximo = $maximo - 1;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos + 5;
        if ($k > strlen($texto)) {
            $k = strlen($texto) - 1;
        }
        $k = $k - 1;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL MES
        $m1 = stripos($texto, 'enero', $z);
        $m2 = stripos($texto, 'febrero', $z);
        $m3 = stripos($texto, 'marzo', $z);
        $m4 = stripos($texto, 'abril', $z);
        $m5 = stripos($texto, 'mayo', $z);
        $m6 = stripos($texto, 'junio', $z);
        $m7 = stripos($texto, 'julio', $z);
        $m8 = stripos($texto, 'agosto', $z);
        $m9 = stripos($texto, 'septiembre', $z);
        $m10 = stripos($texto, 'octubre', $z);
        $m11 = stripos($texto, 'noviembre', $z);
        $m12 = stripos($texto, 'diciembre', $z);
        $m13 = stripos($texto, 'Ñero', $z);

        // CONVERSIÓN DEL MES A VALOR NUMÉRICO
        $mes = '01';
        if ($m1 > 0) $mes = '01';
        else if ($m2 > 0) $mes = '02';
        else if ($m3 > 0) $mes = '03';
        else if ($m4 > 0) $mes = '04';
        else if ($m5 > 0) $mes = '05';
        else if ($m6 > 0) $mes = '06';
        else if ($m7 > 0) $mes = '07';
        else if ($m8 > 0) $mes = '08';
        else if ($m9 > 0) $mes = '09';
        else if ($m10 > 0) $mes = '10';
        else if ($m11 > 0) $mes = '11';
        else if ($m12 > 0) $mes = '12';
        else if ($m13 > 0) $mes = '01';
        else {
            if ($mostrar) echo "<br>(5) - ESTA SALIENDO PUES mes es erróneo -- <br>";
            return $fecha_invalida;           
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_mes($texto, $mes, $pos, $long) {
    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;
        if ($maximo > strlen($texto)) {
            $maximo = strlen($texto) - 1;
        }

        $maximo = $maximo - 1;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos + $long;
        if ($k > strlen($texto)) {
            $k = $k - 1;
        }

        $k = $k - 1;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $dia = '';
        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_mes_antecedido_dia($texto, $mes, $pos, $long) {
    // VARIABLE PARA DEPURACIÓN
    $mostrar = 0;

    // SE DEFINE LA CONSTANTE: fecha-no-valida
    // ESTA CONSTANTE SE UTILIZARÁ PARA INFORMAR A QUIEN LLAMA A ESTA FUNCIÓN
    // QUE LA fecha NO SE ENCUENTRA DENTRO DEL TEXTO RECIBIDO
    $fecha_invalida = 'fecha-no-valida';
    $fecha_no_encontrada = 'fecha-no-encontrada';

    // SE ENCUENTRA LA POSICIÓN EN DONDE ESTÁ LA PALABRA fecha
    // SE UTILIZA LA VARIABLE $inicio, PARA LOGRAR QUE ESTA FUNCIÓN PERMITA RECORRER
    // EN UN CICLO DE LLAMADAS TODO EL CONJUNTO DEL TEXTO
    // $pos = stripos($texto, 'fecha', $inicio);

    if ($pos > 0) {

        if ($mostrar) {
            echo "HEMOS ENTRADADO EN LA FUNCIÓN QUE HACE EL TRABAJO, al ciclo donde pos > 0" . "<br>";
        }

        // SI LA POSICIÓN EN DONDE ESTÁ FECHA ES MAYOR A CERO

        // SE ELIGE LA MÁXIMA POSICIÓN HASTA DONDE SE BUSCARÁ LA FECHA
        // SE TOMA UN VALOR DE 30, PUES ES UN VALOR QUE DEBE CONTENER CUALQUIER OCURRENCIA DE FECHA
        $maximo = $pos + 60;

        if ($mostrar) {
            echo "<br>POS = " . $pos . "<br>";
            echo "MÁXIMO = " . $maximo . "<br>";
            echo "LONGITUD = " . strlen($texto) . "<br>";
        }

        // SI EL VALOR MÁXIMO SOBREPASA LA LONGITUD DEL TEXTO RECIBIDO, ENTONCES
        // NO SERÁ FACTIBLE ENCONTRAR LA FECHA (EN CASI TODOS LOS CASOS)
        if ($maximo > strlen($texto)) {
            if ($mostrar) echo "<br>(1) - ESTA SALIENDO PUES maximo > longitud texto -- " . $maximo . " > " . strlen($texto) . "<br>";
            return $fecha_invalida;
        }

        // SE SALTA 5 LUGARES A LA DERECHA DE pos. ESTO GARANTIZA QUE SE EMPIEZA A EVALUAR
        // LA FECHA DESPUÉS DE LA CADENA: fecha
        $k = $pos - 10;

        // SE BUSCA EL INICIO DE UN NÚMERO (usualmente el día)
        // SE EVALÚA SI ES UN NÚMERO EN LETRAS

        $dia = '';
        $num_letras = '';
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            if (es_letra($texto[$k])) {
                $num_letras = $num_letras . $texto[$k];               
            }
            else {
                $dia = valor_dia_en_letras($num_letras);
                if (strcmp($dia, '') != 0) 
                    break;
                $num_letras = '';
            }
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(2) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        $z = $k;

        if (strcmp($dia, '') == 0) {
            // DIA ENCONTRADO. SE EVALÚA SU VALOR
            while (is_numeric($texto[$z]) && $z < $maximo) {
                $z = $z + 1;
            }

            if ($z >= $maximo) {
                if ($mostrar) echo "<br>(3) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
                return $fecha_invalida;
            }

            // SE CALCULA EL VALOR DEL DÍA
            $dia = substr($texto, $k, $z - $k);
        }

        // SE VERIFICA SI ESTÁ EN EL RANGO CORRECTO
        $dummy = intval($dia);

        if ( !($dummy >= 1 && $dummy <= 31) )  {
            if ($mostrar) echo "<br>(4) - ESTA SALIENDO PUES día no está en rango de 1 - 31 -- " . $dummy . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO
        $k = $z;
        while (!is_numeric($texto[$k]) && $k < $maximo) {
            $k = $k + 1;
        }

        if ($k >= $maximo) {
            if ($mostrar) echo "<br>(6) - ESTA SALIENDO PUES k >= maximo -- " . $k . ">= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // AÑO NUMERICO
        $z = $k;
        while (is_numeric($texto[$z]) && $z < $maximo) {
            $z = $z + 1;
        }

        if ($z >= $maximo) {
            if ($mostrar) echo "<br>(7) - ESTA SALIENDO PUES z >= maximo -- " . $z . " >= " . $maximo . "<br>";
            return $fecha_invalida;
        }

        // DETECCIÓN DEL AÑO (cadena)
        $anio = substr($texto, $k, $z - $k);

        $dummy = intval($anio);
        if ($dummy < 30) {
            $dummy = $dummy + 2000;
            $anio = strval($dummy);
        }
        else if ($dummy < 1950) {
            return $fecha_no_encontrada;
        }

        if ($mostrar) {
            echo "<br>AÑO ENCONTRADO = " . $anio . " MES = " . $mes . " DÍA = " . $dia . "<br>";
        }

        // SE CONSTRUYE LA FECHA
        $aux = $anio . "/" . $mes. "/" . $dia;

        // SE CONVIERTE A FORMATO UNIX
        $d = strtotime($aux);

        // SE CONVIERTE A FORMATO FECHA PHP
        $fecha = date('Y-m-d', $d);

        // SE RETORNA EL VALOR DE FECHA CALCULADO
        return $fecha;
    }
    else {
        return $fecha_no_encontrada;
    }
}

function encontrar_fecha_por_slash($texto, $pos = 0) {

}

function encontrar_fecha_por_guion($texto, $pos = 0) {

}

/* function ajustar_nombres_ruta($ruta, $nivel) {


   echo "DENTRO DE LA FUNCIÓN <br><br>";
   // abre un directorio y lo lista recursivamente
   if (is_dir($ruta)) {

      if ($dh = opendir($ruta)) {
         while (($file = readdir($dh)) !== false) {
            // Encuentra el directorio o un archivo
            // Verifica inicialmente que el directorio no se corresponda con un punto de retorno al directorio padre
            // o con el directorio punto, el cual es el punto de entrada al directorio
            if ($file != "." && $file != "..") {

                if (strcasecmp(filetype($ruta.$file), 'file') == 0) {
                    // ES UN ARCHIVO
                    // c:\xampp\htdocs\assurance\expedientes\PEX01\13001600112920110293600\C01.pdf
                    $extension = pathinfo($ruta.$file, PATHINFO_EXTENSION); // pdf
                    $basename = pathinfo($ruta.$file, PATHINFO_BASENAME); // C01.pdf
                    $filename = pathinfo($ruta.$file, PATHINFO_FILENAME); // C01
                    $dirname = pathinfo($ruta.$file, PATHINFO_DIRNAME); // c:\xampp\htdocs\assurance\expedientes\PEX01\13001600112920110293600

                    $final = substr($dirname, strlen($dirname) - 11);

                    echo "<br>DATOS EXTRAÍDOS DEL DIRECTORIO <br>";
                    echo "RUTA = " . $ruta . "<br>";
                    echo "FILE = " . $file . "<br>";
                    $extension = pathinfo($ruta.$file, PATHINFO_EXTENSION);
                    $basename = pathinfo($ruta.$file, PATHINFO_BASENAME);
                    $filename = pathinfo($ruta.$file, PATHINFO_FILENAME);
                    $dirname = pathinfo($ruta.$file, PATHINFO_DIRNAME);
                    echo "EXTENSION = " . $extension . "<br>";
                    echo "BASENAME = " . $basename . "<br>";               
                    echo "FILENAME = " . $filename . "<br>";                
                    echo "DIRNAME = " . $dirname . "<br>";
                    echo "TIPO DEL ARCHIVO = " . filetype($ruta.$file) . "<br>";
                    echo "LINK = " . $ruta.$file . "<br>";

                    echo "FINAL = " . $final . "<br><br>";

                    $parte_nombre = substr($file, 0, 2);

                    if (strcasecmp($parte_nombre, "C0") == 0) {
                       rename($ruta.$file, $ruta."Proceso".$final.$file); 
                       $file = $final.$file;  
                       echo "NUEVO FILE = " . $file . "<br>";                     
                    }
                }

                // SE PROCESA EL ARCHIVO, ya sea un directorio o un documento
                // Se agregan sus características a la tabla PHP
                
                // Las caracteríticas que se agregan a la tabla son las siguientes:
                
                // (nombre del archivo o directorio), (tipo del archivo), (nivel de profundidad)
                // Se agregan dos campos con valor cero, que posteriormete serán modificados
                // Estos se corresponden con el total de expedientes y archivos (diferentes a directorios) que
                // dependen del nodo actual
            }
            if (is_dir($ruta . $file) && $file!="." && $file!=".."){
               // Estamos aqu¨ª si el archivo es un directorio, distinto de "." y ".."
               // EL PROCESO CONTIN0‰3A DE MANERA RECURSIVA

               listar_directorios_ruta($ruta . $file . "\\", $nivel + 1);
            }
         }

      closedir($dh);
      }
   } else {
        // La ruta no es valida

   }    
} */

function nro_expedientes($ruta) {
    global $nroExpedientes;
    if (is_dir($ruta)) {
        if ($dh = opendir($ruta)) {
            while (($file = readdir($dh)) !== false) {
                if ($file != "." && $file != "..") {
                    if (filetype($ruta . $file) == 'dir') {
                        $nroExpedientes = $nroExpedientes + 1;
                    } 
                }
                if (is_dir($ruta . $file) && $file != "." && $file != "..") {
                    nro_expedientes($ruta . $file . "\\");
                }                
            }
        }
    }
    else {
        $nroExpedientes = 0;
    }
}

function listar_directorios_ruta($ruta, $nivel){
    
   global $tabla;

   $depuracion = 0;

   if ($depuracion) {
    echo "RUTA = " . $ruta . "<br>";
   }

   // abre un directorio y lo lista recursivamente
   if (is_dir($ruta)) {

      if ($dh = opendir($ruta)) {
         while (($file = readdir($dh)) !== false) {
            // Encuentra el directorio o un archivo
            // Verifica inicialmente que el directorio no se corresponda con un punto de retorno al directorio padre
            // o con el directorio punto, el cual es el punto de entrada al directorio
            if ($file != "." && $file != "..") {

                // SE PROCESA EL ARCHIVO, ya sea un directorio o un documento
                // Se agregan sus características a la tabla PHP
                
                // Las caracteríticas que se agregan a la tabla son las siguientes:
                
                // (nombre del archivo o directorio), (tipo del archivo), (nivel de profundidad)
                // Se agregan dos campos con valor cero, que posteriormete serán modificados
                // Estos se corresponden con el total de expedientes y archivos (diferentes a directorios) que
                // dependen del nodo actual
                $arr = Array();

                $arr[] = $file; // 0 = archivo (file)
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                if ($ext == '') {
                    $arr[] = 'dir';
                } else {
                    $arr[] = filetype($ruta.$file); // 1 = tipo del archivo                    
                }

                $arr[] = $nivel; // 2 = nivel
                $arr[] = 0; // 3 = número de expedientes
                $arr[] = 0; // 4 = número de documentos
                $arr[] = $ruta.$file; // 5 = LINK

                /* 
                    indice 0 = nombre del archivo
                    índice 1 = tipo del archivo (dir, file)
                    índice 2 = nivel
                    índice 3 = número de expedientes
                    índice 4 = número de documentos
                    índice 5 = LINK
                */

                $tabla[] = $arr;
            }
            if (is_dir($ruta . $file) && $file!="." && $file!=".."){
               // Estamos aqu¨ª si el archivo es un directorio, distinto de "." y ".."
               // EL PROCESO CONTIN0‰3A DE MANERA RECURSIVA

               listar_directorios_ruta($ruta . $file . "\\", $nivel + 1);
            }
         }

      closedir($dh);
      }
   } else {
        // La ruta no es valida
        $tabla = null;         
   }
}

function compactar_archivos($ruta) {

    $depuracion = 0;

    global $tabla;
    global $rutaD;
    global $path;
    global $pdo;

    $path = RAIZ;

    /* 
        indice 0 = nombre del archivo
        índice 1 = tipo del archivo (dir, file)
        índice 2 = nivel
        índice 3 = número de expedientes
        índice 4 = número de documentos
        índice 5 = LINK
    */

    global $tabla;
    global $archivos;

    // Se busca el nivel más alto
    $nivel_max = 0;

    $i = 0;
    while ($i < count($tabla)) {
        if ($tabla[$i][2] > $nivel_max) {
            $nivel_max = $tabla[$i][2];
        }
        $i = $i + 1;
    }

    // Se evalúa desde el nivel 1 hasta el nivel más alto
    // Para cada nivel se genera una lista de archivos por expediente (o por carpeta)
    // Se procesa cada lista de archivos. Se busca el documento origen
    // Se preserva el nombre del documento origen

    $lista = Array();

    $i = 0;
    while ($i <= $nivel_max) {
        $directorios = Array();

        if ($depuracion) {
            echo "PASADA CUANDO i = " . $i . "<br>";
            echo "LONGITUD TABLA = " . count($tabla) . "<br>";
        }

        $k = 0;
        while ($k < count($tabla)) {
            if ($tabla[$k][2] == $i && strcasecmp($tabla[$k][1], 'dir') == 0) {
                $directorios[] = $k;
            }
            $k = $k + 1;
        }

        $directorios[] = count($tabla);

        if ($depuracion) {
            echo "SE MUESTRA EL 'directorio'" . "<br><br>";
        }

        $tt = 0;
        while ($tt < count($directorios)) {

            if ($depuracion) {
                echo $directorios[$tt] . " - ";
            }

            $tt = $tt + 1;
        }

        if ($depuracion) {
            echo "<br><br>";
            echo "LONGITUD DEL DIRECTORIO = " . count($directorios) . "<br>";
        }

        $z = 0;
        while ($z < count($directorios) - 1) {

            if ($depuracion) {
                echo "VALOR z = " . $z . " VALOR LIMITE (count directorios) = " . count($directorios) . "<br>";
            }

            $nivel = $tabla[$directorios[$z]][2];
            $w1 = $directorios[$z];
            if ($z == count($directorios) - 1) {
                $w2 = $w1;
            }
            else {
                $w2 = $directorios[$z + 1];
            }

            $arreglo = Array();
            $nn = $w1;

            if ($depuracion) {
                echo "<br><br>";
            }

            while ($nn < $w2) {
                $archi = $tabla[$nn][0];
                $dummy = explode(".", $archi);
                $ext = $dummy[count($dummy) - 1];
                if (strcasecmp($ext, "pdf") == 0 && $tabla[$nn][2] == $nivel + 1) {

                    if ($depuracion) {
                        echo $nn . " -- ARCHIVO = " . $tabla[$nn][0] . "<br>";
                    }

                    $arreglo[] = $tabla[$nn][5];
                }
                $nn = $nn + 1;
            }

            $lista[] = $arreglo;

            $z = $z + 1;
        }     
        $i = $i + 1;
    }

    // Se imprime la lista para validar el trabajo

    if ($depuracion) {
        echo "ARCHIVO DE COMPACTACIÓN: " . "<br><br>";
    }

    $fila = 0;

    if ($depuracion) {
        echo count($lista) . " Longitud de lista" . "<br>";
    }

    while ($fila < count($lista)) {
        $columna = 0;
        while ($columna < count($lista[$fila])) {

            if ($depuracion) {
                echo "LISTA [" . $fila . ", " . $columna ."] = " . $lista[$fila][$columna] . "<br>";
            }
            
            $columna = $columna + 1;
        }

        if ($depuracion) { 
            echo "<br>";
        }

        $fila = $fila + 1;
    }

    $dummy = explode("\\", $lista[0][0]);
    $expediente = $dummy[count($dummy) - 2];

    if ($depuracion) {
        echo "LONGITUD LISTA = " . count($lista[0]) . "<br>";
        echo "<br><br><br>EL EXPEDIENTE =====================>>>>>> " . $expediente . "<br><br>";
    }

    try {

        $sql = "UPDATE tblportada SET nro_carpetas = " . count($lista[0]) . ", nro_carpetas_digital = 3  WHERE id = " . $expediente;
        $query = $pdo->prepare($sql);
        $query->execute();
    }
    catch (PDOException $ex) {
        print_r($ex);
    } // TRY de tabla páginas  

    // Se elige el nombre clave del paquete de archivos. Se utiliza el que tenga el nombre más corto

    if ($depuracion) {
        echo "<br>";
    }

    $fila = 0;
    while ($fila < count($lista)) {
        $columna_elegida = -1;
        $min_len = 0;
        $columna = 0;
        while ($columna < count($lista[$fila])) {
            $pos1 = stripos($lista[$fila][$columna], "C01");
            $pos2 = stripos($lista[$fila][$columna], "C1");
            if ($columna_elegida == -1 && $pos1 > 0) {
                $columna_elegida = $columna;
            }
            if ($columna_elegida == -1 && $pos2 > 0) {
                $columna_elegida = $columna;
            }
            if (strlen($lista[$fila][$columna]) < strlen($lista[$fila][$min_len])) {
                $min_len = $columna;
            }
            $columna = $columna + 1;
        }
        if ($columna_elegida == -1) {
            $columna_elegida = $min_len;
        }

        if ($depuracion) {
            echo "FILA ACTIVA = " . $fila . " COLUMNA SELECCIONADA = " . $columna_elegida . " LINK = " . $lista[$fila][$columna_elegida] . "<br>";
        }

        $instruccion_1 = "pdftk ";

        $archivos = $lista[$fila][$columna_elegida] . " ";

        if ($depuracion) {
            echo "<br>PROCESO PREVIO A LA GRABACIÓN<br><br>";
            echo "ARCHIVOS = " . $archivos . "<br>";
        }

        if ($depuracion) {
            echo "SE IMPRIME LA FILA, PARA LUEGO EJERCER CONTROL SOBRE LAS COLUMNAS<br><br>";

            echo "FILA = " . $fila . "<br>";
            $colu = 0;
            while ($colu < count($lista[$fila])) {
                if ($colu == $columna_elegida) echo ">>>>> ";
                echo "Columna = " . $colu . " LINK = " . $lista[$fila][$colu] . "<br>";
                $colu = $colu + 1;
            }
        }

        // SE RECUERDA CUALES SON LAS COLUMNAS QUE SE AGREGARON

        $recuerdo = Array();

        $cc = 0;
        while ($cc < count($lista[$fila])) {
            if ($cc != $columna_elegida) {

                $origen = $lista[$fila][$cc];
                $destino = str_replace('.pdf', '.txt', $origen);
                $instru = "pdftotext -f 1 -l 1 " . $origen . " " . $destino;
                if ($depuracion) {
                    echo "EN LA COLUMNA = " . $cc . "<br>";
                    echo "INSTRUCCIÓN DE EXTRACCIÓN DE CONTENIDO:<br>" . $instru . "<br><br>";
                } 
                exec($instru, $salida, $retorno); 
                $contenido = file_get_contents($destino);
                $contenido = " " . $contenido . " ";

                $contenido = slugify($contenido);

                $contenido = str_replace('Á', 'á', $contenido);
                $contenido = str_replace('É', 'é', $contenido);
                $contenido = str_replace('Í', 'í', $contenido);
                $contenido = str_replace('Ó', 'ó', $contenido);
                $contenido = str_replace('Ú', 'ú', $contenido);
                $contenido = strtolower($contenido);
                $contenido = str_replace('.', ' ', $contenido);
                $contenido = str_replace(',', ' ', $contenido);
                $contenido = str_replace('/', ' ', $contenido);
                $contenido = str_replace('\\', ' ', $contenido);
                $contenido = str_replace('!', ' ', $contenido);
                $contenido = str_replace('¡', ' ', $contenido);
                $contenido = str_replace('*', ' ', $contenido);
                $contenido = str_replace('^', '', $contenido); 
                $contenido = str_replace('«', '', $contenido); 
                $contenido = str_replace('(', ' ', $contenido); 
                $contenido = str_replace(')', ' ', $contenido); 
                $contenido = str_replace('<', ' ', $contenido); 
                $contenido = str_replace('>', ' ', $contenido);
                $contenido = str_replace('«', ' ', $contenido); 
                $contenido = str_replace('|', ' ', $contenido);
                $contenido = str_replace('i«', ' ', $contenido);  
 
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);

                $contenido = utf8_encode($contenido);              

                if ($depuracion) {
                    echo "CONTENIDO ====> " . $contenido . "<br><br>";
                }

                // CRITERIOS
                $criterio_11 = 'COPIAS';
                $criterio_12 = '(COPIAS)';

                $criterio_21 = 'medidas cautelares';
                $criterio_22 = 'medida cautelar';
                $criterio_23 = 'EMBARGO SECUESTRO';
                $criterio_24 = 'EMBARGO Y SECUESTRO';

                $criterio_31 = 'SEGUNDA INSTANCIA';
                $criterio_32 = 'APELACIÓN';
                $criterio_33 = 'APELACION';
                $criterio_34 = 'EJECUTIVOS EN APELACIÓN';
                $criterio_35 = 'EJECUTIVOS EN APELACION';
                $criterio_36 = 'TRIBUNAL SUPERIOR';
                $criterio_37 = 'TRIBUNAL';

                $pos_11 = stripos($contenido, $criterio_11);
                $pos_12 = stripos($contenido, $criterio_12);
                $pos_21 = stripos($contenido, $criterio_21);
                $pos_22 = stripos($contenido, $criterio_22);
                $pos_23 = stripos($contenido, $criterio_23);
                $pos_24 = stripos($contenido, $criterio_24);
                $pos_31 = stripos($contenido, $criterio_31);
                $pos_32 = stripos($contenido, $criterio_32);
                $pos_33 = stripos($contenido, $criterio_33);
                $pos_34 = stripos($contenido, $criterio_34);
                $pos_35 = stripos($contenido, $criterio_35);
                $pos_36 = stripos($contenido, $criterio_36);
                $pos_37 = stripos($contenido, $criterio_37);

                // ESTE MODELO DEBE SER MEJOR ANALIZADO. POR AHORA, SE COMPRIMEN TODOS 
                // LOS ARCHIVOS EN UNO SOLO

                //////////////////////////////////////////////////////////////////////


                //         MEJORAR ELECCIÓN DE NO COMPRIMIDOS !!!


                //////////////////////////////////////////////////////////////////////

                $archivos = $archivos . $lista[$fila][$cc] . " ";               

                /*

                if ($pos_11 > 0 || $pos_12 > 0 || $pos_21 > 0 || $pos_22 > 0 || $pos_23 > 0 || $pos_24 > 0 || $pos_31 > 0 || $pos_32 > 0 || $pos_33 > 0 || $pos_34 > 9 || $pos_35 > 0 || $pos_36 > 0 || $pos_37 > 0) {

                    $recuerdo[] = $cc;

                    if ($depuracion) {
                        echo "ARCHIVO NO INCUIDO = " . $lista[$fila][$cc] . " EN COLUMNA = " . $cc . "<br>";
                    }
                }
                else { 

                    $archivos = $archivos . $lista[$fila][$cc] . " ";

                    if ($depuracion) {
                        echo "ARCHIVO INCLUÍDO : " . $lista[$fila][$cc] . " EN COLUMNA = " . $cc . "<br>";
                    }
                } 

                */
            }

            $cc = $cc + 1;
        }

        if ($depuracion) {
            echo "LISTA DE ARCHIVOS NO INCLUIDOS: <br>";
            $ft = 0;
            while ($ft < count($recuerdo)) {
                echo "RECUERDO [" . $ft . "] = " . $recuerdo[$ft] . "<br>";
                $ft = $ft + 1;
            }
        }

        if ($depuracion) {
            echo "INSTRUCCIÓN 1 = " . $instruccion_1 . "<br>";
        }

        $instruccion_2 = " cat output ";
        $destino = $lista[$fila][$columna_elegida];

        if ($depuracion) {
            echo "INSTRUCCIÓN 2 = " . $instruccion_2 . "<br>";
        }

        if ($depuracion) {
            echo "NOMBRE PRESERVADO, PARA DESPUÉS DE BORRAR = " . $destino . "<br>";
        }

        $aux = explode("\\", $destino);
        $aux2 = $aux[count($aux) - 1];
        $aux3 = "";
        $alfa = 0;
        while ($alfa < count($aux) - 1) {
            $aux3 = $aux3 . $aux[$alfa] . "\\";
            $alfa = $alfa + 1;
        }

        $salida = $aux3 . "temporal.pdf";

        if ($depuracion) {
            echo "LA SALIDA ES = " . $salida . "<br>"; 
        }

        $instruccion = $instruccion_1 . $archivos . $instruccion_2 . $salida;

        if ($depuracion) {
            echo "INSTRUCCIÓN A EJECUTAR = " . $instruccion . "<br><br>"; 
        }

        shell_exec($instruccion);

        /////////////////////////////// SE BORRAN LOS ARCHIVOS, EXCEPTO EL TEMPORAL

        if ($depuracion) {
            echo "BORRADO DE INSTRUCCIONES<br><br>";
        }

        $lista_txt = $lista[0][0];
        $trayectoria = pathinfo($lista_txt, PATHINFO_DIRNAME);
        $trayectoria_txt = $trayectoria . '\\*.txt';
        if ($depuracion) {
            echo "<br>VALOR INICIAL DE lista txt = " . $lista_txt . "<br>";
            echo "TRAYECTORIA = " . $trayectoria . "<br>";
            echo "TRAYECTORIA_TXT = " . $trayectoria_txt . "<br><br>";
        }

        shell_exec("del " . $trayectoria_txt);

        $ba = 0;
        while ($ba < count($lista[$fila])) {
            $instruccion1 = "del " . $lista[$fila][$ba];

            if ($depuracion) {
                echo $instruccion1 . "<br>";
            }

            $borrable = 1;
            $tt = 0;
            while ($tt < count($recuerdo)) {
                if ($recuerdo[$tt] == $ba) {
                    $borrable = 0;
                    break;
                }
                $tt = $tt + 1;
            }

            if ($depuracion) {
                echo "<br><br>MIRANDO LOS BORRABLES<br>";
                if (!$borrable) {
                    echo "COLUMNA = " . $ba . " NO BORRABLE PUES TT = " . $tt . " EN RECUERDOS" . "<br>";
                }
                else {
                    echo "COLUMNA = " . $ba . " BORRABLE PUES TT = " . $tt . " NO ESTÁ EN RECUERDOS" . "<br>";    
                }
            }

            if ($borrable) {
                shell_exec($instruccion1);
            }

            $ba = $ba + 1;       
        }

        rename($salida, $destino);
        $fila = $fila + 1;
    }

    // Se compactan todos los archivos en uno solo
    // Se le cambia el nombre al archivo, por el del origen
}

function calcular_expedientes() {

    global $tabla;
 
    $i = 0;
    while ($i < count($tabla)) {
       
        if ($tabla[$i][2] == 0) {
    
            $nExp = 0;
            $nDoc = 0;
    
            $j = $i + 1;
            while ( ($j < count($tabla)) && ($tabla[$j][2] != 0) ) {
    
                if ( (strcmp($tabla[$j][1], 'dir') == 0) && ($tabla[$j][2] == 1) ) {
                    $nExp = $nExp + 1;
                }
                if (strcmp($tabla[$j][1], 'file') == 0) {
                    $archi = $tabla[$j][0];
                    // echo "ARCHIVO = " . $archi . "<br>";
                    $dummy = explode('.', $archi);
                    // print_r($dummy);
                    if (strcasecmp($dummy[count($dummy) - 1], 'pdf') == 0) {
                        $nDoc = $nDoc + 1;
                        // echo "VALOR DE nDoc = " . $nDoc . "<br>";
                    }
                }
    
                $j = $j + 1;
            }
    
            $tabla[$i][3] = $nExp;
            $tabla[$i][4] = $nDoc;
    
        }
    
        else if (strcmp($tabla[$i][1], 'dir') == 0) {
            
            $nExp = 0;
            $nDoc = 0;
    
            $auxNivel = $tabla[$i][2];
            
            $j = $i + 1;
            while ( ($j < count($tabla)) && ($tabla[$j][2] != $auxNivel) ) {
                if (strcmp($tabla[$j][1], 'file') == 0) {
                    $archi = $tabla[$j][0];
                    // echo "ARCHIVO = " . $archi . "<br>";
                    $dummy = explode('.', $archi);
                    // print_r($dummy);
                    if (strcasecmp($dummy[count($dummy) - 1], 'pdf') == 0) {
                        $nDoc = $nDoc + 1;
                        // echo "VALOR DE nDoc = " . $nDoc . "<br>";
                    }
                }
                $j = $j + 1;
            }
            
            $tabla[$i][4] = $nDoc;
        } else {
            // Para los restantes elementos no se ejecuta ninguna acciÓn        
        }
        
        $i = $i + 1;
    }
    // LA TABLA ESTÁ LISTA
}

function mostrar_tabla_expedientes() {
    
    global $tabla;
    global $rutaD;
    global $path;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
    				 // y tabla se debe preservar como es originalmente
    
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . $local[$f][0] . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else if ($c == 5) {
                $_link = trim(str_replace('c:\xampp\htdocs', '', $tabla[$f][$c]));
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . '<a href="' . $_link . '" target="_blank">' . $local[$f][$c] . '</a>';
            } else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<hr><span>DOCUMENTOS: ' . $path . $rutaD . '</span><hr><br>';
    $s = $s . '<table border="1">';
    $s .= '<tr><th>Archivo</th><th>Tipo</th><th>Nivel</th><th>Exp</th><th>Doc</th><th>LINK</th></tr>';
    foreach ( $local as $r ) {
            $s .= '<tr>';
            foreach ( $r as $v ) {
                    $s .= '<td>'.$v.'</td>';
            }
            $s .= '</tr>';
    }
    $s .= '</table><br><hr>';

    if ($depuracion) {
        echo $s;    
    }
}

function mostrar_tabla_expedientes_front($tipo) {
    
    global $tabla;
    global $rutaD;
    global $path;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
                     // y tabla se debe preservar como es originalmente
    
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                if ($f == 0) {
                    $_link = "#";
                }
                else { 
                    $_link = trim(str_replace('c:\xampp\htdocs', '', $tabla[$f][5]));
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . '<a href="' . $_link . '" target="_blank">' . $local[$f][0] . '</a>' . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else if ($c == 5) {
                $_link = trim(str_replace('c:\xampp\htdocs', '', $tabla[$f][$c]));
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . '<a href="' . $_link . '" target="_blank">' . $local[$f][$c] . '</a>';
            } else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<tr><th>Nro</th><th>Archivo</th>';

    $secuencial = 0;

    $f = 0;
    while ($f < count($local)) {

        if ($f == 0) {
            $ext = '';
            $filtro = 1;
        }
        else {
            $dummy = explode(".", $local[$f][0]);
            $ext = $dummy[count($dummy) - 1];
            $ext = '=' . $ext;
            if (stripos($ext, 'pdf') > 0) {
                $ext = 'pdf';
            }
            else if (stripos($ext, 'wma') > 0 || stripos($ext, 'mp4') > 0 || stripos($ext, 'wmv')) {
                $ext = 'mp4';
            }
            else if (stripos($ext, 'mp3') > 0) {
                $ext = 'mp3';
            }
            else if (stripos($ext, 'zip') > 0 || stripos($ext, 'rar') > 0 || stripos($ext, 'tar') > 0)  {
                $ext = 'zip';
            }
            else {
                $ext = '';
            }

            $filtro = 0;

            if ($tipo == 0) {
                $filtro = 1;
            }
            else if ($tipo == 1 && strcasecmp($ext, 'pdf') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 2 && strcasecmp($ext, 'mp4') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 3 && strcasecmp($ext, 'mp3') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 4 && strcasecmp($ext, 'zip') == 0) {
                $filtro = 1;
            }
            else {
                $filtro = 0;
            }            
        }

        if ($filtro) {
            $s .= '<tr class="w3-hover-teal">';
            $s .= '<td>' . $secuencial . '</td>';
            $secuencial = $secuencial + 1;
            $c = 0;
            while ($c < count($local[0])) {
                if ($c != 1 && $c != 2 && $c != 3 && $c != 4) {

                    if ($c == 0 && $ext == '') {
                        $s .= '<td><span style="font-size:18px; font-weight:bold;">' . $local[$f][$c] . '</span></td>';
                    }
                    else if ($c == 0) {
                        $s .= '<td><span style="font-size:18px;">' . $local[$f][$c] . '</span></td>';                       
                    }
                    else {
                        /* $s .= '<td>' . $local[$f][$c] . '</td>'; */                   
                    }
                }
                $c = $c + 1;
            }
            $s .= '</tr>';
        }

        $f = $f + 1;
    }  

    if ($depuracion) {
        echo $s;    
    }
}

function mostrar_tabla_expedientes_subir($tipo) {
    
    global $tabla;
    global $rutaD;
    global $path;

    global $pdo;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
                     // y tabla se debe preservar como es originalmente

    // Se limpia la tabla tblejecucion
    $sql = "TRUNCATE TABLE tblejecucion";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $secuencial = 1;
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . $local[$f][0] . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                    // AQUÍ SE GRABA LA CARPETA EN LA TABLA tblejecucion
                    try {
                        if ($f == 0) {
                            $estado = 1; // Esto es porque el primer registro se procesa a continuación
                        }
                        else {
                            $estado = 0; // Los regitros después del 0 serán procesados después
                        }
                        $elexp = str_replace("&nbsp", "", $local[$f][0]);
                        $elexp = str_replace("&n", "", $elexp);
                        $sql8 = "INSERT INTO tblejecucion (expediente, estado) VALUES (:expediente, :estado)";
                        $query = $pdo->prepare($sql8);
                        $query->bindParam(':expediente', $elexp, PDO::PARAM_STR);
                        $query->bindParam(':estado', $estado, PDO::PARAM_INT);
                        $query->execute();
                        $secuencial = $secuencial + 1;  
                    }
                    catch (PDOException $ex) {
                        print_r($ex);
                    }
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else if ($c == 5) {
                $_link = trim(str_replace('c:\xampp\htdocs', '', $tabla[$f][$c]));
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . '<a href="' . $_link . '" target="_blank">' . $local[$f][$c] . '</a>';
            } else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<tr><th>Archivo</th><th>LINK</th></tr>';

    $f = 0;
    while ($f < count($local)) {

        if ($f == 0) {
            $ext = '';
            $filtro = 1;
        }
        else {
            $dummy = explode(".", $local[$f][0]);
            $ext = $dummy[count($dummy) - 1];
            $ext = '=' . $ext;
            if (stripos($ext, 'pdf') > 0) {
                $ext = 'pdf';
            }
            else if (stripos($ext, 'wma') > 0 || stripos($ext, 'mp4') > 0 || stripos($ext, 'wmv')) {
                $ext = 'mp4';
            }
            else if (stripos($ext, 'mp3') > 0) {
                $ext = 'mp3';
            }
            else if (stripos($ext, 'zip') > 0 || stripos($ext, 'rar') > 0 || stripos($ext, 'tar') > 0)  {
                $ext = 'zip';
            }
            else {
                $ext = '';
            }

            $filtro = 0;

            if ($tipo == 0) {
                $filtro = 1;
            }
            else if ($tipo == 1 && strcasecmp($ext, 'pdf') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 2 && strcasecmp($ext, 'mp4') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 3 && strcasecmp($ext, 'mp3') == 0) {
                $filtro = 1;
            }
            else if ($tipo == 4 && strcasecmp($ext, 'zip') == 0) {
                $filtro = 1;
            }
            else {
                $filtro = 0;
            }            
        }

        if ($filtro) {
            $s .= '<tr class="w3-hover-teal">';
            $c = 0;
            while ($c < count($local[0])) {
                if ($c != 1 && $c != 2 && $c != 3 && $c != 4) {

                    if ($c == 0 && $ext == '') {
                        $s .= '<td><span style="font-size:18px; font-weight:bold;">' . $local[$f][$c] . '</span></td>';
                    }
                    else if ($c == 0) {
                        $s .= '<td><span style="font-size:18px;">' . $local[$f][$c] . '</span></td>';                       
                    }
                    else {
                        $var = $local[$f][0]; $var = substr($var, 5, 23); $var = (string)$var;
                        $s = $s . '<td><a href="http://localhost/assurance/' . ENTIDAD . '/s_asignar_expediente.php?numero=' . $var . '" target="_self">SUBIR</td>'; 
                        //$s = $s . '<td><button class="w3-button w3-black" id="subir" onclick="proceso_subir(' . $var . ')">Subir</button></td>';                   
                    }
                }
                $c = $c + 1;
            }
            $s .= '</tr>';
        }

        $f = $f + 1;
    }  

    if ($depuracion) {
        echo $s;    
    }
}

function ajustar_expedientes($tipo) {
    
    global $tabla;
    global $rutaD;
    global $path;
    global $pdo;

    $sql = "TRUNCATE TABLE tblAnexos";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $sql = "SELECT * FROM tblAbogados WHERE activo = 1";
    $query = $pdo->prepare($sql);
    $query->execute();
    $resAbogados = $query->fetchAll(PDO::FETCH_ASSOC);

    $elExpediente = $resAbogados[0]['expediente'];

    $depuracion = 0;
    $con_carpetas = 1; 

    if ($depuracion) {
        echo "RUTA_D = " . $rutaD . "<br>";
        echo "PATH = " . $path . "<br>";
    }
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
                     // y tabla se debe preservar como es originalmente

    // INICIALMENTE SE CUENTAN LOS TIPOS DE EXTENSIÓN, PARA VALIDAR LA CREACIÓN
    // DE UNA CARPETA QUE LOS GUARDE A TODOS

    $nro_pdf = 0;
    $nro_videos = 0;
    $nro_audios = 0;
    $nro_zip = 0;

    // CICLO EN EL QUE SE CUENTAN LOS TIPOS DE ARCHIVO

    $path_repo = REPOSITORIO . $local[0][0] . "\\";
    $path_repo_anexo = RESULTADOS . 'AnexosDVDAudiencias\\';

    if ($depuracion) {
        echo "REPOSITORIO = " . $path_repo . "<br>";
        echo "ANEXO = " . $path_repo_anexo . "<br>";
    }

    $f = 0;
    while ($f < count($local)) {

        if ($f == 0) {
            $ext = '';
            $filtro = 1;
        }
        else {
            $dummy = explode(".", $local[$f][0]);
            $ext = $dummy[count($dummy) - 1];
            $ext = '=' . $ext;
            if (stripos($ext, 'pdf') > 0) {
                $ext = 'pdf';
                $nro_pdf = $nro_pdf + 1;
            }
            else if (stripos($ext, 'wma') > 0 || stripos($ext, 'mp4') > 0 || stripos($ext, 'wmv')) {
                $ext = 'mp4';
                $nro_videos = $nro_videos + 1;
            }
            else if (stripos($ext, 'mp3') > 0) {
                $ext = 'mp3';
                $nro_audios = $nro_audios + 1;
            }
            else if (stripos($ext, 'ASF') > 0) {
                $ext = 'mp3';
                $nro_audios = $nro_audios + 1;
            }
            else if (stripos($ext, 'zip') > 0 || stripos($ext, 'rar') > 0 || stripos($ext, 'tar') > 0)  {
                $ext = 'zip';
                $nro_zip = $nro_zip + 1;
            }
            else {
                $ext = '';
            }
        }

        $f = $f + 1;
    }

    // SE SUMA LA CANTIDAD DE TIPOS DE ARCHIVO

    $total = $nro_videos + $nro_audios + $nro_zip;

    if ($depuracion) {
        echo "SE HAN CONTADO LOS ANEXOS. Total = " . $total . "<br><br>";
    }

    // SE CREA LA CARPETA (EN CASO DE EXISTIR TIPOS DE ARCHIVO)

    if ($total > 0) { // SE PUEDE CREAR LA CARPETA
        if (file_exists($path_repo_anexo)) {
            shell_exec('rmdir ' . $path_repo_anexo . ' /s /q');
        }
        shell_exec('mkdir ' . $path_repo_anexo);
    }

    // SI EXISTEN ANEXOS, SE RECORRE LA CARPETA, SE EXTRAE LA FECHA
    // SE  CREA UN ARRAY TEMPORAL DE ANEXOS Y SE ORDENA POR FECHA
    // FINALMENTE, SE AGREGA UN ÍNDICE A CADA ANEXO SEGÚN LA NUEVA POSICIÓN QUE 
    // OCUPE EN LA TABLA ORDENADA. SE MODIFICA EL NOMBRE EN LA CARPETA DEL FILE SYSTEM

    /* 
        indice 0 = nombre del archivo
        índice 1 = tipo del archivo (dir, file)
        índice 2 = nivel
        índice 3 = número de expedientes
        índice 4 = número de documentos
        índice 5 = LINK
    */

    if ($depuracion) {
        echo "PROCESO DE EXTRACCIÓN DE FECHA. CRECIÓN DE TABLA SIN ORDENAMIENTO<br><br>";
    }

    $arrAnexo = Array();

    $f = 0;
    while ($f < count($local)) {

        $dummy = explode(".", $local[$f][0]);
        $ext = $dummy[count($dummy) - 1];

        if ($depuracion) {
            echo "ARCHIVO: " . $local[$f][0] . "<br>";
            echo "EXTENSIÓN: " . $ext . "<br><br>";
        }

        $kt = stripos('.', $local[$f][0]);

        if (strcasecmp($ext, 'pdf') == 0 || $ext == '' || $f == 0) {
            if ($depuracion) {
                echo "NO SE PROCESA, PUES ES UN PDF" . "<br><br>";
            }
        }
        else {     
 
            if ($depuracion) {
                echo "ES DIFERENTE A PDF. SE PROCESA !!!<br><br>";
            }

            $nom_arch = $local[$f][0];
            $array_fecha = buscar_fecha_de_anexos($nom_arch); // Devuelve [0] Posición [1] Año

            $pos_folio = stripos("=" . $nom_arch, 'Folio') - 1; // La posición del Folio

            if ($array_fecha[0] == $pos_folio + 5) {
                $array_fecha = buscar_fecha_de_anexos($nom_arch, $pos_folio + 6);
            }

            $cuaderno = buscar_cuaderno($nom_arch, 1);  // Devuelve el nombre del Cuaderno. C1, C2, etc. 
                                                        // O "" si no hay cuaderno

            $folio = buscar_folio($nom_arch, $array_fecha[0]);

            $arrAnexo[] = $local[$f][0];
            $arrAnexo[] = $array_fecha[1];
            $arrAnexo[] = $folio;
            $arrAnexo[] = $cuaderno;
            $arrAnexo[] = $local[$f][5];

            if ($depuracion) {
                echo "<br><br>ESTE PODRÍA SER LA RESPUESTA AL EXTRAÑO CASO: <br>";
                echo "LINK PRIMERO = " . $local[$f][5] . "<br><br>";
            }

            // SE GRABA EL REGISTRO EN UN ARCHIVO (para el futuro índice)

            try {
                $sql = "INSERT INTO tblAnexos (expediente, nombre, fecha, folio, cuaderno, link) VALUES (:expediente, :nombre, :fecha, :folio, :cuaderno, :link)";
                $query = $pdo->prepare($sql);
                $query->bindParam(':expediente', $elExpediente, PDO::PARAM_STR);
                $query->bindParam(':nombre', $local[$f][0], PDO::PARAM_STR);
                $query->bindParam(':fecha', $array_fecha[1], PDO::PARAM_STR);
                $query->bindParam(':folio', $folio, PDO::PARAM_STR);
                $query->bindParam(':cuaderno', $cuaderno, PDO::PARAM_STR);
                $query->bindParam(':link', $local[$f][5], PDO::PARAM_STR);
                $query->execute();            
            } catch (Exception $ex) {
              print_r($ex);
            }

            // ESTA INFORMACIÓN DEBE COLOCARSE EN UNA TABLA PARA LA GRABACIÓN ANEXANDO AL NOMBRE
            // DE LAS CARPETAS
        }
                                                   
        $f = $f + 1;
    }

    if ($depuracion) {
        echo "SE HA TERMINADO LA EXTRACCIÓN DE FECHAS.<br>EN BASE DE DATOS Y SIN ORDENAMIENTO !!!!!<br>";
    }

    // SE MODIFICA EL NOMBRE DE LOS ARCHIVOS CON EXTENSIÓN DIFERENTE A PDF
    ///////////////////////////////////////////////////////////////////////////

    $sql = "SELECT * FROM tblAnexos WHERE 1 ORDER BY cuaderno ASC, fecha ASC";
    $query = $pdo->prepare($sql);
    $query->execute();
    $resAnexos = $query->fetchAll(PDO::FETCH_ASSOC);

    $f = 0;
    while ($f < count($resAnexos)) {

        $origen = $resAnexos[$f]['link'];
        $archivo = pathinfo($resAnexos[$f]['link'], PATHINFO_BASENAME);

        $first = strval($f + 1);
        while (strlen($first) < 2) { 
            $first = '0' . $first;
        }

        $destino = $first . "_" . $archivo;
        $path_destino = $path_repo_anexo . $destino;

        if ($depuracion) {
            echo "PATH = " . $path . "<br>";
            echo "ORIGEN = " . $origen . "<br>";
            echo "DESTINO (nuevo nombre) = " . $destino . "<br>";
            echo "PATH DESTINO (nuevo link) = " . $path_destino . "<br>";
        }

        $inst = "ren " . $origen . " " . $destino;

        if ($depuracion) {
            echo "INSTRUCCIÓN PARA CAMBIAR NOMBRE DE LOS anexos = " . $inst . "<br><br>";
        }

        shell_exec($inst);

        $resAnexos[$f]['nombre'] = $destino;
        $resAnexos[$f]['link'] = $path_destino;

        if ($depuracion) {
            echo "CREO QUE AQUÍ VAMOS BIEN CON EL NOMBRE Y CON EL LINK ===========>>><br><br>";
            echo "NOMBRE (destino) = " . $resAnexos[$f]['nombre'] . "<br>";
            echo "LINK (path_destino) = " . $resAnexos[$f]['link'] . "<br>";
        }

        $f = $f + 1;
    }




        // SE PROCEDE A LA DESCOMPRESIÓN DE ARCHIVOS. PARA CADA UNO DEBE CREARSE LA CARPETA
        // CORRESPONDIENTE

        if ($con_carpetas) {

            $f = 0;
            while ($f < count($resAnexos)) {

                if ($depuracion) {
                    echo "NOMBRE DE ARCHIVO CANDIDATO PARA DESCOMPRESIÓN = " . $resAnexos[$f]['nombre'] . "<br>";
                }

                $dummy = explode(".", $resAnexos[$f]['nombre']);
                $ext = $dummy[count($dummy) - 1];
                $ext = '=' . $ext;

                if (stripos($ext, 'rar') > 0) {

                /*    $origen = $path_repo . $resAnexos[$f]['nombre'];

                    if ($depuracion) {
                        echo "<br><br>EL ARCHIVO A DESCOMPRIMIR: " . $resAnexos[$f]['nombre'] . "<br>";
                        echo "EL LINK: " . $resAnexos[$f]['link'] . "<br><br>";
                        echo "LA TRAYECTORIA ORIGEN = " . $origen . "<br>";
                    }

                    $rar_file = rar_open($origen) or die("No se puede abrir el archivo RAR");

                    $entries = rar_list($rar_file);

                    foreach ($entries as $entry) {
                        echo 'Filename: ' . $entry->getName() . "\n";
                        echo 'Packed size: ' . $entry->getPackedSize() . "\n";
                        echo 'Unpacked size: ' . $entry->getUnpackedSize() . "\n";

                        $entry->extract('c:\\xampp\\htdocs\\assurance\\abogado01\\resultados\\AnexosDVDAudiencias\\');
                    }

                    rar_close($rar_file); */
                    
                    // c:\xampp\htdocs\assurance\abogado01\resultados\AnexosDVDAudiencias\ 

                    $nombre = $resAnexos[$f]['nombre'];
                    $aux = pathinfo($nombre, PATHINFO_BASENAME);
                    $dummy = explode(".", $aux);
                    $archivo = $dummy[0];

                    if ($depuracion) {
                        echo "DATOS DE CONTROL =============================>" . "<br>";
                        echo "NOMBRE: " . $resAnexos[$f]['nombre'] . "<br>";
                        echo "ARCHIVO: " . $archivo . "<br><br>";
                        echo "LINK = " . $resAnexos[$f]['link'] . "<br><br>";
                    }

                    shell_exec("mkdir " . $path_repo_anexo . $archivo);
                    // shell_exec("move " . $path_repo . $nombre . " " . $path_repo_anexo . $archivo);

                    $origen = $path_repo . $nombre;

                    if ($depuracion) {
                        echo "<br><br><br><br>ORIGEN = " . $origen . "<br><br><br>";
                    }

                    $rar_file = rar_open($origen) or die("No se puede abrir el archivo RAR");

                    $entries = rar_list($rar_file);

                    foreach ($entries as $entry) {
                        /*echo 'Filename: ' . $entry->getName() . "\n";
                        echo 'Packed size: ' . $entry->getPackedSize() . "\n";
                        echo 'Unpacked size: ' . $entry->getUnpackedSize() . "\n";*/

                        $entry->extract('c:\\xampp\\htdocs\\assurance\\abogado01\\resultados\\AnexosDVDAudiencias\\' . $archivo);
                    }

                    rar_close($rar_file);

                }
                else if (stripos($ext, 'zip') > 0) {


                }

                $f = $f + 1;
            }
        }

        // SE VUELVEN A GRABAR LOS REGISTROS DE tblAnexos, A PARTIR DE LAS MODIFICACIONES
        // DISPONIBLES EN LA NUEVA VERSIÓN DE resAnexos

        $sql = "TRUNCATE TABLE tblAnexos";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $f = 0;
        while ($f < count($resAnexos)) {
            $resAnexos[$f]['link'] = str_replace(".rar", "", $resAnexos[$f]['link']);
            $resAnexos[$f]['nombre'] = str_replace(".rar", "", $resAnexos[$f]['nombre']);
            try {
                $sql = "INSERT INTO tblAnexos (expediente, nombre, fecha, folio, cuaderno, link) VALUES (:expediente, :nombre, :fecha, :folio, :cuaderno, :link)";
                $query = $pdo->prepare($sql);
                $query->bindParam(':expediente', $resAnexos[$f]['expediente'], PDO::PARAM_STR);
                $query->bindParam(':nombre', $resAnexos[$f]['nombre'], PDO::PARAM_STR);
                $query->bindParam(':fecha', $resAnexos[$f]['fecha'], PDO::PARAM_STR);
                $query->bindParam(':folio', $resAnexos[$f]['folio'], PDO::PARAM_STR);
                $query->bindParam(':cuaderno', $resAnexos[$f]['cuaderno'], PDO::PARAM_STR);
                $query->bindParam(':link', $resAnexos[$f]['link'], PDO::PARAM_STR);
                $query->execute();            
            } catch (Exception $ex) {
              print_r($ex);
            }
            $f = $f + 1;
        }





    $congelado = 0;

    if ($congelado == 0) { 





        // SE COPIAN TODOS LOS ARCHIVOS DESDE LA CARPETA ACTUAL HACIA LA NUEVA CARPETA

        $ins1 = "move " . $path_repo . "*.mp4 " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 1 = " . $ins1 . "<br>";
        $ins2 = "move " . $path_repo . "*.wma " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 2 = " . $ins2 . "<br>";
        $ins3 = "move " . $path_repo . "*.wmv " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 3 = " . $ins3 . "<br>";
        $ins4 = "move " . $path_repo . "*.mp3 " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 4 = " . $ins4 . "<br>";
        $ins5 = "move " . $path_repo . "*.ASF " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 5 = " . $ins5 . "<br>";
        $ins6 = "move " . $path_repo . "*.asf " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 6 = " . $ins6 . "<br>";

        $ins7 = "del " . $path_repo . "*.zip " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 7 = " . $ins7 . "<br>";
        $ins8 = "del " . $path_repo . "*.rar " . $path_repo_anexo;
        if ($depuracion) echo "INSTRUCCIÓN 8 = " . $ins8 . "<br>";

        shell_exec($ins1);
        shell_exec($ins2);
        shell_exec($ins3);
        shell_exec($ins4);
        shell_exec($ins5);
        shell_exec($ins6);

        // shell_exec($ins7);        
        // shell_exec($ins8);


    } // congelado

}

function mostrar_tabla_expedientes_base() {
    
    global $tabla;
    global $rutaD;
    global $path;

    $depuracion = 1;
    
    $local = Array();
    $local = $tabla; // tabla se graba en local. La razón es porque local es modificado
                     // y tabla se debe preservar como es originalmente
    
    for ($f = 0; $f < count($local); $f++) {
        for ($c = 0; $c < count($local[0]); $c++) {
            if ($c == 0) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][0] = strtoupper($local[$f][0]);
                    $local[$f][5] = strtoupper($local[$f][5]);
                }
                else {
                    $local[$f][0] = strtolower($local[$f][0]);
                    $local[$f][5] = strtolower($local[$f][5]);
                }
                $local[$f][0] = '&nbsp' . espacios($local[$f][2]) . $local[$f][0] . '&nbsp';
                $local[$f][5] = '&nbsp' . $local[$f][5] . '&nbsp';
            } else if ($c == 1) {
                if (strcmp($local[$f][1], 'dir') == 0) {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "carpeta" . '&nbsp' . '&nbsp';
                } else {
                    $local[$f][$c] = '&nbsp;' . '&nbsp' . "documento " . '&nbsp' . '&nbsp';
                }
            }
            else {
                $local[$f][$c] = '&nbsp;' . '&nbsp;' . $local[$f][$c];
            }
        }
    }

    $s = '<hr><span>DOCUMENTOS: ' . $path . $rutaD . '</span><hr><br>';
    $s = $s . '<table border="1">';
    $s .= '<tr><th>Archivo</th><th>Tipo</th><th>Nivel</th><th>Exp</th><th>Doc</th><th>LINK</th></tr>';
    foreach ( $local as $r ) {
        $s .= '<tr>';
        foreach ( $r as $v ) {
            $s .= '<td>'.$v.'</td>';
        }
       $s .= '</tr>';
    }
    $s .= '</table><br><hr>';

    if ($depuracion) { 
        echo $s;
    }

}

function grabar_tabla_expedientes_base() {

    global $tabla;

    // AQUI SE DEBE GRABAR LA TABLA UTILIZANDO PDO
    // SE GRABA EN LA TABLA: tblExpedientes

    //////////////////////////////////////////////////////////////////
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
 
    $sql = "TRUNCATE TABLE tblexpedientesbase";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    try {
        for($i=0; $i<count($tabla); $i++){
            if ($tabla[$i][2] == 0) {
                $sql = "INSERT INTO tblexpedientesbase (archivo, tipo, nivel, expedientes, documentos, link) VALUES (:archivo, :tipo, :nivel, :expedientes, :documentos, :link)";
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':archivo', $tabla[$i][0], PDO::PARAM_STR);
                $stmt->bindParam(':tipo', $tabla[$i][1], PDO::PARAM_STR);
                $stmt->bindParam(':nivel', $tabla[$i][2], PDO::PARAM_INT);
                $stmt->bindParam(':expedientes', $tabla[$i][3], PDO::PARAM_INT);
                $stmt->bindParam(':documentos', $tabla[$i][4], PDO::PARAM_INT);
                $stmt->bindParam(':link', $tabla[$i][5], PDO::PARAM_STR);
                $stmt->execute();
            }
        }
        
    } catch (Exception $ex) {
      print_r($ex);
    }

    ///////////////////////////////////////////////////////////////////
}

function grabar_tabla_expedientes() {

    global $tabla;

    // AQUI SE DEBE GRABAR LA TABLA UTILIZANDO PDO
    // SE GRABA EN LA TABLA: tblExpedientes

    //////////////////////////////////////////////////////////////////
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
    
    try {
        for($i=0; $i<count($tabla); $i++){
            $sql = "INSERT INTO tblexpedientes (archivo, tipo, nivel, expedientes, documentos, link) VALUES (:archivo, :tipo, :nivel, :expedientes, :documentos, :link)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':archivo', $tabla[$i][0], PDO::PARAM_STR);
            $stmt->bindParam(':tipo', $tabla[$i][1], PDO::PARAM_STR);
            $stmt->bindParam(':nivel', $tabla[$i][2], PDO::PARAM_INT);
            $stmt->bindParam(':expedientes', $tabla[$i][3], PDO::PARAM_INT);
            $stmt->bindParam(':documentos', $tabla[$i][4], PDO::PARAM_INT);
            $stmt->bindParam(':link', $tabla[$i][5], PDO::PARAM_STR);
            $stmt->execute();
    }
        
    } catch (Exception $ex) {
      print_r($ex);
    }

    ///////////////////////////////////////////////////////////////////
}

function limpiar_tabla_expedientes() {
    
    //////////////////////////////////////////////////////////////////

    $depuracion = 0;
     
    try {
      $pdo = new PDO(
        "mysql:host=" . DB_HOST . ";charset=" . DB_CHARSET . ";dbname=" . DB_NAME,
        DB_USER, DB_PASSWORD, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
          PDO::ATTR_EMULATE_PREPARES => false
        ]
      );
    } catch (Exception $ex) {
      print_r($ex);
      die("Error conectándose a la base de datos");
    }
 
    $sql = "TRUNCATE TABLE tblexpedientes";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    if ($depuracion) {
        echo "LIMPIEZA DE TABLA EXPEDIENTES COMPLETADA<br>";
        echo "Proceso Base Completado<hr>";        
    }

}

function renderizar( ) {
    global $tabla, $respuesta;

    $paso = 0;

    $nivel = -1;

    $f = 0;
    while ($f < count($tabla)) {
          // El Nivel del elemento actual puede se de tres tipos distintos:
          // a) El Nivel es mayor al actual.
          // b) El Nivel es igual al actual. 
          // c) El Nivel es menor al actual. En este caso, debemos cerrar TODOS los niveles anteriores
          
          // Primer caso: a) Nuevo elemento, Nivel_nodo > Nivel_actual

          if ($tabla[$f][2] > $nivel) {
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '<li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';                    
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                    
            }
            $nivel = $nivel + 1;
          }
          
          // Segundo caso: b) Cierre de directorio, Nivel_nodo == Nivel_actual
          
          else if ($tabla[$f][2] == $nivel) {
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '</ul></li><li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';
                $nivel = $nivel + 1;
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                    
            }
          }
          
          // Tercer caso: c) Nivel_actual < Nivel_nodo
          // Cierre de TODOS los niveles hasta el Nivel del nuevo nodo.
          
          else {
             while ($nivel > $tabla[$f][2]) {
                 $respuesta = $respuesta . '</ul></li>';
                 $nivel = $nivel - 1;
            }
            $nivel = $tabla[$f][2];
            if (strcmp($tabla[$f][1], 'dir') == 0) {
                $respuesta = $respuesta . '<li id="' . $f . '">' . strtoupper(basename($tabla[$f][0])) . ' : (' . $tabla[$f][3] . '/'. $tabla[$f][4] . ')<ul>';                     
            } else {
                $respuesta = $respuesta . '<li id="'. $f. '" data-jstree=\'{"icon":"//jstree.com/tree.png"}\'>' . basename($tabla[$f][0]) . '</li>';                     
            }
          }
        $f = $f + 1;
    }
}

function buscar_numero($texto, $cad = '') {

    $mostrar_f = 0;

    $numero = '';

    $pos = 0;
    if (strcmp($cad, '') != 0) {
        $pos = stripos($texto, $cad);
    }

    $i = $pos;
    while (!is_numeric($texto[$i]) && $i < strlen($texto)) {
        $i = $i + 1;
    }

    if ($i == strlen($texto)) {
        return $numero; // No encontró un número
    }

    $k = $i;

    if ($mostrar_f) {
        echo "K = " . $k . "<br>I = " . $i . "<br>POS = " . $pos . "<br>";
        echo "LONGITUD DE TEXTO = " . strlen($texto) . "<br>";
    }

    while ($i < strlen($texto) && is_numeric($texto[$i]) ) {
        $i = $i + 1;   
    }
    $numero = mb_substr($texto, $k, $i - $k);
    return $numero;
}

function limpiar_paginas_nueva_trd() {

    global $pdo;

    try {
        $sql = "UPDATE tblpaginas SET procesado_nueva_trd = 0";
        $query = $pdo->prepare($sql);
        $query->execute();
    }
    catch (PDOException $ex) {
        print_r($ex);
    }

}

function organizar_cadena($s) {
    $s = trim($s);
    while (stripos($s, "  ") > 0) {
        str_replace("  ", " ", $s);
    }
    return $s;
}

function buscar_nombres_partes_a($selector, $depuracion = 0, $num_pal_seleccion_valida = 2, $num_pal_aevaluar = 6, $tipo = 0) {

    global $pdo;

    $depuracion = 0;

    $resultado = "";

    // Se limpia el selector. Se quitan espacios a la derecha y a la izquierda de la cadena
    $selector = organizar_cadena($selector);

    // Se divide en palabras separadas por un espacio
    $palabras = explode(" ", $selector);

    if ($depuracion) {
        echo "Número de palabras = " . count($palabras) . "<br>";
        print_r($palabras);  echo "<br>";
        echo "El selector base = " . $palabras[0] . "<br>";
    }

    if (count($palabras) == 1) {
        // Se buscan las páginas en donde se encuentra el selector

        try {
            $sql2 = "SELECT * FROM tblpaginas WHERE MATCH (contenido) AGAINST ('" . $palabras[0] . "' IN BOOLEAN MODE) ORDER BY pagina ASC";
            $query2 = $pdo->prepare($sql2);
            $query2->execute();
            $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
            $totalPaginas = $query2->rowCount();
            }
        catch (PDOException $ex) {
            print_r($ex);
        } // TRY de tabla páginas

        if ($depuracion) {
            echo "HA BUSCADO EL selector: " . $palabras[0] . "<br>";
            echo "Número de paginas en donde está el selector: " . count($resPaginas) . "<br>";
            $fz = 0;
            while ($fz < count($resPaginas)) {
                echo "Página = " . $resPaginas[$fz]['pagina'] . "<br>";
                $fz = $fz + 1;
            }
        }

        // Se recorren todas las páginas buscando el selector

        if ($depuracion) {
            echo "<br>SE INICIA EL PROCESO DE BÚSQUEDA DE SELECTORES<br>";
        }

        $p = 0;
        while ($p < count($resPaginas)) {

            if ($depuracion) {
                echo "PÁGINA EVALUADA: " . $resPaginas[$p]['pagina'] . "<br>";
            }

            // Se crea el array que almacenará los nombres potenciales
            // Posee tres columnas
            // ----------------------------
            // id
            // nombre
            // éxito
            // ----------------------------
            // Cada fila del array almacena una de las palabras que potencialmente son el nombre
            // buscado, y que se encuentran a la derecha del selector. Analizando dichas palabras
            // a la derecha, se espera encontrar el nombre deseado

            $arr = Array();

            // El selector puede estar en varios sitios dentro de la página

            // Mientras el selector se encuentre dentro de la página, 
            // se evaluará si cumple con los nombres
            $posic = 0;
            $x = stripos($resPaginas[$p]['contenido'], $palabras[0], $posic);
            $posic = $x + 1;
            while ($x > 0) {

                if ($depuracion) {
                    echo "==> Posición x del selector: " . $x . "<br>";
                }

                // Se elige un "trozo" de texto a la derecha del selector evaluado

                $dummy = $x + strlen($palabras[0]) + 81;
                while ($dummy > strlen($resPaginas[$p]['contenido'])) {
                    $dummy = $dummy - 1;
                }

                // Se extrae el "trozo" en donde potencialmente están los nombres buscados
                $aux = substr($resPaginas[$p]['contenido'], ($x + strlen($palabras[0]) + 1), $dummy - $x);

                $aux = depurar_cadena($aux);

                if ($depuracion) {
                    echo "====> Texto a evaluar: " . $aux . "<br>";
                }

                // Se encuentran los 'tokens' que compnonen el texto a buscar. Los tokens se
                // caracterizan porque están separados por espacios en blanco

                $aux = organizar_cadena($aux);
                $arr_aux = explode(" ", $aux);

                $tokens = Array();
                $xw = 0;
                while ($xw < count($arr_aux)) {
                    if (strlen($arr_aux[$xw]) > 2) {
                        $tokens[] = $arr_aux[$xw];
                    }
                    $xw = $xw + 1;
                }               

                if ($depuracion) {
                    echo "======> Tokens = ";
                    $hw = 0;
                    while ($hw < count($tokens)) {
                        echo $tokens[$hw] . " -- ";
                        $hw = $hw + 1;
                    }
                    echo "<br>";
                }

                // SE EVALÚA SI AL MENOS DOS TOKENS SON NOMBRES. En caso positivo, se entiende que
                // este es el nombre buscado. Se retorna este nombre. En caso negativo, el proceso
                // continúa

                if ($depuracion) {
                    echo "========> Evaluando los Tokens" . "<br>";
                }

                $elNombre = Array();

                $t = 0;
                while ($t < count($tokens)) {
                    // Se evalúa si el token es un nombre válido

                    try {
                        $sql2 = "SELECT * FROM tblnombresbase WHERE MATCH (demandado, demandante) AGAINST ('" . $tokens[$t] . "' IN NATURAL LANGUAGE MODE);";
                        $query2 = $pdo->prepare($sql2);
                        $query2->execute();
                        $resNombres = $query2->fetchAll(PDO::FETCH_ASSOC);
                        $totalNombres = $query2->rowCount();
                        }
                    catch (PDOException $ex) {
                        print_r($ex);
                    } // TRY de tabla páginas    

                    if ($query2->rowCount() > 0) {
                        $valido = 1;
                        $esValido = "SI se encuentra";
                    }               
                    else {
                        $valido = 0;
                        $esValido = "NO se encuentra";
                    } 

                    if ($depuracion) {
                        echo "========> (" . $t . ") " . $tokens[$t] . " - " . $esValido . "<br>";
                    }

                    $arr = Array();

                    $arr[] = $t;
                    $arr[] = $tokens[$t];
                    $arr[] = $valido;

                    $elNombre[] = $arr;

                    $t = $t + 1;
                }

                // AQUÍ EVALUAMOS SI LOS tokens ANALIZADOS (PERTENECIENTES A UNA FRASE),
                // en su conjunto, son válidos como respuesta a los nombres

                // EN PRIMER LUGAR SE IMPRIIME PARA VALIDACIÓN
                if ($depuracion) {

                    $s = '<br><table border="1" style="margin-left:80px;">';

                    $s .= '<tr><th>ID</th><th>Pos_Token</th><th>Token</th><th>Válido</th></tr>';

                    $nn = 0;
                    while ($nn < count($elNombre)) {  
                        $s .= '<tr>';
                        $s .= '<td>' . $nn . '</td><td>' . $elNombre[$nn][0] . '</td><td>' . $elNombre[$nn][1] . '</td><td>' . $elNombre[$nn][2] . '</td></tr>';
                        $s .= '</tr>';      
                        $nn = $nn + 1;
                    }

                    $s .= '</table>';

                    echo $s . "<br>";

                }

                // EVALUACIÓN

                // 1. Se cuenta el número de válidos. Debe ser mayor a 2 para ser considerado
                //    como correcto. Otro criterio es que los válidos estén consecutivos o
                //    separados máximo por una posición NO válida

                // Número de válidos
                $num_val = 0;
                $xs = 0;
                while ($xs < count($elNombre)) {
                    if ($elNombre[$xs][2] == 1) {
                        $num_val = $num_val + 1;
                    }
                    $xs = $xs + 1;
                }

                // LA HEURÍSTICA ES LA SIGUIENTE:
                // 1. Si el primer token es 1, es el caso con la mayor posibilidad
                // 2. Si el segundo token es el que vale 1, puede ser considerado el caso correcto
                // 3. Si hay otro token, después del uno, en la posición 2, se considera válido
                //    En este caso pasaría a analizarse el cierre
                // NOTA: Esta es la filosofía base

                $encontrado = 0;

                if ($elNombre[0][2] == 1) {
                    $inicio = 0;
                    if ($elNombre[1][2] == 1 || ($elNombre[2][2] == 1)) {
                        $encontrado = 1;
                        $cierre = $inicio;

                        $ff = 3;
                        while ($ff < count($elNombre)) {
                            if ($elNombre[$ff][2] == 0) {
                                $cierre = $ff - 1;
                                break;
                            }
                            $ff = $ff + 1;
                        }

                        /* 
                        $ff = 3;
                        while ($ff >= 0) {
                            if ($elNombre[$ff][2] == 1) {
                                $cierre = $ff;
                                break;
                            }
                            $ff = $ff - 1;
                        }
                        */

                        $resultado = '';
                        $kn = $inicio;
                        while ($kn <= $cierre) {
                            $resultado = $resultado . $elNombre[$kn][1] . " ";
                            $kn = $kn + 1;
                        }

                        if ($depuracion) {
                            echo "EL RESULTADO FINAL ES: " . $resultado . "<br><br>";
                        }

                        return $resultado;

                    }
                }

                if ($depuracion) {
                    echo "========> Número de válidos = " . $num_val . "<br>";
                }


                echo "<br>";

                // En caso de requerirse, esta es la siguiente iteración
                $x = stripos($resPaginas[$p]['contenido'], $palabras[0], $posic);
                $posic = $x + 1;

            }

            // Aquí se dispone del array 'elNombre' el cual contiene la evaluación de los tokens

            $p = $p + 1;
        }
    }

    if ($depuracion) {
        echo "<br>Se devuelve el resultado = " . $resultado . "<br>";
    }

    return $resultado;

} // Cierre de la función

//////////////////////////////////////////////////////////////////////////////////////////////

function buscar_nombres_partes_b($selector, $origen, $depuracion = 0) {

    global $pdo;

    $depuracion = 0;

    $resultado = "";

    // Se limpia el selector
    $selector = organizar_cadena($selector);
    // Se divide en palabras separadas por un espacio
    $palabras = explode(" ", $selector);

    if ($depuracion) {
        echo "Número de palabras = " . count($palabras) . "<br>";
        print_r($palabras);  echo "<br>";
        echo "El selector base = " . $palabras[0] . "<br>";
    }

    if (count($palabras) >= 1) {
        // Se buscan las páginas en donde se encuentra el selector

        $inst = "SELECT * FROM tblpaginas WHERE MATCH (contenido) AGAINST ('";
        $nm = 0;
        while ($nm < count($palabras)) {
            $inst = ' +' . $palabras[$nm];
            $nm = $nm + 1;
        }

        $inst = $inst + "' IN BOOLEAN MODE) ORDER BY pagina ASC";

        try {
            $sql2 = $inst;
            $query2 = $pdo->prepare($sql2);
            $query2->execute();
            $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
            $totalPaginas = $query2->rowCount();
            }
        catch (PDOException $ex) {
            print_r($ex);
        } // TRY de tabla páginas

        if ($depuracion) {
            echo "TRABAJANDO CON EL selector: " . $palabras[0] . "<br>";
            echo "Número de paginas en donde está el selector: " . count($resPaginas) . "<br>";
            $fz = 0;
            while ($fz < count($resPaginas)) {
                echo "Página = " . $resPaginas[$fz]['pagina'] . "<br>";
                $fz = $fz + 1;
            }
        }

        // Se recorren todas las páginas buscando el selector

        if ($depuracion) {
            echo "<br>SE INICIA EL PROCESO DE BÚSQUEDA DE SELECTORES<br>";
        }

        $p = 0;
        while ($p < count($resPaginas)) {

            if ($depuracion) {
                echo "PÁGINA EVALUADA: " . $resPaginas[$p]['pagina'] . "<br>";
            }

            // Se crea el array que almacenará los nombres potenciales
            // Posee tres columnas
            // ----------------------------
            // id
            // nombre
            // éxito
            // ----------------------------
            // Cada fila del array almacena una de las palabras que potencialmente son el nombre
            // buscado, y que se encuentran a la derecha del selector. Analizando dichas palabras
            // a la derecha, se espera encontrar el nombre deseado

            $arr = Array();

            // El selector puede estar en varios sitios dentro de la página

            // Mientras el selector se encuentre dentro de la página, 
            // se evaluará si cumple con los nombres
            $posic = 0;
            $x = stripos($resPaginas[$p]['contenido'], $origen, $posic);
            $posic = $x + 1;
            while ($x > 0) {

                if ($depuracion) {
                    echo "==> Posición x del selector: " . $x . "<br>";
                }

                $xx = stripos($resPaginas[$p]['contenido'], 'nombre', $posic);

                // AQUÍ VA EL ANÁLISIS - AQUÍ VA EL ANÁLISIS

                // Se elige un "trozo" de texto a la derecha del selector evaluado

                $dummy = $x + strlen($palabras[0]) + 81;
                while ($dummy > strlen($resPaginas[$p]['contenido'])) {
                    $dummy = $dummy - 1;
                }

                // Se extrae el "trozo" en donde potencialmente están los nombres buscados
                $aux = substr($resPaginas[$p]['contenido'], ($x + strlen($palabras[0]) + 1), $dummy - $x);

                if ($depuracion) {
                    echo "====> Texto a evaluar: " . $aux . "<br>";
                }

                // Se encuentran los 'tokens' que compnonen el texto a buscar. Los tokens se
                // caracterizan porque están separados por espacios en blanco

                $aux = organizar_cadena($aux);
                $arr_aux = explode(" ", $aux);

                $tokens = Array();
                $xw = 0;
                while ($xw < count($arr_aux)) {
                    if (strlen($arr_aux[$xw]) > 2) {
                        $tokens[] = $arr_aux[$xw];
                    }
                    $xw = $xw + 1;
                }               

                if ($depuracion) {
                    echo "======> Tokens = ";
                    $hw = 0;
                    while ($hw < count($tokens)) {
                        echo $tokens[$hw] . " -- ";
                        $hw = $hw + 1;
                    }
                    echo "<br>";
                }

                // SE EVALÚA SI AL MENOS DOS TOKENS SON NOMBRES. En caso positivo, se entiende que
                // este es el nombre buscado. Se retorna este nombre. En caso negativo, el proceso
                // continúa

                if ($depuracion) {
                    echo "========> Evaluando los Tokens" . "<br>";
                }

                $elNombre = Array();

                $t = 0;
                while ($t < count($tokens)) {
                    // Se evalúa si el token es un nombre válido

                    try {
                        $sql2 = "SELECT * FROM tblnombresbase WHERE MATCH (demandado, demandante) AGAINST ('" . $tokens[$t] . "' IN NATURAL LANGUAGE MODE);";
                        $query2 = $pdo->prepare($sql2);
                        $query2->execute();
                        $resNombres = $query2->fetchAll(PDO::FETCH_ASSOC);
                        $totalNombres = $query2->rowCount();
                        }
                    catch (PDOException $ex) {
                        print_r($ex);
                    } // TRY de tabla páginas    

                    if ($query2->rowCount() > 0) {
                        $valido = 1;
                        $esValido = "SI se encuentra";
                    }               
                    else {
                        $valido = 0;
                        $esValido = "NO se encuentra";
                    } 

                    if ($depuracion) {
                        echo "========> (" . $t . ") " . $tokens[$t] . " - " . $esValido . "<br>";
                    }

                    $arr = Array();

                    $arr[] = $t;
                    $arr[] = $tokens[$t];
                    $arr[] = $valido;

                    $elNombre[] = $arr;

                    $t = $t + 1;
                }

                // AQUÍ EVALUAMOS SI LOS tokens ANALIZADOS (PERTENECIENTES A UNA FRASE),
                // en su conjunto, son válidos como respuesta a los nombres

                // EN PRIMER LUGAR SE IMPRIIME PARA VALIDACIÓN
                if ($depuracion) {

                    $s = '<br><table border="1" style="margin-left:80px;">';

                    $s .= '<tr><th>ID</th><th>Pos_Token</th><th>Token</th><th>Válido</th></tr>';

                    $nn = 0;
                    while ($nn < count($elNombre)) {  
                        $s .= '<tr>';
                        $s .= '<td>' . $nn . '</td><td>' . $elNombre[$nn][0] . '</td><td>' . $elNombre[$nn][1] . '</td><td>' . $elNombre[$nn][2] . '</td></tr>';
                        $s .= '</tr>';      
                        $nn = $nn + 1;
                    }

                    $s .= '</table>';

                    echo $s . "<br>";

                }

                // EVALUACIÓN

                // 1. Se cuenta el número de válidos. Debe ser mayor a 2 para ser considerado
                //    como correcto. Otro criterio es que los válidos estén consecutivos o
                //    separados máximo por una posición NO válida

                // Número de válidos
                $num_val = 0;
                $xs = 0;
                while ($xs < count($elNombre)) {
                    if ($elNombre[$xs][2] == 1) {
                        $num_val = $num_val + 1;
                    }
                    $xs = $xs + 1;
                }

                // LA HEURÍSTICA ES LA SIGUIENTE:
                // 1. Si el primer token es 1, es el caso con la mayor posibilidad
                // 2. Si el segundo token es el que vale 1, puede ser considerado el caso correcto
                // 3. Si hay otro token, después del uno, en la posición 2, se considera válido
                //    En este caso pasaría a analizarse el cierre
                // NOTA: Esta es la filosofía base

                $encontrado = 0;

                if ($elNombre[0][2] == 1) {
                    $inicio = 0;
                    if ($elNombre[1][2] == 1 || ($elNombre[2][2] == 1)) {
                        $encontrado = 1;
                        $cierre = $inicio;

                        $ff = 3;
                        while ($ff >= 0) {
                            if ($elNombre[$ff][2] == 1) {
                                $cierre = $ff;
                                break;
                            }
                            $ff = $ff - 1;
                        }

                        $resultado = '';
                        $kn = $inicio;
                        while ($kn <= $cierre) {
                            $resultado = $resultado . $elNombre[$kn][1] . " ";
                            $kn = $kn + 1;
                        }

                        if ($depuracion) {
                            echo "EL RESULTADO FINAL ES: " . $resultado . "<br><br>";
                        }

                        return $resultado;

                    }
                }

                if ($depuracion) {
                    echo "========> Número de válidos = " . $num_val . "<br>";
                }


                echo "<br>";

                // En caso de requerirse, esta es la siguiente iteración
                $x = stripos($resPaginas[$p]['contenido'], $palabras[0], $posic);
                $posic = $x + 1;

            }

            // Aquí se dispone del array 'elNombre' el cual contiene la evaluación de los tokens

            $p = $p + 1;
        }
    }

    if ($depuracion) {
        echo "<br>Se devuelve el resultado = " . $resultado . "<br>";
    }

    return $resultado;

} // Cierre de la función

function extraer_portada() {

    global $pdo;

    try {
      $sql2 = "SELECT * FROM tblbas_ciudades";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resBasCiudades = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas

    $ciudad_menor = '';
    $id_ciudad_menor = -1;
    $pagina_menor = 99999;
    $f = 0;
    while ($f < count($resBasCiudades)) {
        try {
          $sql2 = "SELECT * FROM tblpaginas WHERE MATCH (contenido) AGAINST ('" . $resBasCiudades[$f]['ciudad'] . "' IN BOOLEAN MODE) ORDER BY pagina ASC";
          $query2 = $pdo->prepare($sql2);
          $query2->execute();
          $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $ex) {
            rint_r($ex);
        } // TRY de tabla páginas
        if ($query2->rowCount() > 0) {
            if ($resPaginas[0]['pagina'] < $pagina_menor) {
                $pagina_menor = $resPaginas[0]['pagina'];
                $ciudad_menor = $resBasCiudades[$f]['ciudad'];
                $id_ciudad_menor = $resBasCiudades[$f]['codigo'];
            }
        }
        $f = $f + 1;
    }

    $ciudad = $ciudad_menor;

    $despacho = '';
    if (strcasecmp($ciudad, '') != 0) {
        try {
          $sql2 = "SELECT * FROM tblweb_juzgado WHERE ciudad_id = " . $id_ciudad_menor;
          $query2 = $pdo->prepare($sql2);
          $query2->execute();
          $resJuzgados = $query2->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $ex) {
          print_r($ex);
        } // TRY de tabla páginas        
    }

    $juzgado_menor = '';
    $id_juzgado_menor = -1;
    $pagina_menor = 99999;
    $f = 0;
    while ($f < count($resJuzgados)) {
        try {
          $sql2 = "SELECT * FROM tblpaginas WHERE MATCH (contenido) AGAINST ('" . $resJuzgados[$f]['nombre'] . "' IN BOOLEAN MODE) ORDER BY pagina ASC";
          $query2 = $pdo->prepare($sql2);
          $query2->execute();
          $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $ex) {
            print_r($ex);
        } // TRY de tabla páginas
        if ($query2->rowCount() > 0) {
            if ($resPaginas[0]['pagina'] < $pagina_menor) {
                $pagina_menor = $resPaginas[0]['pagina'];
                $juzgado_menor = $resJuzgados[$f]['nombre'];
                $id_juzgado_menor = $resJuzgados[$f]['id'];
            }
        }
        $f = $f + 1;
    }

    $despacho = $juzgado_menor; 

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas 

    $x = stripos($resPaginas[0]['contenido'], 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD'); 

    if ($x > 0) {
        $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
    }     

    return $despacho;
}

function posicion_relativa_compacto($maximo, $pos) {
    $unidad = intdiv($maximo, 3);
    if ($pos < $unidad) return LOC_ARRIBA;
    if ($pos < $unidad * 2) return LOC_MITAD;
    return LOC_ABAJO;
}

function generar_portada($elExpediente) {

    global $pdo;

    $depuracion = 0;

     // SE RECORREN TODAS LAS PÁGINAS BUSCANDO PATRONES
     try {
        $sql2 = "SELECT * FROM tblpaginas";
        $query2 = $pdo->prepare($sql2);
        $query2->execute();
        $paginas = $query2->fetchAll(PDO::FETCH_ASSOC);
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas

      // ----------------------------------------------------------------------------------
      //                                      DESPACHO
      // ----------------------------------------------------------------------------------

      $despacho = "";

      $f = 0;
      while ($f < count($paginas)) {

        // --------------------------------------------------------------------------------
        // Se evalúa si la página es un acta de reparto
        // En ella se obtiene el juzgado y el indiciado
        $x1 = stripos("=" . $paginas[$f]['contenido'], 'acta individual de reparto');
        if ($x1 > 0) {
            if ($depuracion) {
                echo "PÁGINA = " . $paginas[$f]['pagina'] . "<br>";
                echo "CONTENIDO = " . $paginas[$f]['contenido'] . "<br>";
            }
            $x2 = stripos("=". $paginas[$f]['contenido'], 'juzgado', $x1 + 1);
            $x3 = stripos("=". $paginas[$f]['contenido'], 'juzgado', $x2 + 1);
            $x4 = stripos("=". $paginas[$f]['contenido'], 'conocimiento', $x3 + 1); 
            $despacho = substr($paginas[$f]['contenido'], $x3 - 1, $x4 + 12 - $x3);

            if ($depuracion) {
                echo "ANÁLISIS DEL TRABAJO REALIZADO" . "<br>";
                echo "X1 = " . $x1 . "<br>";
                echo "X2 = " . $x2 . "<br>";
                echo "X3 = " . $x3 . "<br>";            
                echo "X4 = " . $x4 . "<br>";
                echo "LONGITUD = " . $x4 + 12 - $x3;
            }

            $despacho = slugify($despacho);

            if ($depuracion) {
                echo "DESPACHO = " . $despacho . "<br>";
            }

            // Se busca la tercera palabra
            $texto = $paginas[$f]['contenido'];
            break;     
        }

        $f = $f + 1;
      }

      if (strcasecmp($despacho, '') != 0) {
        // Se dispone de un juzgado
        $despacho = strtoupper($despacho);
        try {
            $sql3 = "UPDATE tblportada SET despacho = '" . $despacho . "' WHERE expediente = " . $elExpediente;
            $query3 = $pdo->prepare($sql3);
            $query3->execute();
        }
        catch (PDOException $ex) {
            print_r($ex);
        }
      }

      // ----------------------------------------------------------------------------------
      //                                      PARTE PROCESAL A
      // ----------------------------------------------------------------------------------

      $parte_procesal_a = "";

      $f = 0;
      while ($f < count($paginas)) {

        // --------------------------------------------------------------------------------
        // Se evalúa si la página es un acta de reparto
        // En ella se obtiene el juzgado y el indiciado

        $paginas[$f]['contenido'] = str_replace('\r\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\r', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = preg_replace("/[\r\n|\n|\r]+/", " ", $paginas[$f]['contenido']);

        $x1 = stripos("=" . $paginas[$f]['contenido'], 'acusado');
        if ($x1 > 0) {
            if ($depuracion) {
                echo "PÁGINA = " . $paginas[$f]['pagina'] . "<br>";
                echo "CONTENIDO = " . $paginas[$f]['contenido'] . "<br>";
            }

            $x2 = 0; $x3 = 0; $x4 = 0; $x5 = 0; $x6 = 0;

            $x2 = stripos("=". $paginas[$f]['contenido'], 'primer nombre', $x1 + 1);

            if ($x2 > 0)
                $x3 = stripos("=". $paginas[$f]['contenido'], 'segundo nombre', $x2 + 1);
            if ($x3 > 0)
                $x4 = stripos("=". $paginas[$f]['contenido'], 'primer apellido', $x3 + 1);
            if ($x4 > 0)
                $x5 = stripos("=". $paginas[$f]['contenido'], 'segundo apellido', $x4 + 1);
            if ($x5 > 0)
                $x6 = stripos("=". $paginas[$f]['contenido'], 'fecha', $x5 + 1);

            $primer_nombre = ""; $segundo_nombre = "";
            $primer_apellido = ""; $segundo_apellido = "";

            if ($x2 > 0 && $x3 > 0)
                $primer_nombre = substr($paginas[$f]['contenido'], $x2 + 13, $x3 - ($x2 + 14));

            if ($x3 > 0 && $x4 > 0)
                $segundo_nombre = substr($paginas[$f]['contenido'], $x3 + 14, $x4 - ($x3 + 15));

            if ($x4 > 0 && $x5 > 0)
                $primer_apellido = substr($paginas[$f]['contenido'], $x4 + 15, $x5 - ($x4 + 16));

            if ($x5 > 0 && $x6 > 0) 
                $segundo_apellido = substr($paginas[$f]['contenido'], $x5 + 16, $x6 - ($x5 + 17));

            $parte_procesal_a = $primer_nombre . " " . $segundo_nombre . " " . $primer_apellido . " " . $segundo_apellido;

            if ($depuracion) {
                echo "ANÁLISIS DEL TRABAJO REALIZADO" . "<br>";
                echo "X1 = " . $x1 . "<br>";
                echo "X2 = " . $x2 . "<br>";
                echo "X3 = " . $x3 . "<br>";            
                echo "X4 = " . $x4 . "<br>";
                echo "X5 = " . $x5 . "<br>";
                echo "X6 = " . $x6 . "<br>";

                echo "PRIMER NOMBRE = " . $primer_nombre . "<br>";
                echo "SEGUNDO NOMBRE = " . $segundo_nombre . "<br>";
                echo "PRIMER APELLIDO = " . $primer_apellido . "<br>";
                echo "SEGUNDO APELLIDO = " . $segundo_apellido . "<br>";

                echo "PARTE PROCESAL A = " . $parte_procesal_a . "<br>";
            }

            $parte_procesal_a = slugify($parte_procesal_a);

            if ($depuracion) {
                echo "DESPACHO = " . $parte_procesal_a . "<br>";
            }

            $cont = 0;
            if ($x2 > 0) $cont = $cont + 1;
            if ($x3 > 0) $cont = $cont + 1;
            if ($x4 > 0) $cont = $cont + 1;
            if ($x5 > 0) $cont = $cont + 1;

            if ($cont < 2)
                $parte_procesal_a = "";

            break;     
        }

        $f = $f + 1;
      }

      if (strcasecmp($parte_procesal_a, '') != 0) {
        // Se dispone de un juzgado

        $parte_procesal_a = preg_replace("/[\r\n|\n|\r]+/", " ", $parte_procesal_a);
        $parte_procesal_a = str_replace("N/A", "", $parte_procesal_a);
        $parte_procesal_a = strtoupper($parte_procesal_a);

        try {
            $sql3 = "UPDATE tblportada SET parte_procesal_a = '" . $parte_procesal_a . "' WHERE expediente = " . $elExpediente;
            $query3 = $pdo->prepare($sql3);
            $query3->execute();
        }
        catch (PDOException $ex) {
            print_r($ex);
        }
      }

      // ----------------------------------------------------------------------------------
      //                     PARTE PROCESAL B - EL QUE DEMANDA (el fiscal)
      // ----------------------------------------------------------------------------------

      $parte_procesal_b = "";

      $f = 0;
      while ($f < count($paginas)) {

        // --------------------------------------------------------------------------------
        // Se evalúa si la página es un acta de reparto
        // En ella se obtiene el demandante

        $paginas[$f]['contenido'] = str_replace('\r\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\r', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = preg_replace("/[\r\n|\n|\r]+/", " ", $paginas[$f]['contenido']);

        $x1 = stripos("=" . $paginas[$f]['contenido'], 'datos del fiscal');
        if ($x1 > 0) {
            if ($depuracion) {
                echo "PÁGINA = " . $paginas[$f]['pagina'] . "<br>";
                echo "CONTENIDO = " . $paginas[$f]['contenido'] . "<br>";
            }

            $x2 = stripos("=". $paginas[$f]['contenido'], 'nombres y apellidos', $x1 + 1);

            if ($x2 > 0) {
                $x3 = stripos("=". $paginas[$f]['contenido'], 'dirección', $x2 + 1);
                $parte_procesal_b = substr($paginas[$f]['contenido'], $x2 + 18, $x3 - ($x2 + 19));
                $parte_procesal_b = slugify($parte_procesal_b); 
                break;                   
            }
        }

        $f = $f + 1;

      }

      if (strcasecmp($parte_procesal_b, '') != 0) {
        // Se dispone del demandante

        $parte_procesal_b = preg_replace("/[\r\n|\n|\r]+/", " ", $parte_procesal_b);
        $parte_procesal_b = str_replace("N/A", "", $parte_procesal_b);
        $parte_procesal_b = strtoupper($parte_procesal_b);

        try {
            $sql3 = "UPDATE tblportada SET parte_procesal_b = '" . $parte_procesal_b . "' WHERE expediente = " . $elExpediente;
            $query3 = $pdo->prepare($sql3);
            $query3->execute();
        }
        catch (PDOException $ex) {
            print_r($ex);
        }

      }

      // ----------------------------------------------------------------------------------
      //                                 TERCEROS - Víctima(s)
      // ----------------------------------------------------------------------------------

      $depuracion = 1;

      $terceros = "";

      $f = 0;
      while ($f < count($paginas)) {

        // --------------------------------------------------------------------------------
        // Se evalúa si la página es un acta de reparto
        // En ella se obtiene el juzgado y el indiciado

        $paginas[$f]['contenido'] = str_replace('\r\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\r', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = preg_replace("/[\r\n|\n|\r]+/", " ", $paginas[$f]['contenido']);

        $x1 = stripos("=" . $paginas[$f]['contenido'], 'datos de la victima');
        if ($x1 > 0) {
            if ($depuracion) {
                echo "PÁGINA = " . $paginas[$f]['pagina'] . "<br>";
                echo "CONTENIDO = " . $paginas[$f]['contenido'] . "<br>";
            }

            $x2 = 0; $x3 = 0; $x4 = 0;

            $x2 = stripos("=". $paginas[$f]['contenido'], 'nombres', $x1 + 1);

            if ($x2 > 0)
                $x3 = stripos("=". $paginas[$f]['contenido'], 'apellidos', $x2 + 1);
            if ($x3 > 0)
                $x4 = stripos("=". $paginas[$f]['contenido'], 'lugar de residencia', $x3 + 1);

            $nombres = ""; $apellidos = "";

            if ($x2 > 0 && $x3 > 0)
                $nombres = substr($paginas[$f]['contenido'], $x2 + 7, $x3 - ($x2 + 8));

            if ($x3 > 0 && $x4 > 0)
                $apellidos = substr($paginas[$f]['contenido'], $x3 + 9, $x4 - ($x3 + 10));

            $terceros = " " . $nombres . " " . $apellidos . " ";
            $terceros = comprimir_cadena_especial($terceros);

            if ($depuracion) {
                echo "ANÁLISIS DEL TRABAJO REALIZADO" . "<br>";
                echo "X1 = " . $x1 . "<br>";
                echo "X2 = " . $x2 . "<br>";
                echo "X3 = " . $x3 . "<br>";            
                echo "X4 = " . $x4 . "<br>";

                echo "NOMBRES = " . $nombres . "<br>";
                echo "APELLIDOS = " . $apellidos . "<br>";
                echo "TERCEROS = " . $terceros . "<br>";
            }

            $terceros = slugify($terceros);

            if (strcasecmp(substr($terceros, 0, 2), 'I ') == 0) {
                $terceros = substr($terceros, 2);
            }

            if (strcasecmp(substr($terceros, 0, 2), 'i ') == 0) {
                $terceros = substr($terceros, 2);
            }

            if ($depuracion) {
                echo "TERCEROS = " . $terceros . "<br>";
            }

            $exito = 0;
            if (($x2 > 0 && $x4 > 0) || ($x3 > 0 && $x4 > 0)) $exito = 1;

            if (!$exito)
                $terceros = "";

            break;     
        }

        $f = $f + 1;
      }

      if (strcasecmp($terceros, '') != 0) {
        // Se dispone de un tercero

        $terceros = preg_replace("/[\r\n|\n|\r]+/", " ", $terceros);
        $terceros = str_replace("N/A", "", $terceros);
        $terceros = strtoupper($terceros);

        try {
            $sql3 = "UPDATE tblportada SET terceros = '" . $terceros . "' WHERE expediente = " . $elExpediente;
            $query3 = $pdo->prepare($sql3);
            $query3->execute();
        }
        catch (PDOException $ex) {
            print_r($ex);
        }
      }

      $depuracion = 0;

      // ----------------------------------------------------------------------------------
      //                                      LA CIUDAD
      // ----------------------------------------------------------------------------------

      $ciudad = "";

      $f = 0;
      while ($f < count($paginas)) {

        // --------------------------------------------------------------------------------
        // Se evalúa si la página es un acta de reparto
        // En ella se obtiene la ciudad

        $paginas[$f]['contenido'] = str_replace('\r\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\r', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = str_replace('\n', ' ', $paginas[$f]['contenido']);
        $paginas[$f]['contenido'] = preg_replace("/[\r\n|\n|\r]+/", " ", $paginas[$f]['contenido']);

        $texto = "=" . $paginas[$f]['contenido'];

        $xcartagena = stripos($texto, 'cartagena');
        $xcucuta1 = stripos($texto, 'cucuta');
        $xcucuta2 = stripos($texto, 'cúcuta');
        $xarauca = stripos($texto, 'arauca');

        if ($xcartagena > 0) {
            $ciudad = "CARTAGENA";
            break;
        }

        if ($xcucuta1 > 0) {
            $ciudad = "CÚCUTA";
            break;
        }

        if ($xcucuta2 > 0) {
            $ciudad = "CÚCUTA";
            break;
        }

        if ($arauca > 0) {
            $ciudad = "ARAUCA";
            break;
        }

        $f = $f + 1;

      }


    try {
        $sql3 = "UPDATE tblportada SET ciudad = '" . $ciudad . "' WHERE expediente = " . $elExpediente;
        $query3 = $pdo->prepare($sql3);
        $query3->execute();
    }
    catch (PDOException $ex) {
        print_r($ex);
    }
   

} // CIERRE DE LA FUNCIÓN generar_portada()

/////////////////////////////////////////////////////////////////////////////////////////////

?>

<script>
    function proceder() {
        alert($id);
    }
</script>