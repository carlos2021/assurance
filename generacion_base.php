<?php include 'funciones.php'; ?>

<?php $depuracion = 0; ?>

<?php

	if ($depuracion) {
		echo "INGRESÓ A generacion_base.php" . "<br><br><br>";
	}

	$ruta = $argv[1]; // c:\xampp\htdocs\assurance\expedientes
	$ruta = $ruta . "\\"; // c:\xampp\htdocs\assurance\expedientes\
	$path = RAIZ; // c:\xampp\htdocs\assurance\

    $sql = "TRUNCATE TABLE tblpaginas";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $sql = "TRUNCATE TABLE tbldetalle";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $sql = "TRUNCATE TABLE tblportada";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
	
	$tabla = Array();
	listar_directorios_ruta($ruta, 0);

	if ($tabla == null) {

		if ($depuracion) {
			echo "NO EXISTE LA CARPETA: " . $ruta . "<br>";
		}
		
	} else {
		calcular_expedientes();

		?>

		<?php if ($depuracion) { ?>

			<span style="font-weight:bold;">REGRESAR AL MENÚ</span>
			<a href="index.php"><button>ATRÁS</button></a>
			<hr>PROCESO BASE

		<?php } ?>

		<?php

		if ($depuracion) {
			echo "MOSTRAR LA TABLA" . "<br>";
			mostrar_tabla_expedientes_base();
		}

		grabar_tabla_expedientes_base();
		limpiar_tabla_expedientes();

	} ?>

	<?php if ($depuracion) { ?>

		<span style="font-weight:bold;">REGRESAR AL MENÚ</span>
		<a href="index.php"><button>ATRÁS</button></a>
		<hr><br>

	<?php } ?>
	
	<?php

	if ($depuracion) {
		echo "SALIENDO !!! DE: generacion_base.php" . "<br><br><br>";
	}
?>
