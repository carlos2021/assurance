<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('procesar');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <div class="w3-panel w3-margin-top w3-bottombar" style="margin-top:22px; margin-left:16px; margin-right:16px;">
 
    <div class="w3-panel">
      <h4><b><i class="fa fa-folder-open-o"></i> &nbsp;Procesar Expedientes</b></h4>
      <div class="w3-section">
        <span class="w3-margin-right" style="font-size:18px;">Actividad:</span> 
        <div class="w3-dropdown-hover">
          <button class="w3-button w3-black" id="compactar" onclick="activar()">Ejecutar</button>
        </div>
      </div>
    </div>

  </div>

  <script>
    function activar() {
      document.getElementById("abogado_01").click();
      document.getElementById("abogado_02").click();
      document.getElementById("abogado_03").click();
      document.getElementById("abogado_04").click();
      document.getElementById("abogado_05").click();
      document.getElementById("abogado_06").click();
      document.getElementById("abogado_07").click();
      document.getElementById("abogado_08").click();
      document.getElementById("abogado_09").click();
      document.getElementById("abogado_10").click();
      document.getElementById("abogado_11").click();
      document.getElementById("abogado_12").click();
    }
  </script>

  <header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-user"></i> &nbsp;A B O G A D O S</b></h5>
  </header>

  <?php if ($nro_abogados > 0) { ?>
    <div class="w3-row-padding w3-margin-bottom">
      <div class="w3-quarter">
        <div class="w3-container w3-round w3-red w3-padding-8">
          <div class="w3-left"><i class="fa fa-comment w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 1";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
          <div class="w3-clear"></div>
          <a id = "abogado_01" href="http://localhost/assurance/abogado01/comando.php" target="_blank"><h3>Primer Abogado</h3></a>
        </div>
      </div>
    <?php } ?>

  <?php if ($nro_abogados > 1) { ?>
    <div class="w3-quarter">
      <div class="w3-container w3-blue w3-padding-8">
        <div class="w3-left"><i class="fa fa-eye w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 2";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_02" href="http://localhost/assurance/abogado02/comando.php" target="_blank"><h3>Segundo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 2) { ?>
    <div class="w3-quarter">
      <div class="w3-container w3-teal w3-padding-8">
        <div class="w3-left"><i class="fa fa-share-alt w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 3";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_03" href="http://localhost/assurance/abogado03/comando.php" target="_blank"><h3>Tercer Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 3) { ?>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 4";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_04" href="http://localhost/assurance/abogado04/comando.php" target="_blank"><h3>Cuarto Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 4) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-red w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 5";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_05" href="http://localhost/assurance/abogado05/comando.php" target="_blank"><h3>Quinto Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 5) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-blue w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 6";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_06" href="http://localhost/assurance/abogado06/comando.php" target="_blank"><h3>Sexto Abogado</h3></a>          
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 6) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-teal w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 7";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_07" href="http://localhost/assurance/abogado07/comando.php" target="_blank"><h3>Séptimo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 7) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-orange w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 8";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>        <div class="w3-center">
          <h4>0 Expedientes</h4>
        </div>
        <div class="w3-clear"></div>
        <a id = "abogado_08" href="http://localhost/assurance/abogado08/comando.php" target="_blank"><h3>Octavo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 8) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-red w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 9";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_09" href="http://localhost/assurance/abogado09/comando.php" target="_blank"><h3>Noveno Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 9) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-blue w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 10";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_10" href="http://localhost/assurance/abogado10/comando.php" target="_blank"><h3>Décimo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 10) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-teal w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 11";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_11" href="http://localhost/assurance/abogado11/comando.php" target="_blank"><h3>Onceavo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  <?php if ($nro_abogados > 11) { ?>
    <div class="w3-quarter" style="margin-top:20px;">
      <div class="w3-container w3-orange w3-text-white w3-padding-8">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
          <div class="w3-center">
            <?php 
              $sql = "SELECT * FROM tblabogados WHERE id = 12";
              $query = $pdo->prepare($sql);
              $query->execute();
              $res = $query->fetchAll(PDO::FETCH_ASSOC);
              $t1 = "Expedientes";
              $t2 = "Expediente";
              if ($res[0]['nro_expedientes'] == 0) {
                $cad = "0 " . $t1;
              }
              else {
                $cad = "(1) => " . $t2;
              }
            ?>
            <h4><?php echo $cad; ?></h4>
          </div>
        <div class="w3-clear"></div>
        <a id = "abogado_12" href="http://localhost/assurance/abogado12/comando.php" target="_blank"><h3>Doceavo Abogado</h3></a>
      </div>
    </div>
  <?php } ?>

  </div>

</div>
