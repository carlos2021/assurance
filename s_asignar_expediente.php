<?php include "db_config_exp.php"; ?>
<?php include "configuracion.php"; ?>

<?php

	$depuracion = 0;

	$numero = $_GET['numero']; 

	$origen = ORIGEN . $numero . "\\";
	$destino = REPOSITORIO . $numero . "\\";
	$dir_secuencia = ENTREGABLES . "secuencia.txt";
	$expedientes = EXPEDIENTES;

	if ($depuracion) {
		echo "DIR_SECUENCIA = " . $dir_secuencia . "<br>";
	}

	$secuencia = file_get_contents($dir_secuencia);

	if ($depuracion) {
		echo "SECUENCIA = " . $secuencia . "<br>";
	}

	$entregables = ENTREGABLES . $secuencia . $numero . "\\" . $numero . "\\"; 

	try {
		$sql = "UPDATE tblabogados SET activo = 1, expediente = " . $numero . ", nro_expedientes = 1, id_avance = 2 WHERE id_abogado = " . ID_ABOGADO;
		$query = $pdo->prepare($sql);
		$query->execute();
	} catch (PDOException $e) {
	    die("Error en la consulta");
	    $e->printErrMsg();
	}

	if ($depuracion) {
		echo "<br><br><br><br>";

		echo "NÚMERO = " . $numero . "<br>";
		echo "ORIGEN = " . $origen . "<br>";
		echo "DESTINO = " . $destino . "<br>";
		echo "ENTREGABLES = " . $entregables . "<br>";
		echo "EXPEDIENTES = " . $expedientes . "<br>";
	}

	shell_exec("rmdir " . REPOSITORIO . " /S /Q");
	shell_exec("mkdir " . $destino);

	shell_exec("rmdir " . $expedientes . " /S /Q");

	$instruccion = "robocopy " . $origen . " " . $destino . " /MIR";
	if ($depuracion) {
		echo "INSTRUCCIÓN PARA DESTINO: " . $instruccion . "<br>";
	}	

	shell_exec("robocopy " . $origen . " " . $destino . " /MIR");

	// shell_exec("rmdir " . $entregables . " /s /q");
	// shell_exec("mkdir " . $entregables);

	$instruccion = "robocopy " . $origen . " " . $entregables . " /MIR";

	if ($depuracion) {
		echo "INSTRUCCIÓN PARA ENTREGABLES: " . $instruccion . "<br>";
	}

	shell_exec($instruccion);

	if ($depuracion) {
		// header("location: http://localhost/assurance/" . _ENTIDAD . "/compactar_archivos.php?tipo=0");
	}
	else {
		header("location: http://localhost/assurance/" . _ENTIDAD . "/compactar_archivos.php?tipo=0");
	}
?>