<?php include "funciones.php"; ?>

<?php $depuracion = 1; ?>

<?php 
  if ($depuracion) {
    echo "<br><br>INGRESANDO A PREPARACIÓN DE DETALLES<br><br>";
  }
?>

<?php if ($depuracion) { ?>

  <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>

<?php } ?>

<?php

    $mostrar = 1;
    $mostrar_1 = 1;
    $mostrar_2 = 1;
    $mostrar_44 = 1;
    $mostrar_99 = 1;

    if ($depuracion) {
	     echo "<hr><span style='margin-left:30px;'>PREPARAR DETALLES</span><hr>";
    }

    $sql = "SELECT * FROM tblexpedientes";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $sql = "TRUNCATE TABLE tblpaginas";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    //////////////////////////////////////////////////////////////////////////
    //         SE CREAN LAS CARPETAS Y LAS PÁGINAS DE CADA DOCUMENTO        //
    //////////////////////////////////////////////////////////////////////////

    // SE RECORRE EL CICLO DE EXPEDIENTES. CADA FILA PUEDE SER UN file O UN dir
    // SOLO SE TOMARÁN LOS file. Y ADEMÁS, EL FILE A PROCESAR DEBE TENER EXTENSIÓN
    // pdf. ADICIONALMENTE, SE DEBE TOMAR EN CUENTA SI ES EL ARCHIVO BASE O SI ES
    // UN CUADERNO

    // SE CREAN (O RECREAN) LAS CARPETAS CORRESPONDIENTES

    $nivel_1 = '01PrimeraInstancia';
    $base = _RAIZ;
    $raiz = RAIZ . 'resultados';
    $dir_paginas = RAIZ . 'paginas';
    $dir_exp = RAIZ . 'expedientes';

    shell_exec('rmdir ' . $dir_paginas . ' /s /q');
    shell_exec('mkdir ' . $dir_paginas);

    $carpeta_01 = 'ExpedientesProcesosJudiciales';
    $carpeta_02 = 'ExpedientesProcesosJudicialesPenalesLey906_2004';

    $raiz_base_1 = $raiz . '\\' . $carpeta_01;
    $raiz_base_2 = $raiz_base_1 . '\\' . $carpeta_02;

    shell_exec('rmdir ' . $raiz_base_1 . ' /s /q');
    shell_exec('mkdir ' . $raiz_base_1);

    shell_exec('rmdir ' . $raiz_base_2 . ' /s /q');
    shell_exec('mkdir ' . $raiz_base_2); 

    $raiz = $raiz_base_2;   

    // SE RECORRE LA TABLA DE EXPEDIENTES PARA PROCEDER A LA CREACIÓN DE LAS CARPETAS DERIVADAS

    // SE DEBE TENER EN CUENTA QUE AL CAMBIAR DE EXPEDIENTES SE DEBE GENERAR UNA CARPETA
    // MADRE DE LA CUAL DEPENDEN LOS ARCHIVOS

    $elexpediente = '';

    $i = 0;
    while ($i < count($results)) {

      // Si es un archivo o una carpeta de nivel mayor al de la raiz, se omite.
      if ($results[$i]['nivel'] > 2) {
        $i = $i + 1;
        continue;
      }

      // ESTAMOS EN PRESENCIA DE UN EXPEDIENTE
      if (strcmp($results[$i]['tipo'], 'dir') == 0 && $results[$i]['nivel'] == 0 && strcmp($results[$i]['archivo'], $elexpediente) != 0) {
        // ES UN EXPEDIENTE NUEVO

        $elexpediente = $results[$i]['archivo'];

        if ($depuracion) {
          echo "<br><br>EXPEDIENTE ENCONTRADO = " . $elexpediente . "<br><br>";
        }

        $sql = "SELECT * FROM tblportada WHERE expediente = " . $elexpediente;
        $query = $pdo->prepare($sql);
        $query->execute();
        $datPortadas = $query->fetchAll(PDO::FETCH_ASSOC);

        shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1);
        shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente);
        shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1);

        $carpeta_principal = "C01Principal";

        $sql33 = "UPDATE tblportada SET carpeta_principal = '" . $carpeta_principal . "' WHERE expediente = " . $elexpediente;
        $query33 = $pdo->prepare($sql33);
        $query33->execute();

        // SE BUSCA LA FECHA DE LA PORTADA
        $sql33 = "SELECT * FROM tblportada WHERE expediente = " . $elexpediente;
        $query33 = $pdo->prepare($sql33);
        $query33->execute();
        $resPortada = $query33->fetchAll(PDO::FETCH_ASSOC);

        $fecha_portada = $resPortada[0]['fecha'];

        if ($depuracion) {
          echo "<br><br><br>" . "ENCONTRADA LA PORTADA<br>";
          echo "Fecha de la portada: " . $fecha_portada . "<br><br>";
        }  
      }

      // ESTAMOS EN PRESENCIA DE UN ARCHIVO
      if ($results[$i]['tipo'] == 'file') {
        // SE EVALUA LA EXTENSIÓN
        $fileinfo = pathinfo($results[$i]['archivo']);
        $ext = $fileinfo['extension'];

        // EL ARCHIVO NO ES UN PDF. SE GRABA DE MANERA ESPECIAL
        // NOTA: SI NO ES PDF, NO SE GRABA

        if (strcmp($ext, 'pdf') != 0) { 

        }

        // EL ARCHIVO ES UN PDF, SE PROCESA
        if (strcmp($ext, 'pdf') == 0) {

          if ($mostrar) {
            echo "ARCHIVO = " . $results[$i]['archivo'] . "<br><br>";
          }

          $s = $results[$i]['archivo'];

          $uc = stripos($s, 'cuaderno');
          $upro = stripos($s, 'roceso');

          if ($mostrar) {
            echo "S (cadena de archivo) = " . $s . "<br>";
            echo "UC (posición de cuaderno) = " . $uc . "<br>";
            echo "UPRO (posición de proceso) = " . $upro . "<br>";
          }

          if ($uc == 0 && $upro == 0) {
            $nivel_11 = $s;
          } else if ($uc == 0 && $upro > 0) {
            $nivel_11 = "C01" . "Principal";
          } else if ($uc > 0 && $upro == 0) {
            $num = buscar_numero($s, 'cuaderno');
            while (strlen($num) < 2) {
              $num = '0' . $num;
            }
            $nivel_11 = "C" . $num . "Principal";
          } else { // $uc > 0 && $upro > 0
            $num = buscar_numero($s, 'cuaderno');
            while (strlen($num) < 2) {
              $num = '0' . $num;
            }
            $nivel_11 =  "C" . $num . "Principal";            
          }

          if (strcasecmp($s, 'C01.pdf') == 0 ||
              strcasecmp($s, 'C02.pdf') == 0 ||
              strcasecmp($s, 'C03.pdf') == 0 ||
              strcasecmp($s, 'C04.pdf') == 0 ||
              strcasecmp($s, 'C05.pdf') == 0 ||
              strcasecmp($s, 'C06.pdf') == 0 ||
              strcasecmp($s, 'C07.pdf') == 0 ||
              strcasecmp($s, 'C08.pdf') == 0 ||
              strcasecmp($s, 'C09.pdf') == 0 ||
              strcasecmp($s, 'C10.pdf') == 0 ||
              strcasecmp($s, 'C11.pdf') == 0 ||
              strcasecmp($s, 'C12.pdf') == 0 ||
              strcasecmp($s, 'C13.pdf') == 0 ||
              strcasecmp($s, 'C14.pdf') == 0 ||
              strcasecmp($s, 'C15.pdf') == 0 ||
              strcasecmp($s, 'C16.pdf') == 0 ||
              strcasecmp($s, 'C17.pdf') == 0 ||
              strcasecmp($s, 'C18.pdf') == 0 ||
              strcasecmp($s, 'C19.pdf') == 0 ||
              strcasecmp($s, 'C20.pdf') == 0 ) {
            $nivel_11 = substr($s, 0, 3) . "Principal";
          }

          if ($mostrar) {
            echo "<br><br><br>SE MUESTRAN LOS NIVELES PARA LAS CARPETAS<br><br>";
            echo "0 - nivel_1 = " . $nivel_1 . "<br>";
            echo "1 - nivel_11 = " . $nivel_11 . "<br>";
          }

          // SE CREAN LAS CARPETAS

          shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11);
          shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11); 

          // EN ESTE PUNTO SE CREAN LOS ARCHIVOS, PÁGINA POR PÁGINA, EN LA CARPETA
          // DE PÁGINAS, A PARTIR DEL CONTENIDO DEL ARCHIVO pdf ENCONTRADO

          // POR AHORA, SE IMPRIME EL LINK Y SE CALCULA EL NÚMERO DE
          // PÁGINAS CONTENIDAS EN EL ARCHIVO

          $link = $results[$i]['link'];
          shell_exec('pdfinfo ' . $link . ' > temporal.txt');
          $esarchivo = file_get_contents('temporal.txt');

          if ($mostrar) {
            echo "LINK = ( " . $link . " )" . "<br>";
            echo "esarchivo = " . $esarchivo . "<br>";
          }

          $pos33 = stripos($esarchivo, 'Pages');

          $num33 = '';
          $tw = $pos33;
          while (!is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
            $tw = $tw + 1;
          }
          while (is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
            $num33 = $num33 . $esarchivo[$tw];
            $tw = $tw + 1;
          }
          $nro_paginas = $num33;

          // SE CREAN LAS CARPETAS
          shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11);
          shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11); 

          // EN ESTE PUNTO SE CREAN LOS ARCHIVOS, PÁGINA POR PÁGINA, EN LA CARPETA
          // DE PÁGINAS, A PARTIR DEL CONTENIDO DEL ARCHIVO pdf ENCONTRADO

          // POR AHORA, SE IMPRIME EL LINK Y SE CALCULA EL NÚMERO DE
          // PÁGINAS CONTENIDAS EN EL ARCHIVO

          $link = $results[$i]['link'];
          shell_exec('pdfinfo -meta ' . $link . ' > paginas.csv');
          $csv = array_map('str_getcsv', file('paginas.csv'));

          if ($mostrar) {
            echo "<br>VALOR DEVUELTO POR EL csv <br>" . var_dump($csv);
            echo "<br>";
          }

          $dummy = $csv[6][0];
          $dummy = str_replace('Pages: ', '', $dummy);
          $paginas = intval($dummy);

          // ANÁLISIS: ESTE VALOR SE MANTIENE DENTRO DE LA ITERACIÓN
          // VALIDAR !!!

          $nro_paginas = $paginas;

          if ($mostrar) {
            echo "<br>PÁGINAS CALCULADAS INICIALMENTE = " . $nro_paginas . "<br><br>";
          }

          //////////////////////////////////////////////////////////

          // SE DIVIDE EL ARCHIVO EN PÁGINAS UTILIZANDO pdftk
          // SE MUEVEN LAS PÁGINAS A LA CARPETA CORRESPONDIENTE
          // SE MUEVE TAMBIÉN EL ARCHIVO QUE CONTIENE EL NÚMERO DE PÁGINAS

          $instruccion1 = 'pdftk ' . $results[$i]['link'] . ' burst';
          exec( $instruccion1, $salida, $valor_retornado );

          $instruccion2 = 'move ' . $base . '\\pg*.pdf ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;

          $link_paginas = $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;

          // echo "EL LINK DIRECTORIO DE LAS PÁGINAS : " . $link_paginas . "<br>";

          exec( $instruccion2, $salida, $valor_retornado );

          $instruccion3 = 'move ' . $base . '\\paginas.csv ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;
          exec( $instruccion3, $salida, $valor_retornado );          
        }        
      }
      $i = $i + 1;
    }

    //////////////////////////////////////////////////////////////////////////
    //            FIN CREACIÓN DE CARPETAS Y GENERACIÓN DE ARCHIVOS         //
    //////////////////////////////////////////////////////////////////////////

    $tabla = Array();

    $totPaginas = 0;
    $totExpedientes = 0;

    $s = '<div style="margin-left:30px"><span style="font-weight:bold;">PORTADAS: Archivos PDF base, todos ellos contenidos en los expedientes (nivel 1)</span><br><br>';

    // VERIFICA SI EXISTEN REGISTROS EN LA LISTA DE DOCUMENTOS
    if ($query->rowCount() > 0){
      $s .= '<table border="1">';
      $s .= '<tr>';
      $s .= '<th>&nbsp;Nro&nbsp;</th><th>Archivo</th><th>Link</th>';
      $s .= '</tr>';
      $i = 0;
    	foreach($results as $result) {

        if ($result['nivel'] > 2) {
          continue;
        }

        if ($result['nivel'] == 2 && strcmp($result['tipo'], 'dir') == 0) {
          continue;
        }

        if ($result['nivel'] == 0) {
          // EL ARCHIVO ES UN EXPEDIENTE (NIVEL CERO)
          $totExpedientes = $totExpedientes + 1;
          $expediente = $result['archivo'];
          $i++;
          $s .= '<tr>';
          $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;' . $result['archivo'] . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
          $s .= '</tr>';

        } // CIERRE NIVEL CERO : EXPEDIENTES

        if ($result['nivel'] == 1 && strcmp($result['tipo'], "file") == 0) {

          // ES UN ARCHIVO DE NIVEL 1 (potencialmente archivos, aunque pueden ser directorios)
          $dummy = explode(".", $result['archivo']);
          $ext = $dummy[count($dummy) - 1];

          if (strcmp($ext, "pdf") == 0 /* && strlen($result['archivo']) <= 20 */) {
            $i++;
            $s .= '<tr>';
            $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
            $s .= '<td>' . espacios(1) . '&nbsp;' . $result['archivo'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '</tr>';
            ////////////////////////////////////////////////////////////////////////////////

            $link = $result['link'];
            $destino = $link;
            $destino = str_replace('.pdf', '.txt', $destino);

            // SE CALCULA EL nivel_11

            if ($mostrar_44) {
              echo "I = " . $i . "<br>";
              echo "LONGITUD TABLA = " . count($results) . "<br><br>";
            }

            if ($i == count($results)) {
              $i = $i + 1;
              continue;
            }

            $s = $results[$i]['archivo'];

            $uc = stripos($s, 'cuaderno');
            $upro = stripos($s, 'roceso');

            if ($mostrar) {
              echo "S (cadena de archivo) = " . $s . "<br>";
              echo "UC (posición de cuaderno) = " . $uc . "<br>";
              echo "UPRO (posición de proceso) = " . $upro . "<br>";
            }

            if ($uc == 0 && $upro == 0) {
              $nivel_11 = $s;
            } else if ($uc == 0 && $upro > 0) {
              $nivel_11 = "01" . "Principal";
            } else if ($uc > 0 && $upro == 0) {
              $num = buscar_numero($s, 'cuaderno');
              while (strlen($num) < 2) {
                $num = '0' . $num;
              }
              $nivel_11 = $num . "Principal";
            } else { // $uc > 0 && $upro > 0
              $num = buscar_numero($s, 'cuaderno');
              while (strlen($num) < 2) {
                $num = '0' . $num;
              }
              $nivel_11 = $num . "Principal";            
            }

            if ($mostrar) {
              echo "1 - Nivel11 = " . $nivel_11 . "<br>";
            }

            if ($mostrar) {
              echo "2 - dir_paginas = " . $dir_paginas . "<br>";
              echo "2 - expediente = " . $expediente . "<br>";
              echo "2 - nivel_1 = " . $nivel_1 . "<br>";
              echo "2 - Nivel11 = " . $nivel_11 . "<br>";
            }

            // SE CALCULA EL NÚMERO DE PÁGINAS DEL ARCHIVO A PARTIR DEL CONTENIDO
            // DE paginas.txt CONTENIDO EN LA CARPETA

            $arch_paginas = $dir_paginas . '\\' . $expediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . 'paginas.csv';

            if ($mostrar) {
              echo "arch_paginas = " . $arch_paginas . "<br>";
            }

            $link = $result['link'];
            shell_exec('pdfinfo ' . $link . ' > temporal.txt');
            $esarchivo = file_get_contents('temporal.txt');

            if ($mostrar) {
              echo "LINK = ( " . $link . " )" . "<br>";
              echo "esarchivo = " . $esarchivo . "<br>";
            }

            $pos33 = stripos($esarchivo, 'Pages');

            $num33 = '';
            $tw = $pos33;
            while (!is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
              $tw = $tw + 1;
            }
            while (is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
              $num33 = $num33 . $esarchivo[$tw];
              $tw = $tw + 1;
            }
            $nro_paginas = $num33;

            /* // CICLO DE PROCESAMIENTO DE CADA ARCHIVO
            // VERSIÓN PREVIA UTILIZANDO tet, VERSIÓN AVANZADA

            $h = 0;
            while ($h < $nro_paginas) {

                // AQUÍ SE EXTRAE EL CONTENIDO TEXTUAL DE CADA UNA DE LAS PÁGINAS
                // DEL ARCHIVO. SE UTILIZARÁ tet EN LUGAR DE pdftotext. EL PROGRAMA
                // tet ES MÁS PRECISO. EN SU VERSIÓN DE PRUEBA SOLO ADMITE UNA PÁGINA
                // Y UN ARCHIVO DE TAMAÑO MENOR A 1 MEGABYTE

                $elfile = strval($h + 1);
                while (strlen($elfile) < 4) {
                  $elfile = '0' . $elfile;
                }

                $elorigen = $dir_paginas . '\\' . $expediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . 'pg_' . $elfile . '.pdf';
                $fileinfo = pathinfo($elorigen);
                $eldestino = $fileinfo['basename'];
                $eldestino = str_replace('.pdf', '.txt', $eldestino);

                $mostrar = 0;
                if ($mostrar) {
                  echo "ENLACE ORIGEN. ciclo = " . $h . " ORIGEN = " . $elorigen . " (validar este enlace) => ";
                  echo "Y EL DESTINO ES = " . $eldestino . "<br>";
                }
                $mostrar = 0;

                exec( "tet " . $elorigen, $salida, $retorno );

                // exec("pdftotext -f " . strval($h) . " -l " . strval($h) . " " . $link . " " . $destino, $salida, $retorno);  

                $contenido = file_get_contents($eldestino);
                $contenido = utf8_encode($contenido);
                $contenido = slugify($contenido);

                $nro_pag = $h + 1;  // SE SUMA 1 AL VALOR DE LA ITERCIÓN, PUES MIENTRAS QUE EL
                                    // CICLO EMPIEZA CON CERO, LAS PÁGINAS EMPIEZAN CON 1
                $id_criterio = 15;

                // ESTE CAMPO DEBE MODIFICARSE CON BASE EN EL CONTENIDO DE LA PÁGINA
                $aux = "10:30pm 03 28 2022";
                $d = strtotime($aux);
                $fecha = date('Y-m-d h:i:s a', $d);

                $pagina = $h + 1; // VALIDAR ESTA INSTRUCCIÓN, SEGÚN CONTENIDO DE LA TABLA detalles
                $criterio = 0;
                if (strlen($contenido) <= 200) {
                  $criterio = 99999;
                }

                $archivo = $result['archivo'];
                $enlace = $result['link'];

                $arr = Array();
                $arr[] = $expediente;
                $arr[] = $archivo;
                $arr[] = $criterio;
                $arr[] = $pagina;
                $arr[] = $fecha;
                $arr[] = $enlace;
                $arr[] = $contenido;

                $tabla[] = $arr;

                /////////////////////////////////////////////////////////////////////////
                try {

                  $sql = "INSERT INTO tblpaginas (expediente, archivo, pagina, fecha, link, contenido, criterio) VALUES (:expediente, :archivo, :pagina, :fecha, :link, :contenido, :criterio)";
                  $stmt = $pdo->prepare($sql);
                  $stmt->bindParam(':expediente', $expediente, PDO::PARAM_STR);
                  $stmt->bindParam(':archivo', $archivo, PDO::PARAM_STR);
                  $stmt->bindParam(':pagina', $pagina, PDO::PARAM_INT);
                  $stmt->bindParam(':fecha', $fecha, PDO::PARAM_STR);
                  $stmt->bindParam(':link', $enlace, PDO::PARAM_STR);
                  $stmt->bindParam(':contenido', $contenido, PDO::PARAM_STR);
                  $stmt->bindParam(':criterio', $criterio, PDO::PARAM_INT);
                  $stmt->execute();
                    
                } catch (Exception $ex) {
                  print_r($ex);
                }
                /////////////////////////////////////////////////////////////////////////

                // if ($h == 2) {
                  //$ciclo = 0;                  
                //}

                $h = $h + 1;

            } // CIERRE CICLO DE PROCESAMIENTO DE CADA ARCHIVO */

            // CICLO DE PROCESAMIENTO DE CADA ARCHIVO
            // VERSIÓN PREVIA UTILIZANDO pdftotext



            $h = 0;
            while ($h < $nro_paginas) {
                $h = $h + 1;

                // AQUÍ SE EXTRAE EL CONTENIDO TEXTUAL DE CADA UNA DE LAS PÁGINAS
                // DEL ARCHIVO. SE UTILIZARÁ tet EN LUGAR DE pdftotext. EL PROGRAMA
                // tet ES MÁS PRECISO. EN SU VERSIÓN DE PRUEBA SOLO ADMITE UNA PÁGINA
                // Y UN ARCHIVO DE TAMAÑO MENOR A 1 MEGABYTE

                $elfile = strval($h);
                while (strlen($elfile) < 4) {
                  $elfile = '0' . $elfile;
                }

                $elorigen = $dir_paginas . '\\' . $expediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . 'pg_' . $elfile . '.pdf';
                $fileinfo = pathinfo($elorigen);
                $eldestino = $fileinfo['basename'];

                if ($mostrar) {
                  echo "ENLACE ORIGEN. ciclo = " . $h . " ORIGEN = " . $elorigen . " (validar este enlace) => ";
                  echo "Y EL DESTINO ES = " . $eldestino . "<br>";
                }

                exec("pdftotext -f " . strval($h) . " -l " . strval($h) . " " . $link . " " . $destino, $salida, $retorno); 

                $contenido = file_get_contents($destino);

                $direccion_documento = $link_paginas . '\\pg_' . $elfile . '.pdf';

                /* if ($mostrar_99) {
                  echo "DIRECCIÓN DOCUMENTO: " . $direccion_documento . "<br>";
                  echo "TAMAÑO: " . strlen($contenido) . " PÁGINA: " . $h . "<hr>";
                }

                if ($h == 85 || $h == 131) {
                  echo "CONTENIDO = " . $contenido . "<hr>";
                }

               if (strlen($contenido) <= 350) {
                  $pdf = $parser->parseFile($direccion_documento);
                  $contenido = $pdf->getText(); 
                  if ($mostrar_99) {
                    echo "PROCESADA LA NUEVA CARGA CON PdfParser !!!" . "<hr><br>";
                  }                 
                } */

                $contenido = slugify($contenido);

                $contenido = str_replace('Á', 'á', $contenido);
                $contenido = str_replace('É', 'é', $contenido);
                $contenido = str_replace('Í', 'í', $contenido);
                $contenido = str_replace('Ó', 'ó', $contenido);
                $contenido = str_replace('Ú', 'ú', $contenido);
                $contenido = strtolower($contenido);
                $contenido = str_replace('.', ' ', $contenido);
                $contenido = str_replace(',', ' ', $contenido);
                $contenido = str_replace('/', ' ', $contenido);
                $contenido = str_replace('\\', ' ', $contenido);
                $contenido = str_replace('!', ' ', $contenido);
                $contenido = str_replace('¡', ' ', $contenido);
                $contenido = str_replace('*', ' ', $contenido);
                $contenido = str_replace('^', '', $contenido); 
                $contenido = str_replace('«', '', $contenido); 
                $contenido = str_replace('(', ' ', $contenido); 
                $contenido = str_replace(')', ' ', $contenido); 
                $contenido = str_replace('<', ' ', $contenido); 
                $contenido = str_replace('>', ' ', $contenido);
                $contenido = str_replace('«', ' ', $contenido); 
                $contenido = str_replace('|', ' ', $contenido);
                $contenido = str_replace('i«', ' ', $contenido);  
 
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);

                $contenido = utf8_encode($contenido);                 

                if ($mostrar)
                  echo "(" . strval($h) . "-" . strval($h) . ") : CONTENIDO = " . strlen($contenido) . "<br>"; 

                $nro_pag = $h;
                $id_criterio = 15;

                $aux = "2019-01-28";
                $d = strtotime($aux);
                $fecha = date('Y-m-d', $d);

                $pagina = $h;
                $criterio = 0;
                if (strlen($contenido) <= 350) {
                  $criterio = 99999;
                }

                $archivo = $result['archivo'];
                $enlace = $result['link'];

                $arr = Array();
                $arr[] = $expediente;
                $arr[] = $archivo;
                $arr[] = $criterio;
                $arr[] = $pagina;
                $arr[] = $fecha;
                $arr[] = $enlace;
                $arr[] = $contenido;

                $tabla[] = $arr;


                // PENDIENTE ENCONTRAR LA FECHA 

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                //          $fecha = encontrar_fecha($contenido);

                $fecha = '2019-01-28'; // TEMPORAL
                $fecha_exp = '2019-01-28';

                if ($h == 1)
                  $fecha_exp = $fecha;

                if (strcmp($fecha, 'fecha-no-valida') == 0 || strcmp($fecha, 'fecha-no-encontrada') == 0) {
                  $fecha = $fecha_exp;
                }

                if ($mostrar_1) {
                  echo "PÁGINA = " . $h . " FECHA = " . $fecha . " FECHA_EXP = " . $fecha_exp . "<br>";
                }

                /////////////////////////////////////////////////////////////////////////
                try {

                  $sql = "INSERT INTO tblpaginas (expediente, archivo, pagina, fecha, link, contenido, criterio) VALUES (:expediente, :archivo, :pagina, :fecha, :link, :contenido, :criterio)";
                  $stmt = $pdo->prepare($sql);
                  $stmt->bindParam(':expediente', $expediente, PDO::PARAM_STR);
                  $stmt->bindParam(':archivo', $archivo, PDO::PARAM_STR);
                  $stmt->bindParam(':pagina', $pagina, PDO::PARAM_INT);
                  $stmt->bindParam(':fecha', $fecha, PDO::PARAM_STR);
                  $stmt->bindParam(':link', $enlace, PDO::PARAM_STR);
                  $stmt->bindParam(':contenido', $contenido, PDO::PARAM_STR);
                  $stmt->bindParam(':criterio', $criterio, PDO::PARAM_INT);
                  $stmt->execute();
                    
                } catch (Exception $ex) {
                  print_r($ex);
                }
                /////////////////////////////////////////////////////////////////////////

                // if ($h == 2) {
                  //$ciclo = 0;                  
                //}      
            } // CIERRE CICLO DE PROCESAMIENTO DE CADA ARCHIVO */

            $totPaginas = $totPaginas + $h;

            ////////////////////////////////////////////////////////////////////////////////
          }
        }
    	} // CIERRE DEL CICLO DE PÁGINAS
      $s .= '</table></div>';

      if ($depuracion) {
        echo $s;
      }

    } // CIERRE DEL if QUE CONTROLA LA EXISTENCIA DE PÁGINAS

    /////////////////////////////////////////////////////////////////////////////
    //        CORRECCIÓN (TEMPORAL !!!) DE LAS PÁGINAS CON FECHA ERRÓNEA       //
    /////////////////////////////////////////////////////////////////////////////


    // TEMPORAL

    /*

    $sql20 = "SELECT * FROM tblpaginas";
    $query20 = $pdo->prepare($sql20);
    $query20->execute();
    $filas = $query20->fetchAll(PDO::FETCH_ASSOC);

    if ($mostrar_2)
      echo "PROCESO DE CORRECCIÓN DE FECHAS ERRÓNEAS<br><br>";
    
    $i = 0;
    while ($i < count($filas)) {
      if (strcmp($filas[$i]['fecha'],'0000-00-00') == 0) {
        if ($mostrar_2)
          echo "ERROR EN I = " . $i . " FECHA = " . $filas[$i]['fecha'] . "<br>";
        $k = $i + 1;
        while ($k < count($filas)) {
          if (strcmp($filas[$k]['fecha'],'0000-00-00') != 0) {
            $filas[$i]['fecha'] = $filas[$k]['fecha'];
            if ($mostrar_2) {
              echo "SE VA A PRODUCIR EL CAMBIO<br>";
              echo "K = " . $k . " FECHA = " . $filas[$k]['fecha'] . "<br>";
              echo "EL ID EN DONDE SE GRABA = " . $filas[$i]['id'] . "<br>";
            }
            $sql30 = "UPDATE tblpaginas SET fecha = '" . $filas[$k]['fecha'] . "' WHERE id = " . $filas[$i]['id'];
            $query30 = $pdo->prepare($sql30);
            $query30->execute();
            break;
          }
          $k = $k + 1;
        }
      }
      if ($mostrar_2)
        echo "I = " . $i . " FECHA = " . $filas[$i]['fecha'] . "<br>";
      $i = $i + 1;
    } */

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////   S E G U N D A  P A S A D A  /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE RECORREN TODAS LAS PÁGINAS Y SE EVALÚAN NUEVAMENTE LAS TRD QUE SON PROBLEMÁTICAS
    // POR TEMA DE CRUCE CON OTRAS TRD. EL OBJETIVO ES NO MODIFICAR DE NUEVO LOS PATRONES O
    // EXCEPCIONES (para que no se reflejen en los restantes TRD) Y REALIZAR LAS
    // MODIFICACIONES DE MANERA PUNTUAL.
    ////////////////////////////////////////////////////////////////////////////////////


    // TEMPORALMENTE SE DESHABILITAN

    /* 
    define("OFICIO", 612);
    define("ESCRITOACUSACION", 583);

    $es = array();
    $es[0] = ESCRITOACUSACION;

    $debe_ser = array();
    $debe_ser[0] = OFICIO;

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $k = 0;
    $i = 0;
    while ($i < count($results)) {
      if ($results[$i]['criterio'] == ESCRITOACUSACION) {
        $pos1 = stripos($results[$i]['contenido'], 'proceso penal');
        $pos2 = stripos($results[$i]['contenido'], 'oficio');
        if ($pos1 > 0 && $pos2 == 0) {
          // SE DEBE CAMBIAR EL CRITERIO
          echo "<hr><hr>FINAL FINAL cambiando escrito acusación por oficio !!! <hr><hr>";
        }
      }
      $i = $i + 1;
    } */

  $pdo = null;

  ?>






  <?php if ($depuracion) { ?>

    <div style="margin-left:30px;">
      <hr>PROCESO CONCLUIDO<hr>
    </div>

    <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
    <a href="index.php"><button>ATRÁS</button></a>

  <?php } ?>

  <?php
    if ($depuracion) { 
      echo "<br><br>SALIENDO !!! DE: preparación de detalles<br><br>";
    }
  ?>

