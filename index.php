<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('configuracion');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>C A R T A G E N A</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Procesamiento Masivo</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">
    <form method="post" action="s_modificar_configuracion.php">
      <div class="w3-row-padding" style="margin:0 -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-calendar-o"></i> <span style="font-weight: bold;">REPOSITORIO</span></label>
          <input class="w3-input w3-border" type="text" style="font-size: 16px;" value="<?php echo $repositorio; ?>" placeholder="Carpeta del Repositorio" name="repositorio" readonly required>
        </div>
        <div class="w3-half">
          <label><i class="fa fa-calendar-o"></i> <span style="font-weight: bold;">ORIGEN DE LOS DATOS</span></label>
          <input class="w3-input w3-border" type="text" style="font-size: 16px;" value="<?php echo $origen; ?>" placeholder="Carpeta de las Descargas" name="descarga" readonly required>
        </div>
      </div>
      <div class="w3-row-padding" style="margin:8px -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">Nro ABOGADOS</span></label>
          <input class="w3-input w3-border" type="number" style="font-size: 18px;" value="<?php echo $nro_abogados; ?>" name="nro_abogados" min="0" max="12">
        </div>
        <div class="w3-half">
          <label><i class="fa fa-child"></i> <span style="font-weight: bold;">ESTADO</span></label>
        <input class="w3-input w3-border" type="text" style="font-size: 16px;" value="<?php echo $nombre_estado; ?>" name="estado" readonly>
        </div>
      </div>
      <div class="w3-row-padding" style="margin:8px -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">Nro EXPEDIENTES</span></label>
          <input class="w3-input w3-border" type="number" style="font-size: 18px;" value="<?php echo $nroExpedientes; ?>" name="nro_expedientes" readonly>
        </div>
      </div>
      <button class="w3-button w3-dark-grey" type="file"><i class="fa fa-search w3-margin-right"></i> <span style="font-weight: bold;">SUBIR EXPEDIENTE</span></button>
    </form>
  </div>

  <!-- End page content -->
</div>
