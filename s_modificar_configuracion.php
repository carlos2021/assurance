<?php include "funciones.php"; ?>

<?php 
	$otros_abogados = 0;

	$nro_abogados = $_POST['nro_abogados'];

	$sql = "UPDATE tblconfiguracion SET nro_abogados = " . $nro_abogados;
	$query = $pdo->prepare($sql);
	$query->execute();

	// SE BORRAN LAS CARPETAS DE LOS ABOGADOS
	
	if ($nro_abogados >= 1) {
	    shell_exec('rmdir ' . EXPEDIENTES . ' /s /q');
	    shell_exec('mkdir ' . EXPEDIENTES);
	    shell_exec('rmdir ' . RESULTADOS . ' /s /q');
	    shell_exec('mkdir ' . RESULTADOS);
	    shell_exec('rmdir ' . REPOSITORIO . ' /s /q');
	    shell_exec('mkdir ' . REPOSITORIO);
	}

	if ($otros_abogados) {
		if ($nro_abogados >= 2) {
			$abogado_02 = $raiz . "abogado02\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_02 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_02);
		}

		if ($nro_abogados >= 3) {
			$abogado_03 = $raiz . "abogado03\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_03 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_03);
		}

		if ($nro_abogados >= 4) {
			$abogado_04 = $raiz . "abogado04\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_04 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_04);
		}

		if ($nro_abogados >= 5) {
			$abogado_05 = $raiz . "abogado05\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_05 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_05);
		}

		if ($nro_abogados >= 6) {
			$abogado_06 = $raiz . "abogado06\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_06 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_06);
		}

		if ($nro_abogados >= 7) {
			$abogado_07 = $raiz . "abogado07\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_07 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_07);
		}

		if ($nro_abogados >= 8) {
			$abogado_08 = $raiz . "abogado08\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_08 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_08);
		}

		if ($nro_abogados >= 9) {
			$abogado_09 = $raiz . "abogado09\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_09 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_09);
		}

		if ($nro_abogados >= 10) {
			$abogado_10 = $raiz . "abogado10\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_10 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_10);
		}

		if ($nro_abogados >= 11) {
			$abogado_11 = $raiz . "abogado11\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_11 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_11);
		}

		if ($nro_abogados == 12) {
			$abogado_12 = $raiz . "abogado12\\expedientes\\PEX01\\";
		    shell_exec('rmdir ' . $abogado_12 . ' /s /q');
		    shell_exec('mkdir ' . $abogado_12);
		}
	}

	// SE COLOCA A CERO EL NÚMERO DE EXPEDIENTES DE CADA NUEVO ABOGADO
	if ($nro_abogados >= 1) {
		$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 1";
		$query = $pdo->prepare($sql);
		$query->execute();
	}

	if ($otros_abogados) {
		if ($nro_abogados >= 2) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 2";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 3) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 3";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 4) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 4";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 5) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 5";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 6) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 6";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 7) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 7";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 8) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 8";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 9) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 9";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 10) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 10";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados >= 11) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 11";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
		if ($nro_abogados == 12) {
			$sql = "UPDATE tblabogados SET nro_expedientes = 0, id_avance = 1 WHERE id = 12";
			$query = $pdo->prepare($sql);
			$query->execute();
		}
	}

	header("Location: p_subir_expedientes.php?procesado=inicio");
?>