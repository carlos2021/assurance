<?php include "funciones.php"; ?>
<?php include "configuracion.php"; ?>

<?php

	$elExpediente = $argv[1];
	$elArchivo = $argv[2];

	$depuracion = 0;

?>

<?php
	require('fpdf/fpdf.php');

	class PDF extends FPDF
	{
		// Cabecera de página
		function Header()
		{

			$this->Image('img/logo.png',10,8,33);
			// Arial bold 15
			$this->SetFont('Arial','B',10);
			// Movernos a la derecha
			$this->Cell(80);
			// Título
			$this->Cell(30,8,t('ÍNDICE ELECTRÓNICO DEL EXPEDIENTE JUDICIAL'),0,0,'C');
			// Salto de línea
			$this->Ln();

		}

		// Pie de página
		function Footer()
		{
			// Posición: a 1,5 cm del final
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',9);
			// Número de página
			$this->Cell(0,10,t('Página: ').$this->PageNo().'/{nb}',0,0,'C');
		}

		function ExtraerPortada() {

			global $elExpediente;
			global $elArchivo;
			global $pdo;

			$sql = "SELECT * FROM tblportada WHERE expediente = '" . $elExpediente . "'";
			$query = $pdo->prepare($sql);
			$query->execute();
			$datPortada = $query->fetchAll(PDO::FETCH_ASSOC);
			return $datPortada;
		}

		function ExtraerDetalles() {

			global $elExpediente;
			global $elArchivo;
			global $pdo;

			$sql = "SELECT * FROM tblsalida WHERE expediente = '" . $elExpediente . "' AND archivo = '" . $elArchivo . "'";
			$query = $pdo->prepare($sql);
			$query->execute();
			$datDetalles = $query->fetchAll(PDO::FETCH_ASSOC);
			return $datDetalles;
		}

		function serie_subserie($serie, $subserie) {
			global $pdo;

			$sql = "SELECT * FROM tbltrd WHERE serie = " . $serie . " AND subserie = " . $subserie;
			$query = $pdo->prepare($sql);
			$query->execute();
			$datSubserie = $query->fetchAll(PDO::FETCH_ASSOC);
			return $datSubserie;
		}

		function AlturaPortada($s, $n) {
			$lon = $this->GetStringWidth($s);
			$lon = intval($lon);
			if ($lon > 60) {
				return 4;
			}
			else {
				return 8;
			}
		}

		function TablaPortadaBase($data) {

			$col1 = 10;
			$col2 = 50;
			$fila = 25;
			$delta_fila = 8;
			$fila_unica = 8;
			$fila_media = 4;
			$anchura1 = 40;
			$anchura2 = 60;

			$col3 = 120;
			$col4 = 155;
			$anchura3 = 70;
			$anchura3_1 = 35;
			$anchura3_2 = 35;

			$laciudad = substr($data[0]['ciudad'], 0, 60);
			$eldespacho = substr($data[0]['despacho'], 0, 60);
			$laparteprocesala = substr($data[0]['parte_procesal_a'], 0, 60);
			$laparteprocesalb = substr($data[0]['parte_procesal_b'], 0, 60);
			$elproceso = substr($data[0]['proceso'], 0, 60);
			$eltercero = substr($data[0]['terceros'], 0, 60);

			$this->SetFont('Times','',8);

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Ciudad'),1);
			$this->SetXY($col2,$fila);
			$this->MultiCell($anchura2,$fila_unica,t($laciudad),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Despacho Judicial'),1);
			$this->SetXY($col2,$fila);

			$f = $this->AlturaPortada($data[0]['despacho'], 0);
			$this->MultiCell($anchura2,$f,t($eldespacho),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Serie o Subserie Documental'),1);
			$this->SetXY($col2,$fila);
			$datSubserie = $this->serie_subserie($data[0]['serie'], $data[0]['subserie']);	

			$f = $this->AlturaPortada($datSubserie[0]['tipo_doc'], 15);
			$f = 4;
			$this->MultiCell($anchura2,$f,t($datSubserie[0]['tipo_doc']),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Nro. Radicación del Proceso'),1);
			$this->SetXY($col2,$fila);
			$this->MultiCell($anchura2,$fila_unica,t($data[0]['radicacion']),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$f = $this->AlturaPortada('Partes Procesales (Parte A) (demandado, procesado)', 50);
			$f = 4;
			$this->MultiCell($anchura1,$f,t('Partes Procesales (Parte A) (demandado, procesado)'),1);
			$this->SetXY($col2,$fila);

			$f = $this->AlturaPortada($data[0]['parte_procesal_a'], 50);
			$this->MultiCell($anchura2,$f,t($laparteprocesala),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$f = $this->AlturaPortada('Partes Procesales (Parte B) (demandante, denunciante)', 50);
			$this->MultiCell($anchura1,$f,t('Partes Procesales (Parte B) (demandante, denunciante)'),1);
			$this->SetXY($col2,$fila);

			$f = $this->AlturaPortada($data[0]['parte_procesal_b'], 50);
			$this->MultiCell($anchura2,$f,t($laparteprocesalb),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Terceros Intervinientes'),1);
			$this->SetXY($col2,$fila);

			$f = $this->AlturaPortada($data[0]['terceros'], 70);
			$this->MultiCell($anchura2,$f,t($eltercero),1,'L');
			$fila = $fila + $delta_fila;

			$this->SetXY($col1,$fila);
			$this->Cell($anchura1,$fila_unica,t('Cuaderno'),1);

			$this->SetXY($col2,$fila);
			$this->MultiCell($anchura2,$fila_unica,t($data[0]['carpeta_principal']),1,'L');
			$fila = $fila + $delta_fila;

			//////////////////////////////////////////////////////////////////////

			$fila = 25;
			$this->SetXY($col3, $fila);
			$this->SetFont('Times','B',8);
			$this->Cell($anchura3, $fila_unica,sp(27).t('EXPEDIENTE FÍSICO'),1,'C');
			$fila = $fila + $delta_fila;

			$this->SetXY($col3, $fila);
			$this->SetFont('Times','',8);
			$this->MultiCell($anchura3_1,$fila_media,t('El expediente judicial posee documentos físicos:'),1);

			$nct = intval($data[0]['nro_carpetas']);
			$ncd = intval($data[0]['nro_carpetas_digital']);
			if ($nct > $ncd) {
				$cf = "Si";
			}
			else {
				$cf = "Si";
			}
			$this->SetXY($col4,$fila);
			$this->Cell($anchura3_2,$fila_unica,sp(20).t($cf),1);
			$fila = $fila + $delta_fila;

			$this->SetXY($col3, $fila);
			$this->MultiCell($anchura3_1,$fila_media,t('No. de carpetas (cuadernos), legajos o tomos:'),1);
			$this->SetXY($col4,$fila);
			$this->Cell($anchura3_2,$fila_unica,sp(20).t($data[0]['nro_carpetas']),1);
			$fila = $fila + $delta_fila;

			$this->SetXY($col3, $fila);
			$f = 4;
			$this->MultiCell($anchura3_1,$f,t('No. de carpetas (cuadernos), legajos o tomos digitalizados'),1);
			$this->SetXY($col4,$fila);
			$this->Cell($anchura3_2,$fila_unica,sp(20).t($data[0]['nro_carpetas_digital']),1);
			$fila = $fila + $delta_fila;

		}

		function TablaDetalles($data) {

			/* $i = 0;
			while ($i < count($data)) {
				echo "I = " .$i . " CARPETA = " . $data[$i]['lacarpeta'] . "<br><br>";
				$i = $i + 1;
			} */

			$col1 = 10;
			$col2 = 50;
			$col3 = 65;
			$col4 = 81;
			$col5 = 95;
			$col6 = 107;
			$col7 = 117;
			$col8 = 127;
			$col9 = 139;
			$col10 = 151;
			$col11 = 165;
			$col12 = 90;

			$fila = 100;

			$fila_inicial = 30;

			$anchura1 = 40;
			$anchura2 = 15;
			$anchura3 = 16;
			$anchura4 = 14;
			$anchura5 = 12;
			$anchura6 = 10;
			$anchura7 = 10;
			$anchura8 = 12;
			$anchura9 = 12;
			$anchura10 = 14;
			$anchura11 = 30;
			$anchura12 = 80;
			$anchura13 = 17;
			$anchuraTotal = 185;

			$fila_unica = 8;
			$fila_media = 4;
			$fila_tercio = 2.65;

			$this->SetFont('Times','',7);

			$this->SetXY($col1,$fila);
			$this->SetFillColor(214,219,228);
			$this->Cell($anchura1,$fila_unica,sp(10).t('Nombre Documento'),1,1,'L',true);			

			$this->SetXY($col2,$fila);
			$this->MultiCell($anchura2,$fila_tercio,t('Fecha Creación Documento'),1,'C','L',true);	

			$this->SetXY($col3,$fila);
			$this->MultiCell($anchura3,$fila_tercio,t('Fecha Incorporación Expediente'),1,'C','L',true);

			$this->SetXY($col4,$fila);
			$this->MultiCell($anchura4,$fila_media,t('Orden Documento'),1,'C','L',true);

			$this->SetXY($col5,$fila);
			$this->MultiCell($anchura5,$fila_media,t('Número Páginas'),1,'C','L',true);

			$this->SetXY($col6,$fila);
			$this->MultiCell($anchura6,$fila_media,t('Página Inicio'),1,'C','L',true);

			$this->SetXY($col7,$fila);
			$this->MultiCell($anchura7,$fila_media,t('Página Fin'),1,'C','L',true);

			$this->SetXY($col8,$fila);
			$this->MultiCell($anchura8,$fila_unica,t('Formato'),1,'C','L',true);

			$this->SetXY($col9,$fila);
			$this->MultiCell($anchura9,$fila_unica,t('Tamaño'),1,'C','L',true);

			$this->SetXY($col10,$fila);
			$this->MultiCell($anchura10,$fila_unica,t('Origen'),1,'C','L',true);

			$this->SetXY($col11,$fila);
			$this->MultiCell($anchura11,$fila_unica,t('Observaciones'),1,'C','L',true);

			//////////////////////////////////////////////////////////////////////////

			$i = 0;
			while ($i < count($data)) {

				// echo "LA CARPETA = " . $data[$i]['lacarpeta'] . "<br>";

				// $data[$i]['lacarpeta'] = str_replace('c:\xampp\htdocs\assurance\cucuta', 'c:\expedientes', $data[$i]['lacarpeta']);

				// echo "NUEVA CARPETA = " . $data[$i]['lacarpeta'] . "<br>";

				$this->SetX($col1);
				$this->Cell($anchura1,$fila_unica,'',1,'L','L');

				$this->SetX($col1);


				$dummy = $data[$i]['nombre_criterio'];

				$anty = $this->GetY();

				if (strlen($data[$i]['nombre_criterio']) > 32) {
					$nf = 32;
					while ($nf > 0 && strcasecmp($dummy[$nf], ' ') != 0) {
						$nf = $nf - 1;
					}
					$dummy1 = substr($dummy, 0, $nf);
					$dummy2 = substr($dummy, $nf + 1);

					$dummy2 = substr($dummy2, 0, 32);

					$this->SetY($anty - 3);
					$this->Write(10, $dummy1,$data[$i]['lacarpeta']);
					$this->SetY($anty);
					$this->Write(10, $dummy2,$data[$i]['lacarpeta']);
				}
				else {
					$this->Write(10, $data[$i]['nombre_criterio'],$data[$i]['lacarpeta']);	
				}

				$this->SetX($col2);
				$this->Cell($anchura2,$fila_unica,t($data[$i]['creacion']),1,'L','L');
				$this->SetX($col3);
				$this->Cell($anchura3,$fila_unica,t($data[$i]['incorporacion']),1,'L','L');
				$this->SetX($col4);
				$this->Cell($anchura4,$fila_unica,sp(8).t($data[$i]['orden']),1,'L','L');
				$this->SetX($col5);
				$this->Cell($anchura5,$fila_unica,sp(8).t($data[$i]['nro_paginas']),1,'L','L');
				$this->SetX($col6);
				$this->Cell($anchura6,$fila_unica,sp(5).t($data[$i]['pag_inicial']),1,'L','L');		
				$this->SetX($col7);
				$this->Cell($anchura7,$fila_unica,sp(5).t($data[$i]['pag_final']),1,'L','L');
				$this->SetX($col8);
				$this->Cell($anchura8,$fila_unica,sp(5).t($data[$i]['formato']),1,'L','L');
				$this->SetX($col9);
				$this->Cell($anchura9,$fila_unica,sp(6).t($data[$i]['tamanio']),1,'L','L');
				$this->SetX($col10);
				$this->Cell($anchura10,$fila_unica,t($data[$i]['origen']),1,'L','L');

				$this->SetX($col11);

				$this->SetFillColor(255,255,255);

				if (intval($this->GetStringWidth($data[$i]['observaciones'])) <= 30) { 
					$this->Cell($anchura11,$fila_unica,t($data[$i]['observaciones']),1,'L','L');
					$this->Ln();
				}
				else {
					$this->MultiCell($anchura11,$fila_media,t($data[$i]['observaciones']),1,'L','L');
				}

				$i = $i + 1;
			}

			$this->SetFont('Arial','',7);			
			$this->SetFillColor(0,0,139);
			$this->SetTextColor(255,255,255);
			$this->SetX($col1);
			$this->Cell($anchuraTotal,6,t('FECHA DE CIERRE DEL EXPEDIENTE: '.'15/04/2022'),1,'L','L',true);
			$this->SetTextColor(0,0,0);	
			$this->Ln();

			$this->SetFillColor(200,220,255);

			$this->SetFont('Times','',6);
			$laFila = $this->GetY();

			$this->Cell($anchura12,8,t('Número de cuadernos del expedientes, (diligencia al momento del archivo definitivo'),1,1);	

			$this->SetXY($col12, $laFila);
			$this->Cell($anchura13,8,t(' '),1,1);							

		}
	}

	// Creación del objeto de la clase heredada
	$pdf = new PDF();

	$datPortada = $pdf->ExtraerPortada();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Times','',12);
	$datPortada = $pdf->ExtraerPortada();

	$pdf->TablaPortadaBase($datPortada);
	$datDetalles = $pdf->ExtraerDetalles();

	$datSubserie = $pdf->serie_subserie(270,245);

	/* $i = 0;
	while ($i < count($datDetalles)) {
		echo "I = " .$i . " CARPETA = " . $datDetalles[$i]['lacarpeta'] . "<br><br>";
		$i = $i + 1;
	} */

	$pdf->TablaDetalles($datDetalles);
	$pdf->Output('F', $datDetalles[0]['destinoindice']);

	///////////////////////////////// GENERACIÓN EXCEL //////////////////////////////////

	require 'vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $style = $spreadsheet->getDefaultStyle();

    $style->getFont()->setName('Calibri');
    $style->getFont()->setSize(11);

	$sheet->getRowDimension('1')->setRowHeight(65);
	$sheet->getStyle('A1:Z300')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
    $drawing->setName('Paid');
    $drawing->setDescription('Paid');
    $drawing->setPath('img/logo.png'); 
    $drawing->setCoordinates('A1');
    $drawing->setOffsetX(10);
    $drawing->setOffsetY(10);
    $drawing->setRotation(0);
    $drawing->getShadow()->setVisible(false);
    $drawing->getShadow()->setDirection(0);
    $drawing->setWorksheet($spreadsheet->getActiveSheet());

	$sheet->getStyle('A1:K1')->getAlignment()->setHorizontal('center');

    $sheet->mergeCells('A1:K1');
	$sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

	$sheet->setCellValue('A1', 'ÍNDICE ELECTRÓNICO DEL EXPEDIENTE JUDICIAL');
	$sheet->getStyle('A1')->getFont()->setSize(14);
	$sheet->getStyle('A1')->getFont()->setBold(true);

	$sheet->getColumnDimension('A')->setWidth(35);
	$sheet->getColumnDimension('B')->setWidth(16);
	$sheet->getColumnDimension('C')->setWidth(16);
	$sheet->getColumnDimension('D')->setWidth(13);
	$sheet->getColumnDimension('E')->setWidth(10);
	$sheet->getColumnDimension('F')->setWidth(9);
	$sheet->getColumnDimension('G')->setWidth(9);
	$sheet->getColumnDimension('H')->setWidth(12);
	$sheet->getColumnDimension('I')->setWidth(12);
	$sheet->getColumnDimension('J')->setWidth(12);
	$sheet->getColumnDimension('K')->setWidth(37);

	$sheet->getRowDimension('2')->setRowHeight(20.25);	
	$sheet->getRowDimension('3')->setRowHeight(20.25);	
	$sheet->getRowDimension('4')->setRowHeight(20.25);	
	$sheet->getRowDimension('5')->setRowHeight(40.50);	
	$sheet->getRowDimension('6')->setRowHeight(40.50);	
	$sheet->getRowDimension('7')->setRowHeight(40.50);	
	$sheet->getRowDimension('8')->setRowHeight(20.25);	
	$sheet->getRowDimension('9')->setRowHeight(20.25);	

	$sheet->getStyle('A2:K9')->getAlignment()->setWrapText(true);

	$sheet->setCellValue('A2', 'Ciudad');
	$sheet->getStyle('A2')->getFont()->setBold(true);
	$sheet->setCellValue('A3', 'Despacho Judicial');
	$sheet->getStyle('A3')->getFont()->setBold(true);
	$sheet->setCellValue('A4', 'Serie o Subserie Documental');
	$sheet->getStyle('A4')->getFont()->setBold(true);
	$sheet->setCellValue('A5', 'Nro. Radicación del Proceso');
	$sheet->getStyle('A5')->getFont()->setBold(true);

	$richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$payable = $richText->createTextRun('Partes Procesales (Parte A) ');
	$payable->getFont()->setBold(true);
	$richText->createText('(demandado, procesado, accionado)');
	$spreadsheet->getActiveSheet()->getCell('A6')->setValue($richText);

	$richText1 = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
	$payable1 = $richText1->createTextRun('Partes Procesales (Parte B) ');
	$payable1->getFont()->setBold(true);
	$richText1->createText('(demandante, denunciante, accionante)');
	$spreadsheet->getActiveSheet()->getCell('A7')->setValue($richText1);

	$sheet->setCellValue('A8', 'Terceros Intervinientes');
	$sheet->getStyle('A8')->getFont()->setBold(true);
	$sheet->setCellValue('A9', 'Cuaderno');
	$sheet->getStyle('A9')->getFont()->setBold(true);

	$styleArray = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        	],
    	],
	];

	$sheet->mergeCells('B2:F2');
	$sheet->mergeCells('B3:F3');
	$sheet->mergeCells('B4:F4');
	$sheet->mergeCells('B5:F5');
	$sheet->mergeCells('B6:F6');
	$sheet->mergeCells('B7:F7');
	$sheet->mergeCells('B8:F8');
	$sheet->mergeCells('B9:F9');

	$sheet->getStyle('A2')->applyFromArray($styleArray);
	$sheet->getStyle('B2:F2')->applyFromArray($styleArray);
	$sheet->getStyle('A3')->applyFromArray($styleArray);
	$sheet->getStyle('B3:F3')->applyFromArray($styleArray);
	$sheet->getStyle('A4')->applyFromArray($styleArray);
	$sheet->getStyle('B4:F4')->applyFromArray($styleArray);
	$sheet->getStyle('A5')->applyFromArray($styleArray);
	$sheet->getStyle('B5:F5')->applyFromArray($styleArray);
	$sheet->getStyle('A6')->applyFromArray($styleArray);
	$sheet->getStyle('B6:F6')->applyFromArray($styleArray);
	$sheet->getStyle('A7')->applyFromArray($styleArray);
	$sheet->getStyle('B7:F7')->applyFromArray($styleArray);
	$sheet->getStyle('A8')->applyFromArray($styleArray);
	$sheet->getStyle('B8:F8')->applyFromArray($styleArray);
	$sheet->getStyle('A9')->applyFromArray($styleArray);
	$sheet->getStyle('B9:F9')->applyFromArray($styleArray);

    $sheet->mergeCells('H2:K2');
	$sheet->setCellValue('H2', 'EXPEDIENTE FÍSICO');
	$sheet->getStyle('H2')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('H2')->getFont()->setSize(12);
	$sheet->getStyle('H2')->getFont()->setBold(true);

    $sheet->mergeCells('H3:I4');
	$sheet->setCellValue('H3', 'El expediente judicial posee documentos físicos');    	
	$sheet->getStyle('H3')->getAlignment()->setWrapText(true);

	$sheet->mergeCells('H5:I5');
	$sheet->mergeCells('H6:I6');
	$sheet->mergeCells('J3:K4');
	$sheet->mergeCells('J5:K5');
	$sheet->mergeCells('J6:K6');

	$sheet->getStyle('H2:K2')->applyFromArray($styleArray);	
	$sheet->getStyle('H3:I4')->applyFromArray($styleArray);	
	$sheet->getStyle('H5:I5')->applyFromArray($styleArray);	
	$sheet->getStyle('H6:I6')->applyFromArray($styleArray);	

	$sheet->getStyle('J3:K4')->applyFromArray($styleArray);	
	$sheet->getStyle('J5:K5')->applyFromArray($styleArray);	
	$sheet->getStyle('J6:K6')->applyFromArray($styleArray);	

	$sheet->setCellValue('H5', 'Nro. de carpetas (cuadernos), legajos o tomos:');    	
	$sheet->getStyle('H5')->getAlignment()->setWrapText(true);	
	$sheet->setCellValue('H6', 'Nro. de carpetas (cuadernos), legajos o tomos digitalizados:');    	
	$sheet->getStyle('H6')->getAlignment()->setWrapText(true);

	$sheet->setCellValue('A11', 'Nombre Documento');	
	$sheet->getStyle('A11:K11')->getFont()->setBold(true);
	$sheet->getStyle('A11:K11')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A11:K11')->getAlignment()->setWrapText(true);

	$styleArray2 = [
	    'fill' => [
	        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
	        'startColor' => [
	            'argb' => '00D6DBE4',
	        ],
	    ],
	];

	$styleArray3 = [
	    'borders' => [
	        'outline' => [
	            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
	            'color' => ['argb' => '00000000'],
	        ],
	    ],
	];

	$sheet->getStyle('A11')->applyFromArray($styleArray2);
	$sheet->getStyle('A11')->applyFromArray($styleArray3);
	$sheet->getStyle('B11')->applyFromArray($styleArray2);
	$sheet->getStyle('B11')->applyFromArray($styleArray3);
	$sheet->getStyle('C11')->applyFromArray($styleArray2);
	$sheet->getStyle('C11')->applyFromArray($styleArray3);
	$sheet->getStyle('D11')->applyFromArray($styleArray2);
	$sheet->getStyle('D11')->applyFromArray($styleArray3);
	$sheet->getStyle('E11')->applyFromArray($styleArray2);
	$sheet->getStyle('E11')->applyFromArray($styleArray3);
	$sheet->getStyle('F11')->applyFromArray($styleArray2);
	$sheet->getStyle('F11')->applyFromArray($styleArray3);
	$sheet->getStyle('G11')->applyFromArray($styleArray2);
	$sheet->getStyle('G11')->applyFromArray($styleArray3);
	$sheet->getStyle('H11')->applyFromArray($styleArray2);
	$sheet->getStyle('H11')->applyFromArray($styleArray3);
	$sheet->getStyle('I11')->applyFromArray($styleArray2);
	$sheet->getStyle('I11')->applyFromArray($styleArray3);
	$sheet->getStyle('J11')->applyFromArray($styleArray2);
	$sheet->getStyle('J11')->applyFromArray($styleArray3);
	$sheet->getStyle('K11')->applyFromArray($styleArray2);
	$sheet->getStyle('K11')->applyFromArray($styleArray3);

	$sheet->setCellValue('B11', 'Fecha Creación Documento');
	$sheet->setCellValue('C11', 'Fecha Incorporación Expediente');
	$sheet->setCellValue('D11', 'Orden Documento');
	$sheet->setCellValue('E11', 'Número Páginas');
	$sheet->setCellValue('F11', 'Página Inicio');
	$sheet->setCellValue('G11', 'Página Fin');
	$sheet->setCellValue('H11', 'Formato');
	$sheet->setCellValue('I11', 'Tamaño');
	$sheet->setCellValue('J11', 'Origen');
	$sheet->setCellValue('K11', 'Observaciones');

	$sheet->setCellValue('B2', $datPortada[0]['ciudad']);
	$sheet->setCellValue('B3', $datPortada[0]['despacho']);
	$sheet->setCellValue('B4', $datSubserie[0]['tipo_doc']);
	$sheet->setCellValue('B5', $datPortada[0]['radicacion']);
	$sheet->setCellValue('B6', $datPortada[0]['parte_procesal_a']);
	$sheet->setCellValue('B7', $datPortada[0]['parte_procesal_b']);
	$sheet->setCellValue('B8', $datPortada[0]['terceros']);
	$sheet->setCellValue('B9', $datPortada[0]['carpeta_principal']);

	$sheet->setCellValue('J3', 'SI');
	$sheet->getStyle('J3')->getAlignment()->setHorizontal('center');

	$sheet->setCellValue('J5', $datPortada[0]['nro_carpetas']);
	$sheet->getStyle('J5')->getAlignment()->setHorizontal('center');

	$sheet->setCellValue('J6', $datPortada[0]['nro_carpetas_digital']);
	$sheet->getStyle('J6')->getAlignment()->setHorizontal('center');

	$lin = 12;
	$i = 0;
	while ($i < count($datDetalles)) {
		$linea = $lin + $i;
		$sheet->getRowDimension($linea)->setRowHeight(40.50);
		$sheet->getStyle($linea)->getAlignment()->setWrapText(true);

		$sheet->setCellValue('A' . strval($linea), $datDetalles[$i]['nombre_criterio']);
		$sheet->getStyle('A' . strval($linea))->applyFromArray($styleArray3);

		$sheet->setCellValue('B' . strval($linea), $datDetalles[$i]['creacion']);
		$sheet->getStyle('B' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('B' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('C' . strval($linea), $datDetalles[$i]['incorporacion']);
		$sheet->getStyle('C' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('C' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('D' . strval($linea), $datDetalles[$i]['orden']);
		$sheet->getStyle('D' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('D' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('E' . strval($linea), $datDetalles[$i]['nro_paginas']);
		$sheet->getStyle('E' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('E' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('F' . strval($linea), $datDetalles[$i]['pag_inicial']);
		$sheet->getStyle('F' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('F' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('G' . strval($linea), $datDetalles[$i]['pag_final']);
		$sheet->getStyle('G' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('G' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('H' . strval($linea), $datDetalles[$i]['formato']);
		$sheet->getStyle('H' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('H' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('I' . strval($linea), $datDetalles[$i]['tamanio']);
		$sheet->getStyle('I' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('I' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('J' . strval($linea), $datDetalles[$i]['origen']);
		$sheet->getStyle('J' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('J' . strval($linea))->getAlignment()->setHorizontal('center');

		$sheet->setCellValue('K' . strval($linea), $datDetalles[$i]['observaciones']);
		$sheet->getStyle('K' . strval($linea))->applyFromArray($styleArray3);
		$sheet->getStyle('K' . strval($linea))->getAlignment()->setHorizontal('center');

		$i = $i + 1;
	}

	$arch_salida = $datDetalles[0]['destinoindice'];
	$arch_salida = str_replace('pdf', 'xlsx', $arch_salida);

	$writer = new Xlsx($spreadsheet);
	$writer->save($arch_salida);
?>