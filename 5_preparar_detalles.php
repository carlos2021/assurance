<?php include "funciones.php"; ?>
<?php include "p_fechas.php"; ?>

<?php $depuracion = 0; ?>

<?php if ($depuracion) { ?>

  <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>

<?php } ?>

<?php

    $mostrar = 0;
    $mostrar_1 = 0;
    $mostrar_2 = 0;
    $mostrar_44 = 0;
    $mostrar_99 = 0;

    if ($depuracion) {
	     echo "<hr><span style='margin-left:30px;'>PREPARAR DETALLES</span><hr>";
    }

    $sql = "SELECT * FROM tblexpedientes";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $nro_filas_expedientes = $query->rowCount();

    $sql = "TRUNCATE TABLE tblpaginas";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    //////////////////////////////////////////////////////////////////////////
    //         SE CREAN LAS CARPETAS Y LAS PÁGINAS DE CADA DOCUMENTO        //
    //////////////////////////////////////////////////////////////////////////

    // SE RECORRE EL CICLO DE EXPEDIENTES. CADA FILA PUEDE SER UN file O UN dir
    // SOLO SE TOMARÁN LOS file. Y ADEMÁS, EL FILE A PROCESAR DEBE TENER EXTENSIÓN
    // pdf. ADICIONALMENTE, SE DEBE TOMAR EN CUENTA SI ES EL ARCHIVO BASE O SI ES
    // UN CUADERNO

    // SE CREAN (O RECREAN) LAS CARPETAS CORRESPONDIENTES

    $nivel_1 = '01PrimeraInstancia';
    $base = _RAIZ;
    $raiz = RAIZ . 'resultados';
    $dir_paginas = RAIZ . 'paginas';
    $dir_exp = RAIZ . 'expedientes';

    /* shell_exec('rmdir ' . $raiz . ' /s /q');
    shell_exec('mkdir ' . $raiz); */

    shell_exec('rmdir ' . $dir_paginas . ' /s /q');
    shell_exec('mkdir ' . $dir_paginas);

    $carpeta_01 = 'ExpedientesProcesosJudiciales';
    $carpeta_02 = 'ExpedientesProcesosJudicialesPenalesLey906_2004';

    $raiz_base_1 = $raiz . '\\' . $carpeta_01;
    $raiz_base_2 = $raiz_base_1 . '\\' . $carpeta_02;

    shell_exec('rmdir ' . $raiz_base_1 . ' /s /q');
    shell_exec('mkdir ' . $raiz_base_1);

    shell_exec('rmdir ' . $raiz_base_2 . ' /s /q');
    shell_exec('mkdir ' . $raiz_base_2); 

    $raiz = $raiz_base_2;   

    // SE RECORRE LA TABLA DE EXPEDIENTES PARA PROCEDER A LA CREACIÓN DE LAS CARPETAS DERIVADAS

    // SE DEBE TENER EN CUENTA QUE AL CAMBIAR DE EXPEDIENTES SE DEBE GENERAR UNA CARPETA
    // MADRE DE LA CUAL DEPENDEN LOS ARCHIVOS

    $elexpediente = '';

    if ($depuracion) {
      echo "NÚMERO DE REGISTROS EN LA TABLA DE EXPEDIENTES: " . count($results) . "<br><br>";
    }

    $i = 0;
    while ($i < count($results)) {

      // ESTAMOS EN PRESENCIA DE UN EXPEDIENTE
      if (strcmp($results[$i]['tipo'], 'dir') == 0 && $results[$i]['nivel'] == 0 && strcmp($results[$i]['archivo'], $elexpediente) != 0) {
        // ES UN EXPEDIENTE NUEVO

        if ($depuracion) {
          echo "EXPEDIENTE NUEVO. SE CREAN LAS CARPETAS. SE GRABA LA FECHA DEL EXPEDIENTE!" . "<br><br>";
        }
        $elexpediente = $results[$i]['archivo'];

        $sql = "SELECT * FROM tblportada WHERE expediente = " . $elexpediente;
        $query = $pdo->prepare($sql);
        $query->execute();
        $datPortadas = $query->fetchAll(PDO::FETCH_ASSOC);

        $fecha_exp = $datPortadas[0]['fecha'];
 
        shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1);
        shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente);
        shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1);

        $carpeta_principal = "C01Principal";

        $sql33 = "UPDATE tblportada SET carpeta_principal = '" . $carpeta_principal . "' WHERE expediente = " . $elexpediente;
        // echo "LA CARPETA PRINCIPAL (SQL) = " . $sql33 . "<br>";
        $query33 = $pdo->prepare($sql33);
        $query33->execute();  
      }

      // ESTAMOS EN PRESENCIA DE UN ARCHIVO

      if ($results[$i]['tipo'] == 'file') {
        if ($depuracion) {
          echo "ESTAMOS EN PRESENCIA DE UN ARCHIVO" . "<br><br>";
        }
        // SE EVALUA LA EXTENSIÓN
        $fileinfo = pathinfo($results[$i]['archivo']);
        $ext = $fileinfo['extension'];

        if ($depuracion) {
          echo "EXTENSIÓN DEL ARCHIVO: " . $ext . "<br>";
        }

        // EL ARCHIVO NO ES UN PDF. SE GRABA DE MANERA ESPECIAL
        if (strcmp($ext, 'pdf') != 0 && strcmp($ext, 'txt') != 'txt') { 
          if ($mostrar) {
            echo "ESTAMOS ANTE UN DVD: " . $results[$i]['archivo'] . "<br>";
          }
          $posi = stripos($results[$i]['archivo'], 'Folio');
          if ($posi == 0) {
            $posi = stripos($results[$i]['archivo'], 'folio');            
          }
          if ($mostrar) {
            echo "POSICIÓN DE FOLIO = " . $posi . "<br>";
          }
          if ($posi > 0) {

            $str1990 = stripos($results[$i]['archivo'], '1990');
            $str1991 = stripos($results[$i]['archivo'], '1991');
            $str1992 = stripos($results[$i]['archivo'], '1992');
            $str1993 = stripos($results[$i]['archivo'], '1993');
            $str1994 = stripos($results[$i]['archivo'], '1994');
            $str1995 = stripos($results[$i]['archivo'], '1995');
            $str1996 = stripos($results[$i]['archivo'], '1996');
            $str1997 = stripos($results[$i]['archivo'], '1997');
            $str1998 = stripos($results[$i]['archivo'], '1998');
            $str1999 = stripos($results[$i]['archivo'], '1999');
            $str2000 = stripos($results[$i]['archivo'], '2000');
            $str2001 = stripos($results[$i]['archivo'], '2001');
            $str2002 = stripos($results[$i]['archivo'], '2002');
            $str2003 = stripos($results[$i]['archivo'], '2003');
            $str2004 = stripos($results[$i]['archivo'], '2004');
            $str2005 = stripos($results[$i]['archivo'], '2005');
            $str2006 = stripos($results[$i]['archivo'], '2006');
            $str2007 = stripos($results[$i]['archivo'], '2007');
            $str2008 = stripos($results[$i]['archivo'], '2008');
            $str2009 = stripos($results[$i]['archivo'], '2009');
            $str2010 = stripos($results[$i]['archivo'], '2010');
            $str2011 = stripos($results[$i]['archivo'], '2011');
            $str2012 = stripos($results[$i]['archivo'], '2012');
            $str2013 = stripos($results[$i]['archivo'], '2013');
            $str2014 = stripos($results[$i]['archivo'], '2014');
            $str2015 = stripos($results[$i]['archivo'], '2015');
            $str2016 = stripos($results[$i]['archivo'], '2016');
            $str2017 = stripos($results[$i]['archivo'], '2017');
            $str2018 = stripos($results[$i]['archivo'], '2018');
            $str2019 = stripos($results[$i]['archivo'], '2019');
            $str2020 = stripos($results[$i]['archivo'], '2020');
            $str2021 = stripos($results[$i]['archivo'], '2021');
            $strCMay = stripos($results[$i]['archivo'], 'C', $posi);
            $strCMin = stripos($results[$i]['archivo'], 'c', $posi);

            if ($mostrar) {
              echo "CASO ESPECIAL (C): strCMay = " . $strCMay . " strCMin = " . $strCMin . "<br>";
            }

            if ($strCMin > 0 || $strCMay > 0) {
              if ($strCMay > 0)
                $pp = $strCMay;
              else
                $pp = $strCMin;
            }
            else {
              if ($str1990 > 0) $pp = $str1990;
              else if ($str1991 > 0) $pp = $str1991;
              else if ($str1992 > 0) $pp = $str1992;
              else if ($str1993 > 0) $pp = $str1993;
              else if ($str1994 > 0) $pp = $str1994;
              else if ($str1995 > 0) $pp = $str1995;
              else if ($str1996 > 0) $pp = $str1996;
              else if ($str1997 > 0) $pp = $str1997;
              else if ($str1998 > 0) $pp = $str1998;
              else if ($str1999 > 0) $pp = $str1999;
              else if ($str2000 > 0) $pp = $str2000;
              else if ($str2001 > 0) $pp = $str2001;
              else if ($str2002 > 0) $pp = $str2002;
              else if ($str2003 > 0) $pp = $str2003;
              else if ($str2004 > 0) $pp = $str2004;
              else if ($str2005 > 0) $pp = $str2005;
              else if ($str2006 > 0) $pp = $str2006;
              else if ($str2007 > 0) $pp = $str2007;
              else if ($str2008 > 0) $pp = $str2008;
              else if ($str2009 > 0) $pp = $str2009;
              else if ($str2010 > 0) $pp = $str2010;
              else if ($str2011 > 0) $pp = $str2011;
              else if ($str2012 > 0) $pp = $str2012;
              else if ($str2013 > 0) $pp = $str2013;
              else if ($str2014 > 0) $pp = $str2014;
              else if ($str2015 > 0) $pp = $str2015;
              else if ($str2016 > 0) $pp = $str2016;
              else if ($str2017 > 0) $pp = $str2017;
              else if ($str2018 > 0) $pp = $str2018;
              else if ($str2019 > 0) $pp = $str2019;
              else if ($str2020 > 0) $pp = $str2020;
              else if ($str2021 > 0) $pp = $str2021;
              else if ($strCMay > 0) $pp = $strCMay;
              else if ($strCMin > 0) $pp = $strCMin;
              else $pp = 0;
            }

            $particula = mb_substr($results[$i]['archivo'], $posi + 5, $pp - ($posi + 5));

            if ($mostrar) {
              echo "LA PARTÍCULA ES: " . $particula . "<br>";
            }

            $arch_origen = $results[$i]['link'];  
            $arch_destino = $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . 'C01Principal' . '\\' . $particula . $results[$i]['archivo'];

            if ($mostrar) {
              echo "ORIGEN: " . $arch_origen . "<br>";
              echo "DESTINO: " . $arch_destino . "<br>";
            }

            shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . 'C01Principal');
            copy($arch_origen, $arch_destino);  
        
          }
          else {

          }
        }

        // EL ARCHIVO ES UN PDF, SE PROCESA
        if (strcmp($ext, 'pdf') == 0) {

          if ($mostrar) {
            echo "ARCHIVO = " . $results[$i]['archivo'] . "<br><br>";
          }

          $s = $results[$i]['archivo'];

          $uc = stripos($s, 'cuaderno');
          $upro = stripos($s, 'roceso');

          if ($mostrar) {
            echo "S (cadena de archivo) = " . $s . "<br>";
            echo "UC (posición de cuaderno) = " . $uc . "<br>";
            echo "UPRO (posición de proceso) = " . $upro . "<br>";
          }

          if ($uc == 0 && $upro == 0) {
            $nivel_11 = $s;
          } else if ($uc == 0 && $upro > 0) {
            $nivel_11 = "C01" . "Principal";
          } else if ($uc > 0 && $upro == 0) {
            $num = buscar_numero($s, 'cuaderno');
            while (strlen($num) < 2) {
              $num = '0' . $num;
            }
            $nivel_11 = "C" . $num . "Principal";
          } else { // $uc > 0 && $upro > 0
            $num = buscar_numero($s, 'cuaderno');
            while (strlen($num) < 2) {
              $num = '0' . $num;
            }
            $nivel_11 = "C" . $num . "Principal";            
          }

          if (strcasecmp($s, 'C01.pdf') == 0 ||
              strcasecmp($s, 'C02.pdf') == 0 ||
              strcasecmp($s, 'C03.pdf') == 0 ||
              strcasecmp($s, 'C04.pdf') == 0 ||
              strcasecmp($s, 'C05.pdf') == 0 ||
              strcasecmp($s, 'C06.pdf') == 0 ||
              strcasecmp($s, 'C07.pdf') == 0 ||
              strcasecmp($s, 'C08.pdf') == 0 ||
              strcasecmp($s, 'C09.pdf') == 0 ||
              strcasecmp($s, 'C10.pdf') == 0 ||
              strcasecmp($s, 'C11.pdf') == 0 ||
              strcasecmp($s, 'C12.pdf') == 0 ||
              strcasecmp($s, 'C13.pdf') == 0 ||
              strcasecmp($s, 'C14.pdf') == 0 ||
              strcasecmp($s, 'C15.pdf') == 0 ||
              strcasecmp($s, 'C16.pdf') == 0 ||
              strcasecmp($s, 'C17.pdf') == 0 ||
              strcasecmp($s, 'C18.pdf') == 0 ||
              strcasecmp($s, 'C19.pdf') == 0 ||
              strcasecmp($s, 'C20.pdf') == 0 ) {
            $nivel_11 = substr($s, 0, 3) . "Principal";
          }

          if ($mostrar) {
            echo "1 - Nivel11 = " . $nivel_11 . "<br>";
          }

          // SE CREAN LAS CARPETAS

          shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11);
          shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11); 

          // EN ESTE PUNTO SE CREAN LOS ARCHIVOS, PÁGINA POR PÁGINA, EN LA CARPETA
          // DE PÁGINAS, A PARTIR DEL CONTENIDO DEL ARCHIVO pdf ENCONTRADO

          // POR AHORA, SE IMPRIME EL LINK Y SE CALCULA EL NÚMERO DE
          // PÁGINAS CONTENIDAS EN EL ARCHIVO

          $link = $results[$i]['link'];
          shell_exec('pdfinfo ' . $link . ' > temporal.txt');
          $esarchivo = file_get_contents('temporal.txt');

          if ($mostrar) {
            echo "LINK = ( " . $link . " )" . "<br>";
            echo "esarchivo = " . $esarchivo . "<br>";
          }

          $pos33 = stripos($esarchivo, 'Pages');

          $num33 = '';
          $tw = $pos33;
          while (!is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
            $tw = $tw + 1;
          }
          while (is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
            $num33 = $num33 . $esarchivo[$tw];
            $tw = $tw + 1;
          }
          $nro_paginas = $num33;

          // SE CREAN LAS CARPETAS
          shell_exec('mkdir ' . $raiz . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11);
          shell_exec('mkdir ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11); 

          // EN ESTE PUNTO SE CREAN LOS ARCHIVOS, PÁGINA POR PÁGINA, EN LA CARPETA
          // DE PÁGINAS, A PARTIR DEL CONTENIDO DEL ARCHIVO pdf ENCONTRADO

          // POR AHORA, SE IMPRIME EL LINK Y SE CALCULA EL NÚMERO DE
          // PÁGINAS CONTENIDAS EN EL ARCHIVO

          $link = $results[$i]['link'];
          shell_exec('pdfinfo -meta ' . $link . ' > paginas.csv');
          $csv = array_map('str_getcsv', file('paginas.csv'));

          if ($mostrar) {
            echo "<br>VALOR DEVUELTO POR EL csv <br>" . var_dump($csv);
            echo "<br>";
          }

          $dummy = $csv[6][0];
          $dummy = str_replace('Pages: ', '', $dummy);
          $paginas = intval($dummy);

          // ANÁLISIS: ESTE VALOR SE MANTIENE DENTRO DE LA ITERACIÓN
          // VALIDAR !!!

          $nro_paginas = $paginas;

          if ($mostrar) {
            echo "<br>PÁGINAS CALCULADAS INICIALMENTE = " . $nro_paginas . "<br><br>";
          }

          //////////////////////////////////////////////////////////

          // SE DIVIDE EL ARCHIVO EN PÁGINAS UTILIZANDO pdftk
          // SE MUEVEN LAS PÁGINAS A LA CARPETA CORRESPONDIENTE
          // SE MUEVE TAMBIÉN EL ARCHIVO QUE CONTIENE EL NÚMERO DE PÁGINAS

          $instruccion1 = 'pdftk ' . $results[$i]['link'] . ' burst';
          exec( $instruccion1, $salida, $valor_retornado );

          $instruccion2 = 'move ' . $base . '\\pg*.pdf ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;

          $link_paginas = $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;

          // echo "EL LINK DIRECTORIO DE LAS PÁGINAS : " . $link_paginas . "<br>";

          exec( $instruccion2, $salida, $valor_retornado );

          $instruccion3 = 'move ' . $base . '\\paginas.csv ' . $dir_paginas . '\\' . $elexpediente . '\\' . $nivel_1 . '\\' . $nivel_11;
          exec( $instruccion3, $salida, $valor_retornado );          
        }        
      }
      $i = $i + 1;
    }

    //////////////////////////////////////////////////////////////////////////
    //            FIN CREACIÓN DE CARPETAS Y GENERACIÓN DE ARCHIVOS         //
    //////////////////////////////////////////////////////////////////////////

    if ($depuracion) {
      echo "<br><br>FINAL DEL CICLO DE ANÁLISIS DE CARPETAS" . "<br><br>";

      echo "INICIO DEL CICLO DE ANÁLISIS DE PÁGINAS" . "<br><br>";
    }

    $tabla = Array();

    $totPaginas = 0;
    $totExpedientes = 0;

    if ($depuracion) {
      echo "<br>NÚMERO FILA DE EXPEDIENTES = " . $nro_filas_expedientes . "<br>";
    }

    $s = '<div style="margin-left:30px"><span style="font-weight:bold;">PORTADAS: Archivos PDF base, todos ellos contenidos en los expedientes (nivel 1)</span><br><br>';

    $sql = "SELECT * FROM tblexpedientes";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $nro_filas_expedientes = $query->rowCount();

    // VERIFICA SI EXISTEN REGISTROS EN LA LISTA DE DOCUMENTOS
    if ($nro_filas_expedientes > 0){

      if ($depuracion) {
        echo "INGRESAMOS AL CICLO ==> " . "<br>";
      }

      $s .= '<table border="1">';
      $s .= '<tr>';
      $s .= '<th>&nbsp;Nro&nbsp;</th><th>Archivo</th><th>Link</th>';
      $s .= '</tr>';
      $i = 0;
    	foreach($results as $result) {

        if ($depuracion) {
          echo "CICLO DE PROCESAMIENTO => <br>";
        }

        if ($result['nivel'] == 0) {
          // EL ARCHIVO ES UN EXPEDIENTE (NIVEL CERO)
          $totExpedientes = $totExpedientes + 1;
          $expediente = $result['archivo'];
          

          // $i++; // PARECE UN ERROR, PUES SE ESTÁ PROCESANDO YA EN i = 0
          

          $s .= '<tr>';
          $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;' . $result['archivo'] . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
          $s .= '</tr>';

        } // CIERRE NIVEL CERO : EXPEDIENTES

        if ($result['nivel'] == 1 && strcmp($result['tipo'], "file") == 0) {

          if ($depuracion) {
            echo "NIVEL = 1 Y ES UN ARCHIVO<br>";
          }

          // ES UN ARCHIVO DE NIVEL 1 (potencialmente archivos, aunque pueden ser directorios)
          $dummy = explode(".", $result['archivo']);
          $ext = $dummy[count($dummy) - 1];

          if (strcmp($ext, "pdf") == 0 /* && strlen($result['archivo']) <= 20 */) { 
            if ($depuracion) {
              echo "ES UN ARCHIVO PDF<br>";
            }

            $i++;

            if ($depuracion) {
              echo "SE INCREMENTA LA VARIABLE I = " . $i . "<br>";
            }

            $s .= '<tr>';
            $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
            $s .= '<td>' . espacios(1) . '&nbsp;' . $result['archivo'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '</tr>';
            ////////////////////////////////////////////////////////////////////////////////

            $link = $result['link'];

            if ($depuracion) {
              echo "LINK DEL ARCHIVO: " . $link . "<br>";
              echo "ESTE SE DEFINE TAMBIÉN COMO EL DESTINO" . "<br>";
            }

            $destino = $link;
            $destino = str_replace('.pdf', '.txt', $destino);

            // SE CALCULA EL nivel_11

            if ($depuracion) {
              echo "I = " . $i . "<br>";
              echo "LONGITUD TABLA = " . count($results) . "<br><br>";
            }

            if ($i == count($results))
              continue;

            $s = $results[$i]['archivo'];

            $uc = stripos($s, 'cuaderno');
            $upro = stripos($s, 'roceso');

            if ($mostrar) {
              echo "S (cadena de archivo) = " . $s . "<br>";
              echo "UC (posición de cuaderno) = " . $uc . "<br>";
              echo "UPRO (posición de proceso) = " . $upro . "<br>";
            }

            if ($uc == 0 && $upro == 0) {
              $nivel_11 = $s;
            } else if ($uc == 0 && $upro > 0) {
              $nivel_11 = "C01" . "Principal";
            } else if ($uc > 0 && $upro == 0) {
              $num = buscar_numero($s, 'cuaderno');
              while (strlen($num) < 2) {
                $num = '0' . $num;
              }
              $nivel_11 = "C" . $num . "Principal";
            } else { // $uc > 0 && $upro > 0
              $num = buscar_numero($s, 'cuaderno');
              while (strlen($num) < 2) {
                $num = '0' . $num;
              }
              $nivel_11 = "C" . $num . "Principal";            
            }

            if ($mostrar) {
              echo "1 - Nivel11 = " . $nivel_11 . "<br>";
            }

            if ($mostrar) {
              echo "2 - dir_paginas = " . $dir_paginas . "<br>";
              echo "2 - expediente = " . $expediente . "<br>";
              echo "2 - nivel_1 = " . $nivel_1 . "<br>";
              echo "2 - Nivel11 = " . $nivel_11 . "<br>";
            }

            // SE CALCULA EL NÚMERO DE PÁGINAS DEL ARCHIVO A PARTIR DEL CONTENIDO
            // DE paginas.txt CONTENIDO EN LA CARPETA

            $arch_paginas = $dir_paginas . '\\' . $expediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . 'paginas.csv';

            if ($mostrar) {
              echo "arch_paginas = " . $arch_paginas . "<br>";
            }

            $link = $result['link'];
            shell_exec('pdfinfo ' . $link . ' > temporal.txt');
            $esarchivo = file_get_contents('temporal.txt');

            if ($mostrar) {
              echo "LINK = ( " . $link . " )" . "<br>";
              echo "esarchivo = " . $esarchivo . "<br>";
            }

            $pos33 = stripos($esarchivo, 'Pages');

            $num33 = '';
            $tw = $pos33;
            while (!is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
              $tw = $tw + 1;
            }
            while (is_numeric($esarchivo[$tw]) && $tw < strlen($esarchivo)) {
              $num33 = $num33 . $esarchivo[$tw];
              $tw = $tw + 1;
            }
            $nro_paginas = $num33;

            $h = 0;
            while ($h < $nro_paginas) {
                $h = $h + 1;

                // AQUÍ SE EXTRAE EL CONTENIDO TEXTUAL DE CADA UNA DE LAS PÁGINAS
                // DEL ARCHIVO. SE UTILIZARÁ tet EN LUGAR DE pdftotext. EL PROGRAMA
                // tet ES MÁS PRECISO. EN SU VERSIÓN DE PRUEBA SOLO ADMITE UNA PÁGINA
                // Y UN ARCHIVO DE TAMAÑO MENOR A 1 MEGABYTE

                $elfile = strval($h);
                while (strlen($elfile) < 4) {
                  $elfile = '0' . $elfile;
                }

                $elorigen = $dir_paginas . '\\' . $expediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . 'pg_' . $elfile . '.pdf';
                $fileinfo = pathinfo($elorigen);
                $eldestino = $fileinfo['basename'];

                if ($mostrar) {
                  echo "ENLACE ORIGEN. ciclo = " . $h . " ORIGEN = " . $elorigen . " (validar este enlace) => ";
                  echo "Y EL DESTINO ES = " . $eldestino . "<br>";
                }

                exec("pdftotext -f " . strval($h) . " -l " . strval($h) . " " . $link . " " . $destino, $salida, $retorno); 

                $contenido = file_get_contents($destino);

                $direccion_documento = $link_paginas . '\\pg_' . $elfile . '.pdf';

                $contenido = slugify($contenido);

                $contenido = str_replace('Á', 'á', $contenido);
                $contenido = str_replace('É', 'é', $contenido);
                $contenido = str_replace('Í', 'í', $contenido);
                $contenido = str_replace('Ó', 'ó', $contenido);
                $contenido = str_replace('Ú', 'ú', $contenido);
                $contenido = strtolower($contenido);
                $contenido = str_replace('.', ' ', $contenido);
                $contenido = str_replace(',', ' ', $contenido);
                $contenido = str_replace('/', ' ', $contenido);
                $contenido = str_replace('\\', ' ', $contenido);
                $contenido = str_replace('!', ' ', $contenido);
                $contenido = str_replace('¡', ' ', $contenido);
                $contenido = str_replace('*', ' ', $contenido);
                $contenido = str_replace('^', '', $contenido); 
                $contenido = str_replace('«', '', $contenido); 
                $contenido = str_replace('(', ' ', $contenido); 
                $contenido = str_replace(')', ' ', $contenido); 
                $contenido = str_replace('<', ' ', $contenido); 
                $contenido = str_replace('>', ' ', $contenido);
                $contenido = str_replace('«', ' ', $contenido); 
                $contenido = str_replace('|', ' ', $contenido);
                $contenido = str_replace('i«', ' ', $contenido);  
 
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);
                $contenido = str_replace('  ', ' ', $contenido);

                $contenido = utf8_encode($contenido);                 

                if ($mostrar)
                  echo "(" . strval($h) . "-" . strval($h) . ") : CONTENIDO = " . strlen($contenido) . "<br>"; 

                $nro_pag = $h;
                $id_criterio = 15;

                ///////////////////////////////////////////////////////////////////////////

                $aux = "2019-01-28";
                $d = strtotime($aux);
                $fecha = date('Y-m-d', $d);

                $pagina = $h;
                $criterio = 0;
                if (strlen($contenido) <= 350) {
                  $criterio = 99999;
                }

                $archivo = $result['archivo'];
                $enlace = $result['link'];

                $arr = Array();
                $arr[] = $expediente;
                $arr[] = $archivo;
                $arr[] = $criterio;
                $arr[] = $pagina;
                $arr[] = $fecha;
                $arr[] = $enlace;
                $arr[] = $contenido;

                $tabla[] = $arr;

                if ($depuracion) {
                  echo "<br>EL CONTENIDO A SER USADO EN LA BÚSQUEDA DE LA FECHA<br>";
                }

                $aux = encontrar_fecha_nueva($contenido, $fecha_exp);

                if ($depuracion) {
                  echo "<br>FECHA ENCONTRADA =====================>>>>>>>>>>>>>>>>>>>>>> " . $aux . "<br>";
                }

                // $fecha = '2022-01-10';

                // if ($h == 1)
                  // $fecha = $fecha_exp;

                //if (strcmp($fecha, 'fecha-no-valida') == 0 || strcmp($fecha, 'fecha-no-encontrada') == 0) {
                  // $fecha = $fecha_exp;
                // }

                $d = strtotime($aux);
                $fecha = date('Y-m-d', $d);

                if ($depuracion) {
                  echo "LA FECHA, EN EL FORMATO CORRECTO: " . $fecha . "<br>";
                }

                /////////////////////////////////////////////////////////////////////////
                try {

                  $sql = "INSERT INTO tblpaginas (expediente, archivo, pagina, fecha, link, contenido, criterio) VALUES (:expediente, :archivo, :pagina, :fecha, :link, :contenido, :criterio)";
                  $stmt = $pdo->prepare($sql);
                  $stmt->bindParam(':expediente', $expediente, PDO::PARAM_STR);
                  $stmt->bindParam(':archivo', $archivo, PDO::PARAM_STR);
                  $stmt->bindParam(':pagina', $pagina, PDO::PARAM_INT);
                  $stmt->bindParam(':fecha', $fecha, PDO::PARAM_STR);
                  $stmt->bindParam(':link', $enlace, PDO::PARAM_STR);
                  $stmt->bindParam(':contenido', $contenido, PDO::PARAM_STR);
                  $stmt->bindParam(':criterio', $criterio, PDO::PARAM_INT);
                  $stmt->execute();
                    
                } catch (Exception $ex) {
                  print_r($ex);
                }
                /////////////////////////////////////////////////////////////////////////

                // if ($h == 2) {
                  //$ciclo = 0;                  
                //}      
            } // CIERRE CICLO DE PROCESAMIENTO DE CADA ARCHIVO */

            $totPaginas = $totPaginas + $h;

            ////////////////////////////////////////////////////////////////////////////////
          }
        }
    	} // CIERRE DEL CICLO DE PÁGINAS
      $s .= '</table></div>';

      if ($depuracion) {
        echo "<br><br>==> PUNTO DE CONTROL. SE IMPRIME LA TABLA" . "<br>";
        echo $s;
      }

    } // CIERRE DEL if QUE CONTROLA LA EXISTENCIA DE PÁGINAS

    /////////////////////////////////////////////////////////////////////////////
    //        CORRECCIÓN (TEMPORAL !!!) DE LAS PÁGINAS CON FECHA ERRÓNEA       //
    /////////////////////////////////////////////////////////////////////////////

    $sql20 = "SELECT * FROM tblpaginas";
    $query20 = $pdo->prepare($sql20);
    $query20->execute();
    $filas = $query20->fetchAll(PDO::FETCH_ASSOC);

    if ($mostrar_2)
      echo "PROCESO DE CORRECCIÓN DE FECHAS ERRÓNEAS<br><br>";
    
    $i = 0;
    while ($i < count($filas)) {
      if (strcmp($filas[$i]['fecha'],'0000-00-00') == 0) {
        if ($mostrar_2)
          echo "ERROR EN I = " . $i . " FECHA = " . $filas[$i]['fecha'] . "<br>";
        $k = $i + 1;
        while ($k < count($filas)) {
          if (strcmp($filas[$k]['fecha'],'0000-00-00') != 0) {
            $filas[$i]['fecha'] = $filas[$k]['fecha'];
            if ($mostrar_2) {
              echo "SE VA A PRODUCIR EL CAMBIO<br>";
              echo "K = " . $k . " FECHA = " . $filas[$k]['fecha'] . "<br>";
              echo "EL ID EN DONDE SE GRABA = " . $filas[$i]['id'] . "<br>";
            }
            $sql30 = "UPDATE tblpaginas SET fecha = '" . $filas[$k]['fecha'] . "' WHERE id = " . $filas[$i]['id'];
            $query30 = $pdo->prepare($sql30);
            $query30->execute();
            break;
          }
          $k = $k + 1;
        }
      }
      if ($mostrar_2)
        echo "I = " . $i . " FECHA = " . $filas[$i]['fecha'] . "<br>";
      $i = $i + 1;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////   S E G U N D A  P A S A D A  /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE RECORREN TODAS LAS PÁGINAS Y SE EVALÚAN NUEVAMENTE LAS TRD QUE SON PROBLEMÁTICAS
    // POR TEMA DE CRUCE CON OTRAS TRD. EL OBJETIVO ES NO MODIFICAR DE NUEVO LOS PATRONES O
    // EXCEPCIONES (para que no se reflejen en los restantes TRD) Y REALIZAR LAS
    // MODIFICACIONES DE MANERA PUNTUAL.
    ////////////////////////////////////////////////////////////////////////////////////

    define("OFICIO", 612);
    define("ESCRITOACUSACION", 583);

    $es = array();
    $es[0] = ESCRITOACUSACION;

    $debe_ser = array();
    $debe_ser[0] = OFICIO;

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $k = 0;
    $i = 0;
    while ($i < count($results)) {
      if ($results[$i]['criterio'] == ESCRITOACUSACION) {
        $pos1 = stripos($results[$i]['contenido'], 'proceso penal');
        $pos2 = stripos($results[$i]['contenido'], 'oficio');
        if ($pos1 > 0 && $pos2 == 0) {
          // SE DEBE CAMBIAR EL CRITERIO
          if ($depuracion) {
            echo "<hr><hr>FINAL FINAL cambiando escrito acusación por oficio !!! <hr><hr>";
          }
        }
      }
      $i = $i + 1;
    }

  $pdo = null;

  ?>

  <?php if ($depuracion) { ?>

    <div style="margin-left:30px;">
      <hr>PROCESO CONCLUIDO<hr>
    </div>

    <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
    <a href="index.php"><button>ATRÁS</button></a>

  <?php } ?>

