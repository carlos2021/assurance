<?php include "funciones.php"; ?>

<?php $depuracion = 0; $depuracion2 = 0; ?>

<?php

  if ($depuracion) {
    echo "<br><br>INGRESANDO A GENERACIÓN PORTADA<br><br>";
  }

  ?>

  <?php if ($depuracion) { ?>

    <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
    <a href="index.php"><button>ATRÁS</button></a>

  <?php } ?>

  <?php if ($depuracion2) {
    echo "<span style='font-weight:bold;'><h2>EL PRIMER ABOGADO ESTÁ TRABAJANDO !!!</h2></span><hr>";
    echo "<h2>Procesando Expedientes</h2>" . "<hr>";
  } ?>

  <?php

    $mostrar = 0;

    if ($depuracion) {
  	  echo "<hr><span style='margin-left:30px;'>GENERAR PORTADAS</span><hr>";
    }

    $sql = "SELECT * FROM tblexpedientes";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $tabla = Array();

    $s = '<div style="margin-left:30px"><span style="font-weight:bold;">PORTADAS: Archivos PDF base, todos ellos contenidos en los expedientes (nivel 1)</span><br><br>';
    if ($query->rowCount() > 0){
      $s .= '<table border="1">';
      $s .= '<tr>';
      $s .= '<th>&nbsp;Nro&nbsp;</th><th>Archivo</th><th>Link</th>';
      $s .= '</tr>';
      $i = 0;
    	foreach($results as $result) {
        if ($result['nivel'] == 0) {
          $expediente = $result['archivo'];
          $num_documentos = $result['documentos'];
          $i++;
          $s .= '<tr>';
          $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;' . $result['archivo'] . '&nbsp;' . '</td>';
          $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
          $s .= '</tr>';
        }
        if ($result['nivel'] == 1 && strcmp($result['tipo'], "file") == 0) {
          $dummy = explode(".", $result['archivo']);
          $ext = $dummy[count($dummy) - 1];
          if (strcmp($ext, "pdf") == 0 && strlen($result['archivo']) <= 20) {
            $i++;
            $s .= '<tr>';
            $s .= '<td>' . '&nbsp;' . $i . '&nbsp;' . '</td>';
            $s .= '<td>' . espacios(1) . '&nbsp;' . $result['archivo'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '<td>' . '&nbsp;&nbsp;' . $result['link'] . '&nbsp;&nbsp;' . '</td>';
            $s .= '</tr>';
            ////////////////////////////////////////////////////////////////////////////////
            $link = $result['link'];
            $destino = $link;
            $destino = str_replace('.pdf', '.txt', $destino);
            shell_exec("pdftotext -f " . "1" . " -l " . "1" . " " . $link . " " . $destino);
            $elc = file_get_contents($destino);
            $elc = utf8_encode($elc);

            if ($mostrar) {
              echo "CONTENIDO PRIMERA PÁGINA: <br>";
              echo $elc; echo "<br><br>";
            }

            $ciudad = 'ARAUCA';

            $pos = stripos($elc, 'juzgado');
            if ($pos > 0)            
              $despacho = substr($elc, $pos, 120);
            else
              $despacho = '';

            $despacho = str_replace('QtViL', 'CIVIL', $despacho);
            $despacho = str_replace('CIRCUITO 1', 'CIRCUITO', $despacho);

            $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';

            $serie_subserie = '270-245';

            $pos = stripos($elc, 'radicado');
            $kk = stripos($elc, '54');

            if ($pos > 0) {
              $k = $pos;
              while (!is_numeric($elc[$k])) {
                $k = $k + 1;
              }
              $z = $k;
              while (is_numeric($elc[$z]) || strcmp($elc[$z], '-') == 0) {
                $z = $z + 1;
              }
              $radicacion = substr($elc, $k, $z - $k);
            }
            else if ($kk  > 0) {
              if ($mostrar) {
                echo "<br><br>==><br><br>";
                echo "kk = " . $kk . "<br>";
              }
              $dummy = $kk;
              while (is_numeric($elc[$kk]) || $elc[$kk] == ' ' || $elc[$kk] == '-') {
                $kk = $kk + 1;
              }
              $radicacion = substr($elc, $dummy, $kk - $dummy + 1);
              str_replace(' ', '', $radicacion);
           }
            else {
              $radicacion = '';
            }

            $parte_procesal_a = '';
            $parte_procesal_b = '';

            $pos_ini = 0;
            $pos_fin = 0;

            $pos1 = stripos($elc, 'indiciado');
            $pos2 = stripos($elc, 'procesado');
            $pos3 = stripos($elc, 'imputado');
            $pos4 = stripos($elc, 'acusado');
            $pos5 = stripos($elc, 'sentenciado');

            if ($pos1 > 0) 
              $pos_ini = $pos1 + 11;
            else if ($pos2 > 0)
              $pos_ini = $pos2 + 10;
            else if ($pos3 > 0)
              $pos_ini = $pos3 + 9;
            else if ($pos4 > 0)
              $pos_ini = $pos4 + 7;
            else if ($pos5 > 0)
              $pos_ini = $pos5 + 12;

            $pos11 = stripos($elc, 'solicitante', $pos_ini);
            $pos12 = stripos($elc, 'fiscal', $pos_ini);

            $pos21 = stripos($elc, 'delito', $pos_ini);
            $pos22 = stripos($elc, 'hecho', $pos_ini);
            $pos23 = stripos($elc, 'libro', $pos_ini);

            $pos31 = stripos($elc, 'juzgado');
            $pos32 = stripos($elc, 'solicitud');

            $pos41 = stripos($elc, 'delito');
            $pos42 = stripos($elc, 'fecha');

            $pos501 = stripos($elc, 'arauca');
            $pos502 = stripos($elc, 'cucuta');
            $pos503 = stripos($elc, 'cúcuta');

            if ($pos501 > 0) {
              $ciudad = 'ARAUCA';
            } else if ($pos502 > 0 || $pos503 > 0) { 
              $ciudad = 'CÚCUTA';
            } else {
              $ciudad = '';
            }

            $nt = strpos($despacho, 'CUCUTA');
            if ($nt > 0) {
              $despacho = substr($despacho, 0, $nt + strlen($ciudad));
            }

            $despacho = str_replace('1 ', '', $despacho);

            /* $despacho = '';
            if ($pos31 > 0) {
              if ($pos32 > 0)
                $despacho = trim(substr($elc, $pos31 + 8, $pos32 - ($pos31 + 8))); 
              else {
                $despacho = trim(substr($elc, $pos31 + 8, 80));                 
              }               
            } else
              $despacho = trim(substr($elc, $pos31 + 8, 80)); */

            //////////////////////////////////////////////////////////////////

            /* if (stripos($despacho, 'SEGUNDO') > 0 && stripos($despacho, 'PATIOS')) {
              $despacho = 'SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S';
            }

            if (stripos($despacho, 'SEGUNDO;pENAli MUNICIPAL') > 0) {
              $despacho = 'SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S';
            }

            if (strcmp($result['archivo'], 'Proceso24072017.pdf') == 0) {
              $despacho = 'SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S';
            } */

            //////////////////////////////////////////////////////////////////            

            if ($pos11 > 0)
              $pos_fin = $pos11;

            if ($pos11 == 0 & $pos12 > 0)
              $pos_fin = $pos12;

            if ($pos11 == 0 && $pos12 == 0 && $pos21 > 0)
              $pos_fin = $pos21;

            if ($pos11 == 0 && $pos12 == 0 && $pos21 == 0 && $pos22 > 0)
              $pos_fin = $pos22;

            if ($pos_ini > 0 && $pos_fin > 0 && $pos_ini < $pos_fin)
              $parte_procesal_a = trim(substr($elc, $pos_ini, $pos_fin - $pos_ini));

            // $parte_procesal_a .= ' --- POS1 = ' . strval($pos1) . ' - POS2 = ' . strval($pos2) . ' - POS3 = ' . strval($pos3) . ' - POS4 = ' . strval($pos4) . ' - POS5 = ' . strval($pos5) .' - POS11 = ' . strval($pos11) . ' - POS12 = ' . strval($pos12) . ' - POS21 = ' . strval($pos21) . ' - POS22 = ' . strval($pos22) . ' === POS_INI = ' . strval($pos_ini) . ' == POS_FIN = ' . strval($pos_fin);

            if ($pos11 > 0)
              $pos_ini = $pos11 + 12;

            $parte_procesal_b = substr($elc, $pos11 + 12, 50);

            if ($pos11 == 0 && $pos12 > 0)
              $pos_ini = $pos12 + 8;

            if ($pos23 > 0)
              $parte_procesal_b = substr($elc, $pos_ini, $pos23 - $pos_ini);
            else
              $parte_procesal_b = substr($elc, $pos_ini, 50);

            //////////////////////////////////////////////////////////////////////////////

            $parte_procesal_a = str_ireplace('', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace('HOMICIO CULPOSO', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace('.. v', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace(':', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace('DELITO; HURTO', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace(' i ', '', $parte_procesal_a);
            $parte_procesal_a = str_ireplace(' í ', '', $parte_procesal_a);

            if (strcmp($result['archivo'], 'Proceso662018.pdf') == 0) {
              $parte_procesal_a = 'HÉCTOR JOSÉ MENDOZA VILLAMIZAR';
            }            

            $parte_procesal_a = trim($parte_procesal_a);

            $parte_procesal_b = str_ireplace('', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FISCALIA 2da SECCIONAL DE LOS PATIOS', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FICALIA 2da SECCIONAL DE LOS PATIOS', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FISCALIA: 1 ra SECIÓNAL DE LOS PATIOS í', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('es', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('« SECCIONAL,:', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FICALIA 1ra SECCIONAL DE LOS PATIOS', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FISCALI', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FICALIA 1ra LOCAL DE LOS PATIOS', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('14 PECIALIZADO ADMINISTRACION PUBLICA', '', $parte_procesal_b); 
            $parte_procesal_b = str_ireplace('I FISCAL SEGUNDO LOCAL LOS PATIO', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('Ira SECCIONAL DE LOS PATIOS', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('v ', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace(':', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FISCALIA', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('A 2cla SECCldÑAL;', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace('FISCLIA', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace(' i ', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace(' í ', '', $parte_procesal_b);
            $parte_procesal_b = str_ireplace(' i ', '', $parte_procesal_a);
            $parte_procesal_b = str_ireplace(' í ', '', $parte_procesal_a);

            $parte_procesal_b = trim($parte_procesal_b);

            // TEMPORAL !!!!

            //////////////////////////////////////////////////////////////////////////////

            $proceso = 0;

            if ($pos41 > 0) {
              if ($pos1 > $pos41)
                $proceso = substr($elc, $pos41 + 7, $pos1 - ($pos41 + 7));
              else
                $proceso = substr($elc, $pos41, 80);
            }

            if (strlen($proceso) == 0) {
              $proceso = substr($elc, $pos41 + 7, $pos11 - ($pos41 + 7));
              $proceso = str_replace('INDICIADO:', '', $proceso);
            }

            /////////////////////////////////////////////////////////////////

            $proceso = str_ireplace('HOMICIDIO CULPOSO', '', $proceso);
            $proceso = str_ireplace('.. v', '', $proceso);
            $proceso = str_ireplace(':', '', $proceso);

            if (strcmp($result['archivo'], 'Proceso4832009.pdf') == 0) {
              $proceso = 'SECUESTRO EXTORSIVO AGRAVADO, HURTO CALIFICADO.';
            }

            $nt1 = stripos($elc, 'EJECUTIVO');
            $nt2 = stripos($elc, 'HIPOTECARIO');
            if ($nt1 > 0 && $nt2 > 0) {
              $proceso = 'EJECUTIVO HIPOTECARIO';
            }

            $nt1 = stripos($elc, 'DEMANDANTE');
            $nt2 = stripos($elc, 'DEMANDADO');

            if ($nt1 > 0) {
              $parte_procesal_a = substr($elc, $nt1 + 11, 28);
            }

            $parte_procesal_a = str_replace('Í', 'I', $parte_procesal_a);

            if ($nt2 > 0) {
              $parte_procesal_b = substr($elc, $nt2 + 11, 36);
            }

            $parte_procesal_b = str_replace('iJC', 'IJO', $parte_procesal_b);

            // TEMPORAL !!!!

            /////////////////////////////////////////////////////////////////

            $proceso = trim($proceso);

            $terceros = '';
            $cuaderno = $result['expedientes'] + 1;
            $tipo_expediente = 'digital';
            $nro_carpetas = $num_documentos;
            $nro_carpetas_digital = $num_documentos;

            $serie = 270;
            $subserie = 245;

            // NUEVO CON 6 CUADERNOS Y DVDS
            if (strcmp($result['archivo'], 'Proceso2672014.pdf') == 0) { // 1
              $despacho = 'JUZGADO SEGUNDO PENAL CIRCUITO ESPECIALIZADO';
              $parte_procesal_a = 'MARÍA JOSÉ RODRÍGUEZ CAÑAS';
              $parte_procesal_b = 'FISCAL: AURA NUBIA'; 
              $proceso = 'SECUESTRO EXTORSICO CON CIRCUNSTANCIAS DE AGRAVACION';
              $terceros = 'LUCÍA VICTORIA ARÉVALO TOSCANO';
            }

            if (strcmp($result['archivo'], 'Proceso2102019.pdf') == 0) { // 1
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'CRISTOBAL RUIZ SILVA, MARCO EMILIO CRUZ SILVA, CARLOS JULIO CRUZ SILVA, JOSE RAMON CRUZ GONZALEZ Y DUMAR JAVIER ROZO GELVEZ';
              $parte_procesal_b = 'FISCALIA 1ra SECCIONAL DE LOS PATIOS Dr. HEBER ALEXANDER ALBINO CASTRO'; 
              $proceso = 'VIOLACION De medidas sanitarias';
            }

            if (strcmp($result['archivo'], 'Proceso2532019.pdf') == 0) { // 2
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'VENDER ANDRES CUEVAS QUINTERO';
              $parte_procesal_b = 'FISCAL 1A SECCIONAL,DR.HEBER ALEXANDER ALBINO CASTRO'; 
              $proceso = 'TRÁFICO, FABRICACIÓN O PORTE DE ESTUPEFACIENTES';
            } 

            if (strcmp($result['archivo'], 'Proceso4042019.pdf') == 0) { // 3
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'MARALON ANDRES OSPINA TRUJILLO';
              $parte_procesal_b = 'Dr. HEBER ALEXANDER ALBINO CASTRO'; 
              $proceso = 'FABRICACION, TRAFICO Y PORTE DE ARMAS DE FUEGO O MUNICIONES';
            }

            if (strcmp($result['archivo'], 'Proceso4772019.pdf') == 0) { // 4
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'EDER JULIAN RODRIGUEZ PABON';
              $parte_procesal_b = 'Dra. FANNY AMARTINEZ VILA FISCALIA 2da SECCIONAL DE LOS PATIOS'; 
              $proceso = 'HURTO MAYOR CUANTIA';
            } 

            if (strcmp($result['archivo'], 'Proceso6522019.pdf') == 0) { // 5
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'JONATHAN JAVIER QUINTERO GALVIS';
              $parte_procesal_b = 'FICALIA 2da SECCIONAL DE LOS PATIOS Dra. FAZNNY MARTINEZ VILA'; 
              $proceso = 'VIOLENCIA INTRAFAMILIAR';
              $terceros = 'JONATHAN XAVIER QUINTERO GALVIS';
            }

            if (strcmp($result['archivo'], 'Proceso90612017.pdf') == 0) { // 6
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'LADY VACA CASTILLA';
              $parte_procesal_b = 'FICALIA 1ra LOCAL DE LOS PATIOS Dra. SOCORRO RIVERA HERRERA'; 
              $proceso = 'ALZAMIENTO DE BIENES';
            } 

            if (strcmp($result['archivo'], 'Proceso612017.pdf') == 0) { // 7
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'RUBEN DARIO CRUZ.';
              $parte_procesal_b = 'FFANNY MARTINEZ VILA 2 SECCIONAL LOS PATIOS'; 
              $proceso = 'FAVORECIMIENTO AL CONTRABANDO DE HIDROCARBUROS';
              $terceros = 'DIRECCIÓN DE IMPUESTOS ADUANAS NACIONALES';
            }

            if (strcmp($result['archivo'], 'Proceso24072017.pdf') == 0) { // 8
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'FAIBER ESLEIDER HERNANDEZ SALCEDO';
              $parte_procesal_b = 'FISCALIA: 2cla SECCldÑAL; Dra FANNY MARTINEZ VILA'; 
              $proceso = 'TENTATIVA DE HOMICIDIO';
            } 

            if (strcmp($result['archivo'], 'Proceso24072017.pdf') == 0) { // 9
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'FAIBER ESLÉÍDER HERNANDEZ SALCEDO';
              $parte_procesal_b = 'FISCALÍA: 2da SECCIONAL Dra. FANNY MARTÍNEZ VILA'; 
              $proceso = 'TENTAIVA DE HOMICIDIO';
            }

            if (strcmp($result['archivo'], 'Proceso19452018.pdf') == 0) { // 10
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'JOSE LUIS ROMERO SILVA';
              $parte_procesal_b = 'FISCALIA UNICA DE VILLA DEL ROSARIO'; 
              $proceso = 'HURTO';
            } 

            if (strcmp($result['archivo'], 'Proceso35362018.pdf') == 0) { // 11
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'ALEJANDRO JOSE GUERRA TRILLOS';
              $parte_procesal_b = 'Dr. DANIEL GIOVANNY SARMIENTO DIAZ FISCAL SEGUNDO LOCAL LOS PATIOS
'; 
              $proceso = 'TENTATIVA DE HURTO CALIFDICADO';
            }

            if (strcmp($result['archivo'], 'Proceso752018.pdf') == 0) { // 12
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'JHON ALEXANDER PRADA MARIN';
              $parte_procesal_b = 'Dra. FANNY MARTINEZ VILA FISCAL 2da SECCIONAL DE LOS PATIOS'; 
              $proceso = 'FAVORECIMINTO AL CONTRABANDO DE HIDROCARBUROS Y SUS DERIVADOS';
              $terceros = 'DIRECCIÓN DE IMPUESTOS ADUANAS NACIONALES';
            } 

            if (strcmp($result['archivo'], 'Proceso482019.pdf') == 0) { // 13
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'ERICK SANTIAGO ORTEGA BLANCO Y FABIO NELSON GIRALDO ZULUAGA';
              $parte_procesal_b = 'Dra. SOCORRO RIVERA HERRERA FISCAL 1ro LOCAL DE LOS PATIOS
'; 
              $proceso = 'HURTO CALIFICADO AGRAVADO';
            }

            if (strcmp($result['archivo'], 'Proceso27032012.pdf') == 0) { // 14
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'RENE GUERRERO ASCANIO';
              $parte_procesal_b = 'SOCORRO RIVERA HERRERA 1 LOCAL DE LOS PATIOS'; 
              $proceso = 'INASISTENCIA ALIMENTARIA';
            } 

            if (strcmp($result['archivo'], 'Proceso73682017.pdf') == 0) { // 15
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'MANUEL PANQUEVA GOMEZ';
              $parte_procesal_b = 'Dr. HEBER ALEXANDER ALEONO CASTRO FISCLIAIra SECCIONAL DE LOS PATIOS
'; 
              $proceso = 'OMISION DE AGENTE RETENDOR';
            }

            if (strcmp($result['archivo'], 'Proceso662018.pdf') == 0) { // 16
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'HECTOR JOSE MENDOZA VILLAMIZAR';
              $parte_procesal_b = 'FISCAL: 14 ESPECIALIZADO ADMINISTRACION PUBLICA, DR LINDON JOSE PIRACON PUERTO'; 
              $proceso = 'COHECHO POR DAR U OFRECER Y FAVORECIMIENTO POR SERVIDOR PÚBLICO';
            } 

            if (strcmp($result['archivo'], 'Proceso392018.pdf') == 0) { // 17
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'LEIDY KATHERINE LARGO RIVERA';
              $parte_procesal_b = 'FISCALIA 1ra CAVIF DE CUCUTA Dra. MARTHA LILIANA SANMIGUEL SALAMANCA'; 
              $proceso = 'VIOLENCIA INTRAFAMILIAR AGRAVADA';
              $terceros = 'PEDRO ANTONIO SIERRA GUTIÉRREZ';
            }

            if (strcmp($result['archivo'], 'Proceso800402018.pdf') == 0) { // 18
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'JOSE ALEJANDRO RODRIGUEZ CABEZA';
              $parte_procesal_b = 'Dr. HEBER ALEXANDER ALBINO CASTRO FISCALIA 1ra SECCIONAL DE LOS PATIOS'; 
              $proceso = 'HOMICIDIO CULPOSO';
              $terceros = 'JOSÉ RAMÓN CÁRDENAS MORALES Y OTROS';
            } 

            if (strcmp($result['archivo'], 'Proceso4832009.pdf') == 0) { // 19
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = ': PEDRO JAVIER ALBA SANCHEZ, RUBEN DARIO VILLAN YAÑEZ, LUIS EMILIO YAÑEZ SOLEDAD.';
              $parte_procesal_b = 'FISCALÍA'; 
              $proceso = 'SECUESTRO EXTORSIVO AGRAVADO, HURTO CALIFICADO.';
            }

            if (strcmp($result['archivo'], 'Proceso1772007.pdf') == 0) { // 20
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'JOSÉ GABRIEL BUSTOS CAICEDO';
              $parte_procesal_b = 'FISCALÍA'; 
              $proceso = 'HOMICIDIO';
              $radicacion = '177-2007';
            } 

            if (strcmp($result['archivo'], 'Proceso86652016.pdf') == 0) { // 20
              $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
              $parte_procesal_a = 'CIRO ANTHONY TORRADO VACCA';
              $parte_procesal_b = 'FISCALÍA'; 
              $proceso = 'INASISTENCIA ALIMENTARIA';
            }

    if (strcmp($result['archivo'], 'Proceso1072016.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JORGE ELIECER PARRA URIBE';
      $parte_procesal_b = 'JUZGADO 3 PENAL CIRCUITO CON FUNCIONES DE CONOCIMIENTO DE'; 
      $proceso = 'FABRICACIÓN, TRÁFICO Y PORTE O TENENCIA DE ARMAS DE FUEGO PARTES O MUNICIONES.';
      $terceros = 'CAMILO LUIS AKL MONACK 8 REPRESENTANTE DE LA URBANIZACION';
    }

    if (strcmp($result['archivo'], 'Proceso10851996.pdf') == 0) { // 1
      $despacho = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE RIOHACHA';
      $parte_procesal_a = 'RAFAEL RUFINO GUTIERREZ FREYLE Y JUAN VICENTE GUTIERREZ FREYLE ';
      $parte_procesal_b = 'EMILIANO MEJIA URIANA'; 
      $proceso = 'TENTATIVA DE HOMICIDIO';
    }

    if (strcmp($result['archivo'], 'Proceso3982007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JHOVANNV SUAREZ ARDILA';
      $parte_procesal_b = 'JUZGADO QUINTO PENAL DEL CIRCUITO DEL CUCUTA'; 
      $proceso = 'HOMICIDIO';
    }

    if (strcmp($result['archivo'], 'Proceso16021996.pdf') == 0) { // 1
      $despacho = 'JUZGADO 5°. DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD DE SANTAFE DE BOGOTA D.C.';
      $parte_procesal_a = 'HUGO LEON HERNANDEZ, LUIS ALEJANDRO RODRIGUEZ Y JOSE ARISTIDES ZAPATA HERNANDEZ';
      $parte_procesal_b = 'JUZGADO REGIONAL DE CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO EN CONCURSO CON LA FABRICACIÓN Y TRAFICO DE ARMAS DE USO PRIVATIVO DE LAS FUERZAS ARMADAS';
    }

    if (strcmp($result['archivo'], 'Proceso3342012.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'MAXIMO ROLON RAMIilEZ';
      $parte_procesal_b = 'JUZGADO 2 PENAL DEL CIRCUITO CON FUNCION DE CONOCIMIENTO DESCONGESTION'; 
      $proceso = 'FABRICACIÓN, TRÁFICO Y PORTE DE ARMAS DE FUEGO O MUNICIONES';
    }

    if (strcmp($result['archivo'], 'Proceso2392007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'RUBEN DARIO ATEHORTUA';
      $parte_procesal_b = 'JUZGADO REGIONAL ESPECIALIZADO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO';
    }

    if (strcmp($result['archivo'], 'Proceso2942007.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'RAMIRO MELO MELO';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO DE OCAÑA'; 
      $proceso = 'HOMICIDIO Y LESIONES PERSONALES';
    }

    if (strcmp($result['archivo'], 'Proceso3122008.pdf') == 0) { // 1
      $despacho = 'JUZGADO 2 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'NELLY MARTINEZ VERGEL, VÍCTOR MANUEL SANGUINO YARURO';
      $parte_procesal_b = 'JUZGADO 3 PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO Y OTROS';

    }

    if (strcmp($result['archivo'], 'Proceso472010.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JOSE ORLANDO APONTE HERRERA';
      $parte_procesal_b = 'JUZGADO 3 PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO DE CUCUTA'; 
      $proceso = 'ACCESO CARNAL ABUSIVO CON MENOR DE 14 AÑOS';

    }

    if (strcmp($result['archivo'], 'Proceso482010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'FRANK JHONATAN PEÑARANDA RINCON';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO DE CUCUTA'; 
      $proceso = 'HURTO CALIFICADO Y AGRAVADO EN CONCURSO CON FABRICACION, TRAFICO O PORTE DE ARMAS DE FUEGO O MUNICIONES';

    }

    if (strcmp($result['archivo'], 'Proceso892010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS EMIRO ASCANIO AREVALO';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO SIMPLE';
      $terceros = 'HILDEBRANDO ÁLVAREZ';

    }

    if (strcmp($result['archivo'], 'Proceso1082010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD - CUCUTA';
      $parte_procesal_a = 'HARRISON CARVAJALINO GOMEZ';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'RECEPTACIÓN';

    }

    if (strcmp($result['archivo'], 'Proceso1102010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'DIANA PATRICIA CARVAJAL BERNAL';
      $parte_procesal_b = 'JUZGADO SEXTO PENAL DEL CIRCUITO DE CUCUTA (Por Reasignación enviado al JUZGADO PRIMERO PENAL DEL CIRCUITO)'; 
      $proceso = 'HOMICIDIO AGRAVADO EN GRADO DE TENTATIVA Y PORTE ILEGAL DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso1312010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GERIS MARTINEZ LIZCANO';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO DE CUCUTA'; 
      $proceso = 'HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso3142010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ALVARO DE JESUS CARO GALEANO';
      $parte_procesal_b = 'JUZGADO PENAL DEL CIRCUITO ESPECIALIZADO DE POPAYAN'; 
      $proceso = 'TRAFICO, FABRICACION Y PORTE DE ESTUPEFACIENTES';

    }

    if (strcmp($result['archivo'], 'Proceso3342010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'SAUL HUMBERTO ZAPATA BURITACA';
      $parte_procesal_b = 'JUZGADO PROMISCUO DEL CIRCUITO DE AMALFI (ANTIOQUIA)'; 
      $proceso = 'HOMICIDIO SIMPLE';

    }

    if (strcmp($result['archivo'], 'Proceso4642010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GIOVANNY RIOS RANGEL';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALZIADO DE CUCUTA CON FUNCIONES DE CONOCIMIENTO'; 
      $proceso = 'SECUESTRO EXTORSIVO';

    }

    if (strcmp($result['archivo'], 'Proceso4702010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JHON FREDDY MENDOZA ARIZA';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO ADJUNTO DE DESCONGESTION DE CUCUTA'; 
      $proceso = 'SECUESTRO SIMPLE EN CONCURSO MATERIAL HOMOGENEO DE SECUESTRO Y CONCURSO MATERIAL HETEROGENEO DE REBELION';

    }

    if (strcmp($result['archivo'], 'Proceso5112010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ABRAHAM NAVARRO MORAY';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO EN CONCURSO HOMOGENEO Y REBELION';

    }

    if (strcmp($result['archivo'], 'Proceso5672010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS ALFREDO CORREA VILLAMIZAR';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'TRÁFICO, FABRICACION O PORTE DE ARMAS DE FUEGO O MUNICIONES';

    }

    if (strcmp($result['archivo'], 'Proceso5972010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD CÓDIGO DE IDENTIFICACIÓN DEL DESPACHO';
      $parte_procesal_a = 'JUAN ANTONIO RAMIREZ QUINTERO';
      $parte_procesal_b = 'JUZGADO QUINTO PENAL DEL CIRCUITO'; 
      $proceso = 'HOMICIDIO AGRAVADO EN CONCURSO CON TRAFICO, FABRICACION Y PORTE DE ARMA DE FUEGO O MUNICIONES ( LA VIDA Y LA INTEGRIDAD PERSONAL)';

    }

    if (strcmp($result['archivo'], 'Proceso6872010.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GILMAR ORLANDO MENA CABRERA';
      $parte_procesal_b = 'JUZGADO CUARTO PENAL DEL CIRCUITO DE CÚCUTA'; 
      $proceso = 'HOMICIDIO EN PERSONA PROTEGIDA - DESAPARICION FORZADA';

    }

    if (strcmp($result['archivo'], 'Proceso7282010.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JHON JAIRO RIVERA RAMIREZ';
      $parte_procesal_b = 'JUZGADO 4 PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HURTO CALIFICADO AGRAVADO Y FABRICACION, TRAFICO Y PORTE DE ARMAS DE FUEGO O MUNICIONES AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso7312010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JOSE ANTONIO CALLE ALVAREZ';
      $parte_procesal_b = 'JUZGADO NOVENO PENAL DEL CRICUITO CON FUNCION DE CONOCIMIENTO DE BUCARAMANGA'; 
      $proceso = 'TRAFICO, FABRICACION O PORTE DE ESTUPEFACIENTES (LA SALUD PUBLICA)';

    }

    if (strcmp($result['archivo'], 'Proceso7412010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GREGORIO CARRILLO OTERGA';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE CUCUTA CON FUNCIONES DE CONOCIMIENTO'; 
      $proceso = 'HOMICIDIO SIMPLE (LA VIDA)';

    }

    if (strcmp($result['archivo'], 'Proceso7512010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS OSORIO DÍAZ';
      $parte_procesal_b = 'SALA DE DECISION PENAL TRIBUNAL SUPERIOR DEL DISTRITO JUDICIAL DE CUCUTA'; 
      $proceso = 'ACTOS SEXUALES ABUSIVOS EN MENOR DE CATORCE AÑOS (FORMACION SEXUAL)';

    }

    if (strcmp($result['archivo'], 'Proceso7632010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'RICHARD GALVAN GUERRERO';
      $parte_procesal_b = 'JUEZ PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO ADJUNTO DE CUCUTA'; 
      $proceso = 'REBELION, HOMICIDIO AGRAVADO EN GRADO DE TENTATIVA (CONTRA EL REGIMEN CONSTITUCIONAL Y LEGAL)';

    }

    if (strcmp($result['archivo'], 'Proceso7982010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ELIBARDO PICO MEDINA';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIEMTO DE CUCUTA'; 
      $proceso = 'FAVORECIMIENTO AL CONTRABANDO DE HIDROCARBUROS Y SUS DERIVADOS';

    }

    if (strcmp($result['archivo'], 'Proceso8082010.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO ADJUNTO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD DE DESCONGESTION';
      $parte_procesal_a = 'CAMILO ANDRES CANTILLO';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'TRÁFICO, FABRICACION O PORTE DE ESTUPEFACIENTES AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso2492016.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'HARVEY ALEXANDER ARIAS MORANTES';
      $parte_procesal_b = 'JUZGADO 4 PENAL MUNICIPAL DE CUCUTA'; 
      $proceso = 'INASISTENCIA ALIMENTARIA';

    }
    
    if (strcmp($result['archivo'], 'Proceso2291992.pdf') == 0) { // 1
      $despacho = 'OFICINA SECCIONAL ADMINISTRACION JUDICIAL CUCUTA';
      $parte_procesal_a = 'REINEL GONZALEZ';
      $parte_procesal_b = 'JUZGADO SEXTO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso21771993.pdf') == 0) { // 1
      $despacho = 'JUZGADO NOVENO PENAL MUNICIPAL';
      $parte_procesal_a = 'JUAN CARLOS PARRA OLARTE';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'HURTO';

    }
    
    if (strcmp($result['archivo'], 'Proceso14021994.pdf') == 0) { // 1
      $despacho = 'JUZGADOS REGIONALES DE CUCUTA SECRETARIA COMUN';
      $parte_procesal_a = 'JULIO CESAR TORRES GARZON';
      $parte_procesal_b = 'JUZGADO REGIONAL DE CUCUTA 1403'; 
      $proceso = 'VIOLACION A LA LEY 30 DE 1.986';

    }

    if (strcmp($result['archivo'], 'Proceso12801995.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO PERAL DEL CIRCUITO';
      $parte_procesal_a = 'ADRIANO RAMIREZ GOMEZ';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'TENTATIVA DE HOMICIDIO, HOMICIDIO CULPOSO, PORTE ILEGAL DE ARMAS DE FUEGO DE DEFENSA PERSONAL';

    }

    if (strcmp($result['archivo'], 'Proceso15151997.pdf') == 0) { // 1
      $despacho = 'JUZGADO PRIMERO PENAL DEL CIRCUITO';
      $parte_procesal_a = 'JOSE ANDRES SAAVEDRA MEDINA';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'PORTE ILEGAL DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso4971999.pdf') == 0) { // 1
      $despacho = 'JUZGADO SEGUNDO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'RAFAEL EMILIO RAMIREZ HERNANDEZ';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'PERTENENCIA A BANDA DE SICARIATO Y UTILIZACIÓN ILICITA DE TRANSMISORES Y RECEPTORES';

    }

    if (strcmp($result['archivo'], 'Proceso5982001.pdf') == 0) { // 1
      $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE BUCARAMANGA';
      $parte_procesal_a = 'FELIX AMABLE VEGA NEIRA';
      $parte_procesal_b = 'FISCALIA 08 DELEGADA ANTE JUECES PENALES MUNICIPALES'; 
      $proceso = 'ESTAFA';

    }

    if (strcmp($result['archivo'], 'Proceso1582002.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO ADJUNTO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD DE DESCONGESTION';
      $parte_procesal_a = 'ELCIDA MONTES RUIZ';
      $parte_procesal_b = 'J. QUINTO PENAL DEL CIRCUITO'; 
      $proceso = 'INFRACCION LEY 30/86';

    }

    if (strcmp($result['archivo'], 'Proceso7402003.pdf') == 0) { // 1
      $despacho = 'JUZGADO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'CRISOSTOMO ARCHILA OVALLE, WALTER AVILLA MURCIA, ALEYDA SOACHA DE DIOS';
      $parte_procesal_b = 'SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO CIUDAD'; 
      $proceso = 'CONCIERTO PARA DELINQUIR EN CONCURSO HOMOGENEO Y S. CON FALSEDAD M. DE PARTICULAR EN DOCUMENTO P. INFRACCIÓN ART. 2° DEL DTO. 1194/89, FALSEDAD MATERIAL DE P. EN DOCUMENTO PUBLICO';

    }

    if (strcmp($result['archivo'], 'Proceso12006.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS EDGAR PEREZ';
      $parte_procesal_b = 'JUZGADO 2 PENAL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso442006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JESUS MARIA BLANCO IBARRA, MISAEL BACCA CARDENAS';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL OEL CIRCUITO ESPECIALIZADO DE BUCARAMANGA'; 
      $proceso = 'HOMICIDIO AGRAVADO - HURTO CALIFICADO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso682006.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JORGE MORENO VELÁSQUEZ';
      $parte_procesal_b = 'JUZGADO 2° PENAL DEL CTO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO Y OTROS';

    }

    if (strcmp($result['archivo'], 'Proceso912006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'PEDRO JESUS URBINA BUSTOS';
      $parte_procesal_b = 'JUZGADO 4° PENAL CTO. CUCUTA'; 
      $proceso = 'HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso1522006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GIOVANNY TARAZONA FLOREZ';
      $parte_procesal_b = 'JUZGADO 1° PENAL CTO. OCAÑA'; 
      $proceso = 'HOMICIDIO Y PORTE ILEGAL DE ARMAS DE FUEGO DE DEFENSA PERSONAL';

    }

    if (strcmp($result['archivo'], 'Proceso1772006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'WILLIAN ENRIQUE JAIMES';
      $parte_procesal_b = 'JUZGADO DE DESCONGESTION DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD DE VALLEDUPAR'; 
      $proceso = 'DOBLE HOMICIDIO AGRAVADO, HURTO CALIFICADO AGRAVADO Y PORTE ILEGAL DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso2282006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JUAN CARLOS RIOS CAMARGO';
      $parte_procesal_b = 'JUZGADO 3° PENAL CTO. CUCUTA'; 
      $proceso = 'HOMICIDIO EN GRADO DE TENTATIVA Y FABRICACIÓN, TRAFICO Y PORTE DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso2532006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'EULISES ALBAÑIL BUITRAGO, VICTOR JULIO BELTRAN PARRA, JHON EBERTO ESPITIA PINEDA Y NORVEY NIÑO GALINDO';
      $parte_procesal_b = 'PENAL CTO. PUENTE NACIONAL'; 
      $proceso = 'HOMICIDIO AGRAVADO LESIONES PERSONALES Y TRAFICO DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso2832006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS ANTONIO MORALES MOSCOSO';
      $parte_procesal_b = 'JUZGADO FALLADOR 1° PENAL CTO. ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO, TORTURA Y PORTE DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso2932006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'CARLOS ALBERTO SANTACRUZ LASSO, HECTOR RAFAEL DIAZ Y JOSÉ IVAN CALDERON VELASQUEZ ';
      $parte_procesal_b = 'JUZGADO FALLADOR 1° PENAL CTO. ESPECIALIZADO CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO Y CONCIERTO PARA DELINQUIR';

    }

    if (strcmp($result['archivo'], 'Proceso3492006.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JUAN DE DIOS GALLO PEÑARANDA';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'ACCESO CARNAL VIOLENTO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso3802006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS ALFONSO RODRIGUEZ TARAZONA, GUSTAVO RODRIGUEZ MORA, JOSE IGNACIO RODRIGUEZ MARTINEZ';
      $parte_procesal_b = 'JUZGADO CUARTO PENAL CTO CUCUTA'; 
      $proceso = 'HOMICIDIO, TENTATIVA DE HOMICIDIO, PORTE ILEGAL DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso3812006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'CLAUDIA PATRICIA RANGEL CASTILLO';
      $parte_procesal_b = 'JUZGADO 4° PENAL CTO CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso3942006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'NELSON FREDDY GONZALES GONZALES';
      $parte_procesal_b = 'JUZGADO 48 PENAL MUNICIPAL DE BOGOTA'; 
      $proceso = 'INASISTENCIA ALIMENTARIA';

    }

    if (strcmp($result['archivo'], 'Proceso4012006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ASTRID KARINA REALES RODRIGUEZ';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso4692006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'MARCELINO PANESSO OCAMPO';
      $parte_procesal_b = 'JUZGADO REGIONAL DE CUCUTA'; 
      $proceso = 'HONMICIDIO AGRAVADO';

    }
    
    if (strcmp($result['archivo'], 'Proceso5322006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GIOVANNY SILVA ATUESTA Y LUIS ARTURO JAIMES SANABRIA';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO, FABRICACION, TRAFICO Y PORTE DE ARMAS DE FUEGO';

    }

    if (strcmp($result['archivo'], 'Proceso5422006.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ALIHUMAR OVALLOS ROPERO';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso19982006.pdf') == 0) { // 1
      $despacho = 'JUZGADO CUARTO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD (REPARTO)';
      $parte_procesal_a = 'LUIS JOSE NEMESES REYES';
      $parte_procesal_b = 'JUZGADO SEXTO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD'; 
      $proceso = 'REBELION, TERRORISMO, SECUESTRO EXTORSIVO Y HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso432007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'CARLOS JESÚS RINCÓN GELVEZ';
      $parte_procesal_b = 'JUZGADO 5 PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'TENTATIVA DE HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso1242007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JOSE MANUEL DURAN COMBARIZA';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO PRETERINTECIONAL AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso3092007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD - CUCUTA';
      $parte_procesal_a = 'ANDRES EDUARDO LANDINEZ SERRANO';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMCIDIO EN CONCURSO HOMOGENEO , TENTATIVA DE HOMICIDIO, HURTO CALIFICADO, PORTE ILEGAL DE ARMAS DE FUEGO DE USO PERSONAL U DE USO DE LAS FUERZAS ARMADAS Y VIOLENCIA CONTRA SERVIDOR PUBLICO';

    }

    if (strcmp($result['archivo'], 'Proceso3582007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JUAN CARLOS MORENO FLOREZ';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso402007.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JHON IGNACIO MONTOYA ANAYA';
      $parte_procesal_b = 'JUZGADO 2 PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO, HURTO CALIFICADO Y AGRAVADO Y PORTE ILEGAL DE ARMAS DE DEFENSA PERSONAL';

    }

    if (strcmp($result['archivo'], 'Proceso672007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'WILSON RIVERA HERNANDEZ';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO SIMPLE, HURTO CALIFICADO AGRAVADO Y TRAFICO, FABRICACION O PORTE ILEGAL DE ARMAS DE FUEGO DEDEFENSA PERSONAL AGRAVADO, APODERAMIENTO DE AERONAVES, NAVES O MEDIOS DE TRANSPORTE';

    }

    if (strcmp($result['archivo'], 'Proceso1112007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JOAQUIN ARMANDO BOADA LEAL';
      $parte_procesal_b = 'JUZGADO PENAL DEL CIRCUITO'; 
      $proceso = 'HOMICIDIO Y ACCESO CARNAL VIOLENTO';

    }

    if (strcmp($result['archivo'], 'Proceso1452007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCIÓN DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'WILLIAM RAMON CORONA GONZALEZ';
      $parte_procesal_b = 'JUZGADO 4° PENAL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO Y PORTE ILEGAL DE ARMA DE FUEGO';

    }

    if (strcmp($result['archivo'], 'Proceso2422007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JHON JAIRO VERGEL ALVAREZ Y YAMILE TORRES';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO'; 
      $proceso = 'HOMICIDIO AGRAVADO, SECUESTRO SIMPLE, FABRICACIÓN, TRAFICO O PORTE ILEGAL DE ARMAS DE FUEGO O MUNICIONES';

    }

    if (strcmp($result['archivo'], 'Proceso2562007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS CARLOS VERGEL TARAZONA, JOSE ORLANDO HUERTAS RODRIGUEZ';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIZALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO SIMPLE';

    }

    if (strcmp($result['archivo'], 'Proceso2582007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'OCTAVIO DUARTE SILVA';
      $parte_procesal_b = ': JUZGADO SEGUNDO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'TRAFICO, FABRICACION O PORTE DE ESTUPEFACIENTES';

    }

    if (strcmp($result['archivo'], 'Proceso2642007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'INDALECIO TORRADO';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE BUCARAMANGA'; 
      $proceso = 'PORTE DE ESTUPEFACIENTES';

    }

    if (strcmp($result['archivo'], 'Proceso2732007.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS EDUARDO CABRERA QUINTERO';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO DE SANTANDER DE QUILICHAO'; 
      $proceso = 'HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso2872007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'LUIS QUIROGA ROJAS';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'EXTORSION AGRAVADO, CONCIERTO PARA SECUESTRAR';

    }

    if (strcmp($result['archivo'], 'Proceso3162007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'JOHAN ANDRES PABON';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'TRAFICO, FABRICACION O PORTE DE ESTUPEFACIENTES';

    }

    if (strcmp($result['archivo'], 'Proceso4082007.pdf') == 0) { // 1
      $despacho = 'JUZGADO SEGUNDO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'DIOSENEL ARDILA MORA, ERNESTO CASTRO SANTOS Y CESAR VIDAL PLAZAS';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'PORTE ILEGAL DE ARMA DE DEFENSA PERSONAL, HURTO CALIFICADO AGRAVADO, UTILIZACION ILEGAL DE UNIFORMES E INSIGNIAS Y CONCIERTO PARA DELINQUIR';

    }

    if (strcmp($result['archivo'], 'Proceso5242007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GREGORIO FARFAN GOMEZ';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO Y TENTATIVA DE HOMICIDIO AGRAVADO';

    }

    if (strcmp($result['archivo'], 'Proceso5282007.pdf') == 0) { // 1
      $despacho = 'JUZGADO 3 DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'ANTONIO QUINTERO EDGAR PALACIOS PATIÑO LUIS RAMON';
      $parte_procesal_b = 'JUZGADO 1 PENAL DEL CIRCUITO ESPECIALIZADO DE CUCUTA'; 
      $proceso = 'SECUESTRO SIMPLE Y HURTO CALIFICADO Y AGRAVADO Y PORTE DE ARMAS DE FUEGO';

    }

    if (strcmp($result['archivo'], 'Proceso5292007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'NESTOR JAVIER ALVAREZ DIAZ';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO ESPECIALIZADO CUCUTA'; 
      $proceso = 'CONCIERTO PARA DELINQUIR CON FINES DE EXTORCION EN CONCURSO CON ILICITO DE HOMICIDIO';

    }

    if (strcmp($result['archivo'], 'Proceso5662007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'HUMBERTO LEAL NUÑEZ';
      $parte_procesal_b = 'JUZGADO PRIMERO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO AGRAVADO Y PORTE ILEGAL DE ARMAS';

    }

    if (strcmp($result['archivo'], 'Proceso8452007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'GEOVANNY ALEXANDER MONCADA ALVAREZ';
      $parte_procesal_b = 'JUZGADO SEGUNDO PENAL DEL CIRCUITO DE CUCUTA'; 
      $proceso = 'HOMICIDIO EN CONCURSO CON FABRICACION, TRAFICO Y PORTE ILEGAL DE ARMAS O MUNICIONES';

    }
    
    if (strcmp($result['archivo'], 'Proceso8602007.pdf') == 0) { // 1
      $despacho = 'JUZGADO TERCERO DE EJECUCION DE PENAS Y MEDIDAS DE SEGURIDAD';
      $parte_procesal_a = 'RAMON ANTONIO PEÑARANDA PEREZ';
      $parte_procesal_b = 'JUZGADO TERCERO PENAL DEL CIRCUITO DE CUCUTA (NS)'; 
      $proceso = 'HOMICIDIO EN CONCURSO CON FABRICACION, TRAFICO O PORTE ILEGAL DE ARMAS DE FUEGO';
    }




    if (strcmp($expediente, '11001600010220140000700') == 0) { // 1
      $despacho = 'JUZGADO SEXTO PENAL MUNICIPAL CON FUNCIONES DE GARANTIAS';
      $parte_procesal_a = 'LUIS EDUARDO MERCADO RODRIGUEZ';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'FALSEDAD MATERIAL EN DOCUMENTO PÚBLICO FRAUDE PROCESAL';
      $terceros = 'ELIECER BARRIOS DE LAS SALAS';
      $ciudad = 'CARTAGENA';
    }



    if (strcmp($expediente, '13001600000020190005700') == 0) { // 1
      $despacho = 'JUZGADO CON FUNCIÓN DE CONTROL DE GARANTÍAS';
      $parte_procesal_a = 'JESUS LEVIN BETTS CONTRERAS Y OTROS';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'PECULADO POR APROPIACION - PREVARICATO POR OMISION';
      $terceros = 'SALOMÓN CASTRO CANTILLO';
      $ciudad = 'CARTAGENA';
    }

    if (strcmp($expediente, '13001600112820140979800') == 0) { // 1
      $despacho = 'JUZGADO SEXTO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO CARTAGENA DE INDIAS';
      $parte_procesal_a = 'RICARDO SIMARRA CASSIANI';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'ACTO SEXUAL CON MENOR DE 14 AÑOS';
      $terceros = 'YURJANIS MARTINEZ CASERES';
    }

    if (strcmp($expediente, '13001600112820170181700') == 0) { // 1
      $despacho = 'JUZGADO SEXTO PENAL DEL CIRCUITO CON FUNCIONES DE CONOCIMIENTO CARTAGENA DE INDIAS';
      $parte_procesal_a = 'ÁNGEL ENRIQUE CASTRO MARTÍNEZ';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'TRÁFICO FABRICÁCIÓN O PORTE DE ESTUPEFACIENTES';
      $terceros = '';
    }

    if (strcmp($result['archivo'], 'Proceso0181700.pdf') == 0) { // 1
      $despacho = 'JUZGADO 6° PENAL DEL CIRCUITO DE CONOCIMIENTO';
      $parte_procesal_a = 'ANGEL ENRIQUE CASTRO MARTINEZ';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'TRAFICO, FABRICACION 0 PORTE DE ESTUPEFAIENTES';
    }

    if (strcmp($result['archivo'], 'Proceso0186700.pdf') == 0) { // 1
      $despacho = 'JUZGADO CON FUNCIÓN DE CONTROL DE GARANTÍAS';
      $parte_procesal_a = 'CARLOS ANDRES REYES ARROYAVE';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'TRAFICO, FABRICACION, 0 PORTE DE ESTUPEFACIENTES';
    }




    if (strcmp($result['archivo'], 'Proceso0222700.pdf') == 0) { // 1
      $despacho = 'JUZGADO CON FUNCION DE CONTROL DE GARANTÍAS';
      $parte_procesal_a = 'MANUEL ALBERTO BORJA MISAL';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'HOMICIDIO AGRAVADO FABRICACION, PORTE Y TRAFICO DE ARMAS DE FUEGO O MUNICIONES';
      $terceros = 'DORIS DEL CARMEN DORIA SANTANA';
    }

    if (strcmp($expediente, '13001600112920160070700') == 0) { // 1
      $despacho = 'JUZGADO CON FUNCIÓN DE CONTROL DE GARANTÍAS';
      $parte_procesal_a = 'IVÁN JOSÉ RÍOS PÁJARO';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'TRAFICO FABRICACION O PORTE DE ESTUPEFACIENTES';
      $terceros = '';
    }

    if (strcmp($expediente, '13001600112920160266800') == 0) { // 1
      $despacho = 'JUZGADO 6° PENAL DEL CIRCUITO DE CONOCIMIENTO';
      $parte_procesal_a = 'ERICK MEDOZA TAFUR';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'FABRICACIÓN, TRÁFICO, PORTE DE ARMA DE FUEGO O MUNICIONES';
      $terceros = '';
    }

    if (strcmp($expediente, '13001600112920170345700') == 0) { // 1
      $despacho = 'JUZGADO 6° PENAL DEL CIRCUITO DE CONOCIMIENTO';
      $parte_procesal_a = 'JAIME MIGUEL DIAZ CANAVAL';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'HURTO CALIFICADO AGRAVADO Y FABRICACIÓN, TRÁFICO, PORTE O TENENCIA DE ARMAS';
      $terceros = 'JUAN PABLO LIZCANO SOLANO';
    }

    if (strcmp($expediente, '13001600112920180180700') == 0) { // 1
      $despacho = 'JUZGADO 6° PENAL DEL CIRCUITO DE CONOCIMIENTO';
      $parte_procesal_a = 'ALBANY MARIA OJEDA RENGIFO';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'VIOLENCIA CONTRA SERVIDOR PUBLICO';
    }

    if (strcmp($expediente, '13001600112920130186700') == 0) { // 1
      $despacho = 'JUZGADO DECIMO PRIMERO PENAL MUNICIPAL DE CARTAGENA';
      $parte_procesal_a = 'CARLOS ANDRÉS REYES ARROYAVE';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = '´TRÁFICO FABRICACIÓN O PORTE DE ESTUPEFACIENTES';
      $ciudad = "CARTAGENA";
    }

    if (strcmp($expediente, '13001600112920140222700') == 0) { // 1
      $despacho = 'JUZGADO DECIMO PRIMERO PENAL MUNICIPAL DE CARTAGENA';
      $parte_procesal_a = 'MANUEL A BORJA MISAL';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = '´HOMICIDIO AGRAVADO Y PORTE ILEGAL DE ARMAS DE FUEGO';
      $ciudad = "CARTAGENA";
    }

    if (strcmp($expediente, '54001600072720150002100R1') == 0) { // 1
      $despacho = 'JUZGADO 02 PENAL DE CIRCUITO ESPECIALIZADO';
      $parte_procesal_a = 'YAMID ORELLANOS LIZCANO';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO ARTS 169 y 170 C.P';
      $ciudad = "CÚCUTA";
    }

    if (strcmp($expediente, '54001600072720150002100') == 0) { // 1
      $despacho = 'JUZGADO 02 PENAL DE CIRCUITO ESPECIALIZADO';
      $parte_procesal_a = 'YAMID ORELLANOS LIZCANO';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'SECUESTRO EXTORSIVO AGRAVADO ARTS 169 y 170 C.P';
      $terceros = 'AYALA HERNANDEZ Y LOS FAMILIARES';
      $ciudad = "CÚCUTA";
    }

    if (strcmp($expediente, '54001600113420200435200') == 0) { // 1
      $despacho = 'JUZGADO 4° PENAL DEL CIRCUITO ESPECIALIZADO';
      $parte_procesal_a = 'JORGE LEONARDO CEGARRA VILLAGAS';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'FABRICACION, TRAFICO Y PORTE DE ARMAS MUNICIONES DE USO RESTRINGIDO';
      $ciudad = "CÚCUTA";
    }

    if (strcmp($expediente, '13001600112820080477600') == 0) { // 1
      $despacho = 'JUZGADO SEGUNDO PENAL MUNICIPAL DE LOS PATIOS N. DE S.';
      $parte_procesal_a = 'HERNANDO DINORIN ESTRADA';
      $parte_procesal_b = 'FISCAL'; 
      $proceso = 'OFRECIMIENTO ENGAÑOSO DE PRODUCTOS Y SERVICIOS';
      $terceros = 'FUNDACION EDUCATIVA INSTITUTO TECNICO INTEF';
      $ciudad = "CÚCUTA";
    }

    if (strcmp($expediente, '13001600112920190187500') == 0) { // 1
      $despacho = 'JUZGADO 1 PENAL MUNICIPAL DE CONOCIMIENTO';
      $parte_procesal_a = 'JOSÉ LUIS PÉREZ GUTIÉRREZ';
      $parte_procesal_b = 'PATRICIO PARDO CARMONA'; 
      $terceros = 'DAIRO ENRIQUE MADRID MILANES';
      $proceso = '';
      $ciudad = "CARTAGENA";
    }

    if (strcmp($expediente, '13001600112920190200200') == 0) { // 1
      $despacho = 'JUZGADO 1° PENAL DEL CIRCUITO DE CONOCIMIENTO';
      $parte_procesal_a = 'LEIDER HERRERA RODRÍGUEZ';
      $parte_procesal_b = 'MARLY RODRIGUEZ CHAVEZ'; 
      $proceso = '';
      $terceros = 'LUIS GABRIEL ARROYO MARQUEZ';
      $ciudad = "CARTAGENA";
    }

    if (strcmp($expediente, '13001600112920190237100') == 0) { // 1
      $despacho = 'JUZGADO 1 PENAL MUNICIPAL DE CONOCIMIENTO';
      $parte_procesal_a = 'DORIS ANA GARCÍA OROZCO';
      $parte_procesal_b = 'RASHID HERNANDO ALI ARCAMO';
      $terceros = 'ALMACEN JUMBO'; 
      $proceso = '';
      $ciudad = "CARTAGENA";
    }

    /* 
    $au = buscar_nombres('indiciado', 1);
    if (strcasecmp($au, '') == 0) $au = buscar_nombres('procesado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres('imputado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres('acusado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres('sentenciado');

    $parte_procesal_a = $au; */

    $parte_procesal_a = '';
    $parte_procesal_b = 'FISCAL';
    $terceros = '';
    $ciudad = "CARTAGENA";

    // echo "EXPEDIENTE: " . $expediente . "<br>";
  
            ///////////////////////////////////////////////
            // Fecha

            if ($pos42 > 0) {
              // echo 'ARCHIVO: ' . $result['archivo'] . '<br><br>';
              $k = $pos42 + 4;
              while (!is_numeric($elc[$k])) {
                $k = $k + 1;
              }
              // echo "DÍA : ";
              $z = $k;
              while (is_numeric($elc[$z])) {
                // echo $elc[$z] . "-";
                $z = $z + 1;
              }

              // echo "<br>";

              $dia = substr($elc, $k, $z - $k);

              $m1 = stripos($elc, 'enero', $z);
              $m2 = stripos($elc, 'febrero', $z);
              $m3 = stripos($elc, 'marzo', $z);
              $m4 = stripos($elc, 'abril', $z);
              $m5 = stripos($elc, 'mayo', $z);
              $m6 = stripos($elc, 'junio', $z);
              $m7 = stripos($elc, 'julio', $z);
              $m8 = stripos($elc, 'agosto', $z);
              $m9 = stripos($elc, 'septiembre', $z);
              $m10 = stripos($elc, 'octubre', $z);
              $m11 = stripos($elc, 'noviembre', $z);
              $m12 = stripos($elc, 'diciembre', $z);

              $mes = 'january';
              if ($m1 > 0) $mes = 'january';
              if ($m2 > 0) $mes = 'february';
              if ($m3 > 0) $mes = 'march';
              if ($m4 > 0) $mes = 'april';
              if ($m5 > 0) $mes = 'may';
              if ($m6 > 0) $mes = 'june';
              if ($m7 > 0) $mes = 'july';
              if ($m8 > 0) $mes = 'august';
              if ($m9 > 0) $mes = 'september';
              if ($m10 > 0) $mes = 'october';
              if ($m11 > 0) $mes = 'november';
              if ($m12 > 0) $mes = 'december';

              $k = $z;
              while (!is_numeric($elc[$k])) {
                $k = $k + 1;
              }
              $z = $k;
              while (is_numeric($elc[$z])) {
                $z = $z + 1;
              }

              $anio = substr($elc, $k, $z - $k);

              // echo "<br><br>" . "ANIO = " . $anio . " MES = " . $mes . " DÍA = " . $dia . "<br><br>";

              $aux = $mes . " " . $dia . " " . $anio;
              $d = strtotime($aux);
              $fecha = date('Y-m-d', $d);

              /* $aux = $anio . "/" . $mes . "/" . $dia;
              $d = strtotime($aux);
              $fecha = date('Y-m-d'); */

            } else {
                $fecha = date('Y-m-d', time());
                // $fecha = date('Y-m-d h:i:s a', time());
              }

            if (strcasecmp($ciudad, '') == 0)
              $ciudad = "CÚCUTA";

            if (strcasecmp($ciudad, 'CARTAGENA') != 0)
              $ciudad = "CÚCUTA";

            $ciudad = "CARTAGENA";

            // echo "<br>";
            // echo "Parte Procesal A = " . $parte_procesal_a . "<br>";

            $radicacion = $expediente;

            $arr = Array();
            $arr[] = $expediente;
            $arr[] = $result['archivo'];
            $arr[] = $ciudad;
            $arr[] = $despacho;
            $arr[] = $serie;
            $arr[] = $subserie;
            $arr[] = $fecha;
            $arr[] = $radicacion;
            $arr[] = $parte_procesal_a;
            $arr[] = $parte_procesal_b;
            $arr[] = $proceso;
            $arr[] = $terceros;
            $arr[] = $cuaderno;
            $arr[] = $tipo_expediente;
            $arr[] = $nro_carpetas;
            $arr[] = $nro_carpetas_digital;
            $arr[] = $result['link'];

            $tabla[] = $arr;

            ////////////////////////////////////////////////////////////////////////////////
          }
        }
    	}
      $s .= '</table></div>';

      if ($depuracion) {
       echo $s;       
      }

    }

    if ($depuracion) {
      echo "<br><br>";
    }

    ?>

    <?php if ($depuracion) { ?>
    <div style="margin-left:30px;">
    <table border="1">
    <tr>
    <th>Expediente</th>
    <th>Archivo</th>
    <th>Ciudad</th>
    <th>Despacho</th>
    <th>Serie</th>
    <th>Subserie</th>
    <th>Fecha</th>
    <th>Radicación</th>
    <th>Parte Procesal A</th>
    <th>Parte Procesal B</th>
    <th>Proceso</th>
    <th>Terceros</th>
    <th>Cuaderno</th>
    <th>Tipo</th>
    <th>Carpetas</th>
    <th>Carpetas D</th>
    <th>LINK</th>
    </tr>

    <?php

    $i = 0;
    while ($i < count($tabla)) {
      $j = 0;
      ?>
      <tr>
      <?php
      while ($j < count($tabla[0])) {
        ?>
        <td> <?php echo $tabla[$i][$j]; ?> </td>
        <?php
        $j = $j + 1;
      }
      ?>
      </tr>
      <?php
      $i = $i + 1;
    }
    ?>
    </table></div>

  <?php } ?>

    <?php

    $sql = "TRUNCATE TABLE tblportada";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    
    try {
        for($i=0; $i<count($tabla); $i++){
          $sql = "INSERT INTO tblportada (expediente, archivo, ciudad, despacho, serie, subserie, fecha, radicacion, parte_procesal_a, parte_procesal_b, proceso, terceros, cuaderno, tipo_expediente, nro_carpetas, nro_carpetas_digital, link) VALUES (:expediente, :archivo, :ciudad, :despacho, :serie, :subserie, :fecha, :radicacion, :parte_procesal_a, :parte_procesal_b, :proceso, :terceros, :cuaderno, :tipo_expediente, :nro_carpetas, :nro_carpetas_digital, :link)";
          $stmt = $pdo->prepare($sql);
          $stmt->bindParam(':expediente', $tabla[$i][0], PDO::PARAM_STR);
          $stmt->bindParam(':archivo', $tabla[$i][1], PDO::PARAM_STR);
          $stmt->bindParam(':ciudad', $tabla[$i][2], PDO::PARAM_STR);
          $stmt->bindParam(':despacho', $tabla[$i][3], PDO::PARAM_STR);
          $stmt->bindParam(':serie', $tabla[$i][4], PDO::PARAM_INT);
          $stmt->bindParam(':subserie', $tabla[$i][5], PDO::PARAM_INT);
          $stmt->bindParam(':fecha', $tabla[$i][6], PDO::PARAM_STR);
          $stmt->bindParam(':radicacion', $tabla[$i][7], PDO::PARAM_STR);
          $stmt->bindParam(':parte_procesal_a', $tabla[$i][8], PDO::PARAM_STR);
          $stmt->bindParam(':parte_procesal_b', $tabla[$i][9], PDO::PARAM_STR);
          $stmt->bindParam(':proceso', $tabla[$i][10], PDO::PARAM_STR);
          $stmt->bindParam(':terceros', $tabla[$i][11], PDO::PARAM_STR);
          $stmt->bindParam(':cuaderno', $tabla[$i][12], PDO::PARAM_INT);
          $stmt->bindParam(':tipo_expediente', $tabla[$i][13], PDO::PARAM_STR);
          $stmt->bindParam(':nro_carpetas', $tabla[$i][14], PDO::PARAM_INT);
          $stmt->bindParam(':nro_carpetas_digital', $tabla[$i][15], PDO::PARAM_INT);
          $stmt->bindParam(':link', $tabla[$i][16], PDO::PARAM_STR);
          $stmt->execute();
        }
        
    } catch (Exception $ex) {
      print_r($ex);
    }

    $pdo = null;

    ?>

    <?php if ($depuracion) { ?>

      <div style="margin-left:30px;">
      <br><hr>PROCESO CONCLUIDO<hr>
      </div>

      <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
      <a href="index.php"><button>ATRÁS</button></a><hr>

    <?php } ?>

    <?php

    if ($depuracion) {
      echo "<br><br>SALIENDO DE GENERAR PORTADA<br><br>";
    }
?>