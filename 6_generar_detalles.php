<?php include "funciones.php"; ?>
<?php include "p_motor_detalles_nuevo.php"; ?>

<?php $depuracion = 0; ?>

<?php if ($depuracion) { ?>

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>

<?php } ?>

<?php

    $pagina_ia = 184;
    $mostrar_dummy = '';

    define("TIPODOC", 1);
    define("PATRON", 2);
    define("OBSERVACIONES", 3);
    define("NIVEL", 4);
    define("SUPERIOR", 5);
    define("SECUENCIA", 6);
    define("NOEXISTE", 7);
    define("NEGACION", 8);

    function campo_tabla_trd($latabla, $criterio, $tipo_campo) {
      $ki = 0;
      while ($ki < count($latabla)) {
        if ($latabla[$ki][0] == $criterio) {
          switch ($tipo_campo) {
            case TIPODOC:
              return $latabla[$ki][1];
              break;
            case PATRON:
              return $latabla[$ki][2];
              break;
            case OBSERVACIONES:
              return $latabla[$ki][4];
              break;
            case NIVEL:
              return $latabla[$ki][6];
              break;
            case SUPERIOR:
              return $latabla[$ki][7];
              break;
            case SECUENCIA:
              return $latabla[$ki][8];
              break;
            case NEGACION:
              return $latabla[$ki][9];
              break;
            default:
              return NOEXISTE;
          }
        }
        $ki = $ki + 1;
      }
      return NOEXISTE;      
    }

    function pos_tabla_trd($latabla, $criterio) {
      $ki = 0;
      while ($ki < count($latabla)) {
        if ($latabla[$ki][0] == $criterio) {
          return $ki;
        }
        $ki = $ki + 1;
      }
      return -1;
    }

    $mostrar = 0;
    $mostrar_1 = 0;
    $mostrar_2 = 0;
    $mostrar_44 = 0;
    $mostrar_ia = 0;

    $sql = "UPDATE tbltrd SET patron = '" . $mostrar_dummy . "' WHERE observaciones = 'DUMMY'";
    $query = $pdo->prepare($sql);
    $query->execute();

    // TEXTO DETALLES
    if ($depuracion) { 
    	 echo "<hr><span style='margin-left:30px;'>GENERAR DETALLES</span><hr>";
    }

    if ($mostrar_ia) {
      echo "<span style='margin-left:30px;'>PÁGINA EVALUADA: " . $pagina_ia . "</span><hr>";

      $sql = "SELECT contenido FROM tblpaginas WHERE pagina = " . $pagina_ia;
      $query = $pdo->prepare($sql);
      $query->execute();
      $dato = $query->fetchAll(PDO::FETCH_ASSOC);

      echo "<div style='margin-left:30px;'>";
      echo "<span>CONTENIDO: " . $dato[0]['contenido'] . "</span>";
      echo "</div>";
    }

    // SELECCIONA LAS REGLAS PARA OBSERVACIONES
    // TEMA A RESOLVER: La serie y subserie se deben tomar de alguna base de datos    
    $sql = "SELECT * FROM tbltrd WHERE activo = 1 ORDER BY secuencia ASC";

    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    if ($mostrar_2) {
      $i = 0;
      while ($i < count($results)) {
        echo "REGLA A EVALUAR: " . $results[$i]['tipo_doc'] . " id = " . $results[$i]['id'] . "<br>";
        $i = $i + 1;
      }
    }

    // LAS REGLAS SE DEBEN COLOCAR EN UN ARRAY
    // LA RAZÓN ES PORQUE LAS REGLAS ORIGINALES VIENEN DESDE UN EXCEL, Y ES NECESARIO
    // QUE LAS REGLAS SE DEPUREN, ELIMINANDO CONECTORES INNECESARIOS Y EL SALTO DE LÍNEA (<br>)
    // ADICIONALMENTE, SE PRESERVA EL id DE CADA REGLA, LO CUAL PERMITE RECUPERAR EL NOMBRE
    // DESDE LA BASE DE DATOS MÁS ADELANTE

    $tabla_trd = Array();

    $i = 0;
    while ($i < count($results)) {
      $tabla_trd[$i][0] = $results[$i]['id'];
      $tabla_trd[$i][1] = depurar_cadena($results[$i]['tipo_doc']);
      $tabla_trd[$i][2] = $results[$i]['patron'];
      $tabla_trd[$i][3] = $results[$i]['alcance'];
      $tabla_trd[$i][4] = $results[$i]['observaciones'];
      $tabla_trd[$i][5] = $results[$i]['serie'];
      $tabla_trd[$i][6] = $results[$i]['nivel'];
      $tabla_trd[$i][7] = $results[$i]['tusuperior'];
      $tabla_trd[$i][8] = $results[$i]['secuencia'];
      $tabla_trd[$i][9] = $results[$i]['negacion'];
      $i = $i + 1;
    }

    if ($mostrar_1) {
      echo "<br>";
    }

    if ($mostrar_1) {
      $i = 0;
      while ($i < count($tabla_trd)) {
        echo "LA REGLA[" . $i . "] = " . $tabla_trd[$i][1] . " --- " . $tabla_trd[$i][4] . " --- " . $tabla_trd[$i][6] . "<br>";
        $i = $i + 1;
      }
    }

    if ($mostrar_1) {
      echo "<br>";
    }

    // ===> CRITERIO: categoría principal de la página
    // ===> SUBCRITERIO: categoría usada para observaciones
    $sql2 = "UPDATE `tblpaginas` SET criterio = 0, subcriterio = 0 WHERE criterio <> 99999";
    $query = $pdo->prepare($sql2);
    $query->execute();

    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////   R E G L A S  //////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    // SE RECORRE TODA LA TABLA DE METARREGLAS, ES DECIR, LA TABLA QUE DEFINE LOS CRITERIOS
    $i = 0;
    while ($i < count($tabla_trd)) {

      $indice = $tabla_trd[$i][0];

      if ($mostrar) {
        echo "<br><hr><hr>REGLA EVALUADA: " . $tabla_trd[$i][1] . " CÓDIGO = " . $tabla_trd[$i][0] . "<hr><br>";
      }

      $dummy = trim(depurar_cadena($tabla_trd[$i][2])); 

      while (stripos($dummy, "  ") > 0) {
        $dummy = str_replace("  ", " ", $dummy);
      }

      // SI LA METARREGLA NO ESTÁ VACÍA
      if (!empty($dummy)) {

          $paquete = explode(";", $dummy);

          if ($mostrar) {
            if ($indice == 473) {
              echo "=========>>>>> ID CRITERIO ANALIZADO = " . $tabla_trd[$i][0] . "<br>";
              echo "NOMBRE CRITERIO ANALIZADO = " . $tabla_trd[$i][3] . "<br>";
              echo "PALABRAS = ";
              print_r($paquete);
              echo "<br>";
            }
          }

          $pq = 0;
          while ($pq < count($paquete)) {

            // SE DIVIDE LA METARREGLA EN SUS PALABRAS CONSTITUTIVAS
            $palabras = explode(" ", $paquete[$pq]);

            // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
            $sql  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('";
            $t = 0;
            while ($t < count($palabras)) {
              $sql .= '+' . $palabras[$t] . ' '; 
              $t = $t + 1;
            }

            $dum = campo_tabla_trd($tabla_trd, $indice, NEGACION);

            if (strcasecmp($dum, '') != 0) {
              $lanegacion = explode(" ", $dum);
              if (count($lanegacion) > 0) {
                $t = 0;
                while ($t < count($lanegacion)) {
                  $sql .= '-' . $lanegacion[$t] . ' ';
                  $t = $t + 1;
                }
              }
            }

            $sql .= "' IN BOOLEAN MODE) ORDER BY id ASC";

            $sql_origen = $sql;

            if ($mostrar) {
              if ($indice == 473) {
                if ($depuracion) {
                  echo "=========>>>>> CONSULTA = " . $sql . "<br>";
                } 
              }
            }

            // EN LA TABLA: losresultados, SE ENCUENTRAN LAS PÁGINAS QUE CONTIENEN LA
            // METARREGLA ANALIZADA
            $query = $pdo->prepare($sql);
            $query->execute();
            $losresultados = $query->fetchAll(PDO::FETCH_ASSOC);

            if ($mostrar) {
              if ($indice == 473) {
                if ($depuracion) {
                  echo "=========>>>>> LOS RESULTADOS = "; 
                  print_r($losresultados);
                  echo "<br>";
                }
              }
            }

            /////////////////////////////////////////////////////////////////////////////
            /////////////////////////////  P Á G I N A S ////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////

            $h = 0;
            while ($h < count($losresultados)) {

              if ($mostrar_1) {
                echo "<br><hr>PÁGINA = " . $losresultados[$h]['pagina'] . "<br>";
                echo "CRITERIO PREVIO = " . $losresultados[$h]['criterio'] . "<br>";
                echo "SUB-CRITERIO PREVIO = " . $losresultados[$h]['criterio'] . "<br>";
                echo "CRITERIO NUEVO  = " . $tabla_trd[$i][0] . " - " . $tabla_trd[$i][1] . "<br><br>";       
              }

              if ($mostrar_ia && $losresultados[$h]['pagina'] == $pagina_ia) {
                echo "<hr><span style='margin-left:30px;'>SQL: " . $sql_origen . "</span><br>";
                echo "<span style='margin-left:30px;'>TIPO DOCUMENTAL: " . campo_tabla_trd($tabla_trd, $indice, TIPODOC) . ", ID: " . $indice . ", OBSERVACIONES: " . campo_tabla_trd($tabla_trd, $indice, OBSERVACIONES) . ", NIVEL: " . campo_tabla_trd($tabla_trd, $indice, NIVEL) . "</span><br>";
              }

              // SI EL NIVEL DEL CRITERIO PREVIO EN LA TABLA TRD ES MAYOR O IGUAL QUE EL
              // NIVEL DEL CRITERIO CANDIDATO EN LA TABLA TRD, NO SE PUEDE HACER EL CAMBIO

              $id_criterio_previo = $losresultados[$h]['criterio'];
              $id_subcriterio_previo = $losresultados[$h]['subcriterio'];

              $id_criterio_nuevo = $tabla_trd[$i][0];

              $nivel_criterio_previo = -1;
              $kw = 0;
              while ($kw < count($tabla_trd)) {
                if ($tabla_trd[$kw][0] == $id_criterio_previo) {
                  $nivel_criterio_previo = $tabla_trd[$kw][6];
                }
                $kw = $kw + 1;
              }

              $nivel_subcriterio_previo = -1;
              $kw = 0;
              while ($kw < count($tabla_trd)) {
                if ($tabla_trd[$kw][0] == $id_subcriterio_previo) {
                  $nivel_subcriterio_previo = $tabla_trd[$kw][6];
                }
                $kw = $kw + 1;
              }

              $nivel_criterio_nuevo = $tabla_trd[$i][6];
              $pos_del_criterio_previo = pos_tabla_trd($tabla_trd, $id_criterio_previo);

              if ($mostrar_1) {
                echo "NIVEL CRITERIO PREVIO = " . $nivel_criterio_previo . "<br>";
                echo "NIVEL SUB-CRITERIO PREVIO = " . $nivel_subcriterio_previo . "<br>";
                echo "NIVEL CRITERIO NUEVO = " . $nivel_criterio_nuevo . "<br><br>";
                echo "TU SUPERIOR ACTUAL = " . $tabla_trd[$i][7] . "<br>";
                echo "CRITERIO PREVIO: " . $id_criterio_previo . "<br>";

                if ($id_criterio_previo > 0 && $id_criterio_previo < 99999) {
                  echo "EL SUPERIOR QUE YA ESTABA = " . $tabla_trd[$pos_del_criterio_previo][7] . "<br>";
                }
                else {
                  echo "EL SUPERIOR QUE YA ESTABA NO SE PUEDE CALCULAR, PUES EL CRITERIO ESTABA EN CERO O EN 99999" . "<br>";
                }
              }

              if ( ($nivel_criterio_nuevo < $nivel_criterio_previo) || ($nivel_criterio_nuevo < $nivel_subcriterio_previo) ) {
                $h = $h + 1;
                continue;
              }

              if (intval($tabla_trd[$i][5]) > 0) { // Es un criterio - tipo documental (serie == 0)
                $sql = "UPDATE tblpaginas SET criterio = " . $tabla_trd[$i][0] . " WHERE id = " . $losresultados[$h]['id'];
                $query = $pdo->prepare($sql);
                $query->execute();

                if ($mostrar_ia && $losresultados[$h]['pagina'] == $pagina_ia) {
                  echo "<span style='margin-left:30px;'>SE GRABA EL REGISTRO - es un criterio</span><br>";
                  echo "<span style='margin-left:30px;'>SQL para criterio: " . $sql . "</span><br>";
                }

                $losresultados[$h]['criterio'] = $tabla_trd[$i][0];

                $sql = "UPDATE tblpaginas SET subcriterio = 0 WHERE id = " . $losresultados[$h]['id'];
                $query = $pdo->prepare($sql);
                $query->execute();

                if ($mostrar_ia && $losresultados[$h]['pagina'] == $pagina_ia) {
                  echo "<span style='margin-left:30px;'>SQL para subcriterio: " . $sql . "</span><br>";
                }

                $losresultados[$h]['subcriterio'] = $tabla_trd[$i][0];                

              }
              else { // Es un subcriterio - observaciones
                $sql = "UPDATE tblpaginas SET subcriterio = " . $tabla_trd[$i][0] . " WHERE id = " . $losresultados[$h]['id'];
                $query = $pdo->prepare($sql);
                $query->execute();

                if ($mostrar_ia && $losresultados[$h]['pagina'] == $pagina_ia) {
                  echo "<span style='margin-left:30px;'>SE GRABA EL REGISTRO - es un subcriterio</span><br>";
                  echo "<span style='margin-left:30px;'>SQL para criterio: " . $sql . "</span><br>";
                }

                $losresultados[$h]['subcriterio'] = $tabla_trd[$i][0];

                $sql = "UPDATE tblpaginas SET criterio = 0 WHERE id = " . $losresultados[$h]['id'];
                $query = $pdo->prepare($sql);
                $query->execute();

                if ($mostrar_ia && $losresultados[$h]['pagina'] == $pagina_ia) {
                  echo "<span style='margin-left:30px;'>SQL para subcriterio: " . $sql . "</span><br>";
                }

                $losresultados[$h]['criterio'] = 0;

              }

              $h = $h + 1;
            }

            $pq = $pq + 1;
          }
      }

      $i = $i + 1;
    }

    if ($mostrar_ia) {
      echo "<hr><span style='margin-left:30px;'>FINAL DEL CICLO DE ANÁLISIS DE REGLAS CON TODAS LAS PÁGINAS</span>" . "<hr>";
    }

    ///////////////////////////////////////////////////////////////////////////
    //                           ANÁLISIS DE TERCEROS                        //
    ///////////////////////////////////////////////////////////////////////////

    // echo "EN PROCESO DE BÚSQUEDA DE TERCEROS" . "<br>";

    // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
    $sql77  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('+NOMBRE +VICTIMA' IN BOOLEAN MODE) ORDER BY id ASC";

    // EN LA TABLA: losresultados, SE ENCUENTRAN LAS PÁGINAS QUE CONTIENEN LA
    // METARREGLA ANALIZADA
    $query = $pdo->prepare($sql77);
    $query->execute();
    $res_terceros = $query->fetchAll(PDO::FETCH_ASSOC);

    $h = 0;
    while ($h < count($res_terceros)) {
      // Busca dentro de la página la cadena: 'datos de víctima'
      // Se ha creado una nueva función con este propósito

      $pos_tercero = stripos($res_terceros[$h]['contenido'], 'nombre de victima');

      if ($pos_tercero > 0) {
        // echo "<br>ENCONTRADO EN LA POSICIÓN = " . $pos_tercero . "<br>";
        // echo "DEL EXPEDIENTE: " . $res_terceros[$h]['expediente'] . "<br>";
        // echo "EN EL ARCHIVO: " . $res_terceros[$h]['archivo'] . "<br>";
        $pos_temp = $pos_tercero + 17;
        $pos1 = stripos($res_terceros[$h]['contenido'], 'nombres', $pos_tercero);
        $pos2 = stripos($res_terceros[$h]['contenido'], 'apellidos', $pos_tercero);
        $pos3 = stripos($res_terceros[$h]['contenido'], 'c c', $pos_tercero);

        $eltercero = substr($res_terceros[$h]['contenido'], $pos1 + 8, ($pos2 - $pos1 - 8)) . " " . substr($res_terceros[$h]['contenido'], $pos2 + 11, ($pos3 - $pos2 - 12));

        $eltercero = str_replace("<br>", "", $eltercero);
        $eltercero = str_replace("\n\r", "", $eltercero);
        $eltercero = str_replace("\n", "", $eltercero);
        $eltercero = str_replace("\r", "", $eltercero);
        $eltercero = strtoupper($eltercero);

        // echo "EL TERCERO ==> " . $eltercero . "<br>";

        // echo "NOMBRE DEL TERCERO: " . substr($res_terceros[$h]['contenido'], $pos1 + 8, ($pos2 - $pos1 - 8)) . "<br>";
        // echo "APELLIDOS DEL TERCERO: " . substr($res_terceros[$h]['contenido'], $pos2 + 11, ($pos3 - $pos2 - 12) ) . "<br>";

        $sql66 = "UPDATE tblportada SET terceros = '" . $eltercero . "' WHERE expediente = '" . $res_terceros[$h]['expediente'] . "'";
        $query = $pdo->prepare($sql66);
        $query->execute();
      }
     
      $h = $h + 1;
    }

    ///////////////////////////////////////////////////////////////////////////
    //                        CASOS ESPECIALES BÁSICOS                       //
    ///////////////////////////////////////////////////////////////////////////

    // SI LA PÁGINA 1 ES 99999, Y PUESTO QUE ESTA SE CORRESPONDE CON UNA PORTADA, 
    // EN ESTE REGISTRO SE DEBE MODIFICAR EL SUBCRITERIO PARA QUE REFLEJE LA PORTADA

    ///////////////////////////////////////////////////
    //    VERIFICA LA PORTADA
    ///////////////////////////////////////////////////

    $sql = "SELECT * FROM tblpaginas WHERE pagina = 1";
    $query = $pdo->prepare($sql);
    $query->execute();
    $pagina_1 = $query->fetchAll(PDO::FETCH_ASSOC);

    if ($query->rowCount() > 0) {
       if ($pagina_1[0]['criterio'] == 99999) {
        $sql = "UPDATE tblpaginas SET criterio = 0, subcriterio = 578 WHERE id = " . $pagina_1[0]['id'];
        $query = $pdo->prepare($sql);
        $query->execute();

        $pagina_1[0]['criterio'] = 0;
        $pagina_1[0]['subcriterio'] = 578;
      }     
    }

    ///////////////////////////////////////////////////
    //    VERIFICA LA FICHA TÉCNICA DEL PROCESO
    /////////////////////////////////////////////////// 

    /* 
    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC);   

    $i = 1;
    while ($i < count($res_paginas)) {
      // echo "CRITERIO ANTES = " . $res_paginas[$i]['criterio'] . " SUBCRITERIO ANTES = " . $res_paginas[$i]['subcriterio'] . "<br>";
      if ( ($res_paginas[$i]['criterio'] == 99999) || ($res_paginas[$i]['criterio'] == 0 && $res_paginas[$i]['subcriterio'] == 0) ) {
          $sql = "UPDATE tblpaginas SET criterio = 0, subcriterio = 582 WHERE id = " . $res_paginas[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();

          $res_paginas[$i]['criterio'] = 0;
          $res_paginas[$i]['subcriterio'] = 582;

      } else {
        break;
      }
      $i = $i + 1;
    } */

    ///////////////////////////////////////////////////
    //    RANGO HASTA EL SIGUIENTE REGISTRO
    /////////////////////////////////////////////////// 

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC);

    $i = 0;
    while ($i < count($res_paginas)) {
      $id_criterio = $res_paginas[$i]['criterio'];
      $id_subcriterio = $res_paginas[$i]['subcriterio'];
      if ($id_criterio > 0) {
        $sql = "SELECT * FROM tbltrd WHERE id = " . $id_criterio . " AND siguiente > 0";
        $query = $pdo->prepare($sql);
        $query->execute();
        if ($query->rowCount() > 0) {
          $res_cri = $query->fetchAll(PDO::FETCH_ASSOC);
          $limite = $res_cri[0]['siguiente'];
          $hh = $i + 1;
          while ($hh < count($res_paginas) && $res_paginas[$hh]['criterio'] != $limite) { 
            $sql = "UPDATE tblpaginas SET criterio = " . $id_criterio . " WHERE id = " . $res_paginas[$hh]['id'];
            $query = $pdo->prepare($sql);
            $query->execute();

            $res_paginas[$hh]['criterio'] = $id_criterio;

            $hh = $hh + 1;
          }
          if ($mostrar_2) {
            echo "<br>LÍMITE = " . $limite . " CRITERIO = " . $id_criterio . " PÁGINA = " . $i . "<br>";
          }
        }
      }
      else if ($id_subcriterio > 0) {

      }       
      $i = $i + 1;
    }

    //////////////////////////////////////////////////////////////////////
    //    VERIFICA LOS REGISTROS QUE VALEN CERO Y LOS QUE VALEN 99999
    ////////////////////////////////////////////////////////////////////// 

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC); 

    //////////////////////////////////////////////////////////////////////
    // LOS REGISTROS QUE VALEN 
    // CRITERIO = 0 Y SUBCRITERIO = 0
    // SE REEMPLAZAN CON EL CONTENIDO DE CRITERIO Y SUBCRITERIO ANTERIOR
    //////////////////////////////////////////////////////////////////////

    $mostrar_2 = 0;

    if ($mostrar_2) {
      echo "z10: BUSCANDO criterio = 0 Y subcriterio = 0<br><br>";
    }

    $ii = 1;
    while ($ii < count($res_paginas)) {
      if ( $res_paginas[$ii]['criterio'] == 0 && $res_paginas[$ii]['subcriterio'] == 0 ) {

          $mostrar_2 = 0;

          if ($mostrar_2) {
            if ($res_paginas[$ii]['pagina'] == 2) {
              echo "PÁGINA: " . $res_paginas[$ii]['pagina'] . " CRITERIO = " . $res_paginas[$ii]['criterio'] . " SUB-CRITERIO = " . $res_paginas[$ii]['subcriterio'] . " CRITERIO ANTERIOR = " . $res_paginas[$ii - 1]['criterio'] . " SUB-CRITERIO ANTERIOR = " . $res_paginas[$ii]['subcriterio'] . "<br>";
              echo "<br><br>ASÍ ESTÁN LAS PÁGINAS:<br><br>";
              $nm = 0;
              while ($nm < count($res_paginas)) {
                echo "PÁGINA: " . $res_paginas[$nm]['pagina'] . " CRITERIO: " . $res_paginas[$nm]['criterio'] . " SUB-CRITERIO: " . $res_paginas[$nm]['subcriterio'] . "<br>";
                $nm = $nm + 1;
              }
            }
          }

          try {

            $sql = "UPDATE tblpaginas SET criterio = " . $res_paginas[$ii - 1]['criterio'] . ", subcriterio = " . $res_paginas[$ii - 1]['subcriterio'] . " WHERE id = " . $res_paginas[$ii]['id'];
            $query = $pdo->prepare($sql);
            $query->execute();

            $res_paginas[$ii]['criterio'] = $res_paginas[$ii - 1]['criterio'];
            $res_paginas[$ii]['subcriterio'] = $res_paginas[$ii - 1]['subcriterio'];

          }
          catch (PDOException $ex) {
            print_r($ex);
          }

          $mostrar_2 = 0;
      }
      $ii = $ii + 1;
    }

    //////////////////////////////////////////////////////////////////////
    // LOS REGISTROS QUE VALEN 
    // CRITERIO = 0 Y SUBCRITERIO = 0
    // SE REEMPLAZAN CON EL CONTENIDO DE CRITERIO Y SUBCRITERIO ANTERIOR
    //////////////////////////////////////////////////////////////////////

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC); 

    if ($mostrar_44) {
      echo "<hr>PROCESANDO LA CORRECCIÓN DE LOS REGISTROS 99999" . "<hr>";
    }

    $i = 1;
    while ($i < count($res_paginas)) {

      if ($mostrar_44) {
        echo "PÁGINA = " . $res_paginas[$i]['pagina'] . "<br>";
        echo "CRITERIO = " . $res_paginas[$i]['criterio'] . "<hr>";
        echo "ID = " . $res_paginas[$i]['id'] . "<br>";
      }

      if ( $res_paginas[$i]['criterio'] == 99999 ) {

          // echo "ENCONTRADO UN 99999" . "<br>";

          if ($mostrar_44) {
            if ($res_paginas[$i]['pagina'] == 26) {

            }
          }

          try {

            $sql = "UPDATE tblpaginas SET criterio = " . $res_paginas[$i - 1]['criterio'] . ", subcriterio = " . $res_paginas[$i - 1]['subcriterio'] . " WHERE id = " . $res_paginas[$i]['id'];

            if ($mostrar_44) {
              echo "SQL = " . $sql . "<br>";
            }

            $query = $pdo->prepare($sql);
            $query->execute();

            $res_paginas[$i]['criterio'] = $res_paginas[$i - 1]['criterio'];
            $res_paginas[$i]['subcriterio'] = $res_paginas[$i - 1]['subcriterio'];

            // echo "ASÍ QUEDA EL REGISTRO!" . "<br>";

            // echo "CRITERIO NUEVO = " . $res_paginas[$i]['criterio'] . "<br>";
            // echo "SUB-CRITERIO NUEVO = " . $res_paginas[$i]['subcriterio'] . "<hr>";

          }
          catch (PDOException $ex) {
            print_r($ex);
          }
      }
      $i = $i + 1;
    }

    //////////////////////////////////////////////////////////////////////
    // LOS REGISTROS QUE VALEN 
    // CRITERIO = 0 Y SUBCRITERIO = 0
    // SE REEMPLAZAN CON EL CONTENIDO DE CRITERIO Y SUBCRITERIO ANTERIOR
    //////////////////////////////////////////////////////////////////////

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC); 

    $i = 1;
    while ($i < count($res_paginas)) {
      if ( $res_paginas[$i]['criterio'] == 0 && $res_paginas[$i]['subcriterio'] == 0 ) {

          if ($mostrar_2) {
            if ($res_paginas[$i]['pagina'] == 26) {
              echo "PÁGINA: " . $res_paginas[$i]['pagina'] . " CRITERIO = " . $res_paginas[$i]['criterio'] . " SUB-CRITERIO = " . $res_paginas[$i]['subcriterio'] . " CRITERIO ANTERIOR = " . $res_paginas[$i - 1]['criterio'] . " SUB-CRITERIO ANTERIOR = " . $res_paginas[$i]['subcriterio'] . "<br>";
              echo "<br><br>ASÍ ESTÁN LAS PÁGINAS:<br><br>";
              $nm = 0;
              while ($nm < count($res_paginas)) {
                echo "PÁGINA: " . $res_paginas[$nm]['pagina'] . " CRITERIO: " . $res_paginas[$nm]['criterio'] . " SUB-CRITERIO: " . $res_paginas[$nm]['subcriterio'] . "<br>";
                $nm = $nm + 1;
              }
            }
          }

          try {

            $sql = "UPDATE tblpaginas SET criterio = " . $res_paginas[$i - 1]['criterio'] . ", subcriterio = " . $res_paginas[$i - 1]['subcriterio'] . " WHERE id = " . $res_paginas[$i]['id'];
            $query = $pdo->prepare($sql);
            $query->execute();

            $res_paginas[$i]['criterio'] = $res_paginas[$i - 1]['criterio'];
            $res_paginas[$i]['subcriterio'] = $res_paginas[$i - 1]['subcriterio'];

          }
          catch (PDOException $ex) {
            print_r($ex);
          }
      }
      $i = $i + 1;
    }

    //////////////////////////////////////////////////////////////////
    // CANCELACIÓN INFORME POLICIAL
    //////////////////////////////////////////////////////////////////

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res_paginas = $query->fetchAll(PDO::FETCH_ASSOC); 

    $i = 1;
    while ($i < count($res_paginas)) {
      if ($res_paginas[$i]['criterio'] == 393 && strlen($res_paginas[$i]['contenido']) <= 300) {
        $sql = "UPDATE tblpaginas SET criterio = " . $res_paginas[$i - 1]['criterio'] . ", subcriterio = " . $res_paginas[$i - 1]['subcriterio'] . " WHERE id = " . $res_paginas[$i]['id'];
        $query = $pdo->prepare($sql);
        $query->execute();

        $res_paginas[$i]['criterio'] = $res_paginas[$i - 1]['criterio'];
        $res_paginas[$i]['subcriterio'] = $res_paginas[$i - 1]['subcriterio'];        
      }
      $i = $i + 1;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////   S E G U N D A  P A S A D A  /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE RECORREN TODAS LAS PÁGINAS Y SE EVALÚAN NUEVAMENTE LAS TRD QUE SON PROBLEMÁTICAS
    // POR TEMA DE CRUCE CON OTRAS TRD. EL OBJETIVO ES NO MODIFICAR DE NUEVO LOS PATRONES O
    // EXCEPCIONES (para que no se reflejen en los restantes TRD) Y REALIZAR LAS
    // MODIFICACIONES DE MANERA PUNTUAL.
    ////////////////////////////////////////////////////////////////////////////////////

    define("OFICIO", 612);
    define("ESCRITOACUSACION", 583);
    define("CITACIONAUDIENCIA", 586);
    define("MEMORIAL", 420);
    define("RECURSOAPELACION", 472);
    define("AUTODECIDERECURSO", 506);
    define("ACTAREVOCATORIA", 409);
    define("ACTAAUDIENCIA", 404);
    define("HABEASCORPUS", 615);
    define("CONSTANCIA_SECRETARIAL", 614);

    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $i = 0;
    while ($i < count($results)) {

      if ($results[$i]['criterio'] == HABEASCORPUS) {
        // Se busca el cierre
        $k = $i;
        while ($k < count($results)) {

          $pos11 = stripos($results[$k]['contenido'], 'cópiese');
          $pos12 = stripos($results[$k]['contenido'], 'copiese');
          $pos21 = stripos($results[$k]['contenido'], 'notificar decisión');
          $pos22 = stripos($results[$k]['contenido'], 'notificar decision');
          $pos31 = stripos($results[$k]['contenido'], 'juez');
          $pos32 = stripos($results[$k]['contenido'], 'juez,');

          $ttt = 0;
          if ($pos11 > 0 || $pos12 > 0) $ttt = $ttt + 1;
          if ($pos21 > 0 || $pos22 > 0) $ttt = $ttt + 1;
          if ($pos31 > 0 || $pos32 > 0) $ttt = $ttt + 1;

          if ( $ttt >= 2 ) {

            $w = $i;
            while ($w <= $k) {
              $sql = "UPDATE tblpaginas SET observaciones = " . HABEASCORPUS . " WHERE id = " . $results[$w]['id'];
              $query = $pdo->prepare($sql);
              $query->execute();                
              $w = $w + 1;
            }

            break;
          }

          $k = $k + 1;
        }        
      }
      else if ($results[$i]['criterio'] == ESCRITOACUSACION) {
        $pos1 = stripos($results[$i]['contenido'], 'roceso penal');
        $pos2 = stripos($results[$i]['contenido'], 'oficio');
        $pos3 = stripos($results[$i]['contenido'], 'citaciones audiencias');
        $pos4 = stripos($results[$i]['contenido'], 'citacion audiencia');
        $pos5 = stripos($results[$i]['contenido'], 'citación audiencia');
        if ($pos1 == 0 && $pos2 > 0 && !($pos3 > 0 || $pos4 > 0 || $pos5 > 0)) {
          // SE DEBE CAMBIAR por oficio
          $sql = "UPDATE tblpaginas SET criterio = " . OFICIO . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando escrito acusación por oficio !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";
          }
        }
        if ($pos1 == 0 && $pos2 > 0 && ($pos3 > 0 || $pos4 > 0 || $pos5 > 0)) {
          // SE DEBE CAMBIAR por citación audiencia
          $sql = "UPDATE tblpaginas SET criterio = " . CITACIONAUDIENCIA . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando escrito acusación por citación audiencia !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";          
          }
        }
      }
      else if ($results[$i]['criterio'] == CITACIONAUDIENCIA) {
        $pos1 = stripos($results[$i]['contenido'], 'solicitarle se sirva estudiar');
        if ($pos1 > 0) {
          // SE DEBE CAMBIAR por memorial
          $sql = "UPDATE tblpaginas SET criterio = " . MEMORIAL . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando solicitud audiencia por memorial !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";
          }
        }       
      }
      else if ($results[$i]['criterio'] == RECURSOAPELACION) {
        if ($mostrar_ia) {
          echo "<br>ENCONTRÓ UN RECURSO DE APELACIÓN (podría cambiarse por un auto que decide recurso)" . "<br>";
        }
        $alfa = $i + 1;
        if ($mostrar_ia) {
          echo "ENCONTRADO EN EL REGISTRO = " . $alfa . "<br>";
        }
        $pos1 = stripos($results[$i]['contenido'], 'desata la sala el recurso');
        if ($pos1 > 0) { // Es un criterio de auto decide recurso
          if ($mostrar_ia) {
            echo "AQUÍ SE ENCUENTRA UN CICLO" . "<br>";
          }
          // Se busca el cierre
          $k = $i;
          while ($k < count($results)) {
            $pos2 = stripos($results[$k]['contenido'], 'resuelve');
            if ($pos2 > 0) { // Este es el cierre
              $w = $i;
              while ($w <= $k) {
                $sql = "UPDATE tblpaginas SET criterio = " . AUTODECIDERECURSO . " WHERE id = " . $results[$w]['id'];
                $query = $pdo->prepare($sql);
                $query->execute();                
                $w = $w + 1;
              }
              break;
            }
            $k = $k + 1;
          }
        }
      }
      else if ($results[$i]['criterio'] == ACTAREVOCATORIA) {
        $pos1 = stripos($results[$i]['contenido'], 'llevar a cabo la audiencia');
        $pos2 = stripos($results[$i]['contenido'], 'oficio');
        $pos3 = stripos($results[$i]['contenido'], 'autorizó la remisión de los procesados');
        if ($pos1 > 0 && $pos2 > 0) {
          // SE DEBE CAMBIAR por oficio
          $sql = "UPDATE tblpaginas SET criterio = " . OFICIO . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando escrito acta revocatoria por oficio !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";
          }
        }
        if ($pos3 > 0) {
          // SE DEBE CAMBIAR por oficio
          $sql = "UPDATE tblpaginas SET criterio = " . OFICIO . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando escrito acta revocatoria por oficio !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";
          }
        }           
      }
      else if ($results[$i]['criterio'] == OFICIO) {
        $pos1 = stripos($results[$i]['contenido'], 'Juez Segundo Penal Municipal Con Función de control de Garantías');
        if ($pos1 > 0) {
          // SE DEBE CAMBIAR por oficio
          $sql = "UPDATE tblpaginas SET criterio = " . MEMORIAL . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando oficio por memorial !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";          
          }
        }
      }            
      else if ($results[$i]['criterio'] == ACTAAUDIENCIA) {
        $pos1 = stripos($results[$i]['contenido'], 'oficio');
        $pos2 = stripos($results[$i]['contenido'], 'solicito la designación de sala');
        $pos3 = stripos($results[$i]['contenido'], 'solicito lo designación de solo');
        if ($pos1 > 0 && ($pos2 > 0 || $pos3 > 0)) {
          // SE DEBE CAMBIAR por oficio
          $sql = "UPDATE tblpaginas SET criterio = " . OFICIO . " WHERE id = " . $results[$i]['id'];
          $query = $pdo->prepare($sql);
          $query->execute();
          if ($mostrar_ia) {
            echo "<hr><hr>FINAL FINAL cambiando acta audiencia por oficio !!! " . " PÁGINA = " . $results[$i]['pagina'] . "<hr><hr>";          
          }
        }
      } 

      /* 

      $sql = "UPDATE tblpaginas SET criterio = 593, subcriterio = 0 WHERE id = 4 OR id = 5";
      $query = $pdo->prepare($sql);
      $query->execute();

      */
      
      $i = $i + 1;
    }

    // ------------------------------------------------------------------------------------

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   MOTOR ASOCIADO A CRITERIOS ESPECÍFICOS  ///////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    // SE LIMPIA LA BANDERA EN páginas QUE INDICA QUE YA SE PROCESARON REGISTROS
    // CON ESTE MÉTODO. LA IDEA ES QUE CUANDO SE INGRESE EN EL CICLO, NO SE TOQUEN
    // LAS PÁGINAS QUE YA FUERON PROCESADAS CON LA NUEVA TRD, IMPIDIENDO QUE SE
    // DESHAGAN REGLAS ESTRATÉGICAS QUE YA FUERON APLICADAS

    // limpiar_paginas_nueva_trd();

    // SE ABRE LA TABLA DE LAS NUEVAS TRD

    /* 

    $todas = 1;

    $depuracion = 0;

    try {
      $sql9 = "SELECT * FROM tbltrdextra";
      $query9 = $pdo->prepare($sql9);
      $query9->execute();
      $resTrdExtra = $query9->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    }

    if (count($resTrdExtra) > 0) {

      // SE RECORRE LA TABLA DE LAS NUEVAS TRD. SE DEBE GARANTIZAR QUE UNA VEZ APLICADA
      // UNA REGLA DE LA NUEVA TRD, NO SE AFECTEN LAS PÁGINAS QUE YA HABÍAN SIDO PREVIAMENTE
      // AFECTADAS

      if ($todas) {

        if ($depuracion) {
          echo "<hr>z9 - APLICACIÓN DE LAS NUEVAS TRD (extras)<br>";
        }

        // SE PROCESAN TODAS LAS TRD EXTRAS
        $cont = 0;
        while ($cont < count($resTrdExtra)) {
          if ($depuracion) {
            echo "<hr>APLICANDO LA REGLA EXTRA: " . $resTrdExtra[$cont]['codigo'] . " DE TIPO: " . $resTrdExtra[$cont]['tipo'] . "<br>";
          }
          motor_detalles($resTrdExtra[$cont]['codigo'], $resTrdExtra[$cont]['tipo'], 0);
          $cont = $cont + 1;
        }

        if ($depuracion) {
          echo "<br><hr>";
        }

      }
      else {

        // SE PROCESA UNA SOLA TRD EXTRA. SE HACE SOBRE TODO PARA DEPURACIÓN

        // Solicitud Audiencia - Observaciones
        // formato solicitud de audiencia preliminar;solicitud asignacion audiencia preliminar
        // Se valida el uso del punto y coma

        // motor_detalles(383, 0, 1); // Ficha Técnica
        // motor_detalles(486, 0, 1); // Informes 

      }
      
    } */

    // ------------------------------------------------------------------------------------

?>

<?php

    // ---------------------------------------------------------------------------
    // SE AJUSTA LA PORTADA CON SUS DATOS FUNDAMENTALES
    // ---------------------------------------------------------------------------

    if ($depuracion) {
      echo "tt100 - Modificando Portadas" . "<br>";
    }

     try {
        $sql2 = "SELECT * FROM tblportada";
        $query2 = $pdo->prepare($sql2);
        $query2->execute();
        $resPortada = $query2->fetchAll(PDO::FETCH_ASSOC);
        $elExpediente = $resPortada[0]['expediente'];
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas  

      if ($depuracion) {
        echo "EL EXPEDIENTE ENCONTRADO = " . $elExpediente . "<br>";
      }

      /* 

      if (strcasecmp($elExpediente, '13001600112920180435900') == 0) {
        $parte_procesal_a = 'ILINTON ALCAZAR MENDEZ';
        $parte_procesal_b = 'NOHORA ALARCÓN GANEM';
        $terceros = 'JOSELYN DEL CARMEN AMADOR LLORENTE';
      }
      else if (strcasecmp($elExpediente, '13001600112920180439300') == 0) {
        $parte_procesal_a = 'JOSÉ LUIS CASTRO PERTUZ';
        $parte_procesal_b = 'KAREN SIERRA';
        $terceros = 'FULGENCIO ENRIQUE PEREZ ROMERO';
      }

      $sql11 = "UPDATE tblportada SET parte_procesal_a = '" . $parte_procesal_a . "', parte_procesal_b = '" . $parte_procesal_b . "', terceros = '" . $terceros . "' WHERE expediente = '" . $elExpediente . "'";
      $query11 = $pdo->prepare($sql11);
      $query11->execute(); 

      */

      /*
      try {
        $sql2 = "SELECT * FROM tblbas_portadas WHERE expediente = '" . $elExpediente . "'";
        $query2 = $pdo->prepare($sql2);
        $query2->execute();
        $resEvalPortada = $query2->fetchAll(PDO::FETCH_ASSOC);
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas */

     if ($depuracion) {
      echo "<br>EL EXPEDIENTE: " . $elExpediente . " ==> <br><br>";
      echo "FILAS DE tblbas_portadas = " . $query2->rowCount() . "<br>";
     }

     // -----------------------------------------------------------------------------------
     // DATOS TEMPORALES. SE CORRIGEN POR CÓDIGO

     generar_portada($elExpediente);

     // !!!!!!!!!!!!!!!!!!!!!!!!!!!!     P O R T A D A S     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // SE APROVECHA PARA ACTUALIZAR LAS PORTADAS

    // COMO YA SE CONOCEN LAS PÁGINAS, SE PROCEDE A AJUSTAR LA PORTADA. EN EL SITIO ORIGINAL
    // generar_portadas, NO ERA ADECUADO EN DICHO PUNTO PUES LAS PÁGINAS AÚN NO SE HABÍAN
    // CALCULADO

    /* 
    $au = buscar_nombres_partes_a('indiciado', 1);
    if (strcasecmp($au, '') == 0) $au = buscar_nombres_partes_a('procesado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres_partes_a('imputado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres_partes_a('acusado');
    if (strcasecmp($au, '') == 0) $au = buscar_nombres_partes_a('sentenciado');

    $parte_procesal_a = strtoupper($au);

    $aw = buscar_nombres_partes_a('víctima', 1);
    if (strcasecmp($aw, '') == 0) $aw = buscar_nombres_partes_a('victima');

    $terceros = strtoupper($aw);

    if ($depuracion) {
      echo "ww9 - ACTUALIZACIÓN DE LAS PORTADAS: Parte Procesal A = " . $parte_procesal_a . "<br>";
    }

    // SE DEBE ACTUALIZAR ESTA INFORMACIÓN EN LA PORTADA

    try {
      $sql22 = "UPDATE tblportada SET parte_procesal_a = '" . $parte_procesal_a . "', terceros = '" . $terceros ."' WHERE expediente = '" . $elExpediente . "'";
      $query22 = $pdo->prepare($sql22);
      $query22->execute();
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas 

    */

    // ---------------------------------------------------------------------------
    // SE AJUSTAN NUEVAMENTE LAS PÁGINAS QUE TIENEN criterio = 0 Y subcriterio = 0
    // ---------------------------------------------------------------------------

     try {
        $sql22 = "SELECT * FROM tblportada";
        $query22 = $pdo->prepare($sql22);
        $query22->execute();
        $resPortada = $query22->fetchAll(PDO::FETCH_ASSOC);
        $elExpediente = $resPortada[0]['expediente'];
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas  

     try {
        $sql2 = "SELECT * FROM tblpaginas";
        $query2 = $pdo->prepare($sql2);
        $query2->execute();
        $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
        $elExpediente = $resPortada[0]['expediente'];
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas  

    $ii = 1;
    while ($ii < count($resPaginas)) {
      if ( $resPaginas[$ii]['criterio'] == 0 && $resPaginas[$ii]['subcriterio'] == 0 ) {

          $mostrar_2 = 0;

          if ($mostrar_2) {
            if ($resPaginas[$ii]['pagina'] == 2) {
              echo "PÁGINA: " . $resPaginas[$ii]['pagina'] . " CRITERIO = " . $resPaginas[$ii]['criterio'] . " SUB-CRITERIO = " . $resPaginas[$ii]['subcriterio'] . " CRITERIO ANTERIOR = " . $resPaginas[$ii - 1]['criterio'] . " SUB-CRITERIO ANTERIOR = " . $resPaginas[$ii]['subcriterio'] . "<br>";
              echo "<br><br>ASÍ ESTÁN LAS PÁGINAS:<br><br>";
              $nm = 0;
              while ($nm < count($resPaginas)) {
                echo "PÁGINA: " . $resPaginas[$nm]['pagina'] . " CRITERIO: " . $resPaginas[$nm]['criterio'] . " SUB-CRITERIO: " . $resPaginas[$nm]['subcriterio'] . "<br>";
                $nm = $nm + 1;
              }
            }
          }

          try {

            $sql = "UPDATE tblpaginas SET criterio = " . $resPaginas[$ii - 1]['criterio'] . ", subcriterio = " . $resPaginas[$ii - 1]['subcriterio'] . " WHERE id = " . $resPaginas[$ii]['id'];
            $query = $pdo->prepare($sql);
            $query->execute();

            $resPaginas[$ii]['criterio'] = $resPaginas[$ii - 1]['criterio'];
            $resPaginas[$ii]['subcriterio'] = $resPaginas[$ii - 1]['subcriterio'];

          }
          catch (PDOException $ex) {
            print_r($ex);
          }

          $mostrar_2 = 0;
      }
      $ii = $ii + 1;
    }

    // -------------------------------------------------------------------------------------
    // Se cambia Oficio Remisorio a Oficio
    // -------------------------------------------------------------------------------------

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas  

    $ii = 1;
    while ($ii < count($resPaginas)) {

      try {

        $sql = "UPDATE tblpaginas SET criterio = 612, subcriterio = 0 WHERE criterio = 603";
        $query = $pdo->prepare($sql);
        $query->execute();

      }
      catch (PDOException $ex) {
        print_r($ex);
      }

      $ii = $ii + 1;
    }

    // -------------------------------------------------------------------------------------
    // Se corrigen las portadas : 578
    // -------------------------------------------------------------------------------------

    if ($depuracion) {
      echo "<br>ww100 - AJUSTANDO LAS PORTADAS (antes página 1): " . "<br><br>";
    }

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas

    $ii = 0;
    while ($ii < count($resPaginas)) {

      if ($depuracion) {
        echo "PÁGINA: " . $resPaginas[$ii]['pagina'] . "<br>";
        echo "CONTENIDO = " . $resPaginas[$ii]['contenido'] . "<br>";
      }

      $x = stripos($resPaginas[$ii]['contenido'], 'código único de investigación');
      if ($x == 0) {
        $x = stripos($resPaginas[$ii]['contenido'], 'código único deinvestigación');        
      }

      if ($depuracion) {
        echo "<br>ww100 - Para código = 578, posición x para 'código único deinvestigación'" . $x . "<br>";
      }

      if ($x > 0) { // Es una portada
        if ($depuracion) {
          echo "ES UNA PORTADA (578): " . "<br>";
        }
        try {
          $sql2 = "UPDATE tblpaginas SET criterio = 0, subcriterio = 578 WHERE pagina = " . $resPaginas[$ii]['pagina'];
          $query2 = $pdo->prepare($sql2);
          $query2->execute();
        }
        catch (PDOException $ex) {
          print_r($ex);
        } // TRY de tabla páginas        
      }

      $ii = $ii + 1;
    }

    // -------------------------------------------------------------------------------------
    // Se corrige Acta de Preacuerdo : 3742
    // -------------------------------------------------------------------------------------

   if ($depuracion) {
      echo "<br>ww100 - AJUSTANDO ACTA PREACUERDO: " . "<br><br>";
    }

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas

    $ii = 0;
    while ($ii < count($resPaginas)) {

      if ($depuracion) {
        echo "PÁGINA: " . $resPaginas[$ii]['pagina'] . "<br>";
        echo "CONTENIDO = " . $resPaginas[$ii]['contenido'] . "<br>";
      }

      $x = stripos($resPaginas[$ii]['contenido'], 'acta de preacuerdo');
      if ($x == 0) {
        $x = stripos($resPaginas[$ii]['contenido'], 'acta depreacuerdo');
        if ($x == 0) {
          $x = stripos($resPaginas[$ii]['contenido'], 'acta depreacuerdó'); 
          if ($x == 0) {
            $x = stripos($resPaginas[$ii]['contenido'], 'actadepreacuerdo');
            if ($x == 0) {
              $x = stripos($resPaginas[$ii]['contenido'], 'actadepreacuerdó');             
            }                
          }         
        }        
      }

      if ($depuracion) {
        echo "<br>ww100 - Para código = 3742, posición x para 'acta de preacuerdo'" . $x . "<br>";
      }

      if ($x > 0) { // Es un acta de preacuerdo
        if ($depuracion) {
          echo "ES UNA UN ACTA DE PREACUERDO (3742): " . "<br>";
        }
        try {
          $sql2 = "UPDATE tblpaginas SET criterio = 0, subcriterio = 3742 WHERE pagina = " . $resPaginas[$ii]['pagina'];
          $query2 = $pdo->prepare($sql2);
          $query2->execute();
        }
        catch (PDOException $ex) {
          print_r($ex);
        } // TRY de tabla páginas        
      }

      $ii = $ii + 1;
    }

    // -------------------------------------------------------------------------------------
    // Se cierra entrevista
    // -------------------------------------------------------------------------------------

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas  

    $ii = 1;
    while ($ii < count($resPaginas)) {

      if ($resPaginas[$ii]['criterio'] == 709) { // ENTREVISTA
        // PARA LAS ENTREVISTAS, EN GENERAL (luego se evaluarán otros casos),
        // EL CIERRE SE HACE EN LA SIGUIENTE PÁGINA
       try {

          $sql = "UPDATE tblpaginas SET criterio = 709, subcriterio = 0 WHERE pagina = " . $resPaginas[$ii + 1]['pagina'];
          $query = $pdo->prepare($sql);
          $query->execute();

        }
        catch (PDOException $ex) {
          print_r($ex);
        }       
      }

      $ii = $ii + 1;
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    //
    // -------------------------------------------------------------------------------------

    motor_detalles_ajustes();

    // -------------------------------------------------------------------------------------
    // LISTADO DE PÁGINAS ANTES DE SALIR DE GENERAR DETALLES
    // -------------------------------------------------------------------------------------

  if ($depuracion) {
    echo "AQUÍ MIRAMOS EL ESTADO DE PÁGINAS ANTES DE SALIR DE GENERAR DETALLES<br><br>";

    try {
      $sql2 = "SELECT * FROM tblpaginas";
      $query2 = $pdo->prepare($sql2);
      $query2->execute();
      $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
      $totalPaginas = $query2->rowCount();
      $elExpediente = $resPaginas[0]['expediente'];
    }
    catch (PDOException $ex) {
      print_r($ex);
    } // TRY de tabla páginas  

    $xf = 0;
    while ($xf < count($resPaginas)) {
      echo "Página = " . $xf . " Criterio = " . $resPaginas[$xf]['criterio'] . " Subcriterio = " . $resPaginas[$xf]['subcriterio'] . "<br>";
      $xf = $xf + 1;
    }
  }
?>

<?php if ($depuracion) { ?>

  <hr>
  <div style="margin-left:30px;margin-right:30px;">
  PROCESO CONCLUIDO:
  </div><hr>    

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <span style="font-weight:bold;margin-left:30px;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a><hr>

<?php } ?>
