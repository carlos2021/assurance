<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('procesar_expedientes');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>Generación de Expedientes</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Elaborar contenido de carpeta base</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">
    <form class="w3-container w3-card-4" method = "post" action="s_procesar_expedientes.php">
      <h3>Seleccione la ciudad</h3>
      <select id="ciudad" name="ciudad">
        <option value="" disabled selected>Elija la opción</option>
        <option value="1">Cúcuta</option>
        <option value="2">Cartagena</option>
      </select>
      <p><button class="w3-btn w3-teal">Procesar</button></p>
    </form>
  </div>

  <!-- End page content -->
</div>
