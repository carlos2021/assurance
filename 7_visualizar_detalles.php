<?php include "funciones.php"; ?>

<?php $depuracion = 0; ?>

<?php if ($depuracion) { ?>

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <span style="font-weight:bold;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>

<?php } ?>

<?php

    $mostrar_10 = 0;
    $mostrar_11 = 0;

    if ($depuracion) {
	     echo "<hr>VISUALIZAR DETALLES<hr><br>";
    }

    // SE REINICIA LA TABLA DE DETALLES
    $sql = "TRUNCATE TABLE tbldetalle";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();   

    // SE ABRE LA TABLA DE LAS PÁGINAS PREPARADAS PREVIAMENTE
    $sql = "SELECT * FROM tblpaginas";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    // SI HAY PÁGINAS, SE ENTRA A UN CICLO CONTROLADO POR LA VARIABLE t
    // SE ITERA CON LA TABLA: results, LA CUAL CONTIENE TODAS LAS PÁGINAS DEL DOCUMENTO
    if ($query->rowCount() > 0) {

      // INICIO DEL CICLO
      $t = 0;
      while ($t < count($results)) {

        /////////////////////////////////////////////////////////////////////////

        try {

          // SE EXTRAEN LOS DATOS DEL REGISTRO ACTUAL EN LA ITERACIÓN (t)
          $expediente = $results[$t]['expediente'];
          $archivo = $results[$t]['archivo'];
          $criterio = $results[$t]['criterio'];
          $creacion = $results[$t]['fecha'];
          $orden = 1;
          $nro_paginas = 1;
          $pag_inicial = $results[$t]['pagina'];
          $pag_final = $results[$t]['pagina'];
          $tamanio = 1;
          $link = $results[$t]['link'];
          $subcriterio = $results[$t]['subcriterio'];

          //////////////////////////////////////

          // PRIMERA REGLA: NO HAY CRITERIO NI SUBCRITERIO
          // ==> EL REGISTRO NO POSEE NINGÚN TIPO DE CLASIFICACIÓN
          if ($criterio == 0 && $subcriterio == 0){
              $nombre_criterio = 'Otro';
              $observaciones = 'Documento Soporte - sc';           
          } 

          // SEGUNDA REGLA: NO HAY CRITERIO
          // EL SUBCRITERIO EXISTE. EN ESTE CASO, EN OBSERVACIONES SE COLOCA EL NOMBRE
          // DE LA REGLA QUE DIO ORIGEN AL SUBCRITERIO
          // EN NOMBRE CRITERIO SE DEBE COLOCAR: "Sin Clasificación"
          else if ($criterio == 0 && $subcriterio != 0) {
              $sql5 = "SELECT observaciones FROM tbltrd WHERE id = " . $results[$t]['subcriterio'];
              $query5 = $pdo->prepare($sql5);
              $query5->execute();
              $nuevo_resultado = $query5->fetchAll(PDO::FETCH_ASSOC);
              $observaciones = $nuevo_resultado[0]['observaciones'];
              $nombre_criterio = 'Otro';
          } 

          // TERCERA REGLA: NO HAY SUBCRITERIO
          // EL CRITERIO EXISTE. EN ESTE CASO, OBSERVACIONES SE COLOCA VACÍO
          // EN NOMBRE CRITERIO SE COLOCA EL VALOR EQUIVALENTE DE LA META REGLA ASOCIADA
          else if ($criterio != 0 && $subcriterio == 0 && $criterio != 99999) {
              $observaciones = '';
              $sql5 = "SELECT * FROM tbltrd WHERE id = " . $results[$t]['criterio'];
              $query5 = $pdo->prepare($sql5);
              $query5->execute();
              $nuevo_resultado = $query5->fetchAll(PDO::FETCH_ASSOC);
              $nombre_criterio = $nuevo_resultado[0]['tipo_doc'];                        
          } 

          // CUARTA REGLA: TANTO CRITERIO COMO SUBCRITERIO EXISTEN
          // ESTE CASO ES EXACTAMETE IGUAL AL ANTERIOR
          else if ($criterio != 0 && $subcriterio != 0 && $criterio != 99999) {
              $observaciones = '';
              $sql5 = "SELECT tipo_doc FROM tbltrd WHERE id = " . $results[$t]['criterio'];
              $query5 = $pdo->prepare($sql5);
              $query5->execute();
              $nuevo_resultado = $query5->fetchAll(PDO::FETCH_ASSOC);
              $nombre_criterio = $nuevo_resultado[0]['tipo_doc'];  
          }

          //////////////////////////////////////////////////////////////////////////////////////////////

          // SE GRABA EL REGISTRO TOMADO DE PÁGINAS EN LA TABLA DE DETALLES

          $sql = "INSERT INTO tbldetalle (expediente, archivo, criterio, creacion, orden, nro_paginas, pag_inicial, pag_final, tamanio, observaciones, link, nombre_criterio, subcriterio) VALUES (:expediente, :archivo, :criterio, :creacion, :orden, :nro_paginas, :pag_inicial, :pag_final, :tamanio, :observaciones, :link, :nombre_criterio, :subcriterio)";
          $stmt = $pdo->prepare($sql);
          $stmt->bindParam(':expediente', $expediente, PDO::PARAM_STR);
          $stmt->bindParam(':archivo', $archivo, PDO::PARAM_STR);
          $stmt->bindParam(':criterio', $criterio, PDO::PARAM_INT);
          $stmt->bindParam(':creacion', $creacion, PDO::PARAM_STR);
          $stmt->bindParam(':orden', $orden, PDO::PARAM_INT);
          $stmt->bindParam(':nro_paginas', $nro_paginas, PDO::PARAM_INT);
          $stmt->bindParam(':pag_inicial', $pag_inicial, PDO::PARAM_INT);
          $stmt->bindParam(':pag_final', $pag_final, PDO::PARAM_INT);
          $stmt->bindParam(':tamanio', $tamanio, PDO::PARAM_INT);
          $stmt->bindParam(':observaciones', $observaciones, PDO::PARAM_STR);
          $stmt->bindParam(':link', $link, PDO::PARAM_STR);
          $stmt->bindParam(':nombre_criterio', $nombre_criterio, PDO::PARAM_STR);
          $stmt->bindParam(':subcriterio', $subcriterio, PDO::PARAM_STR);
          $stmt->execute();
            
        } catch (Exception $ex) {
          print_r($ex);
        }
        ///////////////////////////////////////////////////////////////////////// 

        // SE BUSCA EL SIGUIENTE REGISTRO
        $t = $t + 1;       
      }
    } 

    //////////////////////////////////////////////////////////////////////////////////////////////
    //                                NUEVO PROCESO: VISUALIZAR CONTENIDO
    //////////////////////////////////////////////////////////////////////////////////////////////

    // SE EXTRAEN TODOS LOS REGISTROS DE DETALLE
    $sql = "SELECT * FROM tbldetalle";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    // SE INICIALIZAN VARIABLES DE TRBAJO
    $elExpediente = "";
    $elArchivo = "";
    $elCriterio = 0;
    $elOrden = 1;

    ?>

    <!-- SE CREA UNA TABLA PARA VISUALIZAR LOS RESULTADOS -->
    <div>

    <?php if ($depuracion) { ?>

    <table border="1">
    <tr>
    <th>Expedientes</th><th>Archivo</th><th>Nombre del Documento</th><th>Observaciones</th><th>Fecha Creación</th><th>Fecha Incorporación</th><th>Orden</th><th>Nro Páginas</th><th>Pag Inicial</th><th>Pag Final</th><th>Formato</th><th>Tamaño</th><th>Origen</th>
    </tr>

  <?php } ?>

    <!-- SE LLENA LA TABLA REGISTRO A REGISTRO -->
    <?php

      // VERIFICA SI HAY REGISTROS
      if ($query->rowCount() > 0) {

        // LA VARIABLE i CONTROLA EL CICLO SOBRE TODOS LOS REGISTROS DE DETALLE
        $i = 0;
        while ($i < count($results)) {
          ?>

          <!-- SE INCREMENTA EL VALOR DE ORDEN. ESTE NÚMERO SE REINICIA PARA EL SIGUIENTE
               DOCUMENTO QUE SE VAYA A PROCESAR -->
          <?php $results[$i]['orden'] = $orden; $orden = $orden + 1; ?>

          <!-- SE EXTRAE EL LINK DEL DOCUMENTO APUNTADO POR EL REGISTRO -->
          <?php
            $elLink = $results[$i]['link'];
            $elLink = str_replace(RAIZ, '', $elLink);
          ?>

          <!-- SE CREA LA FILA EN LA TABLA -->
          <?php if ($depuracion) { ?>
          <tr>
          <?php } ?>

            <!-- SI HAY CAMBIO DE EXPEDIENTE, SE HACEN DOS COSAS:
                 1. SE ACTUALIZA EL CONTROL DE EXPEDIENTES
                 2. SE ESCRIBE EL NUEVO EXPEDIENTE EN LA PANTALLA. ESTO ES PORQUE NORMALMENTE
                    NO SE ESCRIBE EL EXPEDIENTE, PARA NO SATURAR LA PANTALLA CON UN TEXTO REPETIDO -->
            <?php if (strcmp($elExpediente, $results[$i]['expediente']) != 0) { 
              $elExpediente = $results[$i]['expediente']; ?>

              <?php if ($depuracion) { ?>
                <td><?php echo "&nbsp;" . $results[$i]['expediente'] . "&nbsp;"?></td>
              <?php } ?>

            <?php } else { ?>

              <?php if ($depuracion) { ?> 
                <td></td>
              <?php } ?>

            <?php } ?>

            <!-- SI DENTRO DEL EXPEDIENTE, CAMBIA EL ARCHIVO, ENTONCES:
                 1. SE ACTUALIZA EL CONTROL DE ARCHIVOS
                 2. SE REINICIA EL orden
                 3. SE ACTUALIZA EL orden EN EL REGISTRO (y se lo incrementa para la SIGUENTE VEZ)
                 4. SE IMPRIME archivo CON EL LINK QUE LE CORRESPONDE -->

            <?php

              if ($mostrar_10) {
                echo "i = " . $i . " ARCHIVO ANTERIOR = " . $elArchivo . " ARCHIVO ACTUAL = " . $results[$i]['archivo'] . "<br>";
              }
            ?>

            <?php if (strcmp($elArchivo, $results[$i]['archivo']) != 0 || strcmp($results[$i-1]['expediente'],$results[$i]['expediente']) != 0) { 
              $elArchivo = $results[$i]['archivo'];               
              $orden = 1;
              $results[$i]['orden'] = $orden; 
              $orden = $orden + 1; ?>

              <?php if ($depuracion) { ?>
                <td><?php echo "&nbsp;" . '<a href="' . $elLink . '" target="_blank">' . $results[$i]['archivo'] . '</a>&nbsp;' . '</td>'?></td> 
              <?php } ?>

             <?php } else { ?>

              <?php if ($depuracion) { ?>
                <td></td>
              <?php } ?>

            <?php } ?>

            <!-- EL PIVOTE SE UTILIZA PARA CONTROLAR LOS REGISTROS REPETIDOS -->
            <?php $pivote = $i; 

            // SE MUESTRA INFORMACIÓN DE CONTROL SOBRE criterio Y subcriterio
            // echo "ID DETALLE = " . $results[$i]['id'] . " CRITERIO = " . $results[$i]['criterio'] . " - SUB-CRITERIO = " . $results[$i]['subcriterio'] . "<br>";

            ///////////////////////////////////////////////////////////////////////

            // SE PROCESAN TODOS LOS REGISTROS, A EXCEPCIÓN DEL ÚLTIMO, EL CUAL REQUIERE
            // DE UN TRATAMIENTO DIFERENTE. APARENTEMENTE SE TIENE UN ERROR, EL CUAL DEBE SER VALIDADO
            // ESTE ERROR SIGNIFICA QUE SE TIENE UN PROBLEMA EN LA FRONTERA
            if ($i < count($results) - 1) {

               if ($mostrar_11) {
                 echo "<hr>CONTROL DE ITERACIÓN<br>";
                 echo "I = " . $i . "<br>";
                 echo "PIVOTE = " . $pivote . "<br>";
                 echo "CRITERIO I = " . $results[$i]['criterio'] . "<br>";
                 echo "CRITERIO I + 1 = " . $results[$i + 1]['criterio'] . "<br>";
                 echo "SUB CRITERIO I = " . $results[$i]['subcriterio'] . "<br>";
                 echo "SUB CRITERIO I + 1 = " . $results[$i + 1]['subcriterio'] . "<br>";
                 echo "ARCHIVO I = " . $results[$i]['archivo'] . "<br>";
                 echo "ARCHIVO I + 1 = " . $results[$i + 1]['archivo'] . "<br>";
                 echo "PAGINA INICIAL = " . $results[$i]['pag_inicial'] . "<br>";
               }

               // SE COMPARA SI EL CRITERIO ACTUAL ES IGUAL AL SIGUIENTE
               if ( ! ($results[$i]['criterio'] == $results[$i + 1]['criterio'] && $results[$i]['subcriterio'] == $results[$i + 1]['subcriterio'] && strcmp($results[$i]['archivo'], $results[$i + 1]['archivo']) == 0) ) {
                  // SON DIFERENTES. SE DEBE CONTINUAR EL ANÁLISIS DEL SIGUIENTE REGISTRO
                  if ($mostrar_11) {
                    echo "SON DIFERENTES !!! SE MUEVE AL SIGUIENTE VALOR DE I => i = i + 1 ==> i = " . $i . "<hr>";
                  }
                  $i = $i + 1;
                } 

               else {
                  // EL CRITERIO ACTUAL Y EL SIGUIENTE SON IGUALES
                  // ESTO SIGNIFICA QUE EL SIGUIENTE REGISTRO SE DEBE "EMBEBER" EN UN PAQUETE 
                  // ENCABEZADO POR EL REGISTRO ACTUAL. DEBE CONTEMPLARSE TAMBIÉN QUE PUDIERA SER
                  // QUE EL CRITERIO DEL REGISTRO ACTUAL SEA EXACTAMENTE IGUAL NO SOLO AL DEL
                  // SIGUIENTE REGISTRO, SINO A UN GRUPO DE REGISTROS A CONINUACIÓN
                  // ESTA ES LA RAZÓN DEL CICLO QUE SIGUE A CONTINUACIÓN

                  if ($mostrar_11) {
                    echo "SON IGUALES !!! SE CREA UN CICLO DE 'EMBEBIDO' REGISTROS" . "<hr>";
                  }

                  // LA VARIABLE K CONTROLARÁ EL NUEVO CICLO. INICIA EN EL VALOR ACTUAL DE i
                  $k = $i;

                  // MIENTRAS:
                  // 1. NO LLEGUEMOS AL PENÚLTIMO REGISTRO
                  // 2. EL CRITERIO DE LOS DOS REGISTROS CONSECUTIVOS SEA IGUL
                  // 3. NO CAMBIEMOS DE ARCHIVO
                  // 4. NO ESTEMOS EN LA PRIMERA PÁGINA (es la portada)
                  //    entonces ==> INCREMENTAR K, LO CUAL PERMITE ITERAR SOBRE LOS REPETIDOS
                  while ($k < count($results) - 1 && $results[$k]['criterio'] == $results[$k + 1]['criterio'] && $results[$k]['subcriterio'] == $results[$k + 1]['subcriterio'] && strcmp($results[$k]['archivo'], $results[$k + 1]['archivo']) == 0 && $results[$k]['pag_inicial'] != 1) {

                    if ($mostrar_11) {
                       echo "ESTAMOS EN EL CICLO EMBEBIDO<br>";
                       echo "----------------------------<br>";
                       echo "K = " . $k . "<br>";
                       echo "PIVOTE = " . $pivote . "<br>";
                       echo "CRITERIO K = " . $results[$k]['criterio'] . "<br>";
                       echo "CRITERIO K + 1 = " . $results[$k + 1]['criterio'] . "<br>";
                       echo "SUB CRITERIO K = " . $results[$k]['subcriterio'] . "<br>";
                       echo "SUB CRITERIO K + 1 = " . $results[$k + 1]['subcriterio'] . "<br>";
                       echo "ARCHIVO K = " . $results[$k]['archivo'] . "<br>";
                       echo "ARCHIVO K + 1 = " . $results[$k + 1]['archivo'] . "<br>";
                       echo "PAGINA INICIAL = " . $results[$k]['pag_inicial'] . "<br>";
                    }

                    $k = $k + 1;
                  }

                  if ($mostrar_11) {
                     echo "<hr>HEMOS SALIDO DEL CICLO EMBEBIDO !!!<br>";
                     echo "K = " . $k . "<br>";
                     echo "PIVOTE = " . $pivote . "<br>";
                     echo "CRITERIO K = " . $results[$k]['criterio'] . "<br>";
                     echo "CRITERIO K + 1 = " . $results[$k + 1]['criterio'] . "<br>";
                     echo "SUB CRITERIO K = " . $results[$k]['subcriterio'] . "<br>";
                     echo "SUB CRITERIO K + 1 = " . $results[$k + 1]['subcriterio'] . "<br>";
                     echo "ARCHIVO K = " . $results[$k]['archivo'] . "<br>";
                     echo "ARCHIVO K + 1 = " . $results[$k + 1]['archivo'] . "<br>";
                     echo "PAGINA INICIAL = " . $results[$k]['pag_inicial'] . "<br>";
                  }

                  // LA VARIABLE extra DETERMINA LA CANTIDAD DE REGISTROS QUE SE VAN A SALTAR
                  $extra = $k - $pivote + 1;

                  if ($mostrar_11) {
                    echo "VALOR EXTRA = " . $extra . "<br>";
                  }

                  // INSTRUCCIÓN DE CONTRO DE FRONTERA: SI extra vale 0, entonces extra se debe volver 1
                  // pues esto impedirá que entremos a un ciclo infinito
                  if ($extra == 0) $extra = 1;

                  // SE ACTUALIZA LA PÁGINA FINAL CON BASE EN LA PÁGINA INICIAL Y EL NÚMERO DE
                  // REGISTROS QUE SE SALTARON
                  $results[$pivote]['pag_final'] = $results[$pivote]['pag_inicial'] + $extra - 1;

                  // SE ACTUALIZA EL NÚMERO DE PÁGINAS DEL BLOQUE
                  $results[$pivote]['nro_paginas'] = $extra;

                  if ($mostrar_11) {
                    echo "PÁGINA INICIAL = " . $results[$pivote]['pag_inicial'] . "<br>";
                    echo "PÁGINA FINAL = " . $results[$pivote]['pag_final'] . "<br>";
                    echo "NÚMERO DE PÁGINAS = " . $results[$pivote]['nro_paginas'] . "<br>";
                  }

                  // LA VARIABLE i SE INCREMENTA POR EL VALOR extra, LO CUAL PERMITIRÁ SALTAR LOS
                  // REGISTROS REPETIDOS      
                  $i = $i + $extra;

                  if ($mostrar_11) {
                    echo "NUEVO VALOR DE LA i = " . $i . " ==> i anterior + extra" . "<br>";
                  }           
              }          
            } 

            // PARA EL ÚLTIMO REGISTRO, ÚNICAMENTE SE PROCESA EL INCREMENTO DE LA VARIABLE DE CONTROL (i)
            else $i = $i + 1; ?>

            <!-- ELEMENTOS QUE VAN A FORMAR PARTE DEL DESPLIEGUE DE LA FRACCIÓN DEL DOCUMENTO -->
            <?php $p1 = $results[$pivote]['pag_inicial']; ?>
            <?php $p2 = $results[$pivote]['pag_final']; ?>
            <?php $lin = $results[$pivote]['link']; ?>
            <?php $exp = $results[$pivote]['link']; ?>
            <?php $arc = $results[$pivote]['archivo']; ?>
            <?php $tipo = $results[$pivote]['nombre_criterio']; ?>
            <?php $el_orden = $results[$pivote]['orden']; ?>

            <?php

            ///////////////////////////////////////////////////////////
            // Cambiar criterio hacia observaciones, en caso de obs = 1

            $sql15 = "SELECT * FROM tblmetarreglas WHERE id = " . $results[$pivote]['criterio'];
            $query15 = $pdo->prepare($sql15);
            $query15->execute();
            $resp = $query15->fetchAll(PDO::FETCH_ASSOC);

            if ($query15->rowCount() > 0) {
              if ($resp[0]['obs'] == 1) {
                $results[$pivote]['observaciones'] = $results[$pivote]['nombre_criterio'];
                $results[$pivote]['nombre_criterio'] = 'Otro';
              }
            }

            ///////////////////////////////////////////////////////////

            ?>

            <!-- SI ESTAMOS EN LA PRIMERA PÁGINA, ESTAMOS HABLANDO DE LA PORTADA -->
            <?php 
              if ($pivote == 0) {

                /* 
                $results[$pivote]['observaciones'] = '';
                $results[$pivote]['criterio'] = 0;
                $results[$pivote]['nombre_criterio'] = 'PORTADA';
                */

              }

              if ($el_orden == 1) {

                /*
                $results[$pivote]['observaciones'] = '';
                $results[$pivote]['criterio'] = 0;
                $results[$pivote]['nombre_criterio'] = 'PORTADA';
                */

              }              
            ?>
 
            <!-- INSTRUCCIÓN PARA VISUALIZAR EL DOCUMENTO CON LOS EXTRAS
                 SE UTILIZA UN ARCHIVO PHP INDEPENDIENTE -->

            <?php if ($depuracion) { ?>           
              <td><?php echo ""?> <a href = "visualizar_documento.php?p1=<?php echo $p1; ?>&p2=<?php echo $p2; ?>&exp=<?php echo $elExpediente; ?>&arc=<?php echo $arc; ?>&tipo=<?php echo $tipo; ?>&lin=<?php echo $lin; ?>" target="_blank"> <?php echo $results[$pivote]['nombre_criterio']; ?></a> <?php echo "&nbsp;" ?></td>

              <!-- SE MUESTRAN ELEMENTOS DE LA FILA -->
              <td><?php echo "&nbsp;" . $results[$pivote]['observaciones']; "&nbsp;" ?></td>

              <td><?php echo "&nbsp;" . $results[$pivote]['creacion']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['incorporacion']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['orden']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['nro_paginas']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['pag_inicial']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['pag_final']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['formato']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['tamanio']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['origen']; "&nbsp;" ?></td>

              <!-- /////////////////////////////////////////////////////////////////////// -->

             <!-- FINALIZA LA FILA DENTRO DE LA TABLA -->
             </tr>

           <?php } ?>

          <?php

          // SE MANTIENE EL PIVOTE. ESTO SE DEBE A QUE LOS REGISTROS REPETIDOS HAN CAMBIADO
          // EL VALOR DE LA VARIABLE DE CONTROL (i)
          $pivote = $i;

        }
      }
    ?>


    <?php if ($depuracion) { ?>
      </table>
      </div>
    <?php } ?>

    <?php

    // SE CIERRA LA CONEXIÓN A LA BASE DE DATOS
    $pdo = null;
?>

<?php if ($depuracion) { ?>

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <br><hr>
  <span style="font-weight:bold;">REGRESAR AL --> MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>
  <hr><br>

<?php } ?>