<?php include "funciones.php"; ?>


<?php

function motor_simple($criterio, $tipo = 1, $depuracion = 0) { // tipo = 1, criterio, tipo = 0, subcriterio

	global $pdo;

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   MOTOR ASOCIADO A CRITERIOS ESPECÍFICOS  ///////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE ELIGE UNA TRD Y SE RECORREN TODAS LAS PÁGINAS BUSCANDO PATRONES ESPECÍFICOS
    // SE UTILIZAN CRITERIOS CONCRETOS, ASÍ COMO EL INICIO Y EL FINAL DE LA TRD
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    //                              CONSTANCIA SECRETARIAL --- 614              
    ////////////////////////////////////////////////////////////////////////////////////

    $criterio = 614; // CONSTANCIA SECRETARIAL

    // SE CUENTA EL NÚMERO DE PÁGINAS. ESTE VALOR SERÁ ÚTIL MÁS ADELANTE
    // PARA CALCULAR PÁGINAS EXTRAS EN CASO DE SER REQUERIDO

    try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	    $totalPaginas = $query2->rowCount();
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}    

    // SE EXTRAE LA TRD DEL CRITERIO 615 (HÁBEAS CORPUS)

    try {
	    $sql2 = "SELECT * FROM tbltrd WHERE id = " . $criterio;
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}

    $dummy = trim(depurar_cadena($resTrd[0]['patron'])); 

    while (stripos($dummy, "  ") > 0) {
      $dummy = str_replace("  ", " ", $dummy);
    }

    // SI LA METARREGLA NO ESTÁ VACÍA
    if (!empty($dummy)) {

      $paquete = explode(";", $dummy);

      $paginas = Array();

      $pq = 0;
      while ($pq < count($paquete)) {

        // SE DIVIDE LA METARREGLA EN SUS PALABRAS CONSTITUTIVAS
        $palabras = explode(" ", $paquete[$pq]);

        // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
        $sql  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('";
        $t = 0;
        while ($t < count($palabras)) {
          $sql .= '+' . $palabras[$t] . ' '; 
          $t = $t + 1;
        }

        $sql .= "' IN BOOLEAN MODE) ORDER BY pagina ASC";

        try {
            $query = $pdo->prepare($sql);
            $query->execute();
            $losresultados = $query->fetchAll(PDO::FETCH_ASSOC);
        }
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	}

	  	// SE ALMACENA EL CONJUNTO DE PÁGINAS ENCONTRADAS EN UN ARRAY PARALELO

	  	$fw = 0;
	  	while ($fw < count($losresultados)) {
	  		$paginas[] = $losresultados[$fw];
	  		$fw = $fw + 1;
	  	}

        $pq = $pq + 1;
      }

      // PAQUETES TERMINADOS. SE IMPRIME EL ARRAY DE páginas

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ORDENA EL ARRAY

      $indice = Array();
      foreach ($paginas as $clave => $fila) {
      	$indice[$clave] = $fila['pagina'];
      }

      array_multisort($indice, SORT_ASC, $paginas);

      if ($depuracion) {
      	echo "<br><br>";
      }

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ELIMINAN LOS DUPLICADOS. SE TRANSFIERE A UNA TABLA temporal

      $temporal = Array();

      $fw = 0;
      while ($fw < count($paginas)) {
      	if ($fw == 0) {
      		$temporal[] = $paginas[$fw];
      	}
      	else {
      		if ($paginas[$fw]['pagina'] != $paginas[$fw - 1]['pagina']) {
      			$temporal[] = $paginas[$fw];
      		}
      	}
      	$fw = $fw + 1;
      }

      // SE RECUPERAN LAS PÁGINAS A PARTIR DEL TEMPORAL. ORDENADO Y SIN REPETIDOS

      $paginas = Array();

      $fw = 0;
      while ($fw < count($temporal)) {

      	$s = $temporal[$fw]['contenido'];

      	$pos1 = stripos($s, 'cuenta constancia');
      	$pos2 = stripos($s, 'constancia secretarial de fecha');
      	$pos3 = stripos($s, 'constancia que la sentencia quedo en firme');
      	$pos4 = stripos($s, 'se deja constancia');

      	if ($depuracion) {
      		echo "CONTENIDO DE LA PÁGINA: " . $temporal[$fw]['pagina'] . " ===> " . $temporal[$fw]['contenido'] . "<br><br> ===><br><br>";
      	}

      	// SOLAMENTE SE COPIAN LAS QUE NO CONTENGAN EXCEPCIONES

      	if ($pos1 > 0 || $pos2 > 0 || $pos3 > 0) {
      		// CONTIENE LA EXCEPCIÓN. SE OMITE
      		if ($depuracion) {
      			echo "SE OMITE LA PÁGINA: " . $temporal[$fw]['pagina'] . " PUES CONTIENE LA EXCEPCIÓN: " . "consta constancia" . "<br>";
      		}
      	}
      	else {
      		// NO CONTIENE LA EXCEPCIÓN. SE COPIA
       		$paginas[] = $temporal[$fw];     		
      	}

      	$fw = $fw + 1;
      }

      if ($depuracion) {
	      echo "<br><br>";
	  }

	  if ($depuracion) {
	  	echo "SE OMITEN ALGUNOS REGISTROS<br><br>";
	  }

	  if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

	  // SE CALCULA LA PÁGINA INICIAL Y LA PÁGINA FINAL

	  $pagina_inicial = $paginas[0]['pagina'];
	  $pagina_final = $paginas[count($paginas) - 1]['pagina'];

	  echo "<br>PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  echo "<br>PÁGINA FINAL = " . $pagina_final . "<br><br>";

      // SE CAMBIAN LOS CRITERIOS. N O. NO SE COMPACTAN. SE GRABAN LOS REGISTROS INDIVIDUALMENTE

	  $f = 0;
	  while ($f < count($paginas)) {
	  	$laPagina = $paginas[$f]['pagina'];
	  	try {
		  	$sql2 = "UPDATE tblpaginas SET criterio = " . $criterio . ", subcriterio = 0 WHERE id = " . $paginas[$f]['id'];
		  	$query2 = $pdo->prepare($sql2);
		  	$query2->execute();
		}
		catch (PDOException $ex) {
			print_r($ex);
		}
	  	$f = $f + 1;
	  }  

      try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE criterio = 614";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
	  }
  	  catch (PDOException $ex) {
    	print_r($ex);
  	  } 

  	  if ($depuracion) {
	  	  echo "<br><br>EFECTO DEL MOTOR SOBRE LA TABLA DE PÁGINAS<br><br>";
	  }

	  $f = 0;
	  while ($f < count($resPaginas)) {
	  	  if ($depuracion) {
			 echo "pag = " . $resPaginas[$f]['pagina'] . "<br>";
	  	  }
		  $f = $f + 1;
	  }

      // --------------------------------------------------------------------------------      

    }
}

motor_simple(614, 1, 1); // Número criterio, Tipo del criterio (1=criterio, 0=subcriterio), Depuración (1=Muestra comentarios, 0=Omite los comentarios)

?>