<?php include 'funciones.php'; ?>

<?php $depuracion = 0; ?>

<?php

	if ($depuracion) {
		echo "<br><br>HA INGRESADO A generacion.php (proceso del menú - subproceso)<br><br>";
	}

	$rutaD = $argv[1]; // c:\xampp\htdocs\assurance\expedientes

	// echo "<br>RUTA D = " . $rutaD . "<br>"; // c:\xampp\htdocs\assurance\expedientes\exp??\
	
	$aux = substr($rutaD, strlen($rutaD) - 1);
	if (strcmp($aux, "\\") != 0)
    	$rutaD = substr_replace($rutaD, "\\", strlen($rutaD) - 2, 2);

	$path = RAIZ;

	$tabla = Array();

	listar_directorios_ruta($rutaD, 0);

	if ($tabla != null) {
		calcular_expedientes();

		if ($depuracion) {
			mostrar_tabla_expedientes();
		}
		
		grabar_tabla_expedientes();
	}

	?>

	<?php if ($depuracion) { ?>

		<span style="font-weight:bold;">REGRESAR AL MENÚ</span>
		<a href="index.php"><button>ATRÁS</button></a>
		<hr>

	<?php } ?>

	<?php

	if ($depuracion) {
		echo "<br><br>SALIENDO !!! DE: generacion.php (proceso del menú - subproceso)<br><br>";
	}

?>