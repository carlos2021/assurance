<?php include "funciones.php"; ?>

<?php $depuracion = 0; ?>

<?php if ($depuracion) { ?>

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <span style="font-weight:bold;"><br><br>MÓDULO GRABACIÓN - REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>

<?php } ?>

<?php

    if ($depuracion) {
      echo "<br>PÁGINAS ANTES DE LA GRABACIÓN: <br>";

      try {
        $sql2 = "SELECT * FROM tblpaginas /* WHERE procesado_nueva_trd = 0*/";
        $query2 = $pdo->prepare($sql2);
        $query2->execute();
        $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
        $totalPaginas = $query2->rowCount();
        $elExpediente = $resPaginas[0]['expediente'];
      }
      catch (PDOException $ex) {
        print_r($ex);
      } // TRY de tabla páginas  

      if ($depuracion) {
        echo "EXPEDIENTE: " . $elExpediente . "<br><br>";
      }

      if ($depuracion) {
        echo "TABLA PÁGINAS: <br><br>";
        $xf = 0;
        while ($xf < count($resPaginas)) {
          echo $xf . " Página = " . $resPaginas[$xf]['pagina'] . " Criterio = " . $resPaginas[$xf]['criterio'] . " Subcriterio = " . $resPaginas[$xf]['subcriterio'] . "<br>";
          $xf = $xf + 1;
        }
      }

    }
    
    if ($depuracion) {
	   echo "<hr>GRABAR EXPEDIENTES<hr>Se limpian las tablas: salida - expedientesyarchivos<br>";
    }

    $sql = "TRUNCATE TABLE tblsalida";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $sql = "TRUNCATE TABLE tblexpedientesyarchivos";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    //////////////////////////////////////////////////////////////////////////////////////////////
    //                                NUEVO PROCESO: VISUALIZAR CONTENIDO
    //////////////////////////////////////////////////////////////////////////////////////////////

    // SE EXTRAEN TODOS LOS REGISTROS DE DETALLE
    $sql = "SELECT * FROM tbldetalle";
    $query = $pdo->prepare($sql);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_ASSOC);

    $mostrar = 0;
    $mostrar_1 = 0;
    $mostrar_2 = 0;

    if ($mostrar_2) {

      echo "<br>TABLA DETALLES: <br><br>";

      $k = 0;
      while ($k < count($results)) {
        echo $k . " ORDEN = " . $results[$k]['orden'] . " - CRITERIO = " . $results[$k]['criterio'] . " - PAGINA = " . $results[$k]['pag_inicial'] . " ARCHIVO = " . $results[$k]['archivo'] . "<br>";
        $k = $k + 1;
      }
      echo "<hr>";
    }

    // SE INICIALIZAN VARIABLES DE TRABAJO

    $elExpediente = "";
    $elArchivo = "";
    $elCriterio = 0;
    $elOrden = 1;
    $cambio_archivo = 0;

    ?>

    <?php if ($depuracion) { ?>
      <!-- SE CREA UNA TABLA PARA VISUALIZAR LOS RESULTADOS -->
      <div>
      <table border="1">
    <?php } ?>

    <?php if ($depuracion) { ?>

      <tr>
      <th>Expedientes</th><th>Archivo</th><th>Nombre del Documento</th><th>Observaciones</th><th>Fecha Creación</th><th>Fecha Incorporación</th><th>Orden</th><th>Nro Páginas</th><th>Pag Inicial</th><th>Pag Final</th><th>Formato</th><th>Tamaño</th><th>Origen</th>
      </tr>

    <?php } ?>

    <!-- SE LLENA LA TABLA REGISTRO A REGISTRO -->

    <?php

      // VERIFICA SI HAY REGISTROS
      if ($query->rowCount() > 0) {

        if ($depuracion) {
          echo "<br>NÚMERO DE REGISTROS PROCESADOS: " . $query->rowCount() . "<br><br>";
        }

        $nivel_1 = '01PrimeraInstancia';
        $base = _RAIZ;
        $raiz = RAIZ . 'resultados';
        $dir_paginas = RAIZ . 'paginas';

        $carpeta_01 = 'ExpedientesProcesosJudiciales';
        $carpeta_02 = 'ExpedientesProcesosJudicialesPenalesLey906_2004';

        $raiz_base_1 = $raiz . '\\' . $carpeta_01;
        $raiz_base_2 = $raiz_base_1 . '\\' . $carpeta_02;

        $raiz = $raiz_base_2;  

        // shell_exec('rmdir ' . $raiz . ' /s /q');
        // shell_exec('mkdir ' . $raiz);

        // shell_exec('rmdir ' . $dir_paginas . ' /s /q');

        // LA VARIABLE i CONTROLA EL CICLO SOBRE TODOS LOS REGISTROS DE DETALLE
        $i = 0;
        $secuencial = 1;
        while ($i < count($results)) {

          if ($depuracion) {
            echo "<hr>TABLA DETALLES ==> Registro: " . $i . "<br>";
          }

          ?>

          <!-- SE INCREMENTA EL VALOR DE ORDEN. ESTE NÚMERO SE REINICIA PARA EL SIGUIENTE
               DOCUMENTO QUE SE VAYA A PROCESAR -->
          <!-- <?php // $results[$i]['orden'] = $orden; $orden = $orden + 1; ?> -->

          <!-- SE EXTRAE EL LINK DEL DOCUMENTO APUNTADO POR EL REGISTRO -->
          <?php
            $elLink = $results[$i]['link'];
            $elLink = str_replace(RAIZ, '', $elLink);
          ?>

          <!-- SE CREA LA FILA EN LA TABLA -->
          <tr>

            <!-- SI HAY CAMBIO DE EXPEDIENTE, SE HACEN DOS COSAS:
                 1. SE ACTUALIZA EL CONTROL DE EXPEDIENTES
                 2. SE ESCRIBE EL NUEVO EXPEDIENTE EN LA PANTALLA. ESTO ES PORQUE NORMALMENTE
                    NO SE ESCRIBE EL EXPEDIENTE, PARA NO SATURAR LA PANTALLA CON UN TEXTO REPETIDO -->
            <?php if (strcmp($elExpediente, $results[$i]['expediente']) != 0) { 
              $elExpediente = $results[$i]['expediente']; ?>

            <?php
              $sql = "SELECT * FROM tblportada WHERE expediente = " . $elExpediente;
              $query = $pdo->prepare($sql);
              $query->execute();
              $datPortadas = $query->fetchAll(PDO::FETCH_ASSOC);

              $sql = "SELECT * FROM tbltrd WHERE serie = " . $datPortadas[0]['serie'] . " AND subserie = " . $datPortadas[0]['subserie'] . " AND encabezado = 1";
              $query = $pdo->prepare($sql);
              $query->execute();
              $datLaSerie = $query->fetchAll(PDO::FETCH_ASSOC);

              $sql = "SELECT * FROM tbltrd WHERE serie = " . $datPortadas[0]['serie'] . " AND subserie = " . $datPortadas[0]['subserie'] . " AND encabezado = 2";
              $query = $pdo->prepare($sql);
              $query->execute();
              $datLaSubserie = $query->fetchAll(PDO::FETCH_ASSOC);

              /* ///////////////////////////// TEMPORAL ///////////////////////////////
              $carpeta_01 = 'ExpedientesProcesosJudiciales';

              //$raiz_base_1 = $raiz . '\\' . $datLaSerie[0]['tipo_doc'];

              $raiz_base_1 = $raiz . '\\' . $carpeta_01;

              ///////////////////////////// TEMPORAL ///////////////////////////////
              $carpeta_02 = 'ExpedientesProcesosJudicialesPenalesLey906_2004';
              $raiz_base_2 = $raiz_base_1 . '\\' . $carpeta_02;
              $raiz = $raiz_base_2; */
            ?>

              <?php if ($depuracion) { ?>
                <td>
              <?php } ?>

                <?php 

                if ($depuracion) {
                  echo "&nbsp;" . $results[$i]['expediente'] . "&nbsp;";
                }

                $expediente_grabado = $results[$i]['expediente'];
                // shell_exec('mkdir ' . $raiz . '\\' . $elExpediente);

                ?>
            
            <?php if ($depuracion) { ?>              
              </td>
            <?php } ?>

            <?php } else { ?>

              <?php if ($depuracion) { ?>
                <td></td>
              <?php } ?>

            <?php } ?>

            <!-- SI DENTRO DEL EXPEDIENTE, CAMBIA EL ARCHIVO, ENTONCES:
                 1. SE ACTUALIZA EL CONTROL DE ARCHIVOS
                 2. SE REINICIA EL orden
                 3. SE ACTUALIZA EL orden EN EL REGISTRO (y se lo incrementa para la SIGUENTE VEZ)
                 4. SE IMPRIME archivo CON EL LINK QUE LE CORRESPONDE -->
              <?php 
                if (strcmp($elArchivo, $results[$i]['archivo']) != 0 || strcmp($results[$i-1]['expediente'],$results[$i]['expediente']) != 0) {

                  if ($mostrar_1) {
                    echo "VALOR DE i EN DONDE CAMBIA EL ARCHIVO = " . $i . "<br>" . " ANTERIOR ARCHIVO = " . $elArchivo . " SIGUIENTE ARCHIVO = " . $results[$i]['archivo'] . "<br>";
                    echo "EXPEDIENTE: ";
                    if ($i > 0) {
                      echo "I-1 = " . $resultados[$i-1]['expediente'] . " ";
                    }
                    echo "I = " . $results[$i]['expediente'] . "<br>";
                  }

                  $elArchivo = $results[$i]['archivo'];               
                  $orden = 1;
                  $results[$i]['orden'] = $orden; 
                  $secuencial = 1;

                  if ($mostrar_1)
                    echo "EL ARCHIVO !!! ARCHIVO == == " . $results[$i]['archivo'] . "<br>";
              ?>

              <?php if ($depuracion) { ?>
                <td>
              <?php } ?>

              <?php 

                if ($depuracion) {
                  echo "&nbsp;" . '<a href="' . $elLink . '" target="_blank">' . $results[$i]['archivo'] . '</a>&nbsp;' . '</td>';
                }

                $archivo_grabado = $results[$i]['archivo'];

                ///////////////////////////////////////////////////////////
                // SE GRABAN LOS DATOS NECESARIOS PARA INDEXAR EL
                // ÍNDICE ELECTRÓNICO
                ///////////////////////////////////////////////////////////

                if ($depuracion) {
                  echo "<br>SE GRABA EL NRO EXPEDIENTE EN EL ARCHIVO expedientesyarchivos<br><br>";
                }

                try {
                  // SE GRABA EL REGISTRO EN LA TABLA DE EXPEDIENTES Y ARCHIVOS

                  $sql = "INSERT INTO tblexpedientesyarchivos (expediente, archivo) VALUES (:expediente, :archivo)";

                  $stmt = $pdo->prepare($sql);
                  $stmt->bindParam(':expediente', $expediente_grabado, PDO::PARAM_STR);
                  $stmt->bindParam(':archivo', $archivo_grabado, PDO::PARAM_STR);
                  $stmt->execute();
                }
                catch (Exception $ex) {
                  print_r($ex);
                }      

                // shell_exec('mkdir ' . $raiz . '\\' . $elExpediente . '\\' . $nivel_1);

                $s = $results[$i]['archivo'];

                $uc = stripos($s, 'cuaderno');
                $upro = stripos($s, 'roceso');

                if ($mostrar) {
                  echo "S (cadena de archivo) = " . $s . "<br>";
                  echo "UC (posición de cuaderno) = " . $uc . "<br>";
                  echo "UPRO (posición de proceso) = " . $upro . "<br>";
                }

                if ($uc == 0 && $upro == 0) {
                  $nivel_11 = $s;
                } else if ($uc == 0 && $upro > 0) {
                  $nivel_11 = "C01" . "Principal";
                } else if ($uc > 0 && $upro == 0) {
                  $num = buscar_numero($s, 'cuaderno');
                  while (strlen($num) < 2) {
                    $num = '0' . $num;
                  }
                  $nivel_11 =  "C" . $num . "Principal";
                } else { // $uc > 0 && $upro > 0
                  $num = buscar_numero($s, 'cuaderno');
                  while (strlen($num) < 2) {
                    $num = '0' . $num;
                  }
                  $nivel_11 =  "C" . $num . "Principal";            
                }

                if (strcasecmp($s, 'C01.pdf') == 0 ||
                    strcasecmp($s, 'C02.pdf') == 0 ||
                    strcasecmp($s, 'C03.pdf') == 0 ||
                    strcasecmp($s, 'C04.pdf') == 0 ||
                    strcasecmp($s, 'C05.pdf') == 0 ||
                    strcasecmp($s, 'C06.pdf') == 0 ||
                    strcasecmp($s, 'C07.pdf') == 0 ||
                    strcasecmp($s, 'C08.pdf') == 0 ||
                    strcasecmp($s, 'C09.pdf') == 0 ||
                    strcasecmp($s, 'C10.pdf') == 0 ||
                    strcasecmp($s, 'C11.pdf') == 0 ||
                    strcasecmp($s, 'C12.pdf') == 0 ||
                    strcasecmp($s, 'C13.pdf') == 0 ||
                    strcasecmp($s, 'C14.pdf') == 0 ||
                    strcasecmp($s, 'C15.pdf') == 0 ||
                    strcasecmp($s, 'C16.pdf') == 0 ||
                    strcasecmp($s, 'C17.pdf') == 0 ||
                    strcasecmp($s, 'C18.pdf') == 0 ||
                    strcasecmp($s, 'C19.pdf') == 0 ||
                    strcasecmp($s, 'C20.pdf') == 0 ) {
                  $nivel_11 = substr($s, 0, 3) . "Principal";
                }

                if ($mostrar) {
                  echo "1 - Nivel11 = " . $nivel_11 . "<br>";
                }

                // shell_exec('mkdir ' . $raiz . '\\' . $elExpediente . '\\' . $nivel_1 . '\\' . $nivel_11);

                // shell_exec('rmdir ' . $dir_paginas . '\\' . $nivel_1 . '\\' . $nivel_11 . ' /s /q');
                // shell_exec('mkdir ' . $dir_paginas . '\\' . $nivel_1 . '\\' . $nivel_11);

                // SE EXTRAEN LAS PÁGINAS DEL DOCUMENTO Y SE COLOCAN EN LA CARPETA: paginas

                // $instruccion1 = 'pdftk ' . $results[$i]['link'] . ' burst';
                // exec( $instruccion1, $salida, $valor_retornado );

                // $instruccion2 = 'move ' . $base . '\\pg*.pdf ' . $dir_paginas . '\\' . $nivel_1 . '\\' . $nivel_11;
                // exec( $instruccion2, $salida, $valor_retornado );

              ?>
              
              <?php if ($depuracion) { ?>  
                </td> 
              <?php } ?>

              <?php } else { ?>

              <?php if ($depuracion) { ?>
                <td></td>
              <?php } ?>

              <?php 

              ?>

            <?php } ?>

            <!-- EL PIVOTE SE UTILIZA PARA CONTROLAR LOS REGISTROS REPETIDOS -->
            <?php $pivote = $i; 

            // SE MUESTRA INFORMACIÓN DE CONTROL SOBRE criterio Y subcriterio
            // echo "ID DETALLE = " . $results[$i]['id'] . " CRITERIO = " . $results[$i]['criterio'] . " - SUB-CRITERIO = " . $results[$i]['subcriterio'] . "<br>";

            ///////////////////////////////////////////////////////////////////////

            // SE PROCESAN TODOS LOS REGISTROS, A EXCEPCIÓN DEL ÚLTIMO, EL CUAL REQUIERE
            // DE UN TRATAMIENTO DIFERENTE. APARENTEMENTE SE TIENE UN ERROR, EL CUAL DEBE SER VALIDADO
            // ESTE ERROR SIGNIFICA QUE SE TIENE UN PROBLEMA EN LA FRONTERA
            if ($i < count($results) - 1) {

               // SE COMPARA SI EL CRITERIO ACTUAL ES IGUAL AL SIGUIENTE
               if ( ! ($results[$i]['criterio'] == $results[$i + 1]['criterio'] && $results[$i]['subcriterio'] == $results[$i + 1]['subcriterio'] && strcmp($results[$i]['archivo'], $results[$i + 1]['archivo']) == 0) ) {
                  // SON DIFERENTES. SE DEBE CONTINUAR EL ANÁLISIS DEL SIGUIENTE REGISTRO
                  $i = $i + 1;
                } 

               else {
                  // EL CRITERIO ACTUAL Y EL SIGUIENTE SON IGUALES
                  // ESTO SIGNIFICA QUE EL SIGUIENTE REGISTRO SE DEBE "EMBEBER" EN UN PAQUETE 
                  // ENCABEZADO POR EL REGISTRO ACTUAL. DEBE CONTEMPLARSE TAMBIÉN QUE PUDIERA SER
                  // QUE EL CRITERIO DEL REGISTRO ACTUAL SEA EXACTAMENTE IGUAL NO SOLO AL DEL
                  // SIGUIENTE REGISTRO, SINO A UN GRUPO DE REGISTROS A CONINUACIÓN
                  // ESTA ES LA RAZÓN DEL CICLO QUE SIGUE A CONTINUACIÓN

                  // LA VARIABLE K CONTROLARÁ EL NUEVO CICLO. INICIA EN EL VALOR ACTUAL DE i
                  $k = $i;

                  // MIENTRAS:
                  // 1. NO LLEGUEMOS AL ÚLTIMO REGISTRO
                  // 2. EL CRITERIO DE LOS DOS REGISTROS CONSECUTIVOS SEA IGUAL
                  // 3. NO CAMBIEMOS DE ARCHIVO
                  // 4. NO ESTEMOS EN LA PRIMERA PÁGINA (es la portada)
                  //    entonces ==> INCREMENTAR K, LO CUAL PERMITE ITERAR SOBRE LOS REPETIDOS
                  while ($k < count($results) - 1 && $results[$k]['criterio'] == $results[$k + 1]['criterio'] && $results[$k]['subcriterio'] == $results[$k + 1]['subcriterio'] && strcmp($results[$k]['archivo'], $results[$k + 1]['archivo']) == 0 && $results[$k]['pag_inicial'] != 1) {
                    $k = $k + 1;
                  }

                  ////////////// INSTRUCCIÓN PREVIA BAJO REVISIÓN !!! ///////////////

                  // LA VARIABLE extra DETERMINA LA CANTIDAD DE REGISTROS QUE SE VAN A SALTAR
                  $extra = $k - $pivote + 1;

                  // INSTRUCCIÓN DE CONTRO DE FRONTERA: SI extra vale 0, entonces extra se debe volver 1
                  // pues esto impedirá que entremos a un ciclo infinito
                  if ($extra == 0) $extra = 1;

                  // SE ACTUALIZA LA PÁGINA FINAL CON BASE EN LA PÁGINA INICIAL Y EL NÚMERO DE
                  // REGISTROS QUE SE SALTARON
                  $results[$pivote]['pag_final'] = $results[$pivote]['pag_inicial'] + $extra - 1;

                  // SE ACTUALIZA EL NÚMERO DE PÁGINAS DEL BLOQUE
                  $results[$pivote]['nro_paginas'] = $extra;

                  // LA VARIABLE i SE INCREMENTA POR EL VALOR extra, LO CUAL PERMITIRÁ SALTAR LOS
                  // REGISTROS REPETIDOS      
                  $i = $i + $extra;           
              }          
            } 

            // PARA EL ÚLTIMO REGISTRO, ÚNICAMENTE SE PROCESA EL INCREMENTO DE LA VARIABLE DE CONTROL (i)
            else $i = $i + 1; ?>

            <!-- ELEMENTOS QUE VAN A FORMAR PARTE DEL DESPLIEGUE DE LA FRACCIÓN DEL DOCUMENTO -->
            <?php $p1 = $results[$pivote]['pag_inicial']; ?>
            <?php $p2 = $results[$pivote]['pag_final']; ?>
            <?php $lin = $results[$pivote]['link']; ?>
            <?php $exp = $results[$pivote]['link']; ?>
            <?php $arc = $results[$pivote]['archivo']; ?>
            <?php $tipo = $results[$pivote]['nombre_criterio']; ?>
            <?php $el_nombre_criterio = $results[$pivote]['nombre_criterio']; ?>
            <?php $observaciones = $results[$pivote]['observaciones']; ?>
            <?php $el_orden = $results[$pivote]['orden']; ?>

            <?php

            if ($depuracion) {
              echo "<br>nn100 - NOMBRE CRITERIO: " . $el_nombre_criterio . "<br>";
            }

            ///////////////////////////////////////////////////////////
            // Cambiar criterio hacia observaciones, en caso de obs = 1

            $sql15 = "SELECT * FROM tblmetarreglas WHERE id = " . $results[$pivote]['criterio'];
            $query15 = $pdo->prepare($sql15);
            $query15->execute();
            $resp = $query15->fetchAll(PDO::FETCH_ASSOC);

            if ($query15->rowCount() > 0) {
              if ($resp[0]['obs'] == 1) {
                $results[$pivote]['observaciones'] = $results[$pivote]['nombre_criterio'];
                $results[$pivote]['nombre_criterio'] = 'Sin Clasificacion';
              }
            }

            ///////////////////////////////////////////////////////////

            ?>

            <!-- SI ESTAMOS EN LA PRIMERA PÁGINA, ESTAMOS HABLANDO DE LA PORTADA -->
            <?php 
              if ($pivote == 0) {

                /* 
                $results[$pivote]['observaciones'] = '';
                $results[$pivote]['criterio'] = 0;
                $results[$pivote]['nombre_criterio'] = 'PORTADA';
                */

              }


              if ($results[$pivote]['pag_inicial'] == 1) {

                /*
                $results[$pivote]['observaciones'] = '';
                $results[$pivote]['criterio'] = 0;
                $results[$pivote]['nombre_criterio'] = 'PORTADA';
                */

              } 
            ?>
 
            <!-- INSTRUCCIÓN PARA VISUALIZAR EL DOCUMENTO CON LOS EXTRAS
                 SE UTILIZA UN ARCHIVO PHP INDEPENDIENTE --> 

            <!-- NOTA IMPORTANTE: La siguiente instrucción realiza dos tareas:
                 1. Escribe en pantalla el 'nombre_criterio' en una fila 
                    de la tabla: <td> ... </td>
                 2. Permite, mediante un 'href', que se pueda visualizar en una ventana externa
                    una parte de la información. En particular, el contenido de la página
                 3. Para ver el contenido del criterio, se hace clic sobre el nombre del
                    criterio -->

            <?php if ($depuracion) { ?>
              <td>
            <?php } ?>

              <?php if ($depuracion) { ?>

                <?php echo ""?> <a href = "visualizar_documento.php?p1=<?php echo $p1; ?>&p2=<?php echo $p2; ?>&exp=<?php echo $elExpediente; ?>&arc=<?php echo $arc; ?>&tipo=<?php echo $tipo; ?>&lin=<?php echo $lin; ?>" target="_blank"> <?php echo $results[$pivote]['nombre_criterio']; ?></a> <?php echo "&nbsp;" ?>

              <?php } ?>

            <?php if ($depuracion) { ?>
              </td>
            <?php } ?>

            <?php $link_grabado = $lin; ?>

            <!-- SI ESTAMOS EN LA PRIMERA PÁGINA, ESTAMOS HABLANDO DE LA PORTADA -->
            <!--
            <?php 
              /* if ($pivote == 0) {
                $results[$pivote]['observaciones'] = 'PORTADA';
                $results[$pivote]['criterio'] = 'Sin Clasificación';
              } */
            ?> -->

           <?php 

              $mostrar = 0;
              $mostrar_1 = 0;
              $mostrar_2 = 0;

              if (strcasecmp($tipo, 'Otro') == 0) {
                $tipo = $observaciones;

                /* 
                if ($p1 == 1) {
                  // $tipo = 'PORTADA';
                  $tipo = $observaciones;
                }
                else { 
                  $tipo = $observaciones;
                }
                */

              }

              if ($orden == 1) {
                  // $tipo = 'PORTADA';
                  $tipo = $observaciones;
              }

              if ($depuracion) {
                if ($p1 == 1) {
                  echo "nn100 - AQUÍ MIRAMOS COMO ES TIPO PARA la página 1: " . $tipo . "<br>";
                }
              }

              if ((strcasecmp($tipo, 'Otro') == 0 || strcasecmp($tipo, '') == 0) && $p1 == 1) {
                $tipo = $el_nombre_criterio;
              }
                
              $tipo = trim(str_replace(' ', '', $tipo));
              $tipo = preg_replace("[^A-Za-z0-9]", "", $tipo);

              $str_pagina = $orden;

              if ($mostrar_2) {
                echo "STR_PAGINA = " . $str_pagina . " ORDEN = " . $orden . "<br>";
              }

              $results[$pivote]['orden'] = $orden;;
              while (strlen($str_pagina) < 2) {
                $str_pagina = '0' . $str_pagina;
              }

              $str_pagina_2 = $orden;
              while (strlen($str_pagina_2) < 4) {
                $str_pagina_2 = '0' . $str_pagina_2;
              }

              $orden = $orden + 1;

              if ($mostrar_2) {
                echo "NUEVO ORDEN: " . $orden . "<br>";
              }
            ?>

            <?php
              $destino = $raiz . '\\' . $elExpediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . $str_pagina . $tipo . '.pdf';
              $destino = str_replace('<br>', '', $destino);

              $lacarpeta = $destino;
              $lacarpeta = str_replace('<br>', '', $lacarpeta);

              $destinoindice = $raiz . '\\' . $elExpediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\' . "00IndiceExpedienteElectronico.pdf";

              if ($depuracion) {
                echo "DESTINO ÍNDICE = " . $destinoindice . "<br>";
              }

              if ($mostrar_2) 
                echo "DESTINO = " . $destino . "<br>";
            ?>

            <?php
              $origen = $dir_paginas . '\\' . $elExpediente . '\\' . $nivel_1 . '\\' . $nivel_11 . '\\';
              $origen = str_replace('<br>', '', $origen);
              if ($mostrar_2)
                echo "ORIGEN = " . $origen . "<br>";
            ?>

            <?php
              $dir_actual = getcwd();
              if ($mostrar)
                echo "DIRECTORIO ACTUAL = " . $dir_actual . "<br>";

              chdir($origen);
              $nuevo_dir = getcwd();
              
              if ($mostrar)
                echo "NUEVO DIRECTORIO = " . $nuevo_dir . "<br>";

              $instruccion = "pdftk";
              $ii = $p1;
              while ($ii <= $p2) {
                $dummy = strval($ii);
                while (strlen($dummy) < 4) {
                  $dummy = "0" . $dummy;
                }
                $instruccion = $instruccion . " pg_" . $dummy . '.pdf';
                $ii = $ii + 1;
              }
              $instruccion = $instruccion . " cat output " . $destino;

              if ($mostrar_2) {
                echo "INSTRUCCION = " . $instruccion . "<br>";
              }

              $output = shell_exec( $instruccion );

              if ($mostrar_2) {
                echo "<hr>Resultado de ejecución de la instrución (SHELL_EXEC) ====> " . $output . "<br><br>";
              }

              //$tamanio = filesize($destino);
              //$tamanio = round($tamanio / 1000000);

              $tamanio = 1;
              if ($tamanio == 0)
                $tamanio = 1;

              if ($mostrar_1) {
                echo "TAMAÑO DEL ARCHIVO: " . $destino . " = " . $tamanio . "<br>";
              }

              $results[$pivote]['tamanio'] = strval($tamanio);

              chdir($dir_actual);
              $dir_recuperado = getcwd();
              
              if ($mostrar)
                echo "DIRECTORIO RECUPERADO = " . $dir_recuperado . "<br><br>";

            ?>

            <!-- SE MUESTRAN ELEMENTOS DE LA FILA -->

            <!-- ESTA INSTRUCCIÓN YA SE REALIZÓ PREVIAMENTE:
                 <td><?php //echo "&nbsp;" . $results[$pivote]['nombre_criterio'];
                 //"&nbsp;" ?></td> -->

            <?php if ($depuracion) { ?>

              <td><?php echo "&nbsp;" . $results[$pivote]['observaciones']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['creacion']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['incorporacion']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['orden']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['nro_paginas']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['pag_inicial']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['pag_final']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['formato']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['tamanio']; "&nbsp;" ?></td>
              <td><?php echo "&nbsp;" . $results[$pivote]['origen']; "&nbsp;" ?></td>

            <?php } ?>

            <?php
              //////////////////////////////////////////////////////////////////////////
              //              SE GRABA LA SALIDA EN LA TABLA: tblsalida               //
              //////////////////////////////////////////////////////////////////////////

              /* if ($pivote == 1) {
                $results[$pivote]['nombre_criterio'] = 'PORTADA';
                $results[$pivote]['observaciones'] = '';
              } */

              if ($depuracion) {
                echo "<br>SE GRABA REGISTRO EN TABLA DE SALIDA (utilizado en el índice)<br><br>";
              }

              try {
                // SE GRABA EL REGISTRO TOMADO DE PÁGINAS EN LA TABLA DE DETALLES

                $sql = "INSERT INTO tblsalida (expediente, archivo, nombre_criterio, creacion, incorporacion, orden, nro_paginas, pag_inicial, pag_final, formato, tamanio, origen, observaciones, link, criterio, lacarpeta, destinoindice) VALUES (:expediente, :archivo, :nombre_criterio, :creacion, :incorporacion, :orden, :nro_paginas, :pag_inicial, :pag_final, :formato, :tamanio, :origen, :observaciones, :link, :criterio, :lacarpeta, :destinoindice)";

                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':expediente', $expediente_grabado, PDO::PARAM_STR);
                $stmt->bindParam(':archivo', $archivo_grabado, PDO::PARAM_STR);
                $stmt->bindParam(':nombre_criterio', $results[$pivote]['nombre_criterio'], PDO::PARAM_STR);
                $stmt->bindParam(':creacion', $results[$pivote]['creacion'], PDO::PARAM_STR);
                $stmt->bindParam(':incorporacion', $results[$pivote]['incorporacion'], PDO::PARAM_STR);
                $stmt->bindParam(':orden', $results[$pivote]['orden'], PDO::PARAM_INT);
                $stmt->bindParam(':nro_paginas', $results[$pivote]['nro_paginas'], PDO::PARAM_INT);
                $stmt->bindParam(':pag_inicial', $results[$pivote]['pag_inicial'], PDO::PARAM_INT);
                $stmt->bindParam(':pag_final', $results[$pivote]['pag_final'], PDO::PARAM_INT);
                $stmt->bindParam(':formato', $results[$pivote]['formato'], PDO::PARAM_STR);               
                $stmt->bindParam(':tamanio', $results[$pivote]['tamanio'], PDO::PARAM_INT);               
                $stmt->bindParam(':origen', $results[$pivote]['origen'], PDO::PARAM_STR);               
                $stmt->bindParam(':observaciones', $results[$pivote]['observaciones'], PDO::PARAM_STR);
                $stmt->bindParam(':link', $link_grabado, PDO::PARAM_STR);
                $stmt->bindParam(':criterio', $results[$pivote]['criterio'], PDO::PARAM_INT);
                $stmt->bindParam(':lacarpeta', $lacarpeta, PDO::PARAM_STR);
                $stmt->bindParam(':destinoindice', $destinoindice, PDO::PARAM_STR);

                $stmt->execute();
              }
              catch (Exception $ex) {
                print_r($ex);
              }

              //////////////////////////////////////////////////////////////////////////
            ?>

            <?php $secuencial = $secuencial + 1; ?>

            <!-- /////////////////////////////////////////////////////////////////////// -->

           <!-- FINALIZA LA FILA DENTRO DE LA TABLA -->
           </tr>

           <?php 
              /* echo "MOSTRANDO LA FILA: " . $pivote . "<br>";
              print_r($results[$pivote]);
              echo "<br>"; */
           ?>

           <?php

           // SE MANTIENE EL PIVOTE. ESTO SE DEBE A QUE LOS REGISTROS REPETIDOS HAN CAMBIADO
           // EL VALOR DE LA VARIABLE DE CONTROL (i)
           $pivote = $i;

        }
      }
    ?>

    <?php if ($depuracion) { ?>
      </table>
      </div>
    <?php } ?>

    <?php

    // SE CIERRA LA CONEXIÓN A LA BASE DE DATOS
    $pdo = null;
?>

<?php if ($depuracion) { ?>

  <!-- BOTÓN DE REGRESO AL MENÚ PRINCIPAL -->
  <br><hr>
  <span style="font-weight:bold;">REGRESAR AL MENÚ</span>
  <a href="index.php"><button>ATRÁS</button></a>
  <hr><br>

<?php } ?>
