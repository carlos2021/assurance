<?php include "funciones.php"; ?>

<?php $otros_abogados = 0; ?>
<?php $depuracion = 1; ?>

<?php 
	// SE OBTIENE EL EXPEDIENTE ACTUAL
	
	$sql = "SELECT * FROM tblabogados WHERE activo = 1";
	$query = $pdo->prepare($sql);
	$query->execute();
	$resAbogados = $query->fetchAll(PDO::FETCH_ASSOC);

	$abogadoActivo = $resAbogados[0]['id'];
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>HTML</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
	<style>
	html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
	</style>
</head>

<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  <span style="font-size:16px; font-weight:bold;">Menú</span></button>
  <span class="w3-bar-item w3-right"><span style="font-size:16px; font-weight:bold;">ASSURANCE</span></span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="w3images/avatar2.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span><span style="font-size:18px;">Bienvenido, <strong>Gestión Expedientes</strong></span></span><br>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-cog"></i></a>
    </div>
  </div>
  <hr>
  <div class="w3-container w3-black">
    <h5><span style="font-size:18px; font-weight:bold;">Tablero Digital</span></h5>
  </div>

  <div class="w3-bar-block">
    <a href="index.php" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-users fa-fw"></i>  <span style="font-size:18px;">Activar Abogados</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="index_configuracion.php" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-users fa-fw"></i>  <span style="font-size:18px;">Configuración</span></a>
  </div>

</nav>

<script>
  function activar() {
    document.getElementById("abogado_01").click();
    document.getElementById("abogado_02").click();
    document.getElementById("abogado_03").click();
    document.getElementById("abogado_04").click();
    document.getElementById("abogado_05").click();
  }
</script>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

<div class="w3-container w3-center">
  <h2>Copiando Expedientes</h2>
  <h4>Repositorio ====> Abogados</h4>
  <p><i class="fa fa-spinner w3-spin" style="font-size:64px"></i></p>
</div>

<?php 

  $sql = "SELECT * FROM tblconfiguracion";
  $query = $pdo->prepare($sql);
  $query->execute();
  $resConfiguracion = $query->fetchAll(PDO::FETCH_ASSOC);

  $repositorio = $resConfiguracion[0]['repositorio'];
  $origen = $resConfiguracion[0]['origen'];
  $nro_abogados = $resConfiguracion[0]['nro_abogados'];
  $estado_sistema = $resConfiguracion[0]['estado_sistema'];

  $sql = "SELECT * FROM tblestado WHERE id = " . $estado_sistema;
  $query = $pdo->prepare($sql);
  $query->execute();
  $resEstado = $query->fetchAll(PDO::FETCH_ASSOC);

  $nombre_estado = $resEstado[0]['nombre'];
  $nroExpedientes = nro_expedientes($repositorio);

  if ($depuracion) {
		echo "<h2>Moviendo Expedientes al Servidor de Gestión de Expedientes<br><br>";
	}
	// Se eliminan los expedientes actuales

	$raiz = "c:\\xampp\\htdocs\\assurance\\";
	$ruta = REPOSITORIO;

	$abogado_01 = $raiz . "abogado01\\expedientes\\PEX01\\";
    shell_exec('rmdir ' . $abogado_01 . ' /s /q');
    shell_exec('mkdir ' . $abogado_01);

  if ($otros_abogados) {
		$abogado_02 = $raiz . "abogado02\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_02 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_02);

		$abogado_03 = $raiz . "abogado03\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_03 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_03);

		$abogado_04 = $raiz . "abogado04\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_04 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_04);

		$abogado_05 = $raiz . "abogado05\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_05 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_05);

		$abogado_06 = $raiz . "abogado06\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_06 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_06);

		$abogado_07 = $raiz . "abogado07\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_07 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_07);

		$abogado_08 = $raiz . "abogado08\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_08 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_08);

		$abogado_09 = $raiz . "abogado09\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_09 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_09);

		$abogado_10 = $raiz . "abogado10\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_10 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_10);

		$abogado_11 = $raiz . "abogado11\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_11 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_11);

		$abogado_12 = $raiz . "abogado12\\expedientes\\PEX01\\";
	    shell_exec('rmdir ' . $abogado_12 . ' /s /q');
	    shell_exec('mkdir ' . $abogado_12);
  }

	// Se recorre la carpeta: repositorio

	// TEMPORAL !!!!!!!!!!!!!!!!!!!!!!

	echo "Ingresando al ciclo" . "<br>";
	echo "Número de abogados = " . $nro_abogados . "<br>";
	echo "LA RUTA = " . $ruta . "<br>";

	if (is_dir($ruta)) {

			echo "ES UN DIRECTORIO" . "<br>";

      if ($dh = opendir($ruta)) {

      	echo "SE PUDO ABRIR EL DIRECTORIO" . "<br>";

      	$file = readdir($dh);
      	$i = 1;
         while ($file !== false) {

         		echo "EN EL CICLO BÁSICO INICIAL" . "<br>";
         		echo "FILE = " . $file . "<br>";
         		echo "I = " . $i . "<br>";

            if ($file != "." && $file != "..") {

            	echo "NO ES UN PUNTO O DOS PUNTOS" . "<br>";

            	if ($i == 1 && $nro_abogados >= 1) {

            		echo "ENCONTRADO EL PRIMER ABOGADO" . "<br>";

            		$destino = $abogado_01 . $file;

            		if ($depuracion) {
            			echo "<br>RUTA + ARCHIVO = " . $ruta . $file . "<br>";
            			echo "DESTINO = " . $destino . "<br><br>"; 
            		}

								rename($ruta . $file, $destino);	

								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();
            	}
            	if ($i == 2 && $nro_abogados >= 2) {
            		$destino = $abogado_02 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 3 && $nro_abogados >= 3) {
            		$destino = $abogado_03 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 4 && $nro_abogados >= 4) {
            		$destino = $abogado_04 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 5 && $nro_abogados >= 5) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 6 && $nro_abogados >= 6) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 7 && $nro_abogados >= 7) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 8 && $nro_abogados >= 8) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 9 && $nro_abogados >= 9) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 10 && $nro_abogados >= 10) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 11 && $nro_abogados >= 11) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}
            	if ($i == 12 && $nro_abogados == 12) {
            		$destino = $abogado_05 . $file;
								rename($ruta . $file, $destino);
								$sql = "UPDATE tblabogados SET nro_expedientes = 1, id_avance = 2, expediente = " . $file . " WHERE id = " . $i;
								$query = $pdo->prepare($sql);
								$query->execute();	
            	}

            	$i = $i + 1;
	        	}
	        	$file = readdir($dh);
         }
      }
    } 
    else {
        // La ruta no es valida
    	echo "RUTA NO VÁLIDA. Se esperaba la existencia de la carpeta: 'repositorio'" . "<br>";         
    }

	// Se mueve cada expediente, en orden, hacia cada abogado (1 al 1, 2 al 2, etcétera)
?>

<?php 
	// header("Location: p_procesar.php");
?>

  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>

</body>
</html>
