<?php include "admin/header.php"; ?>

<?php $depuracion = 1; ?>

<?php $procesado = $_GET['procesado']; ?>

<script>
  var div = document.getElementById('configuracion');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:280px;margin-top:43px;">

  <!-- Header -->
  <div class="w3-panel w3-margin-top" style="margin-top:22px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
 
    <div class="w3-panel">
      <h4><b><i class="fa fa-folder-open-o"></i> &nbsp;Subir Expediente</b></h4>

      <div class="w3-section">
        <span class="w3-margin-right" style="font-size:18px;">Selección:</span> 
      </div>

    </div>

    <div class="w3-panel">
      <div class="w3-row">
        <div class="w3-col w3-container m6 l6">
          <table class="w3-table-all w3-card-4">
            <?php
              if ($depuracion) {
                echo "ORIGEN = " . $origen . "<br>";
              }
              listar_directorios_ruta($origen, 0);
              mostrar_tabla_expedientes_subir(0); 
            ?>
          </table>
          <div class="separador-20"></div>
        </div>
      </div>
    </div>

    <div class="w3-container">

      <div class="w3-panel w3-pale-red" id="inicio" style="visibility:hidden;">
        <h3>Inicia Procesamiento de Compactación</h3>
        <p>El sistema une los archivos PDF de acuerdo con las reglas del Protocolo II</p>
        <p><i class="fa fa-spinner w3-spin" style="font-size:64px"></i></p>
      </div>

      <div class="w3-panel w3-green" id = "finalizacion" style="visibility:hidden;">
        <h3>Compactación Finalizada</h3>
        <p>El sistema ha unido los PDF. Para visualizar el resultado, haga clic en el botón: Repositorio</p>
      </div>

      <div class="w3-dropdown-hover" id="panel_boton" style="visibility:hidden;">
        <button class="w3-button w3-black" id="boton_finalizacion" onclick="mostrar_repositorio()">Ver Repositorio</button>
      </div>

      <script>

        var proc = "<?php echo $procesado; ?>";

        if (proc == 'finalizado') {
          document.getElementById("inicio").style.display = 'none';
          document.getElementById("finalizacion").style.visibility = 'visible';
          document.getElementById("panel_boton").style.visibility = 'visible';    
        }

        function proceso_subir(dato) {
          var exp = dato.toString();
          document.getElementById("inicio").style.visibility = 'visible';
          window.open("http://localhost/assurance/" + "<?php echo _ENTIDAD; ?>" + "/s_asignar_expediente.php?numero=" + exp, "_self");
        }

        function mostrar_repositorio() {
          window.open("http://localhost/assurance/" + "<?php echo _ENTIDAD; ?>" + "/p_repositorio.php?tipo=1", "_self");    
        }
      </script>

    </div>

  </div>

  <!-- End page content -->
</div>
