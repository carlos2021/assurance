<?php include_once "funciones.php"; ?>

<?php

function motor_compuesto($criterio, $tipo = 1, $depuracion = 0) { // tipo = 1, criterio, tipo = 0, subcriterio, depuracion = 1 (mostrar comentarios) depuracion = 0 (ocultar comentarios)

	global $pdo;

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   MOTOR ASOCIADO A CRITERIOS ESPECÍFICOS  ///////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE ELIGE UNA TRD Y SE RECORREN TODAS LAS PÁGINAS BUSCANDO PATRONES ESPECÍFICOS
    // SE UTILIZAN CRITERIOS CONCRETOS, ASÍ COMO EL INICIO Y EL FINAL DE LA TRD
    ////////////////////////////////////////////////////////////////////////////////////

    // SE CUENTA EL NÚMERO DE PÁGINAS. ESTE VALOR SERÁ ÚTIL MÁS ADELANTE
    // PARA CALCULAR PÁGINAS EXTRAS EN CASO DE SER REQUERIDO

    try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	    $totalPaginas = $query2->rowCount();
		}
  	catch (PDOException $ex) {
    	print_r($ex);
  	} // cierre try  

    // SE EXTRAE LA TRD DEL CRITERIO

    try {
	    $sql2 = "SELECT * FROM tbltrd WHERE id = " . $criterio;
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
		}
  	catch (PDOException $ex) {
    	print_r($ex);
  	} // cierre try

    $dummy = trim(depurar_cadena($resTrd[0]['patron'])); 

    while (stripos($dummy, "  ") > 0) {
      $dummy = str_replace("  ", " ", $dummy);
    } // cierre while

    // SI LA METARREGLA NO ESTÁ VACÍA
    if (!empty($dummy)) {

    	// SE DESEMPAQUETA EL PATRÓN
      $paquete = explode(";", $dummy);

      // EN paginas SE ALMACENAN 'TODAS' LAS PÁGINAS OBTENIDAS DE 'TODAS' LAS METARREGLAS
      // CADA METARREGLA ES UNA PARTE DEL PAQUETE (generado con el patron de la TRD)
      $paginas = Array();

      // SE ANALIZA CADA PARTE DEL PATRÓN DE BÚSQUEDA
      $pq = 0;
      while ($pq < count($paquete)) {

        // SE DIVIDE LA METARREGLA EN SUS PALABRAS CONSTITUTIVAS
        $palabras = explode(" ", $paquete[$pq]);

        // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
        $sql  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('";
        $t = 0;
        while ($t < count($palabras)) {
          $sql .= '+' . $palabras[$t] . ' '; 
          $t = $t + 1;
        }

        $sql .= "' IN BOOLEAN MODE) ORDER BY pagina ASC";

        // AQUÍ SE ENCUENTRA EL CONJUNTO DE PÁGINAS QUE SALEN DE LA METARREGLA
        try {
            $query = $pdo->prepare($sql);
            $query->execute();
            $losresultados = $query->fetchAll(PDO::FETCH_ASSOC);
	      }
		  	catch (PDOException $ex) {
		    	print_r($ex);
		  	}

		  	// SE ALMACENA EL CONJUNTO DE PÁGINAS ENCONTRADAS EN UN ARRAY PARALELO

		  	$fw = 0;
		  	while ($fw < count($losresultados)) {
		  		$paginas[] = $losresultados[$fw];
		  		$fw = $fw + 1;
		  	}

		  	// BUSCA EL SIGUIENTE CONJUNTO DE PÁGINAS PARA LA SIGUIENTE METARREGLA DEL PAQUETE
        $pq = $pq + 1;
      }

      ////////////////////////////////////////////////////////////////////////////////////
      // PAQUETES TERMINADOS. SE IMPRIME EL ARRAY DE páginas

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }

	  } // CIERRE DEL ANÁLISIS DE TODAS LAS METARREGLAS
	  	// SE PROCEDE CON EL ANÁLISIS DE TODAS LAS PÁGINAS

      // SE ORDENA EL ARRAY DE PÁGINAS UTILIZANDO COMO ÍNDICE EL NÚMERO DE LA PÁGINA

    $indice = Array();
    foreach ($paginas as $clave => $fila) {
    	$indice[$clave] = $fila['pagina'];
    }

    array_multisort($indice, SORT_ASC, $paginas);

    if ($depuracion) {
    	echo "<br><br>";
    }

    // SE IMPRIME EL ARRAY DE PÁGINAS
    if ($depuracion) {
      $fw = 0;
      while ($fw < count($paginas)) {
      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
      	$fw = $fw + 1;
      }
  	}

    // SE ELIMINAN LOS DUPLICADOS. SE TRANSFIERE A UNA TABLA temporal

    $temporal = Array();

    $fw = 0;
    while ($fw < count($paginas)) {
    	if ($fw == 0) {
    		$temporal[] = $paginas[$fw];
    	}
    	else {
    		if ($paginas[$fw]['pagina'] != $paginas[$fw - 1]['pagina']) {
    			$temporal[] = $paginas[$fw];
    		}
    	}
    	$fw = $fw + 1;
    }

    // SE RECUPERAN LAS PÁGINAS A PARTIR DEL TEMPORAL. ORDENADO Y SIN REPETIDOS

    $paginas = Array();

    $fw = 0;
    while ($fw < count($temporal)) {
    	$paginas[] = $temporal[$fw];
    	$fw = $fw + 1;
    }

    if ($depuracion) {
      echo "<br><br>";
  	}

	  if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

	  // HASTA ESTE PUNTO ES PREPARACIÓN DEL CONTEXTO. LA CLAVE SON LAS PÁGINAS ENCONTRADAS
    // ----------------------------------------------------------------------------------

    // SE BUSCA EL DESENCADENANTE. SE REVISAN TODAS LAS PÁGINAS. PROCESO INICIAL

    $inicio = Array();

    $f = 0;
    while ($f < count($paginas)) {

    	// SE EVALÚA CADA UNA DE LAS PÁGINAS
	  	$texto = $paginas[$f]['contenido'];
	  	$texto = "=" . $texto;

	  	// ==============================================================
	  	// REGLAS

	    if ($criterio == 615) { // HABEAS CORPUS
		 		$pos1 = stripos($texto, 'juez');
		 		$pos2 = stripos($texto, 'habeas');
		 		$pos3 = stripos($texto, 'hábeas');
		 		$pos4 = stripos($texto, 'corpus');
		 		$pos5 = stripos($texto, 'referencia');
		 	}
		 	else if ($criterio == 583) { // ESCRITO DE ACUSACIÓN
		 		$pos1 = stripos($texto, 'escrito de acusacion');
		 		$pos2 = stripos($texto, 'escrito de acusación');
		 	}

	  	// ==============================================================
	  	// PONDERACIÓN

	    if ($criterio == 615) { // HABEAS CORPUS
	 			$pond = ($pos1 > 0) * 5 + ($pos5 > 0) * 3 + ($pos2 > 0 || $pos3 > 0) + ($pos4 > 0) * 1;
	 		}
	 		else if ($criterio == 583) { // ESCRITO DE ACUSACIÓN
	 			$pond = ($pos1 > 0 || $pos2 > 0) * 10;
	 		}

	 		// SE ALMACENA EN UN ARRAY LA PONDERACIÓN DE CADA PÁGINA. EL ARRAY ES inicio
	 		$arr = Array();
	 		$arr[] = $f;
	 		$arr[] = $pond;

	 		$inicio[] = $arr;

	 		$f = $f + 1; 

    } // AQUÍ TERMINA EL CICLO DE CÁLCULO DE PONDERACIONES POR PÁGINA

    // --------------------------------------------------------------
    // SE EVALÚAN LAS PONDERACIONES
 
    if ($depuracion) {
      echo "<br>INICIAL<br>";
	  }

	  // SE IMPRIMEN TODAS LAS PONDERACIONES ASOCIADAS A LAS PÁGINAS ENCONTRADAS PARA EL DESENCADENANTE
	  if ($depuracion) {
      $f = 0;
      while ($f < count($inicio)) {
      	echo "PÁGINA = " . $inicio[$f][0] . " PONDERACIÓN = " . $inicio[$f][1] . "<br>";
      	$f = $f + 1;
      }
	  }

  	// ------------------------------------------------------------
    // SE BUSCA LA PÁGINA INICIAL (DESENCADENANTE)

  	// SE BUSCA LA PÁGINA CON LA MAYOR PONDERACIÓN (página que se corresponde con el desencadenante)
    $f = 0;
    $mayor = 0;
    while ($f < count($inicio)) {
    	if ($inicio[$mayor][1] < $inicio[$f][1]) {
    		$mayor = $f;
    	}
    	$f = $f + 1;
    }

    //////////////////////////////////////////////////////////////////////////
    // SE HA ENCONTRADO LA PÁGINA INICIAL

    $pagina_inicial = $paginas[$inicio[$mayor][0]]['pagina'];

    if ($depuracion) {
      echo "<br>LA PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  }

	  /////////////////////////////////////////////////////////////////////////////////
    // SE BUSCA EL CIERRE. SE REVISAN LAS PÁGINAS DESPUÉS DEL DESENCADENANTE. FINAL

    $final = Array();

    $f = 0;
    while ($f < count($paginas)) {

    	$texto = $paginas[$f]['contenido'];
    	$texto = "=" . $texto;

    	// ==============================================================
    	// REGLAS

      if ($criterio == 615) { // HABEAS CORPUS
		 		$pos1 = stripos($texto, 'resuelve');
		 		$pos2 = stripos($texto, 'ratificar');
		 		$pos3 = stripos($texto, 'remitir');
		 		$pos4 = stripos($texto, 'cópiese');
		 		$pos5 = stripos($texto, 'copiese');
		 		$pos6 = stripos($texto, 'juez');
		 	}
		 	else if ($criterio == 583) { // ESCRITO DE ACUSACIÓN
		 		$pos1 = stripos($texto, 'datos');
		 		$pos2 = stripos($texto, 'fiscal');
		 		$pos3 = stripos($texto, 'firma');
		 	}

    	// ==============================================================
    	// PONDERACIÓN

		 	if ($criterio == 615) { // HABEAS CORPUS
		 		$pond = ($pos1 > 0) * 8 + ($pos2 > 0) * 3 + ($pos3 > 0) * 3 + ($pos4 > 0 || $pos5 > 0) * 3 + ($pos6 > 0) * 6;
		 	}
		 	else if ($criterio == 583) { // ESCRITO DE ACUSACIÓN
		 		$pond = ($pos1 > 0) * 3 + ($pos2 > 0) * 3 + ($pos3 > 0) * 3;
		 	}

	 		$arr = Array();
	 		$arr[] = $f;
	 		$arr[] = $pond;

	 		$final[] = $arr;

	 		$f = $f + 1; 

    } // SE HAN ENCONTRADO TODAS LAS PÁGINAS POTENCIALES DE CIERRE

    // SE AGREGA UNA PÁGINA ADICIONAL AL ANÁLISIS (página después de la última)

    $siguiente = $paginas[count($paginas) - 1]['pagina'] + 1;

    // SE ADICIONA LA NUEVA PÁGINA EN CASO DE SER VÁLIDA (menor o igual al total de páginas)
    if ($siguiente <= $totalPaginas) {

    	if ($depuracion) {
	      	echo "<br>LA SIGUIENTE PÁGINA = " . $siguiente . "<br><br>";
	    }

	    try {
		    $sql2 = "SELECT * FROM tblpaginas WHERE pagina = " . $siguiente;
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $ultimaPagina = $query2->fetchAll(PDO::FETCH_ASSOC);
			}
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	} 

    	$texto = $ultimaPagina[0]['contenido'];
    	$texto = "=" . $texto;

    	// AHORA QUE SE TIENE EL TEXTO, SE ANALIZA SU VALIDEZ PARA SER AGREGADA AL FINAL DE paginas
    	// ES VÁLIDA SI CUMPLE LA PONDERACIÓN SOBRE CRITERIOS ESPECÍFICOS EN LA PÁGINA

    	// ==============================================================
    	// REGLAS

    	if ($criterio == 615) { // HABEAS CORPUS
		 		$pos1 = stripos($texto, 'resuelve');
		 		$pos2 = stripos($texto, 'notificar');
		 		$pos3 = stripos($texto, 'remitir');
		 		$pos4 = stripos($texto, 'cópiese');
		 		$pos5 = stripos($texto, 'copiese');
		 		$pos6 = stripos($texto, 'juez');
		 	}

	 		// ==============================================================
	 		// PONDERACIÓN

		 	if ($criterio == 615) { // HABEAS CORPUS
		 		$pond = ($pos1 > 0) * 8 + ($pos2 > 0) * 4 + ($pos3 > 0) * 4 + ($pos4 > 0 || $pos5 > 0) * 3 + ($pos6 > 0) * 8;
		 	}

		 	// AGREGACIÓN DE UNA PÁGINA, PARA AQUELLOS CRITERIOS QUE LO REQUIERAN
		 	if ($criterio == 615) {

		 		$arr = Array();
		 		$arr[] = count($final);
		 		$arr[] = $pond;

		 		$final[] = $arr;

		 		$paginas[] = $ultimaPagina[0];

		 		if ($depuracion) {
			 		echo "<br>SE AGREGÓ UNA PÁGINA AL FINAL DE PÁGINAS<br>";
			 	}

		 		$f = 0;
		 		while ($f < count($paginas)) {
		 			if ($depuracion) {
			 			echo "PAGINA = " . $paginas[$f]['pagina'] . "<br>";
			 		}
		 			$f = $f + 1;
		 		}	  	 

		  }

      // SE EVALÚAN LAS PONDERACIONES

      if ($depuracion) {
		      echo "<br>FINAL - VALIDAR !!!<br>";
		  }

      if ($depuracion) {
	      $f = 0;
	      while ($f < count($final)) {
	      	echo "PÁGINA = " . $final[$f][0] . " PONDERACIÓN = " . $final[$f][1] . "<br>";
	      	$f = $f + 1;
	      }
	  	}
		}

      // SE BUSCA LA PÁGINA FINAL

      $f = 0;
      $mayor = 0;
      while ($f < count($final)) {
      	if ($final[$mayor][1] < $final[$f][1]) {
      		$mayor = $f;
      	}
      	$f = $f + 1;
      }

      if ($depuracion) {
		      echo "<br>TAMAÑO DE LA TABLA FINAL = " . count($final) . "<br>";
		      echo "EL MAYOR DEL FINAL ES: !!! " . $mayor . "<br><br>===>><br><br>";
		  }

      $pagina_final = $paginas[$final[$mayor][0]]['pagina'];

      if ($depuracion) {
		      echo "<br>LA PÁGINA FINAL = " . $pagina_final . "<br>";
		  }

 	  // SÍNTESIS DE CIERRE Y APERTURA

	  if ($depuracion) {
	 	  echo "<br>===========> APERTURA Y CIERRE<br><br>";
	 	  echo "APERTURA = " . $pagina_inicial . "<br>";
	 	  echo "CIERRE = " . $pagina_final . "<br><br>";
	 	}

 	  // ESTRUCTURA COMPLETA DE LA TABLA páginas
 	  // echo "<br>ESTRUCTURA COMPLETA DE LA TABLA páginas<br>";

 	  // -----------------------

 	  // print_r($paginas);

 	  // -----------------------

      // SE EVALÚAN LÁS PÁGINAS ANTES DEL CIERRE

 	  // Primero, se busca la página inicial dentro de la tabla "paginas"

 	  $pag_ini = 0;

 	  $f = 0;
 	  while ($f < count($paginas)) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial) {
 	  		$pag_ini = $f;
 	  		break;
 	  	}
 	  	$f = $f + 1;
 	  }

 	  $f = $pag_ini - 1;
 	  while ($f >= 0) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial - 1) {
			$pagina_inicial = $pagina_inicial - 1;
			$f = $f - 1; 	  		
 	  	}
 	  	else {
 	  		break;
 	  	}
 	  }

 	  // CORROBORACIÓN O NUEVA PÁGINA INICIAL

 	  if ($depuracion) {
	 	  echo "NUEVA PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  }

 	  // SE BUSCAN LOS ÍNDICES DENTRO DE LA TABLA paginas CORRESPONDIENTES A
 	  // LAS VARIABLES: pagina_inicial, pagina_final. CON ESTOS ÍNDICES SE PROCEDE
 	  // A MODIFICAR LOS CRITERIOS O SUBCRITERIOS DE LA TABLA PÁGINA EN BASE DE DATOS
 	  // A PARTIR DEL RECORRIDO EN LA TABLA INTERNA: paginas

 	  // INICIALMENTE, LA PRIMERA PÁGINA ESTA DEFINIDA EN paginas EN LA PRIMERA POSICIÓN (0)
 	  // LA ÚLTIMA PÁGINA EN EL ÚLTIMO REGISTRO DE paginas

 	  $pag_ini = 0;
 	  $pag_fin = count($paginas) - 1;

 	  $f = 0;
 	  while ($f < count($paginas)) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial) {
 	  		$pag_ini = $f;
 	  	}
 	  	else if ($paginas[$f]['pagina'] == $pagina_final) {
 	  		$pag_fin = $f;
 	  		break;
 	  	}
 	  	$f = $f + 1;
 	  }

 	  if ($depuracion) {
	 	  echo "<br>LÍMITES DE APERTURA Y CIERRE PARA LA GRABACIÓN DE CRITERIO Y SUBCRITERIO<br><br>";
	 	  echo "PAG_INI = " . $pag_ini . "<br>";
	 	  echo "PAG_FIN = " . $pag_fin . "<br>";
	  }

      // SE CAMBIAN LOS CRITERIOS Y SE COMPACTAN LOS ARCHIVOS

      // NOTA !!! Inicialmente se establecio criterio = 615. Pero en realidad, este no es
      // un criterio. Es un subcriterio. Se mantuvo el nombre de 'criterio' para evitar
      // modificaciones de código fuente

	  try {
	  	if ($tipo == 0) {
	 	  	$sql2 = "UPDATE tblpaginas SET criterio = 0, subcriterio = " . $criterio . " WHERE pagina >= " . $pagina_inicial . " AND pagina <= " . $pagina_final;
	 	}
	 	else {
	 	  	$sql2 = "UPDATE tblpaginas SET subcriterio = 0, criterio = " . $criterio . " WHERE pagina >= " . $pagina_inicial . " AND pagina <= " . $pagina_final;	 		
	 	}
 	  	$query2 = $pdo->prepare($sql2);
 	  	$query2->execute();
 	  }
  	  catch (PDOException $ex) {
    	print_r($ex);
  	  }

      try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
	  }
  	  catch (PDOException $ex) {
    	print_r($ex);
  	  } 

  	  if ($depuracion) {
	  	  echo "<br><br>EFECTO DEL MOTOR SOBRE LA TABLA DE PÁGINAS<br><br>";
	  }

	  if ($depuracion) {
	  	  $f = 0;
	  	  while ($f < count($resPaginas)) {
	  	  	if ($resPaginas[$f]['pagina'] < $pagina_inicial || $resPaginas[$f]['pagina'] > $pagina_final) {
		  	  	echo "pag = " . $resPaginas[$f]['pagina'] . "<br>";
		  	}
	  	  	if ($resPaginas[$f]['pagina'] >= $pagina_inicial && $resPaginas[$f]['pagina'] <= $pagina_final) {
	  	  		echo "PÁGINA = " . $resPaginas[$f]['pagina'] . " CRITERIO = " . $resPaginas[$f]['criterio'] . " SUBCRITERIO = " . $resPaginas[$f]['subcriterio'] . "<br>";
	  	  	}
	  	  	$f = $f + 1;
	  	  }
	  	}

      // --------------------------------------------------------------------------------      

    }
}

function motor_simple($criterio, $tipo = 1, $depuracion = 0) { // tipo = 1, criterio, tipo = 0, subcriterio, depuracion = 1 (mostrar comentarios) depuracion = 0 (ocultar comentarios)

	global $pdo;

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   MOTOR ASOCIADO A CRITERIOS ESPECÍFICOS  ///////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE ELIGE UNA TRD Y SE RECORREN TODAS LAS PÁGINAS BUSCANDO PATRONES ESPECÍFICOS
    // SE UTILIZAN CRITERIOS CONCRETOS, ASÍ COMO EL INICIO Y EL FINAL DE LA TRD
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    //                              CONSTANCIA SECRETARIAL --- 614              
    ////////////////////////////////////////////////////////////////////////////////////

    // SE CUENTA EL NÚMERO DE PÁGINAS. ESTE VALOR SERÁ ÚTIL MÁS ADELANTE
    // PARA CALCULAR PÁGINAS EXTRAS EN CASO DE SER REQUERIDO

    try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	    $totalPaginas = $query2->rowCount();
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}    

    // SE EXTRAE LA TRD DEL CRITERIO 615 (HÁBEAS CORPUS)

    try {
	    $sql2 = "SELECT * FROM tbltrd WHERE id = " . $criterio;
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}

    $dummy = trim(depurar_cadena($resTrd[0]['patron'])); 

    while (stripos($dummy, "  ") > 0) {
      $dummy = str_replace("  ", " ", $dummy);
    }

    // SI LA METARREGLA NO ESTÁ VACÍA
    if (!empty($dummy)) {

      $paquete = explode(";", $dummy);

      $paginas = Array();

      $pq = 0;
      while ($pq < count($paquete)) {

        // SE DIVIDE LA METARREGLA EN SUS PALABRAS CONSTITUTIVAS
        $palabras = explode(" ", $paquete[$pq]);

        // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
        $sql  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('";
        $t = 0;
        while ($t < count($palabras)) {
          $sql .= '+' . $palabras[$t] . ' '; 
          $t = $t + 1;
        }

        $sql .= "' IN BOOLEAN MODE) ORDER BY pagina ASC";

        try {
            $query = $pdo->prepare($sql);
            $query->execute();
            $losresultados = $query->fetchAll(PDO::FETCH_ASSOC);
        }
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	}

	  	// SE ALMACENA EL CONJUNTO DE PÁGINAS ENCONTRADAS EN UN ARRAY PARALELO

	  	$fw = 0;
	  	while ($fw < count($losresultados)) {
	  		$paginas[] = $losresultados[$fw];
	  		$fw = $fw + 1;
	  	}

        $pq = $pq + 1;
      }

      // PAQUETES TERMINADOS. SE IMPRIME EL ARRAY DE páginas

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ORDENA EL ARRAY

      $indice = Array();
      foreach ($paginas as $clave => $fila) {
      	$indice[$clave] = $fila['pagina'];
      }

      array_multisort($indice, SORT_ASC, $paginas);

      if ($depuracion) {
      	echo "<br><br>";
      }

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ELIMINAN LOS DUPLICADOS. SE TRANSFIERE A UNA TABLA temporal

      $temporal = Array();

      $fw = 0;
      while ($fw < count($paginas)) {
      	if ($fw == 0) {
      		$temporal[] = $paginas[$fw];
      	}
      	else {
      		if ($paginas[$fw]['pagina'] != $paginas[$fw - 1]['pagina']) {
      			$temporal[] = $paginas[$fw];
      		}
      	}
      	$fw = $fw + 1;
      }

      // SE RECUPERAN LAS PÁGINAS A PARTIR DEL TEMPORAL. ORDENADO Y SIN REPETIDOS

      $paginas = Array();

      $fw = 0;
      while ($fw < count($temporal)) {

      	$s = $temporal[$fw]['contenido'];

      	// ==============================================================
      	// REGLAS

      	if ($criterio == 614) { // HABEAS CORPUS
	      	$pos1 = stripos($s, 'cuenta constancia');
	      	$pos2 = stripos($s, 'constancia secretarial de fecha');
	      	$pos3 = stripos($s, 'constancia que la sentencia quedo en firme');
	      	$pos4 = stripos($s, 'se deja constancia');
	    }

      	if ($depuracion) {
      		echo "CONTENIDO DE LA PÁGINA: " . $temporal[$fw]['pagina'] . " ===> " . $temporal[$fw]['contenido'] . "<br><br> ===><br><br>";
      	}

      	// SOLAMENTE SE COPIAN LAS QUE NO CONTENGAN EXCEPCIONES

      	// ==============================================================
      	// PONDERACIÓN

      	if ($criterio == 614) { // HABEAS CORPUS
	      	if ($pos1 > 0 || $pos2 > 0 || $pos3 > 0) {
	      		// CONTIENE LA EXCEPCIÓN. SE OMITE
	      		if ($depuracion) {
	      			echo "SE OMITE LA PÁGINA: " . $temporal[$fw]['pagina'] . " PUES CONTIENE LA EXCEPCIÓN: " . "consta constancia" . "<br>";
	      		}
	      	}
	      	else {
	      		// NO CONTIENE LA EXCEPCIÓN. SE COPIA
	       		$paginas[] = $temporal[$fw];     		
	      	}
	  	}

      	$fw = $fw + 1;
      }

      if ($depuracion) {
	      echo "<br><br>";
	  }

	  if ($depuracion) {
	  	echo "SE OMITEN ALGUNOS REGISTROS<br><br>";
	  }

	  if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

	  // SE CALCULA LA PÁGINA INICIAL Y LA PÁGINA FINAL

	  $pagina_inicial = $paginas[0]['pagina'];
	  $pagina_final = $paginas[count($paginas) - 1]['pagina'];

	  echo "<br>PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  echo "<br>PÁGINA FINAL = " . $pagina_final . "<br><br>";

      // SE CAMBIAN LOS CRITERIOS. N O. NO SE COMPACTAN. SE GRABAN LOS REGISTROS INDIVIDUALMENTE

	  $f = 0;
	  while ($f < count($paginas)) {
	  	$laPagina = $paginas[$f]['pagina'];
	  	try {
	  		if ($tipo == 1) {
			  	$sql2 = "UPDATE tblpaginas SET criterio = " . $criterio . ", subcriterio = 0 WHERE id = " . $paginas[$f]['id'];
			}
			else {
			  	$sql2 = "UPDATE tblpaginas SET subcriterio = " . $criterio . ", criterio = 0 WHERE id = " . $paginas[$f]['id'];				
			}
		  	$query2 = $pdo->prepare($sql2);
		  	$query2->execute();
		}
		catch (PDOException $ex) {
			print_r($ex);
		}
	  	$f = $f + 1;
	  }  

	  if ($depuracion) {
	      try {
	      	if ($tipo == 1) {
			    $sql2 = "SELECT * FROM tblpaginas WHERE criterio = " . $criterio;
			}
			else {
			    $sql2 = "SELECT * FROM tblpaginas WHERE subcriterio = " . $criterio;				
			}
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
		  }
	  	  catch (PDOException $ex) {
	    	print_r($ex);
	  	  } 

	  	  if ($depuracion) {
		  	  echo "<br><br>EFECTO DEL MOTOR SOBRE LA TABLA DE PÁGINAS<br><br>";
		  }

		  $f = 0;
		  while ($f < count($resPaginas)) {
		  	  if ($depuracion) {
				 echo "pag = " . $resPaginas[$f]['pagina'] . "<br>";
		  	  }
			  $f = $f + 1;
		  }
		}

      // --------------------------------------------------------------------------------      

    }
}

/* 

echo "<br><br>MOTOR COMPUESTO: HABEAS CORPUS<br><br>";

motor_compuesto(615, 0, 1); // Número criterio, Tipo del criterio (1=criterio, 0=subcriterio), Depuración (1=Muestra comentarios, 0=Omite los comentarios)

echo "<br><br>MOTOR SIMPLE: CONSTANCIA SECRETARIAL<br><br>";

motor_simple(614, 1, 1); // Número criterio, Tipo del criterio (1=criterio, 0=subcriterio), Depuración (1=Muestra comentarios, 0=Omite los comentarios)

*/

echo "<br><br>MOTOR COMPUESTO: ESCRITO ACUSACIÓN<br><br>";

motor_compuesto(583, 1, 1); // Número criterio, Tipo del criterio (1=criterio, 0=subcriterio), Depuración (1=Muestra comentarios, 0=Omite los comentarios)

?>