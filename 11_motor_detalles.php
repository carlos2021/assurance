<?php include "funciones.php"; ?>

<?php
	$depuracion = 1;
?>

<?php

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   MOTOR ASOCIADO A CRITERIOS ESPECÍFICOS  ///////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    // SE ELIGE UNA TRD Y SE RECORREN TODAS LAS PÁGINAS BUSCANDO PATRONES ESPECÍFICOS
    // SE UTILIZAN CRITERIOS CONCRETOS, ASÍ COMO EL INICIO Y EL FINAL DE LA TRD
    ////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////
    //                              HABEASCORPUS --- 615              
    ////////////////////////////////////////////////////////////////////////////////////

    $criterio = 615; // HÁBEAS CORPUS

    // SE CUENTA EL NÚMERO DE PÁGINAS. ESTE VALOR SERÁ ÚTIL MÁS ADELANTE
    // PARA CALCULAR PÁGINAS EXTRAS EN CASO DE SER REQUERIDO

    try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	    $totalPaginas = $query2->rowCount();
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}    

    // SE EXTRAE LA TRD DEL CRITERIO 615 (HÁBEAS CORPUS)

    try {
	    $sql2 = "SELECT * FROM tbltrd WHERE id = " . $criterio;
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
	}
  	catch (PDOException $ex) {
    	print_r($ex);
  	}

    $dummy = trim(depurar_cadena($resTrd[0]['patron'])); 

    while (stripos($dummy, "  ") > 0) {
      $dummy = str_replace("  ", " ", $dummy);
    }

    // SI LA METARREGLA NO ESTÁ VACÍA
    if (!empty($dummy)) {

      $paquete = explode(";", $dummy);

      $paginas = Array();

      $pq = 0;
      while ($pq < count($paquete)) {

        // SE DIVIDE LA METARREGLA EN SUS PALABRAS CONSTITUTIVAS
        $palabras = explode(" ", $paquete[$pq]);

        // SE BUSCA LA METARREGLA EN EL CONJUNTO DE PÁGINAS DEL EXPEDIENTE
        $sql  = "SELECT * FROM `tblpaginas` WHERE MATCH (contenido) AGAINST ('";
        $t = 0;
        while ($t < count($palabras)) {
          $sql .= '+' . $palabras[$t] . ' '; 
          $t = $t + 1;
        }

        $sql .= "' IN BOOLEAN MODE) ORDER BY pagina ASC";

        try {
            $query = $pdo->prepare($sql);
            $query->execute();
            $losresultados = $query->fetchAll(PDO::FETCH_ASSOC);
        }
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	}

	  	// SE ALMACENA EL CONJUNTO DE PÁGINAS ENCONTRADAS EN UN ARRAY PARALELO

	  	$fw = 0;
	  	while ($fw < count($losresultados)) {
	  		$paginas[] = $losresultados[$fw];
	  		$fw = $fw + 1;
	  	}

        $pq = $pq + 1;
      }

      // PAQUETES TERMINADOS. SE IMPRIME EL ARRAY DE páginas

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ORDENA EL ARRAY

      $indice = Array();
      foreach ($paginas as $clave => $fila) {
      	$indice[$clave] = $fila['pagina'];
      }

      array_multisort($indice, SORT_ASC, $paginas);

      if ($depuracion) {
      	echo "<br><br>";
      }

      if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // SE ELIMINAN LOS DUPLICADOS. SE TRANSFIERE A UNA TABLA temporal

      $temporal = Array();

      $fw = 0;
      while ($fw < count($paginas)) {
      	if ($fw == 0) {
      		$temporal[] = $paginas[$fw];
      	}
      	else {
      		if ($paginas[$fw]['pagina'] != $paginas[$fw - 1]['pagina']) {
      			$temporal[] = $paginas[$fw];
      		}
      	}
      	$fw = $fw + 1;
      }

      // SE RECUPERAN LAS PÁGINAS A PARTIR DEL TEMPORAL. ORDENADO Y SIN REPETIDOS

      $paginas = Array();

      $fw = 0;
      while ($fw < count($temporal)) {
      	$paginas[] = $temporal[$fw];
      	$fw = $fw + 1;
      }

      if ($depuracion) {
	      echo "<br><br>";
	  }

	  if ($depuracion) {
	      $fw = 0;
	      while ($fw < count($paginas)) {
	      	echo "FW = " . $fw . " PÁGINA = " . $paginas[$fw]['pagina'] . " CRITERIO = " . $paginas[$fw]['criterio'] . "<br>";
	      	$fw = $fw + 1;
	      }
	  }

      // --------------------------------------------------------------------------------

      // SE BUSCA EL DESENCADENANTE. SE REVISAN TODAS LAS PÁGINAS. INICIAL

      $inicio = Array();

      $f = 0;
      while ($f < count($paginas)) {

      	$texto = $paginas[$f]['contenido'];
      	$texto = "=" . $texto;


      	// ==============================================================

 		$pos1 = stripos($texto, 'juez');
 		$pos2 = stripos($texto, 'habeas');
 		$pos3 = stripos($texto, 'hábeas');
 		$pos4 = stripos($texto, 'corpus');
 		$pos5 = stripos($texto, 'referencia');

      	// ==============================================================


 		$pond = ($pos1 > 0) * 5 + ($pos5 > 0) * 3 + ($pos2 > 0 || $pos3 > 0) + ($pos4 > 0) * 1;

 		$arr = Array();
 		$arr[] = $f;
 		$arr[] = $pond;

 		$inicio[] = $arr;

 		$f = $f + 1; 

      }

      // SE EVALÚAN LAS PONDERACIONES

      if ($depuracion) {
	      echo "<br>INICIAL<br>";
	  }

	  if ($depuracion) {
	      $f = 0;
	      while ($f < count($inicio)) {
	      	echo "PÁGINA = " . $inicio[$f][0] . " PONDERACIÓN = " . $inicio[$f][1] . "<br>";
	      	$f = $f + 1;
	      }
	  }

      // SE BUSCA LA PÁGINA INICIAL

      $f = 0;
      $mayor = 0;
      while ($f < count($inicio)) {
      	if ($inicio[$mayor][1] < $inicio[$f][1]) {
      		$mayor = $f;
      	}
      	$f = $f + 1;
      }

      $pagina_inicial = $paginas[$inicio[$mayor][0]]['pagina'];

      if ($depuracion) {
	      echo "<br>LA PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  }

      // SE BUSCA EL CIERRE. SE REVISAN LAS PÁGINAS DESPUÉS DEL DESENCADENANTE. FINAL

      $final = Array();

      $f = 0;
      while ($f < count($paginas)) {

      	$texto = $paginas[$f]['contenido'];
      	$texto = "=" . $texto;


      	// ==============================================================

 		$pos1 = stripos($texto, 'resuelve');
 		$pos2 = stripos($texto, 'ratificar');
 		$pos3 = stripos($texto, 'remitir');
 		$pos4 = stripos($texto, 'cópiese');
 		$pos5 = stripos($texto, 'copiese');
 		$pos6 = stripos($texto, 'juez');

      	// ==============================================================

 		$pond = ($pos1 > 0) * 8 + ($pos2 > 0) * 3 + ($pos3 > 0) * 3 + ($pos4 > 0 || $pos5 > 0) * 3 + ($pos6 > 0) * 6;

 		$arr = Array();
 		$arr[] = $f;
 		$arr[] = $pond;

 		$final[] = $arr;

 		$f = $f + 1; 

      }

      // SE AGREGA UNA PÁGINA ADICIONAL AL ANÁLISIS (página después de la última)

      $siguiente = $paginas[count($paginas) - 1]['pagina'] + 1;

      if ($siguiente <= $totalPaginas) {

      	if ($depuracion) {
	      	echo "<br>LA SIGUIENTE PÁGINA = " . $siguiente . "<br><br>";
	    }

	    try {
		    $sql2 = "SELECT * FROM tblpaginas WHERE pagina = " . $siguiente;
		    $query2 = $pdo->prepare($sql2);
		    $query2->execute();
		    $ultimaPagina = $query2->fetchAll(PDO::FETCH_ASSOC);
		}
	  	catch (PDOException $ex) {
	    	print_r($ex);
	  	} 

      	$texto = $ultimaPagina[0]['contenido'];
      	$texto = "=" . $texto;


      	// ==============================================================

 		$pos1 = stripos($texto, 'resuelve');
 		$pos2 = stripos($texto, 'notificar');
 		$pos3 = stripos($texto, 'remitir');
 		$pos4 = stripos($texto, 'cópiese');
 		$pos5 = stripos($texto, 'copiese');
 		$pos6 = stripos($texto, 'juez');

 		// ==============================================================



 		$pond = ($pos1 > 0) * 8 + ($pos2 > 0) * 4 + ($pos3 > 0) * 4 + ($pos4 > 0 || $pos5 > 0) * 3 + ($pos6 > 0) * 8;

 		$arr = Array();
 		$arr[] = count($final);
 		$arr[] = $pond;

 		$final[] = $arr;

 		$paginas[] = $ultimaPagina[0];

 		if ($depuracion) {
	 		echo "<br>SE AGREGÓ UNA PÁGINA AL FINAL DE PÁGINAS<br>";
	 	}

 		$f = 0;
 		while ($f < count($paginas)) {
 			if ($depuracion) {
	 			echo "PAGINA = " . $paginas[$f]['pagina'] . "<br>";
	 		}
 			$f = $f + 1;
 		}	  	 

      }

      // SE EVALÚAN LAS PONDERACIONES

      if ($depuracion) {
	      echo "<br>FINAL - VALIDAR !!!<br>";
	  }

      if ($depuracion) {
	      $f = 0;
	      while ($f < count($final)) {
	      	echo "PÁGINA = " . $final[$f][0] . " PONDERACIÓN = " . $final[$f][1] . "<br>";
	      	$f = $f + 1;
	      }
	  }

      // SE BUSCA LA PÁGINA FINAL

      $f = 0;
      $mayor = 0;
      while ($f < count($final)) {
      	if ($final[$mayor][1] < $final[$f][1]) {
      		$mayor = $f;
      	}
      	$f = $f + 1;
      }

      if ($depuracion) {
	      echo "<br>TAMAÑO DE LA TABLA FINAL = " . count($final) . "<br>";
	      echo "EL MAYOR DEL FINAL ES: !!! " . $mayor . "<br><br>===>><br><br>";
	  }

      $pagina_final = $paginas[$final[$mayor][0]]['pagina'];

      if ($depuracion) {
	      echo "<br>LA PÁGINA FINAL = " . $pagina_final . "<br>";
	  }

 	  // SÍNTESIS DE CIERRE Y APERTURA

	  if ($depuracion) {
	 	  echo "<br>===========> APERTURA Y CIERRE<br><br>";
	 	  echo "APERTURA = " . $pagina_inicial . "<br>";
	 	  echo "CIERRE = " . $pagina_final . "<br><br>";
	 	}

 	  // ESTRUCTURA COMPLETA DE LA TABLA páginas
 	  // echo "<br>ESTRUCTURA COMPLETA DE LA TABLA páginas<br>";

 	  // -----------------------

 	  // print_r($paginas);

 	  // -----------------------

      // SE EVALÚAN LÁS PÁGINAS ANTES DEL CIERRE

 	  // Primero, se busca la página inicial dentro de la tabla "paginas"

 	  $pag_ini = 0;

 	  $f = 0;
 	  while ($f < count($paginas)) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial) {
 	  		$pag_ini = $f;
 	  		break;
 	  	}
 	  	$f = $f + 1;
 	  }

 	  $f = $pag_ini - 1;
 	  while ($f >= 0) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial - 1) {
			$pagina_inicial = $pagina_inicial - 1;
			$f = $f - 1; 	  		
 	  	}
 	  	else {
 	  		break;
 	  	}
 	  }

 	  // CORROBORACIÓN O NUEVA PÁGINA INICIAL

 	  if ($depuracion) {
	 	  echo "NUEVA PÁGINA INICIAL = " . $pagina_inicial . "<br>";
	  }

 	  // SE BUSCAN LOS ÍNDICES DENTRO DE LA TABLA paginas CORRESPONDIENTES A
 	  // LAS VARIABLES: pagina_inicial, pagina_final. CON ESTOS ÍNDICES SE PROCEDE
 	  // A MODIFICAR LOS CRITERIOS O SUBCRITERIOS DE LA TABLA PÁGINA EN BASE DE DATOS
 	  // A PARTIR DEL RECORRIDO EN LA TABLA INTERNA: paginas

 	  // INICIALMENTE, LA PRIMERA PÁGINA ESTA DEFINIDA EN paginas EN LA PRIMERA POSICIÓN (0)
 	  // LA ÚLTIMA PÁGINA EN EL ÚLTIMO REGISTRO DE paginas

 	  $pag_ini = 0;
 	  $pag_fin = count($paginas) - 1;

 	  $f = 0;
 	  while ($f < count($paginas)) {
 	  	if ($paginas[$f]['pagina'] == $pagina_inicial) {
 	  		$pag_ini = $f;
 	  	}
 	  	else if ($paginas[$f]['pagina'] == $pagina_final) {
 	  		$pag_fin = $f;
 	  		break;
 	  	}
 	  	$f = $f + 1;
 	  }

 	  if ($depuracion) {
	 	  echo "<br>LÍMITES DE APERTURA Y CIERRE PARA LA GRABACIÓN DE CRITERIO Y SUBCRITERIO<br><br>";
	 	  echo "PAG_INI = " . $pag_ini . "<br>";
	 	  echo "PAG_FIN = " . $pag_fin . "<br>";
	  }

      // SE CAMBIAN LOS CRITERIOS Y SE COMPACTAN LOS ARCHIVOS

      // NOTA !!! Inicialmente se establecio criterio = 615. Pero en realidad, este no es
      // un criterio. Es un subcriterio. Se mantuvo el nombre de 'criterio' para evitar
      // modificaciones de código fuente

	  try {
 	  	$sql2 = "UPDATE tblpaginas SET criterio = 0, subcriterio = " . $criterio . " WHERE pagina >= " . $pagina_inicial . " AND pagina <= " . $pagina_final;
 	  	$query2 = $pdo->prepare($sql2);
 	  	$query2->execute();
 	  }
  	  catch (PDOException $ex) {
    	print_r($ex);
  	  }

      try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
	  }
  	  catch (PDOException $ex) {
    	print_r($ex);
  	  } 

  	  if ($depuracion) {
	  	  echo "<br><br>EFECTO DEL MOTOR SOBRE LA TABLA DE PÁGINAS<br><br>";
	  }

	  if ($depuracion) {
	  	  $f = 0;
	  	  while ($f < count($resPaginas)) {
	  	  	if ($resPaginas[$f]['pagina'] < $pagina_inicial || $resPaginas[$f]['pagina'] > $pagina_final) {
		  	  	echo "pag = " . $resPaginas[$f]['pagina'] . "<br>";
		  	}
	  	  	if ($resPaginas[$f]['pagina'] >= $pagina_inicial && $resPaginas[$f]['pagina'] <= $pagina_final) {
	  	  		echo "PÁGINA = " . $resPaginas[$f]['pagina'] . " CRITERIO = " . $resPaginas[$f]['criterio'] . " SUBCRITERIO = " . $resPaginas[$f]['subcriterio'] . "<br>";
	  	  	}
	  	  	$f = $f + 1;
	  	  }
	  	}

      // --------------------------------------------------------------------------------      

    }

?>