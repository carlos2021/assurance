<?php include "funciones.php"; ?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>HTML</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
	<style>
	html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
	</style>
</head>

<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  <span style="font-size:16px; font-weight:bold;">Menú</span></button>
  <span class="w3-bar-item w3-right"><span style="font-size:16px; font-weight:bold;">ASSURANCE</span></span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="w3images/avatar2.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span><span style="font-size:18px;">Bienvenido, <strong>Gestión Expedientes</strong></span></span><br>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>
      <a href="#" class="w3-bar-item w3-button"><i class="fa fa-cog"></i></a>
    </div>
  </div>
  <hr>
  <div class="w3-container w3-black">
    <h5><span style="font-size:18px; font-weight:bold;">Tablero Digital</span></h5>
  </div>

  <div class="w3-bar-block">
    <a href="index.php" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-users fa-fw"></i>  <span style="font-size:18px;">Procesar Expedientes</span></a>
  </div>

  <div class="w3-bar-block">
    <a href="index_configuracion.php" class="w3-bar-item w3-button w3-padding w3-white"><i class="fa fa-users fa-fw"></i>  <span style="font-size:18px;">Configuración</span></a>
  </div>

</nav>

<script>
  function activar() {
    document.getElementById("abogado_01").click();
    document.getElementById("abogado_02").click();
    document.getElementById("abogado_03").click();
    document.getElementById("abogado_04").click();
    document.getElementById("abogado_05").click();
  }
</script>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

<div class="w3-container w3-center">
  <h1>Descarga de Expedientes</h1>
  <p><i class="fa fa-spinner w3-spin" style="font-size:64px"></i></p>
</div>

<?php 

  $sql = "SELECT * FROM tblconfiguracion";
  $query = $pdo->prepare($sql);
  $query->execute();
  $resConfiguracion = $query->fetchAll(PDO::FETCH_ASSOC);

  $repositorio = $resConfiguracion[0]['repositorio'];
  $descargas = $resConfiguracion[0]['descargas'];
  $nro_abogados = $resConfiguracion[0]['nro_abogados'];
  $estado_sistema = $resConfiguracion[0]['estado_sistema'];

  $sql = "SELECT * FROM tblestado WHERE id = " . $estado_sistema;
  $query = $pdo->prepare($sql);
  $query->execute();
  $resEstado = $query->fetchAll(PDO::FETCH_ASSOC);

  $nombre_estado = $resEstado[0]['nombre'];
  $nroExpedientes = nro_expedientes($repositorio);

	// Se eliminan los expedientes actuales
	$raiz = "c:\\xampp\\htdocs\\assurance\\";
	$ruta = "c:\\xampp\\htdocs\\repositorio\\";
	
	$abogado_01 = $raiz . "app_exp_01\\expedientes\\PEX01\\";
	$abogado_02 = $raiz . "app_exp_02\\expedientes\\PEX01\\";
	$abogado_03 = $raiz . "app_exp_03\\expedientes\\PEX01\\";
	$abogado_04 = $raiz . "app_exp_04\\expedientes\\PEX01\\";
	$abogado_05 = $raiz . "app_exp_05\\expedientes\\PEX01\\";
	$abogado_06 = $raiz . "app_exp_06\\expedientes\\PEX01\\";
	$abogado_07 = $raiz . "app_exp_07\\expedientes\\PEX01\\";
	$abogado_08 = $raiz . "app_exp_08\\expedientes\\PEX01\\";
	$abogado_09 = $raiz . "app_exp_09\\expedientes\\PEX01\\";
	$abogado_10 = $raiz . "app_exp_10\\expedientes\\PEX01\\";
	$abogado_11 = $raiz . "app_exp_11\\expedientes\\PEX01\\";
	$abogado_12 = $raiz . "app_exp_12\\expedientes\\PEX01\\";
	// Se recorre la carpeta: repositorio

?>

<?php

	function agregar_zip($dir, $zip) {
	  //verificamos si $dir es un directorio
	  if (is_dir($dir)) {
	    //abrimos el directorio y lo asignamos a $da
	    if ($da = opendir($dir)) {
	      //leemos del directorio hasta que termine
	      while (($archivo = readdir($da)) !== false) {
	        /*Si es un directorio imprimimos la ruta
	         * y llamamos recursivamente esta función
	         * para que verifique dentro del nuevo directorio
	         * por mas directorios o archivos
	         */
	        if (is_dir($dir . $archivo) && $archivo != "." && $archivo != "..") {
	          //echo "<strong>Creando directorio: $dir$archivo</strong><br/>";
	          agregar_zip($dir . $archivo . "/", $zip);

	          /*si encuentra un archivo imprimimos la ruta donde se encuentra
	           * y agregamos el archivo al zip junto con su ruta 
	           */
	        } elseif (is_file($dir . $archivo) && $archivo != "." && $archivo != "..") {
	          // echo "Agregando archivo: $dir$archivo <br/>";
	          $zip->addFile($dir . $archivo, $dir . $archivo);
	        }
	      }
	      //cerramos el directorio abierto en el momento
	      closedir($da);
	    }
	  }
	}

	//fin de la función

	$sql = "SELECT * FROM tblconfiguracion";
	$query = $pdo->prepare($sql);
	$query->execute();
	$res = $query->fetchAll(PDO::FETCH_ASSOC);
	$nro_abogados = $res[0]['nro_abogados'];


	/////////////////////////////////// PRIMER ABOGADO /////////////////////////////////////

	if ($nro_abogados >= 1) {
		//creamos una instancia de ZipArchive
		$zip = new ZipArchive();

		/*directorio a comprimir
		 * la barra inclinada al final es importante
		 * la ruta debe ser relativa no absoluta
		 */
		$dir = '..\\app_exp_01\\resultados\\';

		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = "descargas";

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal);
		}

		$sql = "SELECT * FROM tblabogados WHERE id = 1";
		$query = $pdo->prepare($sql);
		$query->execute();
		$res = $query->fetchAll(PDO::FETCH_ASSOC);

		$archivoZip = $res[0]['expediente'] . ".zip";

		if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
		  agregar_zip($dir, $zip);
		  $zip->close();

		  //Muevo el archivo a una ruta
		  //donde no se mezcle los zip con los demas archivos
		  rename($archivoZip, "$rutaFinal/$archivoZip");

		  //Hasta aqui el archivo zip ya esta creado
		  //Verifico si el archivo ha sido creado
		  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

					<div class="w3-container w3-center">
					  <h2>Expedientes Individuales!!</h2>
					  <h3>Formato ZIP</h3>
					  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
					  <h3>1. Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
					</div>
		  <?php
		  } else {
		    echo "Error, archivo zip no ha sido creado!!";
		  }
		}
	}

	//////////////////////// SEGUNDO ABOGADO /////////////////////////////

	if ($nro_abogados >= 2) {
		$zip = new ZipArchive();

		/*directorio a comprimir
		 * la barra inclinada al final es importante
		 * la ruta debe ser relativa no absoluta
		 */
		$dir = '..\\app_exp_02\\resultados\\';

		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = "descargas";

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal);
		}

		$sql = "SELECT * FROM tblabogados WHERE id = 2";
		$query = $pdo->prepare($sql);
		$query->execute();
		$res = $query->fetchAll(PDO::FETCH_ASSOC);

		$archivoZip = $res[0]['expediente'] . ".zip";

		if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
		  agregar_zip($dir, $zip);
		  $zip->close();

		  //Muevo el archivo a una ruta
		  //donde no se mezcle los zip con los demas archivos
		  rename($archivoZip, "$rutaFinal/$archivoZip");

		  //Hasta aqui el archivo zip ya esta creado
		  //Verifico si el archivo ha sido creado
		  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

					<div class="w3-container w3-center">
					  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
					  <h3>2. Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
					</div>
		  <?php
		  } else {
		    echo "Error, archivo zip no ha sido creado!!";
		  }
		}
	}

	//////////////////////// TERCER ABOGADO /////////////////////////////

	if ($nro_abogados >= 3) {
		$zip = new ZipArchive();

		/*directorio a comprimir
		 * la barra inclinada al final es importante
		 * la ruta debe ser relativa no absoluta
		 */
		$dir = '..\\app_exp_03\\resultados\\';

		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = "descargas";

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal);
		}

		$sql = "SELECT * FROM tblabogados WHERE id = 3";
		$query = $pdo->prepare($sql);
		$query->execute();
		$res = $query->fetchAll(PDO::FETCH_ASSOC);

		$archivoZip = $res[0]['expediente'] . ".zip";

		if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
		  agregar_zip($dir, $zip);
		  $zip->close();

		  //Muevo el archivo a una ruta
		  //donde no se mezcle los zip con los demas archivos
		  rename($archivoZip, "$rutaFinal/$archivoZip");

		  //Hasta aqui el archivo zip ya esta creado
		  //Verifico si el archivo ha sido creado
		  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

					<div class="w3-container w3-center">
					  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
					  <h3>3. Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
					</div>
		  <?php
		  } else {
		    echo "Error, archivo zip no ha sido creado!!";
		  }
		}
	}

	//////////////////////// CUARTO ABOGADO /////////////////////////////

	if ($nro_abogados >= 4) {
		$zip = new ZipArchive();

		/*directorio a comprimir
		 * la barra inclinada al final es importante
		 * la ruta debe ser relativa no absoluta
		 */
		$dir = '..\\app_exp_04\\resultados\\';

		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = "descargas";

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal);
		}

		$sql = "SELECT * FROM tblabogados WHERE id = 4";
		$query = $pdo->prepare($sql);
		$query->execute();
		$res = $query->fetchAll(PDO::FETCH_ASSOC);

		$archivoZip = $res[0]['expediente'] . ".zip";

		if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
		  agregar_zip($dir, $zip);
		  $zip->close();

		  //Muevo el archivo a una ruta
		  //donde no se mezcle los zip con los demas archivos
		  rename($archivoZip, "$rutaFinal/$archivoZip");

		  //Hasta aqui el archivo zip ya esta creado
		  //Verifico si el archivo ha sido creado
		  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

					<div class="w3-container w3-center">
					  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
					  <h3>4. Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
					</div>
		  <?php
		  } else {
		    echo "Error, archivo zip no ha sido creado!!";
		  }
		}
	}

	//////////////////////// CUARTO ABOGADO /////////////////////////////

	if ($nro_abogados >= 5) {
		$zip = new ZipArchive();

		/*directorio a comprimir
		 * la barra inclinada al final es importante
		 * la ruta debe ser relativa no absoluta
		 */
		$dir = '..\\app_exp_05\\resultados\\';

		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = "descargas";

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal);
		}

		$sql = "SELECT * FROM tblabogados WHERE id = 5";
		$query = $pdo->prepare($sql);
		$query->execute();
		$res = $query->fetchAll(PDO::FETCH_ASSOC);

		$archivoZip = $res[0]['expediente'] . ".zip";

		if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
		  agregar_zip($dir, $zip);
		  $zip->close();

		  //Muevo el archivo a una ruta
		  //donde no se mezcle los zip con los demas archivos
		  rename($archivoZip, "$rutaFinal/$archivoZip");

		  //Hasta aqui el archivo zip ya esta creado
		  //Verifico si el archivo ha sido creado
		  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

					<div class="w3-container w3-center">
					  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
					  <h3>5. Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
					</div>
		  <?php
		  } else {
		    echo "Error, archivo zip no ha sido creado!!";
		  }
		}
	}

	////////////////////////////// COMPLETO ///////////////////////////////

	$zip = new ZipArchive();

	/*directorio a comprimir
	 * la barra inclinada al final es importante
	 * la ruta debe ser relativa no absoluta
	 */
	$dir = 'descargas\\';

	//ruta donde guardar los archivos zip, ya debe existir
	$rutaFinal = "total";

	if(!file_exists($rutaFinal)){
	  mkdir($rutaFinal);
	}

	$archivoZip = "Expedientes.zip";

	if ($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
	  agregar_zip($dir, $zip);
	  $zip->close();

	  //Muevo el archivo a una ruta
	  //donde no se mezcle los zip con los demas archivos
	  rename($archivoZip, "$rutaFinal/$archivoZip");

	  //Hasta aqui el archivo zip ya esta creado
	  //Verifico si el archivo ha sido creado
	  if (file_exists($rutaFinal. "/" . $archivoZip)) { ?>

				<div class="w3-container w3-center">
					<h2>_</h2>
				  <h2>Expedientes Agrupados!!</h2>
				  <h3>Formato ZIP</h3>
				  <?php $arch = $rutaFinal . "/" . $archivoZip; ?>
				  <h3>Descargar: <a href="<?php echo $arch; ?>"><?php echo $archivoZip; ?></a></h3>
				</div>
	  <?php
	  } else {
	    echo "Error, archivo zip no ha sido creado!!";
	  }
	}


?>

  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>

</body>
</html>
