<?php include "funciones.php"; ?>

<?php 
	$depuracion = 0;

	$patron_inicio = $_POST['patron_inicial'];
	$patron_final = $_POST['patron_final'];
	$ubicacion_inicio = $_POST['ubicacion_inicial'];
	$ubicacion_final = $_POST['ubicacion_final'];
	$codigo= $_POST['criterio'];
	$tipo = $_POST['tipo'];
	$nombre_criterio = $_POST['nombre_criterio'];

	if ($depuracion) {
		echo "DATOS QUE LLEGAN A CREACIÓN DE TRD EXTRA<br><br>";
		echo "PATRÓN INICIAL = " . $patron_inicio . "<br>";;
		echo "UBICACIÓN INICIAL = " . $ubicacion_inicio . "<br>";;
		echo "PATRÓN FINAL = " . $patron_final . "<br>";;
		echo "UBICACIÓN FINAL = " . $ubicacion_final . "<br>";;
		echo "CÓDIGO = " . $codigo . "<br>";
		echo "TIPO = " . $tipo . "<br><br>";
	}

	// ESTOS DATOS EN LA SIGUIENTE VERSIÓN DEBEN VENIR DESDE LA INTERFAZ
	$serie = 270;
	$subserie = 245;

	// VALORES POR DEFECTO
	$activo = 0;
	$nivel = 50;

	// IMPORTANTE !!! ACTIVO SE COLOCA CON VALOR IGUAL A CERO. ESTO GARANTIZA QUE AL CORRER
	// EL PROGRAMA, EL MÓDULO DE generar_detalles.php NO UTILIZA ESTOS REGISTROS. NO DEBEN SER
	// UTILIZADOS PREVIAMENTE, PUES ESTO GENERA EFECTOS COLATERALES

	// SI TIPO = 2 SE DEBE CREAR UN TRD PARA EL SUB-CRITERIO INDICADO EN EL CAMPO criterio
	// DE ESTE MODO SE CREA UN REGISTRO: serie = 0 y subserie = 10

	if ($tipo == 2) {

		$serie_2 = 0;
		$subserie_2 = 10;

		if ($depuracion) {
			echo "Ingresando por el tipo = 2. Se debe crear una TRD subcriterio (observaciones)<br><br>";
		}

		// TIPO = 2 significa que en realidad el tipo es 1 (subcriterio). EL VALOR 2 
		// SIGNIFICA QUE ESTE TIPO subcriterio NO EXISTE, Y DEBE CREARSE. AQUÍ ES IMPORTANTE
		// ACLARAR QUE EL id GENERADO ES AHORA EL NUEVO CRITERIO, Y ESTE DEBE SER EL VALOR
		// QUE MÁS ADELANTE SE GRABE EN LA TRD EXTRA

		// EL VALOR DE TIPO SE CONVIERTE EN 1 PARA QUE DENTRO DE LA 'TRD EXTRA' SE REFLEJE
		// CORRECTAMENTE DICHO VALOR DE subcriterio
		$tipo = 1;

		try {
			$sql = "INSERT INTO tbltrd (serie, subserie, patron, observaciones, activo, nivel) VALUES (:serie, :subserie, :patron, :observaciones, :activo, :nivel)";
			$query = $pdo->prepare($sql);
			$query->bindParam(':serie', $serie_2, PDO::PARAM_INT);
			$query->bindParam(':subserie', $subserie_2, PDO::PARAM_INT);
			$query->bindParam(':patron', $patron_inicio, PDO::PARAM_STR);
			$query->bindParam(':observaciones', $nombre_criterio, PDO::PARAM_STR);
			$query->bindParam(':activo', $activo, PDO::PARAM_INT);
			$query->bindParam(':nivel', $nivel, PDO::PARAM_INT);
			$query->execute();

			$codigo = $pdo->lastinsertid();
		}
		catch (PDOException $ex) {
			print_r($ex);
		}

	}

	// SE CREA EL TRD EXTRA

	if ($depuracion) {
		echo "A PUNTO DE GRABAR EL TRD EXTRA<br><br>";
	}

	try {
		$sql = "INSERT INTO tbltrdextra (serie, subserie, codigo, tipo, patron_inicio, ubicacion_inicio, patron_final, ubicacion_final) VALUES (:serie, :subserie, :codigo, :tipo, :patron_inicio, :ubicacion_inicio, :patron_final, :ubicacion_final)";
		$query = $pdo->prepare($sql);
		$query->bindParam(':serie', $serie, PDO::PARAM_INT);
		$query->bindParam(':subserie', $subserie, PDO::PARAM_INT);
		$query->bindParam(':codigo', $codigo, PDO::PARAM_INT);
		$query->bindParam(':tipo', $tipo, PDO::PARAM_INT);
		$query->bindParam(':patron_inicio', $patron_inicio, PDO::PARAM_STR);
		$query->bindParam(':ubicacion_inicio', $ubicacion_inicio, PDO::PARAM_STR);
		$query->bindParam(':patron_final', $patron_final, PDO::PARAM_STR);
		$query->bindParam(':ubicacion_final', $patron_final, PDO::PARAM_STR);
		$query->execute();

		if ($depuracion) {
			echo "SE HA GRABADO EL REGISTRO !!!" . "<br>";
		}
	}
	catch (PDOException $ex) {
		print_r($ex);
	}

	// SI TIPO = 0 ES UN CRITERIO, NO SE REALIZA NINGÚN CAMBIO EN LAS TRD
	// ESTE CÓDIGO SE USA PARA EL DESPLIEGUE Y AJUSTE DEL EXTRA

	// SI TIPO = 1 ES UN SUB-CRITERIO, NO SE REALIZA NINGÚN CAMBIO EN LAS TRD
	// ESTE CÓDIGO SE USA PARA EL DESPLIEGUE Y AJUSTE DEL EXTRA





	header("Location: index.php"); // Configuración
?>