<?php include "admin/header.php"; ?>

<?php

  try {
    $sql = "SELECT * FROM tbltrd ORDER BY serie ASC, subserie ASC";
    $query = $pdo->prepare($sql);
    $query->execute();
    $resTrd = $query->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $ex) {
    print_r($ex);
  }

?>

<script>
  var div = document.getElementById('revisar_tipos_docs');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>Revisar los tipos documentales</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Filtrar los criterios de las TRD</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">

  <div class="w3-row-padding" style="margin:8px -16px;">
    <div class="w3-half w3-margin-bottom">
      <label><i class="fa fa-male"></i> <span style="font-weight: bold;">BUSCAR CRITERIO</span></label>
      <input class="w3-input w3-border" type="text" style="font-size: 18px;" placeholder = "Elegir criterio de búsqueda" id = "miCriterio" name="miCriterio" onkeyup="buscarCriterio()">
    </div>
    <div class="w3-half w3-margin-bottom">
      <label><i class="fa fa-male"></i> <span style="font-weight: bold;">BUSCAR Típo / Observaciones</span></label>
      <input class="w3-input w3-border" type="text" style="font-size: 18px;" placeholder = "Elegir Tipo / Observaciones" id = "miTipo" name="miCriterio" onkeyup="buscarTipo()">
    </div>
  </div>

   <div class="w3-responsive">
   <table class="w3-table-all" id = "miTabla">
    <thead>
      <tr class="w3-light-grey">
        <th>Id</th>
        <th>Serie</th>
        <th>Subserie</th>
        <th>Criterio</th>
        <th>Tipo</th>
      </tr>
    </thead>
      <?php
        $f = 0;
        while ($f < count($resTrd)) {
          ?>
            <tr class="w3-hover-teal">

              <td><?php echo $resTrd[$f]['id']; ?></td>
              <td><?php echo $resTrd[$f]['serie']; ?></td>
              <td><?php echo $resTrd[$f]['subserie']; ?></td>

              <?php

                if ($resTrd[$f]['observaciones'] == '') {
                  $nombre_criterio = $resTrd[$f]['tipo_doc'];
                }
                else {
                  $nombre_criterio = $resTrd[$f]['observaciones'];
                }
              ?>

              <td><?php echo $nombre_criterio; ?></td>

              <?php if ($resTrd[$f]['observaciones'] == '') { ?>
                <td><?php echo "Tipo documental"; ?></td>
              <?php } else { ?>
                <td><?php echo "Observaciones"; ?></td>
              <?php } ?>

            </tr>
          <?php
          $f = $f + 1;
        }
      ?>
  </table>
  </div>
</div>
  </div>

  <!-- End page content -->
</div>

<script>
function buscarCriterio() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("miCriterio");
  filter = input.value.toUpperCase();
  table = document.getElementById("miTabla");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function buscarTipo() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("miTipo");
  filter = input.value.toUpperCase();
  table = document.getElementById("miTabla");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
