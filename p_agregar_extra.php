<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('agregar_extra');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>Agregar TRD Extra</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Definir y Grabar TRD Extra</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">
    <form method="post" action="s_agregar_extra.php">
      <div class="w3-row-padding" style="margin:0 -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-calendar-o"></i> <span style="font-weight: bold;">PATRÓN INICIAL</span></label>
          <textarea id="patron_inicial" name="patron_inicial" rows="4" cols="57" placeholder="Patrón página inicial" required></textarea>
        </div>
        <div class="w3-half">
          <label><i class="fa fa-calendar-o"></i> <span style="font-weight: bold;">PATRÓN FINAL</span></label>
          <textarea id="patron_final" name="patron_final" rows="4" cols="58" placeholder="Patrón página final"></textarea>
        </div>
      </div>
      <div class="w3-row-padding" style="margin:8px -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">UBICACIÓN INICIAL</span></label>
          <input class="w3-input w3-border" type="text" style="font-size: 16px;" placeholder = "Ubicación patron inicial (#,#;#,...)" id = "ubicacion_inicial" name = "ubicacion_inicial" required>
        </div>
        <div class="w3-half">
          <label><i class="fa fa-child"></i> <span style="font-weight: bold;">UBICACIÓN FINAL</span></label>
          <input class="w3-input w3-border" type="text" style="font-size: 16px;" placeholder = "Ubicación patron final (#,#;#,...)" id = "ubicacion_final" name = "ubicacion_final">
        </div>
      </div>
      <div class="w3-row-padding" style="margin:8px -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">CRITERIO TRD</span></label>
          <input class="w3-input w3-border" type="number" style="font-size: 18px;" placeholder = "Código del criterio TRD" id = "criterio" name = "criterio">
        </div>
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">TIPO</span></label>
          <input class="w3-input w3-border" type="number" style="font-size: 18px;" placeholder = "0 = Criterio, 1 = Observaciones, 2 = No Alterar" min = "0" max = "2" id = "tipo" name="tipo" required>
        </div>
      </div>
      <div class="w3-row-padding" style="margin:8px -16px;">
        <div class="w3-half w3-margin-bottom">
          <label><i class="fa fa-male"></i> <span style="font-weight: bold;">NOMBRE CRITERIO NUEVA OBSERVACIÓN</span></label>
          <input class="w3-input w3-border" type="text" style="font-size: 18px;" placeholder = "Nombre del criterio TRD" id = "nombre_criterio" name = "nombre_criterio">
        </div>
      </div>
      <button class="w3-button w3-dark-grey" type="file"><i class="fa fa-search w3-margin-right"></i> <span style="font-weight: bold;">CREAR EXTRA</span></button>
    </form>
  </div>

  <!-- End page content -->
</div>
