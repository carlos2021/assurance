<?php 
	include "funciones.php";

	$depuracion = 0;

	if ($depuracion) {
		echo "EN ESTE script SE CREA UN NUEVO MÓDULO DE TRABAJO (p_ejecutor.php)<br><br>";
	}

	try {
		$sql = "SELECT * FROM tblejecucion";
		$query = $pdo->prepare($sql);
		$query->execute();
		$resEjecucion = $query->fetchAll(PDO::FETCH_ASSOC);
	}
	catch (PDOException $ex) {
		print_r($ex);
	}

	// Se busca si existe al menos un cero. En caso negativo, termina
	if ($depuracion) {
		echo "BUSCANDO EL PRIMER EXPEDIENTE CON estado == 0<br>";
	}
	$terminado = 1;
	$f = 0;
	while ($f < count($resEjecucion)) {
		if ($depuracion) {
			echo "ITERACIÓN = " . $f . "<br>";
			echo "EXPEDIENTE = " . $resEjecucion[$f]['expediente'] . "<br>";
			echo "ESTADO = " . $resEjecucion[$f]['estado'] . "<br>";
		}
		if ($resEjecucion[$f]['estado'] == 0) {
			if ($depuracion) {
				echo "ESTADO == 0 INGRESANDO AL CICLO QUE EJECUTA EL PROGRAMA DE NUEVO<br>";
			}
			$elExpediente = $resEjecucion[$f]['expediente'];
			if ($depuracion) {
				echo "EL EXPEDIENTE A PROCESAR = " . $elExpediente . "<br>";
				echo "A CONTINUACIÓN SE ACTUALIZA Y SE COLOCA 1 AL ESTADO DEL EXPEDIENTE<br>";
				echo "Sale del ciclo, pues ya encontró el expediente a ejecutar<br>";
				echo "y busca la instrucción header() para iniciar de nuevo el ciclo de trabajo<br>";
			}
			$terminado = 0;
			// Se actualiza la tabla tblejecucion
			try {
				$sql11 = "UPDATE tblejecucion SET estado = 1 WHERE expediente = '" . $elExpediente . "'";
				$query11 = $pdo->prepare($sql11);
				$query11->execute();
			}
			catch (PDOException $ex) {
				print_r($ex);
			}
			break;
		}
		$f = $f + 1;		
	}

	// Si no ha terminado, se ha elegido el primer registro con estado = 0
	// Este registro será procesado de modo similar a como se procesó el primer registro

	if ($depuracion) {
		echo "HA SALIDO DEL CICLO. EL VALOR DE terminado = " . $terminado . "<br>";
	}

	if ($terminado) {

		echo "<h2>FINALIZADOS COMPLETAMENTE TODOS LOS EXPEDIENTE</h2>";
		echo "<h2>Termina el proceso de trabajo secuencial</h2><br>";

		exit(0); // El programa finaliza
	}
	else {
		if ($depuracion) {
			echo "A PUNTO DE EJECUTAR UN NUEVO PROCESO: " . "EXPEDIENTE = " . $elExpediente . "<br>";
		}
		header("Location: s_asignar_expediente.php?numero=" . $elExpediente);		
	}

?>