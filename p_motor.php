<?php

include_once "funciones.php";

$probar_motor = 1;

function posicion_relativa_cadena($texto, $cadena) {

	$pos = stripos($texto, $cadena);
	$longitud = strlen($texto);
	$unidad = intval($longitud, 8);
	if ($pos < $unidad) return 1;
	if ($pos < $unidad * 2) return 2;
	if ($pos < $unidad * 3) return 3;
	if ($pos < $unidad * 4) return 4;
	if ($pos < $unidad * 5) return 5;
	if ($pos < $unidad * 6) return 6;
	if ($pos < $unidad * 7) return 7;
	if ($pos < $unidad * 8) return 8;
	return 9;

}

function posicion_relativa_pos($texto, $pos) {

	$longitud = strlen($texto);
	$unidad = intval($longitud, 8);
	if ($pos < $unidad) return 1;
	if ($pos < $unidad * 2) return 2;
	if ($pos < $unidad * 3) return 3;
	if ($pos < $unidad * 4) return 4;
	if ($pos < $unidad * 5) return 5;
	if ($pos < $unidad * 6) return 6;
	if ($pos < $unidad * 7) return 7;
	if ($pos < $unidad * 8) return 8;
	return 9;

}


function busqueda_parrafo($texto, $patron, $regla, $umbral, $fila = 0) {

	if ($patron == null) {
		return 0;
	}

	$pos = Array();

	// Se calculan las posiciones
	$i = 0;
	while ($i < count($patron)) {
		$px = stripos($texto, $patron[$i]);
		if ($px > 0) {
			$pos[$i] = 1;
		}
		else {
			$pos[$i] = 0;			
		}
		$i = $i + 1;
	}

	// Se calcula la ponderación del conjunto
	$pond = 0;
	$b = 0;
	$i = 0;
	while ($i < count($pos)) {

		$producto = $pos[$i] * $regla[$i * 2];
		$digito = $regla[$i * 2 + 1];

		// Análisis de casos
		if ($producto == 0 && $digito == 0 && $b == 0) {
			// No se ejecuta ninguna acción
		}

		if ($producto == 0 && $digito == 0 && $b == 1) {
			// Se corrige la bandera, pues se acaba de terminar un ciclo de unos
			// No se realiza ninguna otra acción
			$b = 0;
		}

		if ($producto == 0 && $digito == 1 && $b == 0) {
			// No se ejecuta ninguna acción
		}

		if ($producto == 0 && $digito == 1 && $b == 1) {
			// No se ejecuta ninguna acción, pues el prducto vale 0
		}

		if ($producto > 0 && $digito == 0 && $b == 0) {
			// Se acumula en el ponderado, pues es una regla de ponderación directa
			// La bandera es cero, por tanto no se realizan más acciones
			$pond = $pond + $producto;
		}

		if ($producto > 0 && $digito == 0 && $b == 1) {
			// Se realizan dos acciones. Acumular el ponderado
			// Cambiar la bandera a cero, pues estamos en presencia de una ponderación directa
			$pond = $pond + $producto;
			$b = 0;
		}

		if ($producto > 0 && $digito == 1 && $b == 0) {
			// No se ejecuta ninguna acción, excepto la acumulación del ponderado
			$pond = $pond + $producto;
			$b = 1;
		}

		if ($producto > 0 && $digito == 1 && $b == 1) {
			// No se ejecuta ninguna acción
		}

		$i = $i + 1;
	}

/* 
	$laFila = $fila + 0;
	echo "PÁGINA = " . $laFila . " PATRÓN ENCONTRADO = ";

	$tw = 0;
	while ($tw < count($pos)) {
		echo $pos[$tw];
		if ($tw < count($pos) - 1) {
			echo " - ";
		}
		$tw = $tw + 1;
	}

	echo " PONDERADO = " . $pond . "<br>"; */

	if ($pond >= $umbral) {
		return 1;
	}
	else {
		return 0;
	}

}

function busqueda_frase($texto, $patron, $regla, $fila) {

	if ($patron == null) {
		return 0;
	}

	$pos = Array();

	// Se calculan las posiciones
	$i = 0;
	while ($i < count($patron)) {
		$px = stripos($texto, $patron[$i]);
		if ($px > 0) {
			$pos[$i] = 1;
		}
		else {
			$pos[$i] = 0;			
		}
		$i = $i + 1;
	}

	$i = 0;
	while ($i < count($pos)) {
		$pos_pagina = posicion_relativa_pos($texto, $pos[$i]);
		echo "FILA = " . $fila . " LONGITUD TEXTO = " . strlen($texto) . " POSICIÓN = " . $pos[$i] . " POSICIÓN DENTRO DE LA PÁGINA = " . $pos_pagina . "<br>";
		$i = $i + 1;
	} 

}

function motor_compuesto($criterio, $tipo = 1, $depuracion = 0) {

	global $pdo;

	$salida = Array();
	
	try {
	    $sql2 = "SELECT * FROM tblpaginas WHERE 1";
	    $query2 = $pdo->prepare($sql2);
	    $query2->execute();
	    $resPaginas = $query2->fetchAll(PDO::FETCH_ASSOC);
	    $totalPaginas = $query2->rowCount();
	}
	catch (PDOException $ex) {
		print_r($ex);
	}

	/////////////////////////////////////////////////////////////////////////////////
	// Se definen las reglas de acción para cada TRD
	/////////////////////////////////////////////////////////////////////////////////

	// ---------------------- HABEAS CORPUS
	if ($criterio == 615) {

	         $inicio_parrafo = ["juez", "habeas", "hábeas", "corpus", "referencia"];
	    $pond_inicio_parrafo = [5, 0, 7, 1, 7, 1, 7, 0, 3, 0]; // análisis por parejas
	    													   // primer dato = ponderación
	    													   // segundo dato = 0 o 1
	    													   //    0 = se pondera el solo
	    													   //    X = solo aporta uno de ellos
	  $inicio_umbral_parrafo = 20;

	           $inicio_frase = [];
	      $pond_inicio_frase = [];


	          $final_parrafo = ["ratificar", "remitir", "cópiese", "copiese", "juez"];
	     $pond_final_parrafo = [3, 0, 3, 0, 3, 1, 3, 1, 6, 0];
	   $final_umbral_parrafo = 10;

	            $final_frase = ["resuelve"];
	       $pond_final_frase = [0]; // 0 = en cualquier parte, 1 = al comienzo,
	            						 // 2 = en la mitad, 3 = al final
	}

	// ---------------------- ESCRITO DE ACUSACIÓN
	if ($criterio == 583) {

	         $inicio_parrafo = [];
	    $pond_inicio_parrafo = [];
	  $inicio_umbral_parrafo = 0;
	   $inicio_umbral_minimo = 0;	    

	           $inicio_frase = ["escrito acusación", "escrito acusacion"];
	      $pond_inicio_frase = [1, 1]; // 0 = en cualquier parte, 1 = al comienzo,
	            				       // 2 = en la mitad, 3 = al final

	          $final_parrafo = ["datos", "fiscal", "firma"];
	     $pond_final_parrafo = [3, 0, 3, 0, 3, 0];
 	   $final_umbral_parrafo = 6;

	            $final_frase = [];
	       $pond_final_frase = [];
	}

	/////////////////////////////////////////////////////////////////////////////////
	// Se aplican las reglas al conjunto de páginas. Se genera un ciclo sobre las
	// páginas. Se busca iterativamente, primero un comienzo y luego un final
	// El proceso es secuencial controlado por una bandera. Una vez terminado, se 
	// tendrá una lista única con comienzo-final. Usando estas parejas, se graban
	// los correspondientes TRD en la tabla de páginas
	/////////////////////////////////////////////////////////////////////////////////

	$lista_mod = Array();

	$buscando = 1; // 1 = inicio, 2 = final

	$f = 0;
	while ($f < count($resPaginas)) {

		// SE BUSCA EL INICIO
		if ($buscando == 1) {

			// SE BUSCA EN PÁRRAFOS
			$encontrado = busqueda_parrafo($resPaginas[$f]['contenido'], $inicio_parrafo, $pond_inicio_parrafo, $inicio_umbral_parrafo, $f);

			if (!$encontrado) {

				// SE BUSCA EN FRASES
				busqueda_frase("===" . $resPaginas[$f]['contenido'], $inicio_frase, $pond_inicio_frase, $f);

			}
			else {

				/* echo "<br>ENCONTRADO EL INICIO EN LA PÁGINA: " . $resPaginas[$f]['pagina'] . "<br><br>"; */
				$buscando = 2;
				$pag_ini = $resPaginas[$f]['pagina'];
			}

		}

		// SE BUSCA EL FINAL
		if ($buscando == 2) {
			$encontrado = busqueda_parrafo($resPaginas[$f]['contenido'], $final_parrafo, $pond_final_parrafo, $final_umbral_parrafo, $f);
			if ($encontrado) {
				/* echo "<br>ENCONTRADO EL CIERRE EN LA PÁGINA: " . $resPaginas[$f]['pagina'] . "<br>";
				echo "CONTINÚA BUSCANDO OTRAS PAREJAS DE APERTURA Y CIERRE<br><br><br>"; */
				$pag_fin = $resPaginas[$f]['pagina'];

				$arr = Array();
				$arr[] = $pag_ini;
				$arr[] = $pag_fin;

				$salida = $arr;

				$buscando = 1;
			}
		}

		$f = $f + 1;
	}

	if ($buscando == 2) {
		echo "ERROR: No encontró el cierre del último bloque" . "<br>";
	}

	return $salida;

}

function motor_simple($criterio, $tipo = 1, $depuracion = 0) {

}

if ($probar_motor) {
	
	/* $rango_1 = motor_compuesto(615, 0, 1);
	print_r($rango_1); */

	$rango_2 = motor_compuesto(583, 0, 1);

}

?>