<?php include "admin/header.php"; ?>

<?php
  $tipo = $_GET['tipo'];
?>

<script>
  var div = document.getElementById('repositorio');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:280px;margin-top:43px;">

  <!-- Header -->
  <div class="w3-panel w3-margin-top" style="margin-top:22px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
 
    <div class="w3-panel">
      <h4><b><i class="fa fa-folder-open-o"></i> &nbsp;Repositorio</b></h4>

      <div class="w3-section">
        <span class="w3-margin-right" style="font-size:18px;">Filtro:</span> 
        <div class="w3-dropdown-hover">
          <button class="w3-button w3-black">Selección</button>
          <div class="w3-dropdown-content w3-bar-block w3-border">
            <a href="p_repositorio.php?tipo=0" class="w3-bar-item w3-button"><span style="font-size:18px;">Todos</span></a>
            <a href="p_repositorio.php?tipo=1" class="w3-bar-item w3-button"><span style="font-size:18px;">PDF</span></a>
            <a href="p_repositorio.php?tipo=2" class="w3-bar-item w3-button"><span style="font-size:18px;">Vídeos</span></a>
            <a href="p_repositorio.php?tipo=3" class="w3-bar-item w3-button"><span style="font-size:18px;">Audios</span></a>
            <a href="p_repositorio.php?tipo=4" class="w3-bar-item w3-button"><span style="font-size:18px;">Comprimidos</span></a>
          </div>
        </div>
      </div>

    </div>

    <div class="w3-panel">
      <div class="w3-row">
        <div class="w3-col w3-container m6 l6">
          <table class="w3-table-all w3-card-4">
            <?php
              listar_directorios_ruta($repositorio, 0);
              mostrar_tabla_expedientes_front($tipo); 
            ?>
          </table>
          <div class="separador-20"></div>
        </div>
      </div>
    </div>
  </div>

  <!-- End page content -->
<