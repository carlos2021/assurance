<?php include "admin/header.php"; ?>

<?php

  try {
    $sql = "SELECT * FROM tbltrdextra";
    $query = $pdo->prepare($sql);
    $query->execute();
    $resExtra = $query->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $ex) {
    print_r($ex);
  }

?>

<script>
  var div = document.getElementById('listar_extra');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->

  <div class="w3-container w3-center" style="margin-top:53px;">
    <h3>Listar TRD Extras</h3>
  </div>

  <!-- Header -->
  <div class="w3-container w3-teal" style="margin-top:11px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">Reporte TRD Extras</span></h4>
  </div>

  <div class="separador-20"></div>

  <div class="w3-container w3-white w3-padding-16 w3-margin">
   <div class="w3-responsive">
   <table class="w3-table-all">
    <thead>
      <tr class="w3-light-grey">
        <th>Id</th>
        <th>Criterio</th>
        <th>Cod</th>
        <th>Tipo</th>
        <th>Patron_Ini</th>
        <th>Pos_Ini</th>
        <th>Patron_fin</th>
        <th>Pos_fin</th>
      </tr>
    </thead>
      <?php
        $f = 0;
        while ($f < count($resExtra)) {
          ?>
            <tr class="w3-hover-green">
              <td><?php echo $resExtra[$f]['id']; ?></td>

              <?php
                try {
                $sql2 = "SELECT * FROM tbltrd WHERE id = " . $resExtra[$f]['codigo'];
                  $query2 = $pdo->prepare($sql2);
                  $query2->execute();
                  $resTrd = $query2->fetchAll(PDO::FETCH_ASSOC);
                }
                catch(PDOException $ex) {
                  print_r($ex);
                }
                if ($resExtra[$f]['tipo'] == 0) {
                  $nombre_criterio = $resTrd[0]['tipo_doc'];
                }
                else {
                  $nombre_criterio = $resTrd[0]['observaciones'];
                }
              ?>

              <td><?php echo $nombre_criterio; ?></td>
              <td><?php echo $resExtra[$f]['codigo']; ?></td>

              <?php if ($resExtra[$f]['tipo'] == 0) { ?>
                <td><?php echo "Tipo documental"; ?></td>
              <?php } else { ?>
                <td><?php echo "Observaciones"; ?></td>
              <?php } ?>

              <td><?php echo $resExtra[$f]['patron_inicio']; ?></td>
              <td><?php echo $resExtra[$f]['ubicacion_inicio']; ?></td>
              <td><?php echo $resExtra[$f]['patron_final']; ?></td>
              <td><?php echo $resExtra[$f]['ubicacion_final']; ?></td>
            </tr>
          <?php
          $f = $f + 1;
        }
      ?>
  </table>
  </div>
</div>
  </div>

  <!-- End page content -->
</div>
