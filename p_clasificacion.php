<?php include "admin/header.php"; ?>

<script>
  var div = document.getElementById('clasificacion');
  div.classList.remove('w3-white');
  div.classList.add('w3-blue');
</script>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <div class="w3-panel w3-teal w3-margin-top" style="margin-top:22px; margin-left:16px; margin-right:16px; margin-bottom:16px;">
    <h4><i class="fa fa-bed w3-margin-right"></i><span style="font-weight: bold;">CLASIFICACIÓN</span></h4>
  </div>

  <?php include "admin/base_footer.php"; ?>

  <!-- End page content -->
</div>

<?php include "admin/footer.php"; ?>