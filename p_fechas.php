<?php 

	$pruebas = 0;
	$depuracion = 0;

	$meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

	$anios = ['2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022'];

	$meses_comp = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'nov', 'dic'];

	define ("MUY_ARRIBA", 1);
	define ("ARRIBA", 2);
	define ("MITAD_ARRIBA", 3);
	define ("MITAD_ABAJO", 4);
	define ("ABAJO", 5);
	define ("MUY_ABAJO", 6);

	define("FECHA_NO", 7);

	define("FECHA_GUION_ANIO_MES_DIA", 8);
	define("FECHA_GUION_ANIO_DIA_MES", 9);
	define("FECHA_GUION_MES_DIA_ANIO", 10);
	define("FECHA_GUION_DIA_MES_ANIO", 11);

	define("FECHA_SLASH_ANIO_MES_DIA", 12);
	define("FECHA_SLASH_ANIO_DIA_MES", 13);
	define("FECHA_SLASH_MES_DIA_ANIO", 14);
	define("FECHA_SLASH_DIA_MES_ANIO", 15);

	define("FECHA_ANIO_MES_DIA", 16);
	define("FECHA_ANIO_DIA_MES", 17);
	define("FECHA_DIA_MES_ANIO", 18);
	define("FECHA_MES_DIA_ANIO", 19);

	define("FECHA_LETRAS_DIA_MES_ANIO", 20);

	$datFecha = Array();

	$fechas = Array();

	function posicion_relativa($maximo, $pos) {

		$unidad = intdiv($maximo, 6);
		if ($pos < $unidad) return MUY_ARRIBA;
		if ($pos < $unidad * 2) return ARRIBA;
		if ($pos < $unidad * 3) return MITAD_ARRIBA;
		if ($pos < $unidad * 4) return MITAD_ABAJO;
		if ($pos < $unidad * 5) return ABAJO;
		if ($pos < $unidad * 6) return MUY_ABAJO;
		return MUY_ABAJO;

	}

	function a_texto_ubicacion($ubicacion) {

		if ($ubicacion == MUY_ARRIBA) return "Muy Arriba";
		else if ($ubicacion == ARRIBA) return "Arriba";
		else if ($ubicacion == MITAD_ARRIBA) return "Mitad Arriba";
		else if ($ubicacion == MITAD_ABAJO) return "Mitad Abajo";
		else if ($ubicacion == ABAJO) return "Abajo";
		else return "Muy Abajo";
	}

	function fecha_encontrada() {
		$f = 0;
		while ($f < count($datFecha)) {
			if ($datFecha[$f][3] != FECHA_NO) {
				return 1;
			}
			$f = $f + 1;
		}
		return 0;
	}

	function mostrar_array($arr) {

		$f = 0;
		while ($f < count($arr)) {
			echo "t [" . $f . "] = (", $arr[$f][0] . ", " . $arr[$f][1] . ")<br>";
			$f = $f + 1;
		}
	}

	function mostrar_array_ubicacion($arr) {

		$f = 0;
		while ($f < count($arr)) {
			echo "t [" . $f . "] = (", $arr[$f][0] . ", " . $arr[$f][1] . ") => " . a_texto($arr[$f][2]) . "<br>";
			$f = $f + 1;
		}
	}

	function mostrar_fechas() {

		global $datFecha;

		?>

		<style>
			td {
				text-align: center;
			}
		</style>

		<?php

		$s = '<table border="1">';
		$s .= $s . '<tr><th>No</th><th>Original</th><th>Día</th><th>Mes</th><th>Año</th><th>Posición</th><th>Ubicación</th></tr>';
		
		$f = 0;
		while ($f < count($datFecha)) {
			$s .= '<tr>';
			$s .= '<td>' . $f . '<td>' . $datFecha[$f][7] . '</td><td>' . $datFecha[$f][4] . '</td><td>' . $datFecha[$f][5] . '</td><td>' . $datFecha[$f][6] . '</td><td>' . $datFecha[$f][1] . '</td><td>' . a_texto_ubicacion($datFecha[$f][2]) . '</td>';
			$s .= '</tr>';
			$f = $f + 1;
		}

		$s .= '</table>';
		echo $s;		
	}

	function fecha_guiones($s, $f) {

		global $datFecha;
		global $depuracion;

		// SI LA FILA YA FUE EVALUADA PREVIAMENTE POR OTRA FUNCIÓN, ES POSIBLE QUE YA SE LE
		// HUBIERA ASIGNADO UN VALOR. EN TAL CASO, LA CONSTANTE 'FECHA_NO' FUE CAMBIADA POR OTRA

		if ($datFecha[$f][3] != FECHA_NO) {
			// La fecha ya fue asignada. No se puede modificar
			return;
		}

		// RECIBE UNA FILA DE datFecha. ESTA FILA SERA EVALUADA PARA VER SI REALMENTE ES UNA FECHA
		// VALIDA. LO QUE SE RECIBE ES UN AÑO, Y SE DEBEN MIRAR LOS COMPLEMENTOS

		// Buscamos en que posición se encuentra el año candidato dentro de la cadena
		$pos = $datFecha[$f][1];

		// VALIDAR QUE EL AÑO Y EL COMPLEMENTO ESTÁN DENTRO DE LA CADENA ORIGEN
		// ESTO EVITA QUE SE GENERE UN ERROR DE DESBORDE ÍNDICE AL HACER LOS ANÁLISIS
		// SOBRE LOS DATOS. Se suma 10 por la siguiente razón: 4 dígitos del año, el
		// separador '-', dos dígitos del mes, el separador '-' y los dos dígitos del dia
		if ($pos + 10 >= strlen($s))
			return;

		// Las siguientes salidas son para verificar a "ojo" las cadenas existentes
		// en donde quede claro qué información "rodea" el año

		$s1 = substr($s, $pos, 10);
		$s2 = substr($s, $pos - 6, 10);

		// SE ANALIZA LA ESTRUCTURA DE LAS FECHAS
		$guion1 = substr($s, $pos + 4, 1);
		$guion2 = substr($s, $pos + 7, 1);
		$num1 = substr($s, $pos + 5, 2);
		$num2 = substr($s, $pos + 8, 2);
		if ($guion1 == '-' && $guion2 == '-') {
			$datFecha[$f][7] = substr($s, $pos, 10);

			$n1 = intval($num1);
			$n2 = intval($num2);
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_MES_DIA; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}
			if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_DIA_MES;
				$datFecha[$f][4] = strval($n1);
				$datFecha[$f][5] = strval($n2);
				return;
			}
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_MES_DIA; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}		
		}

		if ($pos - 6 < 0)
			return;

		$guion1 = substr($s, $pos - 1, 1);
		$guion2 = substr($s, $pos - 4, 1);
		$num2 = substr($s, $pos - 3, 2);
		$num1 = substr($s, $pos - 6, 2);
		if ($guion1 == '-' && $guion2 == '-') {
			$datFecha[$f][7] = substr($s, $pos - 6, 10);

			$datFecha[$f][7] = substr($s, $pos - 6, 10);
			$n2 = intval($num2);
			$n1 = intval($num1);
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_DIA_MES_ANIO; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}
			if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_DIA_MES_ANIO;
				$datFecha[$f][4] = strval($n1);
				$datFecha[$f][5] = strval($n2);
				return;
			}
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
				$datFecha[$f][3] = FECHA_GUION_MES_DIA_ANIO; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}		
		}
	}

	function fecha_slash($s, $f) {

		global $datFecha;
		global $depuracion;

		// SI LA FILA YA FUE EVALUADA PREVIAMENTE POR OTRA FUNCIÓN, ES POSIBLE QUE YA SE LE
		// HUBIERA ASIGNADO UN VALOR. EN TAL CASO, LA CONSTANTE 'FECHA_NO' FUE CAMBIADA POR OTRA

		if ($datFecha[$f][3] != FECHA_NO) {
			// La fecha ya fue asignada. No se puede modificar
			return;
		}

		// RECIBE UNA FILA DE datFecha. ESTA FILA SERA EVALUADA PARA VER SI REALMENTE ES UNA FECHA
		// VALIDA. LO QUE SE RECIBE ES UN AÑO, Y SE DEBEN MIRAR LOS COMPLEMENTOS

		// Buscamos en que posición se encuentra el año candidato dentro de la cadena
		$pos = $datFecha[$f][1];


		// VALIDAR QUE EL AÑO Y EL COMPLEMENTO ESTÁN DENTRO DE LA CADENA ORIGEN
		// ESTO EVITA QUE SE GENERE UN ERROR DE DESBORDE ÍNDICE AL HACER LOS ANÁLISIS
		// SOBRE LOS DATOS. Se suma 10 por la siguiente razón: 4 dígitos del año, el
		// separador '-', dos dígitos del mes, el separador '-' y los dos dígitos del dia
		if ($pos + 10 >= strlen($s))
			return;

		$s1 = substr($s, $pos, 10);
		$s2 = substr($s, $pos - 6, 10);

		// SE ANALIZA LA ESTRUCTURA DE LAS FECHAS
		$guion1 = substr($s, $pos + 4, 1);
		$guion2 = substr($s, $pos + 7, 1);
		$num1 = substr($s, $pos + 5, 2);
		$num2 = substr($s, $pos + 8, 2);
		if ($guion1 == '/' && $guion2 == '/') {
			$datFecha[$f][7] = substr($s, $pos, 10);

			$n1 = intval($num1);
			$n2 = intval($num2);
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_MES_DIA; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}
			if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_DIA_MES;
				$datFecha[$f][4] = strval($n1);
				$datFecha[$f][5] = strval($n2);
				return;
			}
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
				$datFecha[$f][3] = FECHA_GUION_ANIO_MES_DIA; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}		
		}

		if ($pos - 6 < 0)
			return;

		$guion1 = substr($s, $pos - 1, 1);
		$guion2 = substr($s, $pos - 4, 1);
		$num2 = substr($s, $pos - 3, 2);
		$num1 = substr($s, $pos - 6, 2);
		if ($guion1 == '/' && $guion2 == '/') {
			$datFecha[$f][7] = substr($s, $pos - 6, 10);

			$datFecha[$f][7] = substr($s, $pos - 6, 10);
			$n2 = intval($num2);
			$n1 = intval($num1);
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_DIA_MES_ANIO; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}
			if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
				$datFecha[$f][3] = FECHA_GUION_DIA_MES_ANIO;
				$datFecha[$f][4] = strval($n1);
				$datFecha[$f][5] = strval($n2);
				return;
			}
			if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
				$datFecha[$f][3] = FECHA_GUION_MES_DIA_ANIO; 
				$datFecha[$f][4] = strval($n2);
				$datFecha[$f][5] = strval($n1);
				return;
			}		
		}
	}

	function fecha_digitos($s, $f) {

		global $datFecha;
		global $depuracion;
		global $depuracion_2;

		// SI LA FILA YA FUE EVALUADA PREVIAMENTE POR OTRA FUNCIÓN, ES POSIBLE QUE YA SE LE
		// HUBIERA ASIGNADO UN VALOR. EN TAL CASO, LA CONSTANTE 'FECHA_NO' FUE CAMBIADA POR OTRA

		if ($datFecha[$f][3] != FECHA_NO) {
			// La fecha ya fue asignada. No se puede modificar
			return;
		}

		// RECIBE UNA FILA DE datFecha. ESTA FILA SERA EVALUADA PARA VER SI REALMENTE ES UNA FECHA
		// VALIDA. LO QUE SE RECIBE ES UN AÑO, Y SE DEBEN MIRAR LOS COMPLEMENTOS

		// Buscamos en que posición se encuentra el año candidato dentro de la cadena
		$pos = $datFecha[$f][1];

		// POSICIÓN DENTRO DEL TEXTO DEL AÑO A SER ANALIZADO

		// VALIDAR QUE EL AÑO Y EL COMPLEMENTO ESTÁN DENTRO DE LA CADENA ORIGEN
		// ESTO EVITA QUE SE GENERE UN ERROR DE DESBORDE ÍNDICE AL HACER LOS ANÁLISIS
		// SOBRE LOS DATOS. Se suma 10 por la siguiente razón: 4 dígitos del año, el
		// separador '-', dos dígitos del mes, el separador '-' y los dos dígitos del dia
		if ($pos + 8 >= strlen($s))
			return;

		// Las siguientes salidas son para verificar a "ojo" las cadenas existentes
		// en donde quede claro qué información "rodea" el año

		$s1 = substr($s, $pos, 8);
		$s2 = substr($s, $pos - 4, 8);

		// SE ANALIZA LA ESTRUCTURA DE LAS FECHAS
		$num1 = substr($s, $pos + 4, 2);
		$num2 = substr($s, $pos + 6, 2);

		$datFecha[$f][7] = substr($s, $pos, 8);

		$n1 = intval($num1);
		$n2 = intval($num2);
		if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
			$datFecha[$f][3] = FECHA_ANIO_MES_DIA; 
			$datFecha[$f][4] = strval($n2);
			$datFecha[$f][5] = strval($n1);
			return;
		}
		if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
			$datFecha[$f][3] = FECHA_ANIO_DIA_MES;
			$datFecha[$f][4] = strval($n2);
			$datFecha[$f][5] = strval($n1);
			return;
		}
		if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
			$datFecha[$f][3] = FECHA_ANIO_MES_DIA; 
			$datFecha[$f][4] = strval($n1);
			$datFecha[$f][5] = strval($n2);
			return;
		}		

		if ($pos - 4 < 0)
			return;

		$num2 = substr($s, $pos - 2, 2);
		$num1 = substr($s, $pos - 4, 2);

		$datFecha[$f][7] = substr($s, $pos - 4, 8);

		$n2 = intval($num2);
		$n1 = intval($num1);
		if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 0 && $n2 <= 12) ) {
			$datFecha[$f][3] = FECHA_DIA_MES_ANIO; 
			$datFecha[$f][4] = strval($n2);
			$datFecha[$f][5] = strval($n1);
			return;
		}
		if ( ($n1 > 12 && $n1 <= 31) && ($n2 > 0 && $n2 <= 12) ) {
			$datFecha[$f][3] = FECHA_DIA_MES_ANIO;
			$datFecha[$f][4] = strval($n2);
			$datFecha[$f][5] = strval($n1);
			return;
		}
		if ( ($n1 > 0 && $n1 <= 12) && ($n2 > 12 && $n2 <= 31) ) {
			$datFecha[$f][3] = FECHA_MES_DIA_ANIO; 
			$datFecha[$f][4] = strval($n1);
			$datFecha[$f][5] = strval($n2);
			return;
		}		
	}

	function fecha_letras($s, $f) {

		global $datFecha;
		global $meses;

		global $depuracion;
		
		////////////////////////////////////////////////////////////////

		// julio 15 de 2017
		// 15 de julio de 2017
		// ESTOS SON LOS FORMATOS DE FECHA CON LETRAS QUE SE ANALIZARON

		// 15 de enero de 2017
		// 15 de febrero de 2017
		// 15 de marzo de 2017
		// 15 de abril de 2017
		// 15 de mayo de 2017
		// 15 de junio de 2017
		// 15 de julio de 2017
		// 15 de agosto de 2017
		// 15 de septiembre de 2017 <== LONGITUD MÁXIMA = 23
		// 15 del mes de septiembre del año 2017 <== LÓNGITUD EXCESIVA = 37 (aprox 40)
		// 15 de octubre de 2017
		// 15 de noviembre de 2017
		// 15 de diciembre de 2017

		// PATRONES:

		// mes en letras   dia en número   año en números  ==> julio 07 de 2015
		// día en número   mes en letras   año en números  ==> 07 de julio de 2015

		///////////////////////////////////////////////////////////////

		// SI LA FILA YA FUE EVALUADA PREVIAMENTE POR OTRA FUNCIÓN, ES POSIBLE QUE YA SE LE
		// HUBIERA ASIGNADO UN VALOR. EN TAL CASO, LA CONSTANTE 'FECHA_NO' FUE CAMBIADA POR OTRA

		if ($datFecha[$f][3] != FECHA_NO) {
			// La fecha ya fue asignada. No se puede modificar
			return;
		}

		// RECIBE UNA FILA DE datFecha. ESTA FILA SERA EVALUADA PARA VER SI REALMENTE ES UNA FECHA
		// VALIDA. LO QUE SE RECIBE ES UN AÑO, Y SE DEBEN MIRAR LOS COMPLEMENTOS

		// Buscamos en que posición se encuentra el año candidato dentro de la cadena
		$pos = $datFecha[$f][1];

		// POSICIÓN DENTRO DEL TEXTO DEL AÑO A SER ANALIZADO

		// VALIDAR QUE EL AÑO Y EL COMPLEMENTO ESTÁN DENTRO DE LA CADENA ORIGEN
		// ESTO EVITA QUE SE GENERE UN ERROR DE DESBORDE ÍNDICE AL HACER LOS ANÁLISIS
		// SOBRE LOS DATOS.

		// NOTA: Esto se resuelve agregando a la cadena que se está analizando una subcadena
		// de control tanto a la izquierda como a la derecha. Esta subcadena hace que desaparezcan
		// los problemas de límites hacia adelante y hacia atrás cuando se analizan las fechas

		// En las funciones anteriores, los controles usados pueden sobrar si dichas
		// subcadenas de control se agregan

		// ESTA SUBCADENA CONTIENE EN SU INTERIOR EL PATRÓN BUSCADO
		$dummy = substr($s, $pos - 50, 50); 

		$datFecha[$f][7] = substr($s, $pos - 50, 55);

		$subcadena = substr($s, $pos - 50, 49);

		$p_dummy = $pos - 50;


		$i = 0;
		while ($i < count($meses)) {

			$posic = stripos($subcadena, $meses[$i]);
			if ($posic > 0) {

				// ENCONTRADO EL MES
				// Se busca el día, el cual debe ser un dígito (ejemplo, 7)
				// O una combinación de dígitos (ejemplo, 07)

				// ANALIZA HACIA LA DERECHA
				$un_dia = '';
				$k = $posic;
				while ($k < strlen($subcadena)) {

					if (is_numeric($subcadena[$k])) {
						// ENCONTRADO UN DÍGITO
						// Se buscan todos los digitos numéricos a partir de esta posición

						$un_dia = $un_dia . $subcadena[$k];
						$z = $k + 1;
						while ($z < strlen($subcadena)) {
							if (is_numeric($subcadena[$z])) {

								$un_dia = $un_dia . $subcadena[$z];
							}
							else {
								// PATRÓN PARECE ENCONTRADO. EVALUAR EL DÍA

								$el_dia = intval($un_dia);
								if ($el_dia >= 1 && $el_dia <= 31) {
									// PATRÓN ENCONTRADO

									$datFecha[$f][3] = FECHA_LETRAS_DIA_MES_ANIO; 
									$datFecha[$f][4] = strval($el_dia);
									$datFecha[$f][5] = strval($i + 1);

								}
								else {
									$z = strlen($subcadena) + 10;
									$k = strlen($subcadena) + 10;
									// CON ESTOS VALORES SE SALE DE LOS CICLOS INTERNOS
									// Y CONTINÚA EL ANÁLISIS CON LOS SIGUIENTES MESES
								}
							}
							$z = $z + 1;
						}
					}

					$k = $k + 1;
				}

				// SI NO ENCUENTRA EL DÍA HACIA LA DERECHA, LO BUSCA HACIA LA IZQUIERDA	

				// ANALIZA HACIA LA IZQUIERDA
				if ($un_dia == '') {
					$k = $posic;
					while ($k >= 0) {

						if (is_numeric($subcadena[$k])) {
							// ENCONTRADO UN DÍGITO
							// Se buscan todos los digitos numéricos a partir de esta posición

							$un_dia = $un_dia . $subcadena[$k];
							$z = $k - 1;
							while ($z >= 0) {
								if (is_numeric($subcadena[$z])) {

									$un_dia = $un_dia . $subcadena[$z];
								}
								else {
									// PATRÓN PARECE ENCONTRADO. EVALUAR EL DÍA

									// Se debe invertir la cadena, pues los dígitos se encuentran
									// de derecha hacia la izquierda (opuesto al método normal de
									// dígitos desde la izquierda hacia la derecha)
									$un_dia = strrev($un_dia); 

									$el_dia = intval($un_dia);
									if ($el_dia >= 1 && $el_dia <= 31) {
										// PATRÓN ENCONTRADO

										$datFecha[$f][3] = FECHA_LETRAS_DIA_MES_ANIO; 
										$datFecha[$f][4] = strval($el_dia);
										$datFecha[$f][5] = strval($i + 1);

									}
									else {
										$z = -1;
										$k = -1;
										// CON ESTOS VALORES SE SALE DE LOS CICLOS INTERNOS
										// Y CONTINÚA EL ANÁLISIS CON LOS SIGUIENTES MESES
									}
								}
								$z = $z - 1;
							}
						}

						$k = $k - 1;
					}
				}

			}

			$i = $i + 1;
		}

	}

	function anios_candidatos($s) {

		// SE BUSCA DENTRO DEL TEXTO LOS AÑOS ENTRE 2000 Y 2020. SI SE ENCUENTRA UN AÑO, 
		// ESTE AÑO ES UN CANDIDATO (se debe verificar que tenga mes y día)
		// ESTOS AÑOS SON CANDIDATOS. PARA SER FECHAS REALES, DEBEN ESTAR ACOMPAÑADOS DE UNA FECHA
		// COMPLETA. POR EJEMPLO, SI DETECTA 2022, ENTONCES DEBERÍA ENCONTRAR ALGO COMO ESTO:
		// 2022-01-03 (entre otras posibilidades)

		// En datFecha se almacenan todas las fechas que se detecten en el texto
		global $datFecha;

		// Lista de los años que se pueden analizar. Se evalún luego, uno después de otro
		global $anios;

		global $depuracion;

		$longitud = strlen($s);

		$i = 0;

		// Se buscan todos los años dentro del texto. Se utiliza la tabla "anios" la cual
		// contiene el listado de años entre 2000 a 2020. La idea es ir probando de manera
		// secuencial
		while ($i < count($anios)) {

			$p = 0;
			while ($p < strlen($s)) {

				// Se buscan todas las posiciones correspondiente al año "i"
				// Se almacena en un array toda la información clave relativa a este año
				$posfecha = stripos($s, $anios[$i], $p);

				if ($posfecha > 0) {

					// Se evalúa en donde se encontró la fecha. Si arriba, en la mitad o abajo ...
					$ubicacion = posicion_relativa($longitud, $posfecha);

					// Arreglo que almacena los datos básicos de la fecha
					$arr = Array();

					// Se guarda en un array las características de la fecha
					$arr[] = $anios[$i]; // 0 => Año
					$arr[] = $posfecha;  // 1 => Posición
					$arr[] = $ubicacion; // 2 => Ubicación Relativa
					$arr[] = FECHA_NO;	 // 3 => Tipo de Fecha
					$arr[] = '';         // 4 => Día
					$arr[] = '';         // 5 => Mes
					$arr[] = $anios[$i]; // 6 => Año
					$arr[] = '';		 // 7 => Original

					// Se graban los datos de la fecha com una fila en la tabla de fechas detectadas
					$datFecha[] = $arr;

					// Se prepara para mirar si hay alguna otra ocurrencia de la fecha seleccionada
					// Se incrementa en 1 para ir a la siguente ocurrencia de fecha (en caso de que
					// exista dicha ocurrencia)
					$p = $posfecha + 1;
				}
				else {

					// Si no encuentra la fecha, se sale del ciclo y busca la siguiente fecha
					// Utilizando la variable 'i'

					// Para salirse del ciclo superamos el valor límite
					$p = strlen($s) + 1;
				}
			}
			$i = $i + 1; 
		}
	}

	function validar_anios($s) {

		// AQUI SE VALIDA SI LOS AÑOS ENCONTRADOS SE CORRESPONDEN CON FECHAS REALES,
		// SE BUSCAN LOS COMPLEMENTOS (por ejemplo, si encuentra 2022, debe encontrar un
		// comlemento: 2022-02-15)

		global $datFecha;
		global $depuracion;

		$f = 0;
		while ($f < count($datFecha)) {
			// CON LA VARIABLE f recorre todas las fechas candidatas, a partir de los
			// años encontrados previamente

			// CADA FUNCIÓN A PARTIR DE AHORA VALIDA SI EL AÑO ES CORRECTO. EN CASO POSITIVO
			// AGREGA ALGUNOS VALORES QUE LO CONFIRMAN

			// ESTA FUNCIÓN EVALÚA SI LA FECHA ESTÁ BASADA EN GUIONES
			fecha_guiones($s, $f);

			// ESTA FUNCIÓN EVALÚA SI LA FECHA ESTÁ BASADA EN SLASH
			fecha_slash($s, $f);

			// ESTA FUNCIÓN EVALÚA SI LA FECHA ES LAS FORMAS: 20221103
			// fecha_digitos($s, $f);

			// ESTA FUNCIÓN EVALÚA LAS FECHAS CONSTRUIDAS CON LETRAS
			fecha_letras($s, $f);

			// RECORRE LAS RESTANTES FECHAS CANDIDATAS
			$f = $f + 1;
		}
	}

	function encontrar_fecha_nueva($contenido, $fecha_expediente) {

		global $datFecha;

		$contenido = '======= ======= ======= ======= ======= ' . $contenido . '======= ======= ======= ======= ======= ';

		// echo "FECHA EXPEDIENTE RECIBIDO: ==> " . $fecha_expediente . "<br>";
		// echo "CONTENIDO ANALIZADO EN LA BÚSQUEDA DE FECHAS: ==> " . $contenido . "<br><br>";

		// SE REINICIA EL ARRAY DE FECHAS. ESTE ERA UN GRAN ERROR, PUES AL EJECUTAR
		// POR SEGUNDA VEZ EL ALGORITMO, EL HECHO DE QUE YA TUVIERA LOS DATOS GENERABA UN ERROR
		// SENSIBLE, PUES YA NUNCA LOS VOLVÍA A CALCULAR

		$datFecha = Array();

		// echo "REINICIADAS LAS FECHAS. PARA PROBARLO, SE IMPRIME SU LONGITUD = " . count($datFecha) . "<br><br>";

		anios_candidatos($contenido);
		validar_anios($contenido);

		// echo "MOSTRAR LA TABLA datFecha EN DONDE ESTÁN LOS CÁLCULOS REALIZADOS" . "<br>";
		// echo "EXTRAÑAMENTE DEVUELVE: 15/07/2000 (SIEMPRE !!!)" . "<br><br>";

		// print_r($datFecha);

		// echo "VALIDAR !!!<br><br>";

		// DEVUELVE LA PRIMERA FECHA
		$f = 0;
		while ($f < count($datFecha)) {
			if ($datFecha[$f][3] != FECHA_NO) {

				while (strlen($datFecha[$f][4]) < 2) {
					$datFecha[$f][4] = '0' . $datFecha[$f][4];
				}

				while (strlen($datFecha[$f][5]) < 2) {
					$datFecha[$f][5] = '0' . $datFecha[$f][5];
				}

				while (strlen($datFecha[$f][6]) < 2) {
					$datFecha[$f][6] = '0' . $datFecha[$f][6];
				}

				$fec = strval($datFecha[$f][6]) . '-' . strval($datFecha[$f][5]) . '-' . strval($datFecha[$f][4]);

				// echo "LA FECHA QUE DEVUELVE (calculada) = " . $fec . "<br>";

				return $fec;
			}
			$f = $f + 1;
		}

		// echo "NO ENCONTRÓ LA FECHA. DEVUELVE LA FECHA DEL EXPEDIENTE: ==> <br>";
		return $fecha_expediente;		
	}

	if ($pruebas) {
		$s = "Pereira, julio 15 de 2000. Señores FAVI, La Ciudad. En la fecha 2020/01/17 se presentó ante ustedes una persona que dice haber nacido el 14 de enero de 2016. Dicha persona se graduó como Ingeniero el dia 18 de agosto de 2010. Al parecer, esta fecha es incorrecta, pues se tiene un reporte que afirma que la fecha de graduación es 20-05-2011. Dada esta contradicción, propongo que se realice una prueba el día 7 del mes de julio de 2022, buscando de esta manera determinar que la fecha de graduación fue: 20110314. Otra fecha: mes-día-año: 03-21-2017. Prueba de fechas tipo guión: 1) 2022-01-01, 2) Dado en Pereira, Mayo 27 de 2022, Posdata: Por favor verificar la fecha de graduación: 2013-05-14, pues han llegado algunas noticias sobre que dicha fecha es bastante posible, y algunos afirman incluso que puede ser 2013-06-14. Para salir de dudas, se pide que se confronten los diplomas de grado para verificar que no sea en la fecha: 18/03/2005. Atentamente, Gilberto Vargas, firmado en el mes de enero, día 08 de 2021. Aquí termina el escrito de solicitud.";

		// $s = "Pereira, julio 15 de 2000. Señores FAVI";

		$s = '======= ======= ======= ======= ======= ' . $s . '======= ======= ======= ======= ======= ';

		anios_candidatos($s);
		validar_anios($s);
		mostrar_fechas();

		/* $fecha = encontrar_fecha_nueva($s, '01/01/2030');
		if ($depuracion) {
			echo $fecha;
		}*/
	}

?>